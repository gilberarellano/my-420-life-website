<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* DATABASE */

define('_JEXEC',1);
define('DS',DIRECTORY_SEPARATOR);
define('JPATH_BASE',dirname(dirname(__FILE__)));
session_start();
require_once dirname(dirname(__FILE__))."/configuration.php";
require_once dirname(dirname(__FILE__)).'/includes/defines.php';
require_once dirname(dirname(__FILE__)).'/includes/framework.php';

$config = new JConfig;

// DO NOT EDIT DATABASE VALUES BELOW
// DO NOT EDIT DATABASE VALUES BELOW
// DO NOT EDIT DATABASE VALUES BELOW

define('DB_SERVER',					$config->host							);
define('DB_PORT',					'3306'									);
define('DB_USERNAME',				$config->user							);
define('DB_PASSWORD',				$config->password						);
define('DB_NAME',					$config->db								);
define('TABLE_PREFIX',				$config->dbprefix						);
define('DB_USERTABLE',				'users'									);
define('DB_USERTABLE_NAME',			'name'									);
define('DB_USERTABLE_USERID',		'id'									);
define('DB_USERTABLE_LASTACTIVITY',	'lastvisitDate'							);

$mainframe =& JFactory::getApplication('site');
$mainframe->initialise();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* ADVANCED */

define('SET_SESSION_NAME','');			// Session name
define('DO_NOT_START_SESSION','1');		// Set to 1 if you have already started the session
define('DO_NOT_DESTROY_SESSION','1');	// Set to 1 if you do not want to destroy session on logout
define('SWITCH_ENABLED','1');		
define('INCLUDE_JQUERY','1');	
define('FORCE_MAGIC_QUOTES','0');

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* FUNCTIONS */

function getUserID() {
	$userid = 0;
	
	$my =& JFactory::getUser();

	if (!empty($my->id)) {
		$userid = $my->id;
	}

	return $userid;
}

function getFriendsList($userid,$time) {
	$sql = ("select DISTINCT ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." userid, ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_NAME." username, ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_LASTACTIVITY." lastactivity, ".TABLE_PREFIX."community_users.thumb avatar, ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." link, ".TABLE_PREFIX."community_users.status message, cometchat_status.status from ".TABLE_PREFIX."community_connection join ".TABLE_PREFIX.DB_USERTABLE." on  ".TABLE_PREFIX."community_connection.connect_to = ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." left join cometchat_status on ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." = cometchat_status.userid  left join ".TABLE_PREFIX."community_users on ".TABLE_PREFIX."community_users.userid = ".TABLE_PREFIX."community_connection.connect_to where ".TABLE_PREFIX."community_connection.status = '1' and ".TABLE_PREFIX."community_connection.connect_from = '".mysql_real_escape_string($userid)."' order by username asc");

	if (defined('DISPLAY_ALL_USERS') && DISPLAY_ALL_USERS == 1) {

		$sql = ("select UNIX_TIMESTAMP(NOW()) as time");
		$query = mysql_query($sql);
		$now = mysql_fetch_array($query);
		$difference = ((ONLINE_TIMEOUT)*2)+$now['time']-JFactory::getDate()->toUnix();
		
		$sql = ("select DISTINCT ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." userid, ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_NAME." username, ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_LASTACTIVITY." lastactivity, ".TABLE_PREFIX."community_users.thumb avatar, ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." link, ".TABLE_PREFIX."community_users.status message, cometchat_status.status from ".TABLE_PREFIX.DB_USERTABLE." left join cometchat_status on ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." = cometchat_status.userid left join ".TABLE_PREFIX."community_users on ".TABLE_PREFIX."community_users.userid = ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." where (UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_LASTACTIVITY.") < ".$difference." )  and ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." <> '".mysql_real_escape_string($userid)."' and (cometchat_status.status IS NULL OR cometchat_status.status <> 'invisible') order by username asc");               
	}
 
	return $sql;
}

function getUserDetails($userid) {
	$sql = ("select ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." userid, ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_NAME." username, ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_LASTACTIVITY." lastactivity,  ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." link,  ".TABLE_PREFIX."community_users.thumb avatar, cometchat_status.message, cometchat_status.status from ".TABLE_PREFIX.DB_USERTABLE." left join cometchat_status on ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." = cometchat_status.userid  left join ".TABLE_PREFIX."community_users on ".TABLE_PREFIX."community_users.userid = ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." where ".TABLE_PREFIX.DB_USERTABLE.".".DB_USERTABLE_USERID." = '".mysql_real_escape_string($userid)."'");
	return $sql;
}

function updateLastActivity($userid) {
	$today	=& JFactory::getDate();
	$sql = ("update `".TABLE_PREFIX.DB_USERTABLE."` set ".DB_USERTABLE_LASTACTIVITY." = '".$today->toMySQL()."' where ".DB_USERTABLE_USERID." = '".mysql_real_escape_string($userid)."'");
	return $sql;
}

function getUserStatus($userid) {
	 $sql = ("select ".TABLE_PREFIX."community_users.status message, cometchat_status.status from ".TABLE_PREFIX."community_users left join cometchat_status on ".TABLE_PREFIX."community_users.userid = cometchat_status.userid where ".TABLE_PREFIX."community_users.userid = ".mysql_real_escape_string($userid));
	return $sql;
}

function getLink($link) {
	return BASE_URL.'../index.php?option=com_community&view=profile&userid='.$link;
}

function getAvatar($image) {
	if(empty($image)) {
		$image = 'components/com_community/assets/default_thumb.jpg';
	}
	return BASE_URL.'../'.$image;
}

function getTimeStamp() {
	return JFactory::getDate()->toUnix();
}

function processTime($time) {
	return JFactory::getDate($time)->toUnix();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* HOOKS */

function hooks_statusupdate($userid,$statusmessage) {
	require_once( JPATH_BASE.DS.'components'.DS.'com_community'.DS.'libraries'.DS.'core.php');

 	$db	= & JFactory::getDBO();
	$my	=& CFactory::getUser();
		
	require_once( COMMUNITY_COM_PATH.DS.'libraries' . DS . 'apps.php' );
	
	$appsLib	=& CAppPlugins::getInstance();
	$appsLib->loadApplications();
		
	$args 	= array();
	$args[]	= $my->id;
	$args[]	= $my->getStatus();	
	$args[]	= $statusmessage;
	$appsLib->triggerEvent( 'onProfileStatusUpdate' , $args );
		
	$today	=& JFactory::getDate();
	$data	= new stdClass();
	$data->userid		= $userid;
	$data->status		= $statusmessage; 		
	$data->posted_on    = $today->toMySQL();
		
	$db->updateObject( '#__community_users' , $data , 'userid' );
		
	if($db->getErrorNum()) {
		JError::raiseError( 500, $db->stderr());
	}


	$sql = ("insert into ".TABLE_PREFIX."community_activities (actor,target,title,app,cid,created,points) values ('".$userid."','".$userid."','{actor} ".mysql_real_escape_string(sanitize($statusmessage))."','profile',$userid, '".$today->toMySQL()."',1)");
	$query = mysql_query($sql);	
}

function hooks_forcefriends() {
	
}

function hooks_activityupdate($userid,$status) {

}

function hooks_message($userid,$unsanitizedmessage) {
	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* LICENSE */

include_once(dirname(__FILE__).'/license.php');
$x="\x62a\x73\x656\x34\x5fd\x65c\157\144\x65";
eval($x('JHI9ZXhwbG9kZSgnLScsJGxpY2Vuc2VrZXkpOyRwXz0wO2lmKCFlbXB0eSgkclsyXSkpJHBfPWludHZhbChwcmVnX3JlcGxhY2UoIi9bXjAtOV0vIiwnJywkclsyXSkpOw'));

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
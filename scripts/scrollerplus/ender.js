!function ($, scrollerplus) {
  $.ender({
    scrollerplus: function (options) {
      return new scrollerplus(this[0], options)
    }
  }, true)
}(ender, require('scrollerplus').scrollerplus)
<html class="no-js" lang="en" itemscope itemtype="http://schema.org/Product"> <!--<![endif]-->
<head>
  <meta charset="utf-8">  

  <title>Gumby - A Responsive 960 Grid CSS Framework</title>  
  
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  
  <!-- CSS imports non-minified for staging, minify before moving to production-->
  <link rel="stylesheet" href="css/imports.css">
  <!-- end CSS-->

  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

</head>

<style>
	html, body {
		background: #12220a;
	}
	
	.column, .columns {
		background: #2d3e24;
		-webkit-box-shadow: inset 0 1px 0 #374c2c,
			inset 0 -1px 0 #374c2c,
			inset 1px 0 0 #374c2c,
			inset -1px 0 0 #374c2c
		;
		-moz-box-shadow: inset 0 1px 0 #374c2c,
					inset 0 -1px 0 #374c2c,
					inset 1px 0 0 #374c2c,
					inset -1px 0 0 #374c2c
				;
		box-shadow: inset 0 1px 0 #374c2c,
					inset 0 -1px 0 #374c2c,
					inset 1px 0 0 #374c2c,
					inset -1px 0 0 #374c2c
				;
		-webkit-transition-duration: .3s;
		-moz-transition-duration: .3s;
		-o-transition-duration: .3s;
		-ms-transition-duration: .3s;
		transition-duration: .3s;
	}
	
	.column:hover, .columns:hover {
		background: #445d36;
		cursor: pointer;
		-webkit-box-shadow: inset 0 1px 0 #557445,
			inset 0 -1px 0 #557445,
			inset 1px 0 0 #557445,
			inset -1px 0 0 #557445
		;
		-moz-box-shadow: inset 0 1px 0 #557445,
					inset 0 -1px 0 #557445,
					inset 1px 0 0 #557445,
					inset -1px 0 0 #557445
				;
		box-shadow: inset 0 1px 0 #557445,
					inset 0 -1px 0 #557445,
					inset 1px 0 0 #557445,
					inset -1px 0 0 #557445
				;
		-webkit-transition-duration: .3s;
		-moz-transition-duration: .3s;
		-o-transition-duration: .3s;
		-ms-transition-duration: .3s;
		transition-duration: .3s;
	}
	
	.row {
		margin-bottom: 25px;
		text-align: center;
	}
	
	h1, h2 {
		font: bold italic 220px georgia, times new roman, serif;
		color: #fbfbfb;
		letter-spacing: -.09em;
		text-shadow: 0 10px 0 #000;
		-webkit-transition-duration: 1s;
	}
	
	h2  {
		font-size: 21px;
		text-shadow: 0 4px 0 #000;
		font-weight: normal;
		line-height: 1.6;
	}
	
	p {
		font-size: 16px;
		line-height: 60px;
		margin-bottom: 0;
		color: #78a261;
		
		-webkit-transition-duration: .2s;
		-moz-transition-duration: .2s;
		-o-transition-duration: .2s;
		-ms-transition-duration: .2s;
		transition-duration: .2s;
	}
	
	.column:hover p, .columns:hover p {
		font-size: 25px;
		font-weight: bold;
		color: #bbf09f;
		
		-webkit-transition-duration: .2s;
		-moz-transition-duration: .2s;
		-o-transition-duration: .2s;
		-ms-transition-duration: .2s;
		transition-duration: .2s;
	}
	
	.head, .head:hover {
		box-shadow: none;
		margin-top: 30px;
	}
	
	.head span {
		font-weight: bold;
		color: #ffe400;
	}
	
	.head span span {
		font-size: 32px;
		font-weight: bold !important;
	}
	
	a {
		color: #fbfbfb;
	}
	
	a:hover {
		color: #ff7e00;
	}
	
	.special, .special:hover {
		background: #12220a;
	}
	
	.special:hover p {
		color: #ffe400;
	}

	@media only screen and (max-width: 767px) {
	
		h1 {
			font: bold italic 100px georgia, times new roman, serif;
			-webkit-transition-duration: .5s;
		}
	
	}
	
	
</style>

<body>

  <div class="container">
  
  	<div class="row">
  	  <div class="twelve columns special head">
  	  	<h1>Gumby</h1>
  	  	<h2>A <span>responsive <span>960</span> grid</span>
  	  	from <a href="#">Digital Surgeons</a></h2>
  	  </div>
  	</div>
  
  	<div class="row">
	    <div class="twelve columns">
	    	<p>940px</p>
	    </div>
    </div>
    
    <div class="row">
    	<div class="one columns">
    		<p>60px</p>
    	</div>
      <div class="eleven columns">
      	<p>860px</p>
      </div>
    </div>

    </div> <!--! end of #container -->
	
	
	</section>

  
</body>
</html>
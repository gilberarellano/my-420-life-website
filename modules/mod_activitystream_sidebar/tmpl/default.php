<?php
/**
 * @category	Modules
 * @package		JomSocial
 * @copyright (C) 2008 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="<?php echo $params->get('moduleclass_sfx'); ?>">
	<?php echo $stream; ?>
	<div style="clear:both">&nbsp;</div>
</div>
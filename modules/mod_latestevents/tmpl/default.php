<?php
/**
 * @package		Upcoming Events Module
 * @copyright (C) 2008 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license		GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die('Restricted access');
?>
<?php
if( !empty( $events ) )
{
?>
<ul class="latestevents<?php echo $params->get('moduleclass_sfx'); ?>">
	<?php
	foreach( $events as $event )
	{
		$handler    = CEventHelper::getHandler( $event );
                $tipslength = $params->get( 'tipslength');
                
                if ($event->description != '') {
                    $tooltips = $event->description;
                } else if ($event->summary != '') {
                    $tooltips = $event->summary;
                } else {
                    $tooltips = $event->title;
                }
                
                $tooltips = strip_tags($tooltips);
                $tooltips = CStringHelper::escape($tooltips);
                $tooltips = CStringHelper::truncate($tooltips, $tipslength, '...');
	?>
		<li class="jsRel jomNameTips tipFullWidth" title="<?php echo $tooltips;?>">
			<div class="event-date jsFlLf">
				<div>
					<a href="<?php echo $handler->getFormattedLink( 'index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id );?>">
					<img class="cAvatar jsFlLf" src="<?php echo $event->getThumbAvatar();?>" alt="<?php echo CStringHelper::escape( $event->title );?>" /></a>
				</div>
				<div><?php echo CEventHelper::formatStartDate($event, $config->get('eventdateformat') ); ?></div>
			</div>
			<div class="event-detail">
				<div class="event-title">
					<a href="<?php echo $handler->getFormattedLink( 'index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id );?>">
						<?php echo $event->title;?>
					</a>
				</div>
				<div class="event-loc">
					<?php echo $event->location;?>
				</div>
				<div class="event-attendee small">
					<a href="<?php echo $handler->getFormattedLink('index.php?option=com_community&view=events&task=viewguest&eventid=' . $event->id . '&type='.COMMUNITY_EVENT_STATUS_ATTEND);?>"><?php echo JText::sprintf((cIsPlural($event->confirmedcount)) ? 'COM_COMMUNITY_EVENTS_ATTANDEE_COUNT_MANY':'COM_COMMUNITY_EVENTS_ATTANDEE_COUNT', $event->confirmedcount);?></a>
				</div>
			</div>
			<div class="clr"></div>	
		</li>
	<?php
	}
	?>
</ul>
<?php
}
else
{
?>
	<div><?php echo JText::_( 'COM_COMMUNITY_EVENTS_NO_EVENTS_ERROR' );?></div>
<?php
}
?>
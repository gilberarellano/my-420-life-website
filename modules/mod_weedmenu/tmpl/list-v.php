<?php
/**
* @package   ZOO Item
* @file      list-v.php
* @version   2.4.2
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) 2007 - 2011 YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// include css
$zoo->document->addStylesheet('modules:mod_weedmenu/tmpl/list-v/style.css');



?>

<div class="zoo-item list-v">
         

	<?php if (!empty($items)) : ?>

		<?php
		        
		        $ni0 = mysql_query("SELECT created_by FROM jos_content WHERE id=$_GET[id]") or die(mysql_error());
			$fe0 = mysql_fetch_array($ni0);
		        $current_author_id = $fe0[created_by];
			$itid = array_keys($items);
			
		?>

		<ul>
			
			
			<?php 
				$i = 0; foreach ($items as $item) : ?>
			
			<?php
			$ni = mysql_query("SELECT created_by FROM jos_zoo_item WHERE id=$itid[$i]") or die(mysql_error());
			$fe = mysql_fetch_array($ni);
			$author_id = $fe[created_by];
			
			if ( ($author_id != 42) && ($author_id == $current_author_id) ) 
				{
			?>
			
			<li class="<?php if ($i % 2 == 0) { echo 'odd'; } else { echo 'even'; } ?>">
				
				<?php echo $renderer->render('item.'.$layout, compact('item', 'params')); ?>
			</li>
			
			<?php } ?>
			
			<?php $i++; endforeach; ?>
		</ul>
		
	<?php else : ?>
		<?php echo JText::_('COM_ZOO_NO_ITEMS_FOUND'); ?>
	<?php endif; ?>
</div>

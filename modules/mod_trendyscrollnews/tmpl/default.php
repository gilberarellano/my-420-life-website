<?php 
/**
* Trendy Scroll News - Joomla Module
* Version			: 1.0
* Created by		: RBO Team - RumahBelanja.com & TechnoTravels.com
* Created on		: 15 November 2010 (Joomla 1.5.x) and 17 December 2010 (Joomla 1.6.x)
* Updated on		: n/a
* Package			: Joomla 1.6.x
* License			: http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
*/
// no direct access
defined('_JEXEC') or die('Restricted access'); 
$starttext	= $params->get( 'starttext');
$delay		= $params->get( 'delay', 2000);
$numsteps	= $params->get( 'numsteps', 30);
$stepsdelay	= $params->get( 'stepsdelay', 40);
$time_sys	= $params->get( '$time_sys', 1);
$time_type	= $params->get( 'stepsdelay', 40);
$width		= $params->get( 'width');
$stcolor	= $params->get( 'stcolor', '255,255,255');
$encolor	= $params->get( 'encolor', '0,0,0');
$baseurl 	= JURI::base();
?>
<link rel="stylesheet" href="<?php echo $baseurl; ?>modules/mod_trendyscrollnews/css/style.css" />
<table class="trendysn"><tbody><tr>
<?php if ($starttext=="1") { ?><td class="trendytext" valign="top"><?php echo JText::_('STARTCHAR'); ?></td><?php } else {} ?>
<td class="trendysn">
<script type="text/javascript">

/***********************************************
* Fading Scroller- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

var delay = <?php echo $delay; ?>; //set delay between message change (in miliseconds)
var maxsteps=<?php echo $numsteps; ?>; // number of steps to take to change from start color to endcolor
var stepdelay=<?php echo $stepsdelay; ?>; // time in miliseconds of a single step
//**Note: maxsteps*stepdelay will be total time in miliseconds of fading effect
var startcolor= new Array(<?php echo $stcolor; ?>);
var endcolor=new Array(<?php echo $encolor; ?>);

var fcontent=new Array();
begintag=''; //set opening tag, such as font declarations

	<?php 	$i		= 0;
		foreach ($list as $item) :  ?>
			fcontent[<?php echo $i; ?>]="<?php echo $item->text; $i++;?>"
	<?php endforeach; ?>
	
closetag='';

var fwidth='<?php echo $width; ?>';
var fheight='auto'; //set scroller height i.e 150px

var fadelinks=1;  //should links inside scroller content also fade like text? 0 for no, 1 for yes.

///No need to edit below this line/////////////////


var ie4=document.all&&!document.getElementById;
var DOM2=document.getElementById;
var faderdelay=0;
var index=0;


/*Rafael Raposo edited function*/
//function to change content
function changecontent(){
  if (index>=fcontent.length)
    index=0
  if (DOM2){
    document.getElementById("fscroller").style.color="rgb("+startcolor[0]+", "+startcolor[1]+", "+startcolor[2]+")"
    document.getElementById("fscroller").innerHTML=begintag+fcontent[index]+closetag
    if (fadelinks)
      linkcolorchange(1);
    colorfade(1, 15);
  }
  else if (ie4)
    document.all.fscroller.innerHTML=begintag+fcontent[index]+closetag;
  index++
}

// colorfade() partially by Marcio Galli for Netscape Communications.  ////////////
// Modified by Dynamicdrive.com

function linkcolorchange(step){
  var obj=document.getElementById("fscroller").getElementsByTagName("A");
  if (obj.length>0){
    for (i=0;i<obj.length;i++)
      obj[i].style.color=getstepcolor(step);
  }
}

/*Rafael Raposo edited function*/
var fadecounter;
function colorfade(step) {
  if(step<=maxsteps) {	
    document.getElementById("fscroller").style.color=getstepcolor(step);
    if (fadelinks)
      linkcolorchange(step);
    step++;
    fadecounter=setTimeout("colorfade("+step+")",stepdelay);
  }else{
    clearTimeout(fadecounter);
    document.getElementById("fscroller").style.color="rgb("+endcolor[0]+", "+endcolor[1]+", "+endcolor[2]+")";
    setTimeout("changecontent()", delay);
	
  }   
}

/*Rafael Raposo's new function*/
function getstepcolor(step) {
  var diff
  var newcolor=new Array(3);
  for(var i=0;i<3;i++) {
    diff = (startcolor[i]-endcolor[i]);
    if(diff > 0) {
      newcolor[i] = startcolor[i]-(Math.round((diff/maxsteps))*step);
    } else {
      newcolor[i] = startcolor[i]+(Math.round((Math.abs(diff)/maxsteps))*step);
    }
  }
  return ("rgb(" + newcolor[0] + ", " + newcolor[1] + ", " + newcolor[2] + ")");
}

if (ie4||DOM2)
  document.write('<div id="fscroller" style="width:'+fwidth+';height:'+fheight+'"></div>');

if (window.addEventListener)
window.addEventListener("load", changecontent, false)
else if (window.attachEvent)
window.attachEvent("onload", changecontent)
else if (document.getElementById)
window.onload=changecontent

</script>
</td>
</tr></tbody></table>
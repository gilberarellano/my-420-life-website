<?php
/**
* Trendy Scroll News - Joomla Module
* Version			: 1.0
* Created by		: RBO Team - RumahBelanja.com & TechnoTravels.com
* Created on		: 15 November 2010 (Joomla 1.5.x) and 17 December 2010 (Joomla 1.6.x)
* Updated on		: n/a
* Package			: Joomla 1.6.x
* License			: http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once (dirname(__FILE__).DS.'helper.php');

$list = modTrendyScrollNewsHelper::getList($params);
require JModuleHelper::getLayoutPath('mod_trendyscrollnews', $params->get('layout', 'default'));
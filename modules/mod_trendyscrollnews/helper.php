<?php
/**
* Trendy Scroll News - Joomla Module
* Version			: 1.0
* Created by		: RBO Team - RumahBelanja.com & TechnoTravels.com
* Created on		: 15 November 2010 (Joomla 1.5.x) and 17 December 2010 (Joomla 1.6.x)
* Updated on		: n/a
* Package			: Joomla 1.6.x
* License			: http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
*/

// no direct access
defined('_JEXEC') or die;

require_once JPATH_SITE.'/components/com_content/helpers/route.php';

jimport('joomla.application.component.model');

JModel::addIncludePath(JPATH_SITE.'/components/com_content/models');

abstract class modTrendyScrollNewsHelper
{
	public static function getList(&$params)
	{
		// Get the dbo
		$db = JFactory::getDbo();
		
		//Get the config
		$config =& JFactory::getConfig();
		$offset = $config->getValue('config.offset');
		
		// Get an instance of the generic articles model
		$model = JModel::getInstance('Articles', 'ContentModel', array('ignore_request' => true));

		// Set application parameters in model
		$app = JFactory::getApplication();
		$appParams = $app->getParams();
		$model->setState('params', $appParams);
		
		//Get Parameters in module params
		$count		= (int) $params->get('count', 5);
		$time_type 	= $params->get( 'time_type');
		$time_sys	= (int) $params->get( 'time_sys', 1);
		$totalskip	= (int) $params->get( 'totalskip', 0);
		$show_front	= $params->get('show_front', 1);

		// Set the filters based on the module params
		$model->setState('list.start', (int) $params->get('totalskip', 0));
		$model->setState('list.limit', (int) $params->get('count', 5));
		$model->setState('filter.published', 1);

		// Access filter
		$access = !JComponentHelper::getParams('com_content')->get('show_noauth');
		$authorised = JAccess::getAuthorisedViewLevels(JFactory::getUser()->get('id'));
		$model->setState('filter.access', $access);

		// Category filter
		$model->setState('filter.category_id', $params->get('catid', array()));

		// User filter
		$userId = JFactory::getUser()->get('id');
		switch ($params->get('user_id'))
		{
			case 'by_me':
				$model->setState('filter.author_id', (int) $userId);
				break;
			case 'not_me':
				$model->setState('filter.author_id', $userId);
				$model->setState('filter.author_id.include', false);
				break;

			case '0':
				break;

			default:
				$model->setState('filter.author_id', (int) $params->get('user_id'));
				break;
		}

		// Filter by language
		$model->setState('filter.language',$app->getLanguageFilter());

		//  Featured switch
		switch ($params->get('show_front'))
		{
			//case '1':
				//$model->setState('filter.featured', 'only');
				//break;
			case '0':
				$model->setState('filter.featured', 'hide');
				break;
			case '1':
			default:
				$model->setState('filter.featured', 'show');
				break;
		}
		
		// Set ordering
		$order_map = array(
			'0' => 'a.created',
			'1' => 'a.modified DESC, a.created',
			'2' => 'a.created',
			'3' => 'a.hits',
			'4' => 'a.hits',
			'5' => 'RAND()',
			//'mc_dsc' => 'CASE WHEN (a.modified = '.$db->quote($db->getNullDate()).') THEN a.created ELSE a.modified END',
			//'p_dsc' => 'a.publish_up',
		);
		$ordering = JArrayHelper::getValue($order_map, $params->get('ordering'), 'a.publish_up');
		//$dir = 'DESC';
		//help ordering system for DESC or ASC
		switch ($params->get( 'ordering_by' ))
		{
			case 5:
				$dir = 'DESC';
				break;
			case 4:
				$dir = 'ASC';
				break;
			case 3:
				$dir = 'DESC';
				break;
			case 2:
				$dir = 'ASC';
				break;
			case 1:
				$dir = 'DESC';
				break;
			case 0:
			default:
				$dir = 'DESC';
				break;
		}

		$model->setState('list.ordering', $ordering);
		$model->setState('list.direction', $dir);

		$items = $model->getItems();

		foreach ($items as &$item) {
			$item->slug = $item->id.':'.$item->alias;
			$item->catslug = $item->catid.':'.$item->category_alias;

			if ($access || in_array($item->access, $authorised))
			{
				// We know that user has the privilege to view the article
				$item->link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catslug));
			}
			else {
				$item->link = JRoute::_('index.php?option=com_user&view=login');
			}
			$item->hits = htmlspecialchars( $item->hits );
			$item->created=JHTML::_('date', htmlspecialchars( $item->created ),"$time_type", $offset);
			
			if ($time_sys=="1") {
				$item->text = "<span class=\'trendysn\'>".$item->created."&nbsp;<a href=\'".$item->link."\'>".htmlspecialchars( $item->title )."</a></span>";
			} else {
				$item->text = "<span class=\'trendysn\'><a href=\'".$item->link."\'>".htmlspecialchars( $item->title )."</a></span>";
				//$item->text = "<a href=\'".$item->link."\'>".htmlspecialchars( $item->title )."</a>";
				}
			
			$item->introtext = JHtml::_('content.prepare', $item->introtext);
		}

		return $items;
	}
}
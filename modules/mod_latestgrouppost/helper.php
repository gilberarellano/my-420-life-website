<?php
/**
 * @category	Modules
 * @package		JomSocial
 * @copyright (C) 2008 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license		GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die('Restricted access');

require_once( JPATH_ROOT . DS . 'components' . DS . 'com_community' . DS . 'libraries' . DS . 'core.php' );

class modGroupPosthelper
{
	function getStream( &$params )
	{
		//since @2.6
		$db		=& JFactory::getDBO();
		$query = '';
		//if user is logged in, depends on the settings, either to display updates from participated group or all public groups
		if($params->get('afterlogin_setting') && JFactory::getUser()->id){
			//first lets get the user participated group
			CFactory::load( 'models' , 'groups' );		
			$groupsModel	= CFactory::getModel('groups');
			
			$groupIds = $groupsModel -> getGroupIds(JFactory::getUser()->id);
			$groupIds = implode(',',$groupIds);
			
			$query	= 'SELECT a.*, b.' . $db->nameQuote('name').' AS groupname, b.' . $db->nameQuote('thumb').' AS thumbnail
					   FROM ' . $db->nameQuote( '#__community_activities' ) . ' AS a '
					. 'INNER JOIN '.  $db->nameQuote( '#__community_groups' ) . ' AS b '
					. ' ON b.' . $db->nameQuote('id').'=a.' . $db->nameQuote('groupid')
					. ' WHERE a.' . $db->nameQuote( 'app' ) . '=' . $db->Quote( 'groups.wall' )
					. ' AND a.' . $db->nameQuote('groupid').' IN ('. $groupIds .')'
					. ' ORDER BY a.' . $db->nameQuote('created').' DESC '
					. ' LIMIT ' . $params->get( 'count' );
					
		}else{

			$query	= 'SELECT a.*, b.' . $db->nameQuote('name').' AS groupname, b.' . $db->nameQuote('thumb').' AS thumbnail
					   FROM ' . $db->nameQuote( '#__community_activities' ) . ' AS a '
					. 'INNER JOIN '.  $db->nameQuote( '#__community_groups' ) . ' AS b '
					. ' ON b.' . $db->nameQuote('id').'=a.' . $db->nameQuote('groupid').' AND b.' . $db->nameQuote('approvals').'=' . $db->Quote( 0 )
					. ' WHERE a.' . $db->nameQuote( 'app' ) . '=' . $db->Quote( 'groups.wall' )
					. ' ORDER BY a.' . $db->nameQuote('created').' DESC '
					. ' LIMIT ' . $params->get( 'count' );
		}
		
		/* jom_social fails when the groupIds fails 
		 * Xavier Baez
		 */
		if (isset($groupIds) && !empty($groupIds)) {
			$db->setQuery( $query );

			$rows	= $db->loadObjectList();
		
			// if exist, group the results based on groups
			$results = array();
			if(count($rows)){
				foreach($rows as $row){
					$results[$row->groupid][] = $row;
				}
			}
		
			//reverse the results so that latest will be shown on top
			array_reverse($results);
		
			return $results;
		}
		
		return false;
	}
}

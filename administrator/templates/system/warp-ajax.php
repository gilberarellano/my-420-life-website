<?php
/**
* @package   Warp Theme Framework
* @file      warp-ajax.php
* @version   6.0.2
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// get template data/path
$data   = JRequest::getVar('jform', array(), 'post', 'array');
$config = JPATH_ROOT."/templates/{$data['template']}/config.php";

if (file_exists($config)) {

	// load template config
	require_once($config);

	// trigger save config
	$warp = Warp::getInstance();
	$warp['system']->saveConfig();	

}
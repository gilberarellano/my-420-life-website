DROP TABLE IF EXISTS `#__im_fields`;

CREATE TABLE `#__im_fields` (
  `id` int(11) NOT NULL auto_increment,
  `profiler_id` int(11) default NULL,
  `column_no` int(11) default NULL,
  `column_title` varchar(255) default NULL,
  `field` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
);

DROP TABLE IF EXISTS `#__im_global`;

CREATE TABLE `#__im_global` (
`id` int(11) NOT NULL auto_increment,
`profiler_id` int(11) default NULL,
`field_name` varchar(255) default NULL,
`field_value` varchar(255) default NULL,
PRIMARY KEY  (`id`)
);

DROP TABLE IF EXISTS `#__im_profiler`;

CREATE TABLE `#__im_profiler` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `description` varchar(255) default NULL,
  `number_run` int(11) default NULL,
  `last_run` datetime default NULL,
  `created` datetime default NULL,
  `column_separator` varchar(50) default NULL,
  `author_id` int(11) default NULL,
  `section_id` int(11) default NULL,
  `category_id` int(11) default NULL,
  `pushlished` int(10) unsigned default NULL,
  `meta_keys` text default NULL,
  `meta_des` text default NULL,
  `created_date` datetime default NULL,
  `publish_up_date` datetime default NULL,
  `publish_down_date` datetime default NULL,
  `access_level` tinyint(3) unsigned default NULL,
  `published` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`)
);

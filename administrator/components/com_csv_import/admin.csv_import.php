<?php
/**
 * CSV Import Component for Content and jReviews
 * Copyright (C) 2008-2011 NakedJoomla and ClickFWD LLC
 * This is not free software. Do not distribute it.
 * For license information visit http://www.nakedjoomla.com/license/csv_import_license.html
 * or contact info@nakedjoomla.com
**/

// no direct access
defined('_JEXEC') or die('Direct Access to this location is not allowed.');

define('CMS_JOOMLA15','CMS_JOOMLA15');
if (!defined('DS')) 		define('DS', DIRECTORY_SEPARATOR);

$database = JFactory::getDBO();
$uri = JFactory::getURI();
define( 'PATH_ROOT', JPATH_ROOT . DS);
define('WWW_ROOT', $uri->root() );

require_once( PATH_ROOT . 'administrator' . DS . 'components' . DS . 'com_csv_import' . DS . 'admin.csv_import.html.php' );
require_once( PATH_ROOT . 'administrator' . DS . 'components' . DS . 'com_csv_import' . DS . 'csv_import.class.php' );

# Require csv reader library
require_once(PATH_ROOT . "administrator/components/com_csv_import/csvLib.php");

if(file_exists(PATH_ROOT . 'components' . DS . 'com_jreviews')) 
{
	DEFINE('_JREVIEWS_INSTALLED',1);
} else {
	DEFINE('_JREVIEWS_INSTALLED',0);	
}

echo '<link href="'. WWW_ROOT .'/administrator/components/com_csv_import/csv_import.css" rel="stylesheet" type="text/css" />';
?>
<table class="csvimport_heading">
   <tr>
      <td><img src="../administrator/components/com_csv_import/csv_import_title.gif" alt="CSV Import for Joomla and jReviews" /></td>
   </tr>
</table>

<?php
if(@!is_dir(PATH_ROOT . 'images/csv_import/')) {
	if(!mkdir(PATH_ROOT . '/images/csv_import/',755)) {
		echo '<div style="color:red;font-weight:bold;">It was not possible to create the csv_import folder, please create this folder in /images/csv_import';
	}
}

//Turn off notice, warning

error_reporting(E_ALL);

$cid = JRequest::getVar('cid');
if(is_array($cid)) array_map('intval',$cid);

switch ($task) {	
	case "show_step1":
		showStep1($option);
		break;
	case "process_step1":
		processStep1($option);
		break;
	case "process_step2":
		processStep2($option);
		break;			
	case "process_step3":
		processStep3($option);
		break;
	case "process_step4":
		processStep4($option);
		break;		
	case "remove":
		removeProfilers($cid,$option);
		break;	
	default:   
		profilerList($option);		
		break;		
}

function profilerList($option)
{
    $app = JFactory::getApplication();      
    $config = JFactory::getConfig();
    $db = JFactory::getDBO();
    
	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $config->get('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	$search 	= $app->getUserStateFromRequest( "search{$option}", 'search', '' );
	if (get_magic_quotes_gpc()) {
		$search	= stripslashes( $search );
	}

	$where = array();
	
	if ($search) {
		$where[] = "LOWER(a.name) LIKE '%" . $db->getEscaped( trim( strtolower( $search ) ) ) . "%'";
	}

	// get the total number of records
	$query = "SELECT COUNT(*)"
	. "\n FROM #__im_profiler AS a"
	. (count( $where ) ? "\n WHERE " . implode( ' AND ', $where ) : "")
	;
	
	$db->setQuery( $query );

    jimport('joomla.html.pagination');
	
    $pageNav = new JPagination( $db->loadResult(), $limitstart, $limit );
    
	$query = " Select * From #__im_profiler ";
	
	$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );

	$rows = $db->loadObjectList();
	if ($db->getErrorNum()) {
		echo $db->stderr();
		return false;
	}
	
	HTML_csv_import::showProfilers( $option, $rows,$search, $pageNav );	
}

function showStep1($option)
{
	$database = JFactory::getDBO();
	$profilerId=JRequest::getInt('profiler_id',0);
	$sql="Select * From #__im_profiler Where id = " . $profilerId;
	$database->setQuery($sql);
	$rowProfiler = $database->loadObject();
	HTML_csv_import::showStep1($option,$rowProfiler,$profilerId);		
}

function processStep1($option)
{
	$database = JFactory::getDBO();
    $user = JFactory::getUser();
    $app = JFactory::getApplication();
        
    $separator = JRequest::getVar('csv_separator',',');
	$profilerId = JRequest::getVar('profiler_id',0);
	
	error_reporting(1);
	
	$userId = $user->id;
	
	if($_FILES['csv_file'])
	{
		$fileName=$_FILES['csv_file']['name'];		
		//check file extensions
		$ext=substr($fileName, strrpos($fileName, '.') + 1);
		
		if($ext!='csv')
		{
			$app->redirect("index.php?option=$option&task=show_step1","Invalid file extensions");
		}
		else 
		{
			//Save the file to server
			$fileName=$userId."_".time()."_".$fileName;
			move_uploaded_file($_FILES['csv_file']['tmp_name'], PATH_ROOT . "images/csv_import/$fileName");

			//Read the csv fields
												
			//cell separator, row separator, value enclosure
			$csv = new CSV($separator, "\r\n", '"');
			
			//parse the string content
			$csv->setContent(file_get_contents(PATH_ROOT  ."images/csv_import/$fileName"));
			
			//returns an array with the CSV data
			$csvArray = $csv->getArray();				
            
			//Read the header
			$headers = current($csvArray); 
						
			//Get column mapping from profiler
			if($profilerId > 0)
			{
				$sql="Select * From #__im_fields Where profiler_id=$profilerId order by id";
				$database->setQuery($sql);
				$rowFields=$database->loadObjectList();				
			}

			//Get jos_jreview field
			if(_JREVIEWS_INSTALLED) {
				$sql="Select name From #__jreviews_fields WHERE location = 'content' order by name";
				$database->setQuery($sql);
				$rowJReviewFields=$database->loadObjectList();
			} else {
				$rowJReviewFields = array();
			}

			HTML_csv_import::showStep2($option,$profilerId,$separator,$headers,$rowFields,$fileName,$rowJReviewFields);
									
		}		
		
	}
	else 
	{
		$app->redirect("index.php?option=$option&task=show_step1","Please select a csv file to import");
	}	
}


function processStep2($option)
{          
	//Get the data from posted form
    $database = JFactory::getDBO();
	$rowProfiler = new stdClass();
    $rowCustomGlobalSettings = array();
    
    $rowProfiler->author_id = null;
    $rowProfiler->section_id = null;
    $rowProfiler->category_id = null;
    $rowProfiler->state = 1;
    $rowProfiler->meta_keys = null;
    $rowProfiler->meta_des = null;
    $rowProfiler->created_date = null;
    $rowProfiler->publish_up_date = null;
    $rowProfiler->publish_down_date = null;
    $rowProfiler->access = cmsCompat::getCmsVersion() == 1.5 ? 0 : 1;
    
    $profilerId=JRequest::getVar('profiler_id',0);
	$separator=JRequest::getVar('separator',',');
	$fileName=JRequest::getVar('filename','');
	
	$columns=JRequest::getVar('columns',null);
	$fields=JRequest::getVar('fields',null);
	
	$rowGlobalFields = array();
	$arrFields=array();
	
	for($i=0;$i<count($fields);$i++)
	{
		$field=$fields[$i];
		$arrField=explode(".",$field);
		$tableName=$arrField[0];
		$fieldName=$arrField[1];
		if($tableName=="jos_jreviews_fields" && _JREVIEWS_INSTALLED)
			$arrFields[]="'".$fieldName."'";
	}
	
	if(!empty($arrFields))	{
		$sql="Select name,type From #__jreviews_fields where location = 'content' AND name not in (".implode(",",$arrFields).") order by name";
	
		$database->setQuery($sql);
	
		$rowGlobalFields = $database->loadObjectList();
	}
		
	//Get profiler data
	
	if($profilerId>0)
	{
		$sql="Select * From #__im_profiler where id=$profilerId";
		$database->setQuery($sql);
		$rowProfiler = $database->loadObject();
		//Get setting for value fields
		
		$sql = "Select * From #__im_global Where profiler_id=$profilerId order by id";
		$database->setQuery($sql);
		$rowCustomGlobalSettings = $database->loadObjectList();
	    $rowProfiler->state = 1;
    }
	HTML_csv_import::showStep3($option,$columns,$fields,$rowGlobalFields,$profilerId,$separator,$fileName,$rowProfiler,$rowCustomGlobalSettings);	
	
}


function processStep3($option)
{
	$database = JFactory::getDBO();
	$images = '';

	$profilerId = JRequest::getVar('profiler_id',0);
	$separator = JRequest::getVar('separator',',');
	$fileName = JRequest::getVar('filename','');
	
	$columns = JRequest::getVar('columns',null);
	$fields = JRequest::getVar('fields',null);	
	
	$globalColumns = JRequest::getVar('global_columns',null);
	$globalValues = JRequest::getVar('global_value',null);
	
	//Get global setting	
	$authorId = JRequest::getVar('author_id',0);
	$sectionId = JRequest::getVar('section_id',0);
	$categoryId = JRequest::getVar('category_id',0);
	$state = JRequest::getVar('state',0);
	$metaKeys = JRequest::getVar('meta_keys','');
	$metaDes = JRequest::getVar('meta_des','');
	$createdDate = JRequest::getVar('created_date','');
	$publishUpDate = JRequest::getVar('publish_up_date','');
	$publishDownDate = JRequest::getVar('publish_down_date','');
	
	$access = JRequest::getVar('access',0);
		
	//Added params
	$params = JRequest::getVar('params',null);
	
	//Also, upload file, check now
	
	if($_FILES["default_image"]["name"])
	{
		$fileName1=time()."_".$_FILES["default_image"]["name"];		
		move_uploaded_file($_FILES["default_image"]["tmp_name"], PATH_ROOT . "images/stories/jreviews/$fileName1");
		$images="jreviews/$fileName1|||0||bottom||";
	}
							
	HTML_csv_import::showStep4($option,$columns,$fields,$globalColumns,$globalValues,$profilerId,$separator,$fileName,$authorId,$sectionId,$categoryId,$state,$metaKeys,$metaDes,$createdDate,$publishUpDate,$publishDownDate,$access,$params,$images);	
}


function processStep4($option)
{
	$database = JFactory::getDBO();	
	//Get hidden data
	$separator=JRequest::getVar('separator',',');
		
	//fault here
	$fileName=JRequest::getVar('filename','');
	
	$columns=JRequest::getVar('columns',null);
	$fields=JRequest::getVar('fields',null);
	
	$globalColumns=JRequest::getVar('global_columns',null);
	$globalValues=JRequest::getVar('global_value',null);
	
	//Get global setting
	$authorId=JRequest::getVar('author_id',0);
	$sectionId=JRequest::getVar('section_id',0);
	$categoryId=JRequest::getVar('category_id',0);
	$published=JRequest::getVar('published',0);
	$metaKeys=JRequest::getVar('meta_keys','');
	$metaDes=JRequest::getVar('meta_des','');
	$createdDate=JRequest::getVar('created_date','');
	$publishUpDate=JRequest::getVar('publish_up_date','');
	$publishDownDate=JRequest::getVar('publish_down_date','');
	$access=JRequest::getVar('access',0);
	$state=JRequest::getVar('state',0);

	//Import data now, go to final step
	$rowContent = JTable::getInstance('content');

	//Get all data
	$arrContentFieldName=array();
	$arrContentFieldSTT=array();
	
	$arrJreviewFieldName=array();
	$arrJreviewFieldSTT=array();
	
	$totalColumn=count($columns);
	
	//Mark the ignore field lists
	$arrIgnores=array();
	
	for($i=0;$i<$totalColumn;$i++)
	{
		$column=$columns[$i];
		$field=$fields[$i];
		$arrField=explode(".",$field);
		$tableName=$arrField[0];
		$fieldName=$arrField[1];
		
		if($fieldName!='ignore')
		{
			if($tableName=="jos_content")
			{
				$arrContentFieldName[$i]=$fieldName;
				$arrContentFieldSTT[]=$i;
			}
			else 
			{
				$arrJreviewFieldName[$i]=$fieldName;
				$arrJreviewFieldSTT[]=$i;
			}
		}
		else 
		{
			$arrIgnores[]=$i;
		}
	}
	
	//Set static content vaiable
	$arrGlobalSetting=array();
	$arrGlobalSetting["created_by"]=$authorId;
	$arrGlobalSetting["sectionid"]=$sectionId;
	$arrGlobalSetting["catid"]=$categoryId;
	$arrGlobalSetting["published"]=$published;
	$arrGlobalSetting["metakey"]=$metaKeys;
	$arrGlobalSetting["metadesc"]=$metaDes;
	$arrGlobalSetting["created"]=$createdDate;
	$arrGlobalSetting["publish_up"]=$publishUpDate;
	$arrGlobalSetting["publish_down"]=$publishDownDate;
	$arrGlobalSetting["access"]=$access;
	$arrGlobalSetting["state"]=$state;

	//CustomGlobla Varaible
	$totalGlobalFields=count($globalColumns);
		
	$arrJreviewSettings=array();
	
	for($i=0;$i<$totalGlobalFields;$i++)
	{
		$column=$globalColumns[$i];
		$value=$globalValues[$i];				
		$arrField=explode(".",$column);
						
		$field=$arrField[1];
		$arrJreviewSettings[$field]=$value;							
	}

	//Attributes
	$params = JRequest::getVar('params', '' );
	if (is_array( $params )) {
		$txt = array();
		foreach ( $params as $k=>$v) {
			if (get_magic_quotes_gpc()) {
				$v = stripslashes( $v );
			}
			$v != '' and $txt[] = "$k=$v";
		}
		$attribs = implode( "\n", $txt );
	}
	
	//Read the file here
	
	//Open File and read the correlative data
	//cell separator, row separator, value enclosure
	$csv = new CSV($separator, "\r\n", '"');
	
	//parse the string content
	$csv->setContent(file_get_contents(PATH_ROOT . "images/csv_import/$fileName"));
	
	//returns an array with the CSV data
	$csvArray = $csv->getArray();		

	//Read the header		
	$line=1;
	
	$arrErrorLine=array();
	$arrErrorMessage=array();
	$totalImported=0;
	
	$images=JRequest::getVar('images','');
	
	while( false != ( $cells = next($csvArray) ) )
	{		
		$totalCells=count($cells);
		
		//Check all the cell		
		$continue=false;
		
		for($k=0;$k<count($cells);$k++)
		{
			if($cells[$k])
			{
				$continue=true;
				break;
			}			
		}
		
		
		if($continue)
		{
			if($line>0)		
			{			
				$success=true;
				$errorLine=0;
				$errMsg="";
				
				$arrContentData=array();
				$arrJreviewData=array();

				for($i=0;$i<$totalCells;$i++)
				{
					$cell=$cells[$i];
					if(!in_array($i,$arrIgnores))
					{				
						if(in_array($i,$arrContentFieldSTT))
						{
							$contentField = $arrContentFieldName[$i];
							$arrContentData[$contentField] = $cell;
						}
						else 
						{
							$jReviewField = $arrJreviewFieldName[$i];
							$arrJreviewData[$jReviewField] = $cell;
						}
					}
				}

				$copyArrGlobalSetting = $arrGlobalSetting;
				
				// Overwrite global settings with CSV fields - added v1.0.9
				$csvColumns = array('catid','sectionid','created_by','metakey','metadesc','publish_up','publish_down','images');

				foreach($csvColumns AS $csvColumn) 
				{
                    if(array_key_exists($csvColumn,$arrContentData) && $arrContentData[$csvColumn] != '')
					{
						unset($copyArrGlobalSetting[$csvColumn]);
					} elseif(array_key_exists($csvColumn,$arrContentData)) {
						unset($arrContentData[$csvColumn]);
					}         
				}
                				
                if(!isset($arrContentData['alias']) || $arrContentData['alias'] == '') {
                    $arrContentData['alias'] = cmsCompat::getTitleAlias($arrContentData['title']);
                }
                
				$arrContentData=array_merge($arrContentData,$copyArrGlobalSetting);
				$arrJreviewData=array_merge($arrJreviewData,$arrJreviewSettings);
					
				//Save data to correlative table
                $rowContent = JTable::getInstance('content');
				
				if(!$rowContent->bind($arrContentData,'id'))
				{
					$success=false;	
					$errMsg.=" ".$rowContent->getError();
				}			
				
				$rowContent->id=0;
					
				$rowContent->attribs = $attribs;
				
                // Convert commas into new lines
                $rowContent->images = str_replace(",","\n",$rowContent->images);

                if(!$rowContent->store())
				{
					$success=false;
					$errMsg.=" ".$rowContent->getError();
				}
				
//prx($rowContent);
//exit;
                if($success && _JREVIEWS_INSTALLED)
				{
					$contentId=$rowContent->id;
					
					$arrJreviewData["contentid"]=$contentId;
						
					//Build query to insert into content table
					$err = insertObject("#__jreviews_content",$arrJreviewData);
								
					if($err)
					{
						$success=false;
						$errMsg.=" ".$err;
						$sql="Delete From #__content Where id=$rowContent->id";
						$database->setQuery($sql);
						$database->query();
					}							
				}
	
				if(!$success)
				{
					$arrErrorLine[]=$line+1;
					$arrErrorMessage[]=$errMsg;
				}
				else 
				{
					$totalImported++;
				}
			}
		}
		else 
		{
			break;
		}
		$line++;
	}
	
	//Save profiler
	$profileName=JRequest::getVar('profile_name');
	
	if($profileName)
	{
		$rowProfiler = new CSVProfiler($database);
		
        $_POST['access_level'] = $_POST['access'];
        
		if(!$rowProfiler->bind($_POST))
		{
			echo "<script> alert('".$rowProfiler->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		
		$rowProfiler->name=$profileName;		
		$rowProfiler->number_run=1;
		$rowProfiler->last_run=date("Y-m-d");
		$rowProfiler->created=$rowProfiler->last_run;
		
		if(!$rowProfiler->store())
		{
			echo "<script> alert('".$rowProfiler->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}	
		
		//Insert data about other fields mapping
		$profilerId=$rowProfiler->id;
		for($i=0,$n=count($columns);$i<$n;$i++)
		{
			$column=$columns[$i];
			$field=$fields[$i];
			$columnNo=$i+1;
			$sql="Insert Into 
				 #__im_fields(
				 profiler_id,
				 column_no,
				 column_title,
				 `field`)
				 Values(
				 $profilerId,
				 $columnNo,
				 '$column',
				 '$field'				 
				 )
				";
			$database->setQuery($sql);
			
			if(!$database->query())
			{
				echo "<script> alert('".$database->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
							
		}
		
		//Insert into global setting tables

		for($i=0,$n=count($globalColumns);$i<$n;$i++)
		{
			$column=$globalColumns[$i];
			$value=$globalValues[$i];			
			$sql="Insert Into 
				 #__im_global(
				 profiler_id,
				 field_name,
				 field_value
				 )
				 Values(
				 $profilerId,				 
				 '$column',
				 '$value'				 
				 )
				";
			$database->setQuery($sql);
			
			if(!$database->query())
			{
				echo "<script> alert('".$database->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
							
		}			
	}
		
	//Save profile here
		
	HTML_csv_import::showImportResult($option,$totalImported,$arrErrorLine,$arrErrorMessage,$sectionId,$categoryId);	
}

function removeProfilers( $cid, $option ) {	
	
	$database = JFactory::getDBO();

	if (!is_array( $cid ) || count( $cid ) < 1) {
		echo "<script> alert('Select an item to delete'); window.history.go(-1);</script>\n";
		exit;
	}
	
	//Delete data from related table
	if (count( $cid )) {
		
        foreach($cid AS $key=>$val) {
            $cid[$key] = (int) $val;
        }
        $cid = array_filter($cid);

		$cids = 'profiler_id=' . implode( ' OR profiler_id=', $cid );

		//Delete the related field
				
		$sql="Delete From #__im_fields Where ($cids)";
		
		$database->setQuery($sql);
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
		
		
		//Delete global setting
		
		$sql="Delete From #__im_global Where ($cids)";
		$database->setQuery($sql);
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
		
		//Delete the global data				
		$cids = 'id=' . implode( ' OR id=', $cid );
		
		$query = "DELETE FROM #__im_profiler "
		. "\n WHERE ( $cids )"
		;
		$database->setQuery( $query );
		
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}

        $url = str_replace('&amp;','&',$url);        
        if (headers_sent()) {     
            echo "<script>document.location.href='index.php?option={$option}';</script>\n";
        } else {
            header( 'HTTP/1.1 301 Moved Permanently' );
            header( 'Location: ' . "index.php?option={$option}" );
        }
}

//Build the help function for store data


function insertObject($tableName,$arr)
{
	$database = JFactory::getDBO();
		
	$arrFields=array();
	$arrValues=array();
	foreach ($arr as $key => $value)
	{
		if($key)
		{
			$arrFields[]=$database->NameQuote($key);
			$arrValues[]=$database->Quote($value);
		}
	}	
	$sql="Insert Into $tableName(".implode(",",$arrFields).") Values(".implode(",",$arrValues).")";
	$database->setQuery($sql);
	$database->query();
	return $database->getErrorMsg();
}


/**
 * Returns CMS version
**/
function getPlatform(){
	
    global $version;
    return $version->RELEASE;
	
}

class cmsCompat 
{
    function getCmsVersion() 
    {
        global $version;
        return $version->RELEASE;
    }
    
    function getUserManagerUrl() {
        if(cmsCompat::getCmsVersion() == 1.5) {
            return 'index.php?option=com_users&task=view';
        }        
        return 'index.php?option=com_users&view=users';
    }  
    
    function getCategoryManagerUrl() {
        if(cmsCompat::getCmsVersion() == 1.5) {
            return 'index.php?option=com_categories&section=com_content';
        }
        return 'index.php?option=com_categories&extension=com_content';
    }
    
    function getTitleAlias($title) 
    {
        $alias = htmlentities(utf8_decode($title));
        $alias = preg_replace(
            array('/&szlig;/','/&(..)lig;/', '/&([aouAOU])uml;/','/&(.)[^;]*;/'),
            array('ss',"$1","$1".'e',"$1"),
            $alias);

        // remove any duplicate whitespace, and ensure characters are alphanumeric
        $alias = preg_replace(array('/\s+/'), array('-'), $alias);    
        $alias = preg_replace(array('/[^A-Za-z0-9\-]/'), array(''), $alias);
        $alias = mb_strtolower($alias); 
        return $alias;       
    }
}

function prx($var){
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

<?php
/**
 * CSV Import Component for Content and jReviews
 * Copyright (C) 2008 NakedJoomla and ClickFWD LLC
 * This is not free software. Do not distribute it.
 * For license information visit http://www.nakedjoomla.com/license/csv_import_license.html
 * or contact info@nakedjoomla.com
**/
error_reporting(E_ALL);

// no direct access
defined('_JEXEC') or die('Direct Access to this location is not allowed.');

class TOOLBAR_csv_import {
	function _STEP1() {
		global $id;
		JToolBarHelper::custom('','cancel.png','cancel.png','Cancel',false);
		JToolBarHelper::custom('process_step1','forward.png','forward.png','Next',false);		
	}
	
	function _STEP2() {
		global $id;
		JToolBarHelper::custom('','cancel.png','cancel.png','Cancel',false);
		JToolBarHelper::custom('process_step2','forward.png','forward.png','Next',false);		
	}
	
	
	function _STEP3() {
		global $id;
		JToolBarHelper::custom('','cancel.png','cancel.png','Cancel',false);
		JToolBarHelper::custom('process_step3','forward.png','forward.png','Next',false);		
	}
	
	
	function _STEP4() {
		global $id;
		JToolBarHelper::custom('','cancel.png','cancel.png','Cancel',false);
		JToolBarHelper::custom('process_step4','forward.png','forward.png','Next',false);		
	}
	
	function _RESULTS() {
		JToolBarHelper::addNew('show_step1');
		JToolBarHelper::custom('','default.png','default.png','Home',false);    
	}	
	
	function _DEFAULT() {
        JToolBarHelper::title( 'CSV Import for Joomla &amp; JReviews', 'generic.png' );
        JToolBarHelper::addNewX('show_step1');
        JToolBarHelper::deleteList();
    }
}

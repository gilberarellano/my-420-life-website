<?php

/**
* @package JFusion_mediawiki
* @author JFusion development team
* @copyright Copyright (C) 2008 JFusion. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC' ) or die('Restricted access' );

/**
 * JFusion Admin Class for mediawiki 1.1.x
 * For detailed descriptions on these functions please check the model.abstractadmin.php
 * @package JFusion_mediawiki
 */
//require_once(JPATH_ADMINISTRATOR .DS.'components'.DS.'com_jfusion'.DS.'models'.DS.'model.jfusionadmin.php');
//require_once(JPATH_ADMINISTRATOR .DS.'components'.DS.'com_jfusion'.DS.'models'.DS.'model.abstractadmin.php');

class JFusionAdmin_mediawiki extends JFusionAdmin{

    function getJname()
    {
        return 'mediawiki';
    }

    function getTablename()
    {
        return 'user';
    }

    function setupFromPath($source_path)
    {
        //check for trailing slash and generate file path
        if (substr($source_path, -1) == DS) {
            //remove it so that we can make it compatible with mediawiki's MW_INSTALL_PATH
            $source_path = substr($source_path, 0, -1);
        }

        $myfile = $source_path . DS. 'LocalSettings.php';
        $defines = $source_path . DS. 'includes'. DS. 'Defines.php';
        $defaults = $source_path . DS. 'includes'. DS. 'DefaultSettings.php';
         //try to open the file
         if ( !file_exists($myfile) ) {
            JError::raiseWarning(500,JText::_('WIZARD_FAILURE'). ": ".$myfile." " . JText::_('WIZARD_MANUAL'));
            return false;
         } else {
			$wgDBserver = $wgDBtype = $wgDBname = $wgDBuser = $wgDBpassword = $wgDBprefix = '';
    		defined ('MEDIAWIKI') or define( 'MEDIAWIKI',TRUE );
    		defined ('MW_INSTALL_PATH') or define('MW_INSTALL_PATH', $source_path);
    		include_once($defines);
    		$IP = $source_path;
    		include($defaults);
    		include($myfile);

            $params['database_host'] = $wgDBserver;
            $params['database_type'] = $wgDBtype;
            $params['database_name'] = $wgDBname;
            $params['database_user'] = $wgDBuser;
            $params['database_password'] = $wgDBpassword;
            $params['database_prefix'] = $wgDBprefix;
            $params['source_path'] = $source_path;

            if (!empty($wgCookiePrefix)) {
                $params['cookie_name'] = $wgCookiePrefix;
            }

            return $params;
        }
    }

    function getConfig( $getVar ) {
    	static $config = array();

    	if (isset($config[$getVar])) {
    	    return $config[$getVar];
    	}

    	$params = JFusionFactory::getParams($this->getJname());
    	$source_path = $params->get('source_path');

        //check for trailing slash and generate file path
        if (substr($source_path, -1) == DS) {
            //remove it so that we can make it compatible with mediawiki's MW_INSTALL_PATH
            $source_path = substr($source_path, 0, -1);
        }

        $myfile = $source_path . DS. 'LocalSettings.php';
        $defaults = $source_path . DS. 'includes'. DS. 'DefaultSettings.php';
        $defines = $source_path . DS. 'includes'. DS. 'Defines.php';
		defined ('MEDIAWIKI') or define( 'MEDIAWIKI',TRUE );
		defined ('MW_INSTALL_PATH') or define('MW_INSTALL_PATH', $source_path);
		include_once($defines );
		$IP = $source_path;
		include($defaults);
		include($myfile);
       	$config[$getVar] = (isset($$getVar)) ? $$getVar : '';
		return $config[$getVar];
    }

    function getUserList()
    {
        // initialise some objects
        $db = JFusionFactory::getDatabase($this->getJname());
        $query = 'SELECT user_name as username, user_email as email from #__user';
        $db->setQuery($query );
        $userlist = $db->loadObjectList();

        return $userlist;
    }

    function getUserCount()
    {
        //getting the connection to the db
        $db = JFusionFactory::getDatabase($this->getJname());
        $query = 'SELECT count(*) from #__user';
        $db->setQuery($query );

        //getting the results
        return $db->loadResult();
    }

    function getUsergroupList()
    {
        $wgGroupPermissions = $this->getConfig('wgGroupPermissions');

        $usergrouplist = array();
        foreach($wgGroupPermissions as $key => $value) {
        	if ( $key != '*' ) {
 				$group = new stdClass;
        		$group->id = $key;
        		$group->name = $key;
        		$usergrouplist[] = $group;
        	}
        }
        return $usergrouplist;
    }

    function getDefaultUsergroup()
    {
        $params = JFusionFactory::getParams($this->getJname());
        $usergroup_id = $params->get('usergroup');
        return $usergroup_id;
    }

    function allowRegistration()
    {
		$wgGroupPermissions = $this->getConfig('wgGroupPermissions');
        if (is_array($wgGroupPermissions) && $wgGroupPermissions['*']['createaccount'] == true) {
            return true;
        } else {
            return false;
        }
    }

	function generateRedirectCode()
	{
        	$params = JFusionFactory::getParams($this->getJname());
        	$joomla_params = JFusionFactory::getParams('joomla_int');
		    $joomla_url = $joomla_params->get('source_url');
    		$joomla_itemid = $params->get('redirect_itemid');

			//create the new redirection code
			$redirect_code = '
//JFUSION REDIRECT START
//SET SOME VARS
$joomla_url = \''. $joomla_url . '\';
$joomla_itemid = ' . $joomla_itemid .';
	';
		    $redirect_code .= '
if(!defined(\'_JEXEC\') && strpos($_SERVER[\'QUERY_STRING\'], \'dlattach\') === false && strpos($_SERVER[\'QUERY_STRING\'], \'verificationcode\') === false)';

		    $redirect_code .= '
{
	$pattern = \'#action=(login|admin|featuresettings|news|packages|detailedversion|serversettings|theme|manageboards|postsettings|managecalendar|managesearch|smileys|manageattachments|viewmembers|membergroups|permissions|regcenter|ban|maintain|reports|viewErrorLog)#\';
	if ( !preg_match( $pattern , $_SERVER[\'QUERY_STRING\'] ) ) {
		$file = $_SERVER["SCRIPT_NAME"];
		$break = explode(\'/\', $file);
		$pfile = $break[count($break) - 1];
		$jfusion_url = $joomla_url . \'index.php?option=com_jfusion&Itemid=\' . $joomla_itemid . \'&jfile=\'.$pfile. \'&\' . $_SERVER[\'QUERY_STRING\'];
		header(\'Location: \' . $jfusion_url);
		exit;
	}
}
//JFUSION REDIRECT END
';
	return $redirect_code;
	}

    function enable_redirect_mod()
    {
    	$error = 0;
    	$error = 0;
    	$reason = '';
    	$mod_file = $this->getModFile('index.php',$error,$reason);

		if($error == 0) {
			//get the joomla path from the file
			jimport('joomla.filesystem.file');
			$file_data = JFile::read($mod_file);
	      	preg_match_all('/\/\/JFUSION REDIRECT START(.*)\/\/JFUSION REDIRECT END/ms',$file_data,$matches);

			//remove any old code
			if(!empty($matches[1][0])){
		      	$search = '/\/\/JFUSION REDIRECT START(.*)\/\/JFUSION REDIRECT END/ms';
		      	$file_data = preg_replace($search,'',$file_data);

			}

			$redirect_code = $this->generateRedirectCode();

	      	$search = '/\<\?php/si';
			$replace = '<?php' . $redirect_code;
	      	$file_data = preg_replace($search,$replace,$file_data);
			JFile::write($mod_file, $file_data);
		}
    }

    function disable_redirect_mod()
    {
    	$error = 0;
    	$reason = '';
    	$mod_file = $this->getModFile('index.php',$error,$reason);

		if($error == 0) {
			//get the joomla path from the file
			jimport('joomla.filesystem.file');
			$file_data = JFile::read($mod_file);
	      	$search = '/\/\/JFUSION REDIRECT START(.*)\/\/JFUSION REDIRECT END/si';
	      	$file_data = preg_replace($search,'',$file_data);
			JFile::write($mod_file, $file_data);
		}

    }

	function outputJavascript(){
?>
<script language="javascript" type="text/javascript">
<!--
function auth_mod(action) {
var form = document.adminForm;
form.customcommand.value = action;
form.action.value = 'apply';
submitform('saveconfig');
return;
}

//-->
</script>
<?php
	}

    function show_redirect_mod($name, $value, $node, $control_name)
    {
    	$error = 0;
    	$reason = '';
    	$mod_file = $this->getModFile('index.php',$error,$reason);

		if($error == 0) {
			//get the joomla path from the file
			jimport('joomla.filesystem.file');
			$file_data = JFile::read($mod_file);
	      	preg_match_all('/\/\/JFUSION REDIRECT START(.*)\/\/JFUSION REDIRECT END/ms',$file_data,$matches);

			//compare it with our joomla path
			if(empty($matches[1][0])){
	        	$error = 1;
	        	$reason = JText::_('MOD_NOT_ENABLED');
			}
		}

		//add the javascript to enable buttons
		$this->outputJavascript();

		if ($error == 0){
			//return success
			$output = '<img src="components/com_jfusion/images/check_good.png" height="20px" width="20px">' . JText::_('REDIRECTION_MOD') . ' ' . JText::_('ENABLED');
			$output .= ' <a href="javascript:void(0);" onclick="return auth_mod(\'disable_redirect_mod\')">' . JText::_('MOD_DISABLE') . '</a>';
			$output .= ' <a href="javascript:void(0);" onclick="return auth_mod(\'enable_redirect_mod\')">' . JText::_('MOD_UPDATE') . '</a>';
			return $output;
		} else {
       		$output = '<img src="components/com_jfusion/images/check_bad.png" height="20px" width="20px">' . JText::_('REDIRECTION_MOD') . ' ' . JText::_('DISABLED') .': ' . $reason;
			$output .= ' <a href="javascript:void(0);" onclick="return auth_mod(\'enable_redirect_mod\')">' . JText::_('MOD_ENABLE') . '</a>';
			return $output;
		}

    }
}


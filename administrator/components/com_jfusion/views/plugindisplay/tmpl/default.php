<?php

/**
 * This is view file for wizard
 *
 * PHP version 5
 *
 * @category   JFusion
 * @package    ViewsAdmin
 * @subpackage Plugindisplay
 * @author     JFusion Team <webmaster@jfusion.org>
 * @copyright  2008 JFusion. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.jfusion.org
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
//display the paypal donation button
JFusionFunction::displayDonate();
$images = 'components/com_jfusion/images/';
?>

<form method="post" action="index.php" name="adminForm" id="adminForm">
<input type="hidden" name="option" value="com_jfusion" />
<input type="hidden" name="task" value="" />

<table><tr><td width="100px">
<img src="<?php echo $images; ?>jfusion_large.png" height="75px" width="75px">
</td><td width="100px">
<img src="<?php echo $images; ?>controlpanel.png" height="75px" width="75px">
<td><h2><?php echo JText::_('PLUGIN_CONFIGURATION'); ?></h2></td></tr></table><br/>

<table class="adminlist" cellspacing="1"><thead><tr>
<th class="title" width="20px"><?php echo JText::_('ID'); ?></th>
<th class="title" align="left"><?php echo JText::_('NAME'); ?></th>
<th class="title" align="center"><?php echo JText::_('OPTIONS'); ?></th>
<th class="title" align="center"><?php echo JText::_('DESCRIPTION'); ?></th>
<th class="title" width="40px" align="center"><?php echo JText::_('MASTER'); ?></th>
<th class="title" width="40px" align="center"><?php echo JText::_('SLAVE'); ?></th>
<th class="title" width="40px" align="center"><?php echo JText::_('CHECK_ENCRYPTION'); ?></th>
<th class="title" width="40px" align="center"><?php echo JText::_('DUAL_LOGIN'); ?></th>
<th class="title" align="center"><?php echo JText::_('STATUS'); ?></th>
<th class="title" align="center"><?php echo JText::_('USERS'); ?></th>
<th class="title" align="center"><?php echo JText::_('REGISTRATION'); ?></th>
<th class="title" align="center"><?php echo JText::_('DEFAULT_USERGROUP'); ?></th>
</tr></thead><tbody>

<?php
$masterSet = false;
$row_count = 0;
foreach ($this->rows as $record) {
    //check that the files exist
    if (!file_exists(JPATH_COMPONENT . DS . 'plugins' . DS . $record->name . DS . 'admin.php')) {
        JError::raiseWarning(500, $record->name . ': ' . JText::_('NO_FILES'));
    }

    if ($record->master == '1') {
        $masterSet = true;
    }
    echo '<tr class="row' . $row_count . '">';
    if ($row_count == 1) {
        $row_count = 0;
    } else {
        $row_count = 1;
    }
    $JFusionPlugin = null;
    $JFusionPlugin = & JFusionFactory::getAdmin($record->name);
    //added check for database configuration to prevent error after moving sites
    if ($record->status == '1') {
        $config_status = $JFusionPlugin->checkConfig();
        $disabled = ($config_status['config'] == 1) ? 0 : 1;
        //do a check to see if the status field is correct
        if ($config_status['config'] != $record->status) {
            //Save this error for the integration
            $jdb = & JFactory::getDBO();
            $query = 'UPDATE #__jfusion SET status = ' . $config_status['config'] . ' WHERE name =' . $jdb->Quote($record->name);
            $jdb->setQuery($query);
            $jdb->query();
            //update the record status for the rest of the code
            $record->status = $config_status['config'];
        }
    } else {
        $config_status = array();
        $config_status['config'] = 3;
        $disabled = 1;
        $config_status['message'] = JText::_('NO_CONFIG');
    }
    ?>

    <td><INPUT TYPE=RADIO NAME="jname" VALUE="<?php echo $record->name; ?>"/></td>
    <td><?php echo $record->name ?></td>
    <td><div class="edit"><a href="index.php?option=com_jfusion&task=plugineditor&jname=<?php echo $record->name; ?>" title="Edit Plugin"><img src="<?php echo $images ?>edit.png" alt="Edit plugin" /></a></div>
    <?php
    if ($record->name != 'joomla_int') {
        ?><div class="wizard"><a href="index.php?option=com_jfusion&task=wizard&jname=<?php echo $record->name; ?>" title="Configure Plugin"><img src="<?php echo $images ?>wizard_icon.png" alt="Configure plugin" /></a></div></td>
        <?php
    }
    ?>

    <td><?php $JFusionParam = & JFusionFactory::getParams($record->name);
    $description = $JFusionParam->get('description');
    if ($description) {
        echo $description;
    } else {
        $plugin_xml = JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_jfusion' . DS . 'plugins' . DS . $record->name . DS . 'jfusion.xml';
        if (file_exists($plugin_xml) && is_readable($plugin_xml)) {
            $parser = JFactory::getXMLParser('Simple');
            $xml = $parser->loadFile($plugin_xml);
            $xml = $parser->document;
            echo $xml->description[0]->data();
        } else {
            echo "";
        }
    }
    ?></td>

    <?php //check to see if module is a master
    if ($disabled == 1 && ($record->master == '1' || $record->master == '0')) { ?>
        <td><img src="<?php echo $images; ?>cross_dim.png" border="0" alt="Wrong Config" /></td>
        <?php
    } elseif ($record->master == '1') { ?>
        <td><a href="javascript:void(0);" onclick="setCheckedValue(document.adminForm.jname, '<?php echo $record->name; ?>'); document.adminForm.field_name.value = 'master'; document.adminForm.field_value.value = '0';submitbutton('changesetting')" title="Disable Plugin"><img src="<?php echo $images; ?>tick.png" border="0" alt="Enabled" /></a></td>
        <?php
    } elseif ($record->master == '0') { ?>
        <td><a href="javascript:void(0);" onclick="setCheckedValue(document.adminForm.jname, '<?php echo $record->name; ?>'); document.adminForm.field_name.value = 'master'; document.adminForm.field_value.value = '1';submitbutton('changesetting')" title="Enable Plugin"><img src="<?php echo $images; ?>cross.png" border="0" alt="Disabled" /></a></td>
        <?php
    } else { ?>
        <td><img src="<?php echo $images; ?>checked_out.png" border="0" alt="Unavailable" /></td>
        <?php
    }

    //check to see if module is s slave
    if ($disabled == 1 && ($record->slave == '1' || $record->slave == '0')) { ?>
        <td><img src="<?php echo $images; ?>cross_dim.png" border="0" alt="Wrong Config" /></td>
        <?php
    } elseif ($record->slave == '1') { ?>
        <td><a href="javascript:void(0);" onclick="setCheckedValue(document.adminForm.jname, '<?php echo $record->name; ?>'); document.adminForm.field_name.value = 'slave'; document.adminForm.field_value.value = '0';submitbutton('changesetting')" title="Disable Plugin"><img src="<?php echo $images; ?>tick.png" border="0" alt="Enabled" /></a></td>
        <?php
    } elseif ($record->slave == '0') { ?>
        <td><a href="javascript:void(0);" onclick="setCheckedValue(document.adminForm.jname, '<?php echo $record->name; ?>'); document.adminForm.field_name.value = 'slave'; document.adminForm.field_value.value = '1';submitbutton('changesetting')" title="Enable Plugin"><img src="<?php echo $images; ?>cross.png" border="0" alt="Disabled" /></a></td>
        <?php
    } else { ?>
        <td><img src="<?php echo $images; ?>checked_out.png" border="0" alt="Unavailable" /></td>
        <?php
    }

    //check to see if password encryption is enabled
    if ($disabled == 1 && ($record->check_encryption == '1' || $record->check_encryption == '0')) { ?>
        <td><img src="<?php echo $images; ?>cross_dim.png" border="0" alt="Wrong Config" /></td>
        <?php
    } elseif ($record->check_encryption == '1') { ?>
        <td><a href="javascript:void(0);" onclick="setCheckedValue(document.adminForm.jname, '<?php echo $record->name; ?>'); document.adminForm.field_name.value = 'check_encryption'; document.adminForm.field_value.value = '0';submitbutton('changesetting')" title="Disable Encryption"><img src="<?php echo $images; ?>tick.png" border="0" alt="Enabled" /></a></td>
        <?php
    } elseif ($record->check_encryption == '0') { ?>
        <td><a href="javascript:void(0);" onclick="setCheckedValue(document.adminForm.jname, '<?php echo $record->name; ?>'); document.adminForm.field_name.value = 'check_encryption'; document.adminForm.field_value.value = '1';submitbutton('changesetting')" title="Enable Encryption"><img src="<?php echo $images; ?>cross.png" border="0" alt="Disabled" /></a></td>
        <?php
    } else { ?>
        <td><img src="<?php echo $images; ?>checked_out.png" border="0" alt="Unavailable" /></td>
        <?php
    }

    //check to see if dual login is enabled
    if ($disabled == 1 && ($record->dual_login == '1' || $record->dual_login == '0')) { ?>
        <td><img src="<?php echo $images; ?>cross_dim.png" border="0" alt="Wrong Config" /></td>
        <?php
    } elseif ($record->dual_login == '1') { ?>
        <td><a href="javascript:void(0);" onclick="setCheckedValue(document.adminForm.jname, '<?php echo $record->name; ?>'); document.adminForm.field_name.value = 'dual_login'; document.adminForm.field_value.value = '0';submitbutton('changesetting')" title="Disable Dual Login"><img src="<?php echo $images; ?>tick.png" border="0" alt="Enabled" /></a></td>
        <?php
    } elseif ($record->dual_login == '0') { ?>
        <td><a href="javascript:void(0);" onclick="setCheckedValue(document.adminForm.jname, '<?php echo $record->name; ?>'); document.adminForm.field_name.value = 'dual_login'; document.adminForm.field_value.value = '1';submitbutton('changesetting')" title="Enable Dual Login"><img src="<?php echo $images; ?>cross.png" border="0" alt="Disabled" /></a></td>
        <?php
    } else { ?>
        <td><img src="<?php echo $images; ?>checked_out.png" border="0" alt="Unavailable" /></td>
        <?php
    }

    //check to see what the config status is
    if ($config_status['config'] == 1) {
        echo '<td><img src="' . $images . 'tick.png" border="0" alt="Good Config" />' . $config_status['message'] . '</td>';
        //output the total number of users for the plugin
        $total_users = null;
        $total_users = $JFusionPlugin->getUserCount();
        echo '<td>' . $total_users . '</td>';
        //check to see if new user registration is allowed
        $new_registration = null;
        $new_registration = $JFusionPlugin->allowRegistration();
        if ($record->master != '1') {
            if ($new_registration) {
                echo '<td><img src="' . $images . 'cross.png" border="0" alt="Enabled" />' . JText::_('ENABLED') . '</td>';
            } else {
                echo '<td><img src="' . $images . 'tick.png" border="0" alt="Disabled" />' . JText::_('DISABLED') . '</td>';
            }
        } else {
            if ($new_registration) {
                echo '<td><img src="' . $images . 'tick.png" border="0" alt="Enabled" />' . JText::_('ENABLED') . '</td>';
            } else {
                echo '<td><img src="' . $images . 'cross.png" border="0" alt="Disabled" />' . JText::_('DISABLED') . '</td>';
            }
        }
        //output detailed configuration warnings for the plugin
        if ($record->master == '1' || $record->slave == '1') {
            $JFusionPlugin->debugConfig();
        }
        //display the default usergroup
        $params = & JFusionFactory::getParams($record->name);
		$usergroups = $params->get('usergroup');
        $multiusergroups = $params->get('multiusergroup');
        if (substr($usergroups, 0, 2) == 'a:' || substr($multiusergroups, 0, 2) == 'a:') {
            $usergroup = JText::_('ADVANCED_GROUP_MODE');
        } else {
            $usergroup = $JFusionPlugin->getDefaultUsergroup();
        }
        if ($usergroup) {
            echo "<td>$usergroup</td></tr>";
        } else {
            echo '<td><img src="' . $images . 'cross.png" border="0" alt="Disabled" />' . JText::_('MISSING') . ' ' . JText::_('DEFAULT_USERGROUP') . '</td></tr>';
            JError::raiseWarning(0, $record->name . ': ' . JText::_('MISSING') . ' ' . JText::_('DEFAULT_USERGROUP'));
        }
    } else {
        if ($record->status == '1') {
            echo '<td><img src="' . $images . 'tick.png" border="0" alt="Good Config" />' . JText::_('GOOD_CONFIG') . '</td>';
        } else {
            echo '<td><img src="' . $images . 'cross.png" border="0" alt="Wrong Config" />' . JText::_('NO_CONFIG') . '</td>';
        }
        echo '<td></td><td></td><td></td></tr>';
    }
}
if (!$masterSet) {
    JError::raiseWarning(0, JText::_('NO_MASTER_WARNING'));
}
?>
</tbody></table><br/><br/>

<?php echo JText::_('PLUGIN_CONFIG_INSTR'); ?>

<input type="hidden" name="field_name" value=""/><input type="hidden" name="field_value" value=""/></form>

<table width="100%"><tr><td><img src="<?php echo $images; ?>tick.png" border="0" alt="Enabled" /> = <?php echo JText::_('ENABLED'); ?> </td>
<td><img src="<?php echo $images; ?>cross.png" border="0" alt="Disabled" /> = <?php echo JText::_('DISABLED'); ?> </td>
<td><img src="<?php echo $images; ?>cross_dim.png" border="0" alt="Config Plugin First" /> = <?php echo JText::_('CONFIG_FIRST'); ?> </td>
<td><img src="<?php echo $images; ?>checked_out.png" border="0" alt="Unavailable" /> = <?php echo JText::_('UNAVAILABLE'); ?> </td>
</tr></table></br>
<br/><br/><br/>
<table class="adminlist" cellspacing="1"><thead><tr><th colspan="2" class="title" >
<?php echo JText::_('LEGEND'); ?>
</th></td></thead><tr><td>
<?php echo JText::_('MASTER'); ?>
</td><td>
<?php echo JText::_('LEGEND_MASTER'); ?>
</td></tr><tr><td>
<?php echo JText::_('SLAVE'); ?>
</td><td>
<?php echo JText::_('LEGEND_SLAVE'); ?>
</td></tr><tr><td>
<?php echo JText::_('CHECK_ENCRYPTION'); ?>
</td><td>
<?php echo JText::_('LEGEND_CHECK_ENCRYPTION'); ?>
</td></tr><tr><td>
<?php echo JText::_('DUAL_LOGIN'); ?>
</td><td>
<?php echo JText::_('LEGEND_DUAL_LOGIN'); ?>
</td></tr><tr><td>
<?php echo JText::_('STATUS'); ?>
</td><td>
<?php echo JText::_('LEGEND_STATUS'); ?>
</td></tr><tr><td>
<?php echo JText::_('USERS'); ?>
</td><td>
<?php echo JText::_('LEGEND_USERS'); ?>
</td></tr><tr><td>
<?php echo JText::_('REGISTRATION'); ?>
</td><td>
<?php echo JText::_('LEGEND_REGISTRATION'); ?>
</td></tr><tr><td>
<?php echo JText::_('DEFAULT_USERGROUP'); ?>
</td><td>
<?php echo JText::_('LEGEND_DEFAULT_USERGROUP'); ?>
</td></tr></table>
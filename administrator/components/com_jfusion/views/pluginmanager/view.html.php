<?php

/**
 * This is view file for pluginmanager
 *
 * PHP version 5
 *
 * @category   JFusion
 * @package    ViewsAdmin
 * @subpackage Pluginmanager
 * @author     JFusion Team <webmaster@jfusion.org>
 * @copyright  2008 JFusion. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.jfusion.org
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Renders the main admin screen that shows the configuration overview of all integrations
 *
 * @category   JFusion
 * @package    ViewsAdmin
 * @subpackage Pluginmanager
 * @author     JFusion Team <webmaster@jfusion.org>
 * @copyright  2008 JFusion. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.jfusion.org
 */
class jfusionViewpluginmanager extends JView
{
     /**
     * displays the view
     *
     * @param string $tpl template name
     *
     * @return string html output of view
     */
    function display($tpl = null)
    {
        $bar = new JToolBar('My Toolbar');
        $bar->appendButton('Standard', 'delete', JText::_('REMOVE'), 'uninstallplugin', false, false);
        $bar->appendButton('Standard', 'copy', JText::_('COPY'), 'copyplugin', false, false);
        $toolbar = $bar->render();
        //get the data about the JFusion plugins except for joomla_int
        $db = & JFactory::getDBO();
        $query = "SELECT id, name from #__jfusion WHERE name != 'joomla_int' ORDER BY name";
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        if ($rows) {
            //print out results to user
            $this->assignRef('rows', $rows);
            $this->assignRef('toolbar', $toolbar);
            parent::display($tpl);
        } else {
            JError::raiseWarning(500, JText::_('NO_JFUSION_TABLE'));
        }
    }
}

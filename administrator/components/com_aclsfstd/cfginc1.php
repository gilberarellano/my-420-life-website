<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function opt_form()
{
global $admconf_url, $db_name, $host_name, $db_user, $db_password, $table_ads, 
$urlclscrpt, $opt_header, $opt_footer, $adm_passw, $adm_email,
 $photos_path, $usevrfcode, $plcntdtl,$vstminnmbr,
$plcntpml,$voterate,$maxvalrate,$uselogsvmv, $exprlogs, $table_logs, $use_spmg,
$exp_period, $adsonpage, $sndadmnotif, $moderating, $hltadsf,$exp_perdhlt,$dsplhltpht,
 $pradsdupl,$tmltads, $privacy_mail, $pmailtp, $sendcopytoadm, $redirtoadm, $sndexpmsg1,
$photos_count,$phptomaxsize, $incl_mtmdfile, $mtmdfile_maxs, $flattchext, $plsflsrvrs, $infldflsrv,
$prphscnd, $maximgswith, $resphdb, $maxpixph, $ppactv, $pladddp,$mbac_sndml, $mbac_addad,$mbac_second,
$jsfrmch, $schallusrads, $ch_nmusr, $usrads_max, $usrads_chcktime, $titleclpg, $ht_header, $ht_footer,
$moderating_ct, $moderating_vl, $mbac_second_vl, $mbac_second_ct, 
$mbac_addad_vl, $mbac_addad_ct, $mbac_sndml_vl, $mbac_sndml_ct,
$adsontoppage, $colr_hltads, $capt_hltads, $tpadsindd, $bc2adsfrm, $width_2tpf, $adbnrcll, $fntclr_3, 
$fntclr_2, $tbclr_5, $fntclr_1, $tbclr_4, $tbclr_3, $tbclr_2, $tbclr_1, $prphscrwdth, $incl_prevphoto,
$ad_ind_width, $top_page_width, $maximgswith, $prphscnd, $ad_second_width, $detl_leftcol, $ind_leftcol, 
$btmtophtml, $top_rightcol, $top_leftcol, $prwdly, $photos_url, $displaflusr,$waflnscr, 
$dtl_rightcol, $ind_rightcol, $ht_leftcol, $cnt_htl_det, $cnt_htl_ind, $ordhltrate,$jloginfo, $sturlvar,
$use_ajax, $jstoplads, $jstphlads, $maxttlsz, $lgneditd, $use_rndads, $use_adsphgal,
$rndadsrpt, $rndadstmdl, $rndadsmin, $rndadsfphl, $use_hdn_vrfc, 
$kwsrchfrm, $keywrdtable, $kwminln, $kwmaxsgst, $kwminsgl, $kwminadsind, $spamlnkfl, $spamcntnt,
$actadoptv, $cnfemtmpl, $cnfemtsbj, $expemltmpl, $expmsg_subj,
$fbv_app_id, $fbv_sectret, $fb_intrfc, $fb_adspl, $fb_prmail, $fb_act_domain, $fbappsurl ;

echo "
<p>
<a href='{$admconf_url}'><b>Top:</b></a><br>
<form action='{$admconf_url}' method='post'>
<input type='hidden' name='mdopt' value='saveopts'>
<p>
<table width='700'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp; General  Options
</td></tr></table>

<table width='700'    border='0' cellspacing='10' cellpadding='10'> 
<tr><td valign='top' width='50%'>
<p align='justify'> 
Admin password:<br>
<input type='text' name='adm_passw' value='".opt_qt($adm_passw)."' class='formst'><p>
Admin e-mail:<br>
<input type='text' name='adm_email' value='".opt_qt($adm_email)."' class='formst'>
<p>

Expiration period for ads (days):<br>
<input type='text' name='exp_period' value='".opt_qt($exp_period)."' class='formst'>

<p>
Number of ads displayed on the index page:<br>
<input type='text' name='adsonpage' value='".opt_qt($adsonpage)."' class='formst'>

<p>
Max ads title size for displaying on ads index pages (chars):<br>
<input type='text' name='maxttlsz' value='".opt_qt($maxttlsz)."' class='formst'>
 
<p align='justify'>
Protection for ads duplication:<br>.
<select name='pradsdupl' class='formst'>
<option>".opt_qt($pradsdupl)."<checked><option>yes<option>no</select>

<p align='justify'>
Time (days) for latest ads link <br> on the top page:<br>
<input type='text' name='tmltads' value='".opt_qt($tmltads)."' class='formst'> 

<p align='justify'> 
Use fast search features: <br>
<select name='kwsrchfrm' class='formst'>
<option>".opt_qt($kwsrchfrm)."<checked><option>yes<option>no</select>

<p align='justify'>
Set up JavaScript checking of  ads fields on submitting form:<br>
<select name='jsfrmch' class='formst'>
<option>".opt_qt($jsfrmch)."<checked><option>yes<option>no</select>

</td><td valign='top' width='50%'>

<p align='justify'>
 Place on detailed ad page special link to search 
 all ads posted by a user ( search with the same contact e-mail )
 if user place more than 1 ad:<br>
<select name='schallusrads' class='formst'>
<option>".opt_qt($schallusrads)."<checked><option>yes<option>no</select>


<p align='justify'>
Use AJAX framework: <br>
<select name='use_ajax' class='formst'>
<option>".opt_qt($use_ajax)."<checked><option>yes<option>no</select>
 
<p align='justify'> 
number of latest ads displayed on the top page (works with AJAX option):<br>
<input type='text' name='jstoplads' value='".opt_qt($jstoplads)."' class='formst'> 

<p align='justify'> 
Place 'visits counter' on ads details pages:<br>
<select name='plcntdtl' class='formst'>
<option>".opt_qt($plcntdtl)."<checked><option>yes<option>no</select>

<p align='justify'> 
 Place 'contacts counter' (how many privacy mails have been sent to ad owner) 
 on ads details pages:<br>
<select name='plcntpml' class='formst'>
<option>".opt_qt($plcntpml)."<checked><option>yes<option>no</select>

<p align='justify'> 
Use IP logs to count only one visit, contact message and vote 
  of the same user for the same ad:<br>
<select name='uselogsvmv' class='formst'>
<option>".opt_qt($uselogsvmv)."<checked><option>yes<option>no</select>

<p align='justify'> 
Expiration period for logs (days):<br>
<input type='text' name='exprlogs' value='".opt_qt($exprlogs)."' class='formst'> 

</td></tr></table>

 

<p>
<table width='700'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp; Moderating
</td></tr></table>

<table width='700'    border='0' cellspacing='10' cellpadding='10'> 
<tr><td valign='top' width='50%'>

Set up moderating:<br>
<select name='moderating' class='formst'>
<option>".opt_qt($moderating)."<checked><option>yes<option>no</select>
&nbsp;
<font   class='fnt4'   OnClick=\"displ('mdrt');\">
Special Option
</font>
<DIV id='mdrt' style='DISPLAY: none'>
Set up  different  moderating option:<br>
<select name='moderating_vl' class='formst'>
<option>".opt_qt($moderating_vl)."<checked><option>yes<option>no</select>
<br> for the following categories:<br>
<input type='text' name='moderating_ct' value='".opt_qt($moderating_ct)."' class='formst' size='40'>
<br> 
<font style='FONT-SIZE: 11px;'>(set up list of the categories in the format:  
<center><font style='color: #000099;'>catname1,catname2,catname3</font> </center>  where
<font style='color: #000077;'>catname1, ...</font> - are the short names of the categories)</font>
</div>

<p align='justify'> 
 Use verification code for ads submitting, sending privacy mail:<br>
<select name='usevrfcode' class='formst'>
<option>".opt_qt($usevrfcode)."<checked><option>yes<option>no</select>

<p align='justify'> 
 Use hidden verification by analysing users' activity on submitting forms without
entering verification code:<br>
<select name='use_hdn_vrfc' class='formst'>
<option>".opt_qt($use_hdn_vrfc)."<checked><option>yes<option>no</select>

<p align='justify'>
Check number of placed ads per time period by each user:<br>   
<select name='ch_nmusr' class='formst'>
<option>".opt_qt($ch_nmusr)."<checked><option>yes<option>no</select>
 
<p align='justify'>
if you set up 'yes' for previous option, set up max number of ads allowed to place:<br> 
<input type='text' name='usrads_max' value='".opt_qt($usrads_max)."' class='formst'>

<p align='justify'>
and time period (days) for check:<br> 
<input type='text' name='usrads_chcktime' value='".opt_qt($usrads_chcktime)."' class='formst'>
 
<p align='justify'>

</td><td valign='top' width='50%'>

Activating of ads by clicking on the activation link in the confirmation e-mail: <br>
<select name='actadoptv' class='formst'>
<option>".opt_qt($actadoptv)."<checked><option>yes<option>no</select>

<p align='justify'>
 
Confirmation e-mail subject:<br>
<input type='text' name='cnfemtsbj' value='".opt_qt($cnfemtsbj)."' class='formst' size=40>

<p align='justify'>
 
Confirmation e-mail template:<br>
(--act_link-- will be replaced by activation link)<br>
<textarea rows='8' cols='40' name='cnfemtmpl' wrap=off class=formst>$cnfemtmpl</textarea> 

</td></tr></table>

<p>
<table width='700'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
&nbsp; Privacy Mail, Messages
</td></tr></table>

<table width='700'    border='0' cellspacing='10' cellpadding='10'> 
<tr><td valign='top' width='50%'>
Set up privacy mail: <br>
<select name='privacy_mail' class='formst'>
<option>".opt_qt($privacy_mail)."<checked><option>yes<option>no</select>

<p align='justify'>
Privacy mail template:<br>

<textarea rows='8' cols='40' name='pmailtp' wrap=off class=formst>$pmailtp</textarea> 
 
 If privacy mail is set up, send copy of privacy messages 
 to admin: <br>
<select name='sendcopytoadm' class='formst'>
<option>".opt_qt($sendcopytoadm)."<checked><option>yes<option>no</select>
 
<p align='justify'>
 If privacy mail is set up, redirect privacy messages 
 to admin:<br>
<select name='redirtoadm' class='formst'>
<option>".opt_qt($redirtoadm)."<checked><option>yes<option>no</select>

</td><td valign='top' width='50%'>

<p align='justify'>
 Send expiration message to ads owners when their expired ads were deleted:<br>
<select name='sndexpmsg1' class='formst'>
<option>".opt_qt($sndexpmsg1)."<checked><option>yes<option>no</select>

<p align='justify'>
Expiration e-mail subject:<br>
<input type='text' name='expmsg_subj' value='".opt_qt($expmsg_subj)."' class='formst' size=40>

<p align='justify'>
Expiration e-mail template:<br>
<textarea rows='8' cols='40' name='expemltmpl' wrap=off class=formst>$expemltmpl</textarea> 

</td></tr></table>

<p>
<table width='700'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp;Ads Photos, Attachments
</td></tr></table>

<table width='700'    border='0' cellspacing='10' cellpadding='10'> 
<tr><td valign='top' width='50%'>

Max number of photos allowed:<br>
<input type='text' name='photos_count' value='".opt_qt($photos_count)."' class='formst'> 

<p align='justify'>
Set max size for all photos on the second ad page (bytes):<br>
<input type='text' name='phptomaxsize' value='".opt_qt($phptomaxsize)."' class='formst'> 

<p align='justify'>
Width of preview photos on the second ad page (pixels):<br>
<input type='text' name='prphscnd' value='".opt_qt($prphscnd)."' class='formst'> 

<p align='justify'>
Max width of photos on the second ad page (pixels):<br>
<input type='text' name='maximgswith' value='".opt_qt($maximgswith)."' class='formst'> 
 
<p align='justify'>
Set up resizing of large photos before saving in the database:<br>
<select name='resphdb' class='formst'>
<option>".opt_qt($resphdb)."<checked><option>yes<option>no</select>
 
<p align='justify'>
Max width or height (pixels) of photos for resizing:<br>
<input type='text' name='maxpixph' value='".opt_qt($maxpixph)."' class='formst'>
 
</td><td valign='top' width='50%'>
 
Includes attachment file into ads:<br>
<select name='incl_mtmdfile' class='formst'>
<option>".opt_qt($incl_mtmdfile)."<checked><option>yes<option>no</select>

<p align='justify'>
Max size for attachment file (bytes):<br>
<input type='text' name='mtmdfile_maxs' value='".opt_qt($mtmdfile_maxs)."' class='formst'> 

<p align='justify'>
Types of extensions for ads attachment files:<br>
<input type='text' name='flattchext' value=\"".opt_arr($flattchext)."\" class='formst' size=40> 

<p align='justify'>
Allow users to place files for ads on popular file servers:<br> 
<select name='plsflsrvrs' class='formst'>
<option>".opt_qt($plsflsrvrs)."<checked><option>yes<option>no</select>
 
<p align='justify'>
Info about loading ad files on popular file servers:<br> 
<textarea rows='8' cols='40' name='infldflsrv' wrap=off class=formst>".opt_qt($infldflsrv)."</textarea> 


</td></tr></table>
  

<p>
<table width='700'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp; Membership
</td></tr></table>


<table width='700'    border='0' cellspacing='10' cellpadding='10'> 
<tr><td valign='top' width='50%'>

Set up free (f) or membership (m) option for access to ad second page :<br> 
<select name='mbac_second' class='formst'>
<option>".opt_qt($mbac_second)."<checked><option>f<option>m</select>
&nbsp;
<font   class='fnt4'   OnClick=\"displ('acspg');\">
Special Option
</font>
<DIV id='acspg' style='DISPLAY: none'>
Set up  different   option:<br>
<select name='mbac_second_vl' class='formst'>
<option>".opt_qt($mbac_second_vl)."<checked><option>f<option>m</select>
<br> for the following categories:<br>
<input type='text' name='mbac_second_ct' value='".opt_qt($mbac_second_ct)."' class='formst' size='40'>
<br> 
<font style='FONT-SIZE: 11px;'>(set up list of the categories in the format:  
<center><font style='color: #000099;'>catname1,catname2,catname3</font> </center>  where
<font style='color: #000077;'>catname1, ...</font> - are the short names of the categories)</font>
</div>

<p align='justify'>
Set up free (f) or membership (m) option for submitting new ads:<br> 
<select name='mbac_addad' class='formst'>
<option>".opt_qt($mbac_addad)."<checked><option>f<option>m</select>
&nbsp;
<font   class='fnt4'   OnClick=\"displ('adsbm');\">
Special Option
</font>
<DIV id='adsbm' style='DISPLAY: none'>
Set up  different  option:<br>
<select name='mbac_addad_vl' class='formst'>
<option>".opt_qt($mbac_addad_vl)."<checked><option>f<option>m</select>
<br> for the following categories:<br>
<input type='text' name='mbac_addad_ct' value='".opt_qt($mbac_addad_ct)."' class='formst' size='40'>
<br> 
<font style='FONT-SIZE: 11px;'>(set up list of the categories in the format:  
<center><font style='color: #000099;'>catname1,catname2,catname3</font> </center>  where
<font style='color: #000077;'>catname1, ...</font> - are the short names of the categories)</font>
</div>

<p align='justify'>
Use Joomla users' login to edit/delete ads: 
<br>(this option is active with the membership  option for submitting new ads)
<br><select name='lgneditd' class='formst'>
<option>".opt_qt($lgneditd)."<checked><option>yes<option>no</select>

<p align='justify'>
Set up free (f) or membership (m) option for sending privacy mails:<br>
<select name='mbac_sndml' class='formst'>
<option>".opt_qt($mbac_sndml)."<checked><option>f<option>m</select>
&nbsp;
<font   class='fnt4'   OnClick=\"displ('sndprml');\">
Special Option
</font>
<DIV id='sndprml' style='DISPLAY: none'>
Set up  different option:<br>
<select name='mbac_sndml_vl' class='formst'>
<option>".opt_qt($mbac_sndml_vl)."<checked><option>f<option>m</select>
<br> for the following categories:<br>
<input type='text' name='mbac_sndml_ct' value='".opt_qt($mbac_sndml_ct)."' class='formst' size='40'>
<br> 
<font style='FONT-SIZE: 11px;'>(set up list of the categories in the format:  
<center><font style='color: #000099;'>catname1,catname2,catname3</font> </center>  where
<font style='color: #000077;'>catname1, ...</font> - are the short names of the categories)</font>
</div>

</td><td valign='top' width='50%'>

</td></tr></table>


<p><table width='700'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp;Layout Headers/Footers
</td></tr></table>


<table width='700'    border='0' cellspacing='10' cellpadding='10'> 
<tr><td valign='top' width='50%'>

Set up html header   for pages generated by the script:<br>
<textarea rows='15' cols='90' name='ht_header' wrap=off class=formst>".opt_qt($ht_header)."</textarea>
 
<p align='justify'>
Set up html footer for pages generated by the script:<br>
<textarea rows='15' cols='90' name='ht_footer' wrap=off class=formst>".opt_qt($ht_footer)."</textarea>
 
<p align='justify'>
Set up message for users about membership log in and terms when they try 
  to go into membership areas:<br>
<textarea rows='15' cols='90' name='jloginfo' wrap=off class=formst>".opt_qt($jloginfo)."</textarea>
 
</td></tr></table>
<p>
<input type='submit' value='Save Options' class='formst1'>
</form>
 
";
 
}

function saveoptset()
{
global $opt_post, $conf_dir, $opt_header, $opt_footer;

$opt_post['photos_path']=checkendslash($opt_post['photos_path']);
$opt_post['photos_url']=checkendslash($opt_post['photos_url']);
$opt_post['urlclscrpt']=checkendslash($opt_post['urlclscrpt']);
 
$cfgstr=$cfgstr.save_opt('adm_passw');
$cfgstr=$cfgstr.save_opt('adm_email');
$cfgstr=$cfgstr.save_opt('usevrfcode');
$cfgstr=$cfgstr.save_opt('use_hdn_vrfc');
$cfgstr=$cfgstr.save_opt('plcntdtl');
$cfgstr=$cfgstr.save_opt('plcntpml');
$cfgstr=$cfgstr.save_opt('uselogsvmv');
$cfgstr=$cfgstr.save_opt('exprlogs');
$cfgstr=$cfgstr.save_opt('exp_period');
$cfgstr=$cfgstr.save_opt('adsonpage');
$cfgstr=$cfgstr.save_opt('maxttlsz');
$cfgstr=$cfgstr.save_opt('moderating');
$cfgstr=$cfgstr.save_opt('pradsdupl');
$cfgstr=$cfgstr.save_opt('sturlvar');
$cfgstr=$cfgstr.save_opt('actadoptv');
$cfgstr=$cfgstr.save_opt('cnfemtmpl'); 
$cfgstr=$cfgstr.save_opt('cnfemtsbj'); 
$cfgstr=$cfgstr.save_opt('use_ajax');
$cfgstr=$cfgstr.save_opt('jstoplads');
$cfgstr=$cfgstr.save_opt('jstphlads');
$cfgstr=$cfgstr.save_opt('tmltads');
$cfgstr=$cfgstr.save_opt('privacy_mail');
$cfgstr=$cfgstr.save_opt('pmailtp');
$cfgstr=$cfgstr.save_opt('sendcopytoadm');
$cfgstr=$cfgstr.save_opt('redirtoadm');
$cfgstr=$cfgstr.save_opt('sndexpmsg1');
$cfgstr=$cfgstr.save_opt('expemltmpl');
$cfgstr=$cfgstr.save_opt('expmsg_subj');  
$cfgstr=$cfgstr.save_opt('photos_count');
$cfgstr=$cfgstr.save_opt('phptomaxsize');
$cfgstr=$cfgstr.save_opt('incl_mtmdfile');
$cfgstr=$cfgstr.save_opt('mtmdfile_maxs');
$cfgstr=$cfgstr.save_arr('flattchext');
$cfgstr=$cfgstr.save_opt('plsflsrvrs');
$cfgstr=$cfgstr.save_opt('infldflsrv');
$cfgstr=$cfgstr.save_opt('maximgswith');
$cfgstr=$cfgstr.save_opt('resphdb');
$cfgstr=$cfgstr.save_opt('maxpixph');
$cfgstr=$cfgstr.save_opt('ppactv');
$cfgstr=$cfgstr.save_opt('mbac_sndml');
$cfgstr=$cfgstr.save_opt('mbac_addad');
$cfgstr=$cfgstr.save_opt('mbac_second');
$cfgstr=$cfgstr.save_opt('lgneditd');
$cfgstr=$cfgstr.save_opt('jsfrmch');
$cfgstr=$cfgstr.save_opt('schallusrads');

$cfgstr=$cfgstr.save_opt('kwsrchfrm');

$cfgstr=$cfgstr.save_opt('ch_nmusr');
$cfgstr=$cfgstr.save_opt('usrads_max');
$cfgstr=$cfgstr.save_opt('usrads_chcktime');
$cfgstr=$cfgstr.save_opt('ht_header'); 
$cfgstr=$cfgstr.save_opt('ht_footer');
$cfgstr=$cfgstr.save_opt('jloginfo');
$cfgstr=$cfgstr.save_opt('prphscnd');


$cfgstr="<?php
$cfgstr
?>";

savecfl($cfgstr, "conf_opt.php");
echo "
<p>&nbsp;<p><font class='fntmsg1'> Options have been saved successfully !</font>
<p>
 
";
}
?>
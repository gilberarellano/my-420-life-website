<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

$cspvar1="
";

$cspvar2="

<table width='600' cellspacing='5' cellpadding='5'>
<tr><td>
<p align='justify'>
 &nbsp; 
The list of  cities for one-level (only cities) location selection can be set up in the 
<a href='{$admconf_url}mdopt=ctfldform'>'Set Up Categories/Fields'</a> config area in the options 
for the field 'city'.  If you need to set up two-level fields selection for choosing 
country/city, you have to set up a special option 'Set up two-level for country/city' in the 
<nobr><a href='{$admconf_url}mdopt=ctfldform'>'Set Up Categories/Fields'</a></nobr> config area (at the bottom). 
If you set up this option, then the list of cities for one-level location selection will be deleted and 
if you go back to one-level location selection, you will need  to submit the list of cities again. 
</td></tr></table>
";

$cspvar3="
 
";

?>
<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

if (!defined('_JEXEC')){defined( '_VALID_MOS' ) or die( 'access denied' );}

global $html_header;
# Set up html header for  pages of admin interface
$html_header= "
<STYLE type=text/css>
BODY {  BACKGROUND: #ffffff;  margin: 20px; 
SCROLLBAR-FACE-COLOR: #9999cc; SCROLLBAR-HIGHLIGHT-COLOR: #eeeeff;
SCROLLBAR-SHADOW-COLOR: #000099; SCROLLBAR-3DLIGHT-COLOR: #000099; 
SCROLLBAR-ARROW-COLOR: #eeeeff; SCROLLBAR-TRACK-COLOR: #ddeeee; 
 text-align: left;}
a:link, a:visited {font-size: 12px; color: #0000ff; text-decoration: underline;  }
A:hover{COLOR: #aa0000; BACKGROUND-COLOR: #eeeeee; TEXT-DECORATION: none}
.formst
{margin-TOP: 2px;  
BORDER-RIGHT: #5555dd 0.1em solid; BORDER-TOP: #5555dd 0.1em solid;   BACKGROUND: #eeeeff; 
BORDER-LEFT: #5555dd 0.1em solid; COLOR: #000066; BORDER-BOTTOM: #5555dd 0.1em solid;
font-family:Verdana,Geneva,Arial; font-size: 12px;  
}
TD{FONT-FAMILY: ARIAL, HELVETICA, san-serif ; font-size: 12px; text-align: left; padding:2px;}
font{color: #000077; text-align: left;}
</style>
 
<div id=admcfgid1 style='text-align: left;'>
<font style='font-size: 12px;	font-weight: bold;'>
<b>
Almond Classifieds

<p> 
<table width='100%' bgcolor='#eeeeee' border=0 cellspacing=0 cellpadding=1>
<tr><td> 

<font FACE='ARIAL, HELVETICA' >
&nbsp; <b> Admin Interface </b> 
</font>
</td></tr></table>
<p>
</b></font> 
<p>

";

# Set up html footer  for pages of admin interface
$html_footer="
<p>
<table width='100%' bgcolor='#eeeeee' border='0' cellspacing='1' cellpadding='0'>
<tr><td>
<font FACE='ARIAL, HELVETICA' size='-2'>
 
Copyright &copy; 2012 <a href='http://www.almondsoft.com' style='FONT-SIZE: 11px;'>AlmondSoft.Com</a> All right reserved.

</font>
</td></tr>
</table>
</div>
";


require("{$compnt_path}funcs1.php");
if ($flattchext!=""){include("{$compnt_path}flattch.php");}
$msgs_fl="msg1_".$intlang.".php"; require("{$compnt_path}$msgs_fl");
 
require_once("{$compnt_path}keywrd.php");

$dstnem=$_REQUEST['dstnem'];

foreach ($categories as $key => $value){
if ($key!="evntcl" and  $key!="evcl_rpl")
{$ms22=split("_",$key); if ($ms22[0]== 'title'){$ttlvl=$categories[$key];} else
{$categories[$key][0]=$ttlvl.": ".$categories[$key][0];}}}

if ($md=="logout") {  logout();}

if (($HTTP_GET_VARS['ammlk'] !="") and ($admpassw1==""))
{$admpassw1=$HTTP_GET_VARS['ammlk'];
$admcookpassw=$HTTP_GET_VARS['ammlk'];
}

if ($md=="print_menu") 
{
if($admpassw1 != $adm_passw) {
if ($ammlk != $adm_passw){
$inc_passw="incorrect password !"; 
printpasswordform($inc_passw);
return;
}
}

setcookie("admcookpassw", $admpassw1);
print_menu();
return;
}

if($admcookpassw != $adm_passw){
if ($admcookpassw != ""){
$inc_passw= "Incorrect password or cookies does not work"; 
}
printpasswordform($inc_passw);
return;
}
 
if ($md=="") print_menu();
if ($md=="create_db") create_db();
if ($md=="create_tb") create_tb();
if ($md=="replcad") {replcad($_REQUEST['adid']);}
if ($md=="sql_tb") print_sql_tb();
if ($md=="moderate") {include("{$compnt_path}funcs2.php"); moderate(); }
if ($md=="adm_delete") {include("{$compnt_path}funcs2.php"); adm_delete();}
if ($md=="make_ads_visible") {make_ads_visible();}
if ($md=="make_ads_unvisible") {make_ads_unvisible();}
if ($md=="admrate_ads") admrate_ads();
if ($md=="getemails") getemails($ct);
if ($md=="spmwform") {include("{$compnt_path}spamgrd.php"); spmwform();}
if ($md=="spmwsave") {include("{$compnt_path}spamgrd.php"); spmwsave();}
if ($md=="spamadsdel") {include("{$compnt_path}funcs2.php"); include("{$compnt_path}spamgrd.php"); spamadsdel();}
if ($md=="submdeltad") {include("{$compnt_path}funcs2.php"); submdeltad();}
if ($md=="brdllist") { include("{$compnt_path}funcs2.php"); brdllist();}
if ($md=="editform") { editformredr($ed_id);}


function editformredr($ed_id)
{global $admcookpassw, $indadm_url;
$locvar=$indadm_url."md=editform&ed_id={$ed_id}";
setcookie("admcookpassw", $admcookpassw); header ("Location: $locvar");
return;
}


function replcad($adid)
{
global  $table_ads, $ct, $indadm_url;
 
$sql_query="update $table_ads set catname='$ct' where idnum=$adid";

if (@mysql_query("$sql_query")){

$message="
<font FACE='ARIAL, HELVETICA' size='-1'>
<b>
<a href='{$indadm_url}md=details&id=$adid'>Ad (id# $adid)</a> has been moved successfully !
</b></font>
";
}

else{
$message="
<font FACE='ARIAL, HELVETICA' size='-1'>
<b>
Incorrect query !
</b></font>
";
}
 
admin_message($message);
return;
}


function brdllist()
{
global $HTTP_POST_VARS, $table_ads, $categories, $html_footer, $html_header, 
$admconf_url, $indadm_url, $adm_passw;

 
echo"
$html_header <p>
<font FACE='ARIAL, HELVETICA' size='-1'>
<b><a href='$admconf_url'>Top:</a> &nbsp; &nbsp; Browse/Delete Ads List </b>
</font>
<p> 
";

$keywv=$HTTP_POST_VARS['keyw'];
if ($keywv!="")
{
$sql_query="select * from $table_ads where 
brief like '%{$keywv}%' or title like '%{$keywv}%' or email like '%{$keywv}%' or ipaddr1 like '%{$keywv}%' 
order by idnum desc limit ".$HTTP_POST_VARS['lads']." ";
}
else {
$sql_query="select * from $table_ads where visible=1
order by idnum desc limit ".$HTTP_POST_VARS['lads']." ";
}

$sql_res=mysql_query("$sql_query");

echo "
<form action='{$admconf_url}md=submdeltad' method='post'>
<font FACE='ARIAL, HELVETICA' size='-1' color='#000088'>
";

$i_id=0;
while ($row = mysql_fetch_array ($sql_res))
{

$i_id++;

$idnum=$row['idnum'];
$time1=$row['time'];
$date_posted=get_date($time1); 
$catname=$row['catname'];
if ($row['adphotos']=='yes'){$phvl1="<font color='#bb0000' size=-2>pic</font>";}else{$phvl1="";} 

echo "<p>
 
( <input type='checkbox' name='delad[]' value='$idnum' class='formst' id='$i_id'>delete  &nbsp; 
<a href='{$indadm_url}md=editform&ed_id=$idnum&ed_passw=".$row['passw']."' target='_blank'>edit</a> ) &nbsp 
<a href='{$indadm_url}md=details&id=$idnum' target='_blank'><b>".$row['title']."</a></b> $phvl1
&nbsp;-&nbsp; <font size='-2'><b> #$idnum, $date_posted; ".$categories[$catname][0].",
".$row['email'].", ".$row['ipaddr1']." ".$row['login'].".</b> </font>";  
}  
 
if ($i_id==0){echo "<p><font FACE='ARIAL, HELVETICA' size='-1' color='#000077'><b>No Ads</b></font><p> ";}
else{
echo "<p>
<script language='javascript'><!--
function selectchk(n){ for (i=1; i<=n; i++){ document.getElementById(i).checked=true; }}
function unselectchk(n){ for (i=1; i<=n; i++){ document.getElementById(i).checked=false; }}
--> </script>
<font  style='font-size: 12px;   color: #000077; TEXT-DECORATION: none;'> 
Deleting Ads:</font>
<font   style='font-size: 12px;   color: #0000ff; 
TEXT-DECORATION: underline; cursor: pointer;' OnClick='selectchk({$i_id});'>Check All</font> &nbsp; 
<font  style='font-size: 12px;   color: #0000ff; 
TEXT-DECORATION: underline; cursor: pointer;' OnClick='unselectchk({$i_id});'>Clear All</font> 

<p>
<input type='submit' value='Delete checked ads'>
";
}
echo "
</form> 
</font>
</body></html>
";
return;

}



function submdeltad()
{
global $HTTP_POST_VARS, $table_ads;
$array_id=get_array_id($list_id);

$admss=$HTTP_POST_VARS['submad'];
if ($admss!=""){
foreach ($admss  as $value)
{
$sql_query="update $table_ads set visible=1 where idnum=$value";
mysql_query("$sql_query");

global $kwsrchfrm;
if ($kwsrchfrm=="yes"){insert_kw_ad($value);}

}

}


$admss=$HTTP_POST_VARS['delad'];
if ($admss!=""){
foreach ($admss as $value)
{
delete_photos($value);
$sql_query="delete from $table_ads where idnum=$value";
mysql_query("$sql_query");
 
}
}

$message="
<font FACE='ARIAL, HELVETICA' size='-1'>
<b>
Query has been processed successfully !
</b></font>
"; 
admin_message($message);
return;
}
 
 
function getemails($ct)
{
global  $table_ads, $categories, $html_footer, $html_header, $dstnem, $admconf_url, $admconf_url,
$indadm_url;

$cattitle11=$categories[$ct][0];
if ($ct==""){$cattitle11="all";}
echo "
$html_header
";
echo "
<p>
<font FACE='ARIAL, HELVETICA' size='-1'>
<a href='$admconf_url'>Home:</a> <br>
<b> E-mail List. Category: <font color='#000099'>$cattitle11 </font>
 </b><p> ";
 if ($dstnem==""){
echo"
<font size='-2'><b>(Click on the link to see ad with this email)</b>
</font>
<p>
";
}
if ($dstnem==""){
echo "<a href='{$admconf_url}md=getemails&ct=$ct&dstnem=1'>
print list without duplicating e-mails</a><p>"; 
}
$sellct1="idnum, email";
if ($dstnem=="1"){ $sellct1="distinct email";}
$whrecat11="catname='$ct' and";
if ($ct==""){$whrecat11="";}
$sql_query="select $sellct1 from $table_ads where $whrecat11 visible=1
order by email";
$sql_res=mysql_query("$sql_query");
$email1112="0";
if ($dstnem==""){
while ($row = mysql_fetch_array ($sql_res))
{
$email1112="1";
if ($row['email']!=""){
echo "
<a href='{$indadm_url}ct=$ct&md=details&id=".$row['idnum']."'>
".$row['email']."</a> <br>
";
}
}
}
else
{

while ($row = mysql_fetch_array ($sql_res))
{
$email1112="1";
if ($row['email']!=""){
echo $row['email']."<br>";
}
}
}

if ($email1112=="0")
{echo "<p><font color='#aa0000'>No ads in this category</font>";}
echo "</font> <p>";
echo "
$html_footer
";
return;
}

function make_ads_visible()
{
global $list_id, $table_ads;
$array_id=get_array_id($list_id);
foreach ($array_id as $value)
{
$sql_query="update $table_ads set visible=1 where idnum=$value";
mysql_query("$sql_query");

global $kwsrchfrm;
if ($kwsrchfrm=="yes" and $value!=""){insert_kw_ad($value);}

}

$message="
<font FACE='ARIAL, HELVETICA' size='-1'>
<b>
Submitted ads have been replaced to the ads index
</b></font>
"; 
admin_message($message);
return;
}
 

function make_ads_unvisible()
{
global $list_id, $table_ads;
$array_id=get_array_id($list_id);
foreach ($array_id as $value)
{
$sql_query="update $table_ads set visible=2 where idnum=$value";
mysql_query("$sql_query");

global $kwsrchfrm;
if ($kwsrchfrm=="yes" and $value!=""){delete_kw_ad($value);}

}

$message="
<font FACE='ARIAL, HELVETICA' size='-1'>
<b>
Submitted ads have been replaced to the invisible folder
</b></font>
"; 
admin_message($message);
return;
}

function adm_delete()
{
global $list_id, $table_ads;
$array_id=get_array_id($list_id);
foreach ($array_id as $value)
{
delete_photos($value);
$sql_query="delete from $table_ads where idnum=$value";
mysql_query("$sql_query");
 
}
$message=
"<font FACE='ARIAL, HELVETICA'> <center>
<b> Ads have been deleted succesfully </b>
</center>
</font>
";
admin_message($message);
return;
}

function logout()
{
setcookie("admcookpassw", "0");
printpasswordform("Log out Ok !");
return;
} 

function get_moder_num($vis)
{
global $cat_fields, $table_ads,  $page, $adsonpage ; 
$sql_query="select count(idnum) from $table_ads where 
visible=$vis"; 
$sql_res=mysql_query("$sql_query");
$row=mysql_fetch_row($sql_res);
$count=$row[0];
return $count;
}

function moderate()
{
global $cat_fields, $table_ads,  $page, $adsonpage, $vis, $html_header,
$html_footer, $admconf_url; 

if ($vis=="0") $mode_text="new posted ads";
if ($vis=="2") $mode_text="univisible ads";
if ($vis=="3") $mode_text="ads edited by users";

 
$count=get_moder_num($vis);
echo $html_header;
echo"
<table width='700' >
<tr><td>
<font FACE='ARIAL, HELVETICA' size='-1'>
<b><a href='$admconf_url'>Top:</b>
</font>
</td></tr></table>
<table width='700' bgcolor='#dddddd' >
<tr><td>
 
<font FACE='ARIAL, HELVETICA'>
<b>Browsing $mode_text</b>
 
</td></tr></table>
";
echo"
 <table width='700' bgcolor='#eeeeee'>
<tr><td>
<font FACE='ARIAL, HELVETICA' size='-1'>
Pages:".moder_pages_list($count)."
</td></tr></table>
<p>
";

echo get_moder_ads();
echo $html_footer;
return;
}

 
function get_moder_ads()
{
global $cat_fields, $gener_fields, $specific_fields, 
$table_ads, $ct, $page,$adsonpage, $vis, $ads_fields, $admconf_url, $indadm_url; 

$html_ads="";
$start_num=($page-1)*$adsonpage; 
if($page=="")$start_num=0;
$sql_query="select * from $table_ads where visible=$vis
order by idnum desc limit $start_num, $adsonpage";
 $sql_res=mysql_query("$sql_query");
$html_ads=$html_ads."<p>";

$html_ads=$html_ads."
<form action='{$admconf_url}md=submdeltad' method='post'>
";

$key22="";

while ($row = mysql_fetch_array ($sql_res))
{$key22="1";
$html_ads=$html_ads.print_moder_ad($row);
}
 
if ($key22==""){$html_ads=$html_ads."<p><font FACE='ARIAL, HELVETICA' size=-1>No ads in this folder</font><p>";}

$html_ads=$html_ads."
<input type='submit' value='Submit' class='formst'></form> 
";
return $html_ads;
}

function print_moder_ad ($row)
{
global $cat_fields, $photos_count, $all_fields, $photo_path,$photo_url,
$previewphoto_path,$prphotolimits,$pr_lim_width,$pr_lim_height,
$previewphoto_url,  $select_text, $real_format, $ads_fields, $categories, $indadm_url,
 $adm_passw, $jm_cmpath;

$html_ad="
<table width='700' bgcolor='#eeeeee'><tr><td>
<font FACE='ARIAL, HELVETICA' size=-1>
";
$idnum=$row['idnum'];

$time1=$row['time'];
$date_posted=get_date($time1); 
$catname=$row['catname'];
if ($row['login']!="")
{
$html_ad=$html_ad."member:  
<a href='{$indadm_url}md=browse&mblogin=".$row['login']."&visunvis=1'><b>
".$row['login']."</b></a><br>";
}
$html_ad=$html_ad."Category: <b>".$categories[$catname][0]."</b><br>";
$html_ad=$html_ad."id: $idnum; Date posted: $date_posted<br>";
foreach ($ads_fields as $key => $value) 
{
if (($row[$key]!="") and
($row[$key]!=$select_text) and ($row[$key]!='http://')
and ($row[$key]!='NULL') and ($row[$key]!='null'))
{
if ($all_fields[$key][6]=='real') {$row[$key]=sprintf($real_format, $row[$key]);}
$html_ad=$html_ad."
<font color='#000099'>".$ads_fields[$key][0]."</font> : $row[$key] <br>
";
}
}

get_jpg_path($idnum);

for($i=1; $i<=$photos_count; $i++)
{
 if (file_exists($photo_path[$i])){
$html_ad=$html_ad."&nbsp; <a href='../$photo_url[$i]'><img src='../{$jm_cmpath}sph.php?id=$idnum&wd=60&np=$i' border=0></a>";
}
}
 

$html_ad=$html_ad.print_multimed1($idnum,$row);

$html_ad=$html_ad."<br>
<a href='{$indadm_url}md=editform&ed_id=$idnum&ed_passw=".$row['passw']."' target='edit$idnum'>
<font size='-2'><b>Edit this ad </a>
&nbsp;&nbsp; 
Submit ad<input type='checkbox' name='submad[]' value='$idnum' class='formst'>
&nbsp;&nbsp; 
Delete ad<input type='checkbox' name='delad[]' value='$idnum' class='formst'>
</b></font>
</td></tr></table><p>";
return $html_ad;
}
 
function print_multimed1($idnum,$row)
{
global $incl_mtmdfile,$multimedia_path, $multimedia_url, $multim_link, $msg;
$mm_link="";  
if ($incl_mtmdfile=='yes')
{
get_att_path($idnum);

if (file_exists($multimedia_path)) 
{
$attflsz1=round(filesize($multimedia_path)/1000);
if ($attflsz1==0){$attflsz1=1;}
$mm_link="
<p><li>".$msg['multimedia_file'].": 
<a href='../{$jm_cmpath}$multimedia_url'><b>".$row['attfltl']." (".$row['attflext'].", ".$attflsz1."Kb)</b></a></li>
";
 
}

if (($row['flsrvur1']!="") and ($row['flsrvur1']!="http://")){
$mm_link=$mm_link."<p><b>
<li><a href='".$row['flsrvur1']."' target='_blank'>".$row['flsrvtl1']."</a></li>
</b>";
}

if (($row['flsrvur2']!="") and ($row['flsrvur2']!="http://")){
$mm_link=$mm_link."<p><b>
<li><a href='".$row['flsrvur2']."' target='_blank'>".$row['flsrvtl2']."</a></li>
</b>";
}
if ($row['embdcod']!=""){
$embdcode=ereg_replace('<', '&#060;', $row['embdcod']);
$embdcode=ereg_replace('>', '&#062;', $embdcode);
$mm_link=$mm_link."<p> Embeded code: $embdcode" ;
}
}

global $dsplhltpht;
if (($dsplhltpht=="yes") and (($row['adrate'] < 1) or ($row['adrate']==""))){$mm_link="";}
 
return  $mm_link;
}


function moder_pages_list($ads_count)
{
global $adsonpage, $ct, $page, $vis, $admconf_url;

$num_pages=($ads_count-$ads_count%$adsonpage)/$adsonpage;
if ($ads_count%$adsonpage > 0) {$num_pages++;}
$list_pages="";
for ($i = 1; $i <= $num_pages; $i++) 
{
if ($i != $page){
$list_pages=$list_pages."[<a href='{$admconf_url}md=moderate&vis=$vis&page=$i'>$i</a>]";
}
else
{
$list_pages=$list_pages."[$i]";
}
}
return $list_pages;
}

function  printpasswordform($inc_passw)
{
global $html_header, $html_footer, $admconf_url;

echo $html_header; 
echo "

<p>
<font FACE='ARIAL, HELVETICA' COLOR='#990000'  >
<b> $inc_passw </b>
</font>
<p>
<form action='{$admconf_url}md=print_menu' method='post'>
<font FACE='ARIAL, HELVETICA' COLOR='#000099' size='-1'>
<b>
Input admin password : </b></font>
<input type='password' name='admpassw1' class='formst'>
<input type='submit' value='Submit' class='formst'>
</form>
<font FACE='ARIAL, HELVETICA' COLOR='#000099' size='-2'>
<br><b>(Cookies must be set up. Initial password: 'adm' )
 </b></font>
 
<p>
</center>
";
echo $html_footer;
return;
}


function print_menu()
{
global $html_header, $html_footer, $db_name, $table_ads, 
$moderating, $categories, $use_spmg, $admconf_url, $indadm_url, $adm_passw;


echo $html_header;
 
echo "
<a href='index.php?option=com_aclsfstd&task=config'>Configuration</a><p>
<table width='950'  border=0 cellspacing=0 cellpadding=1>
<tr><td width='50%' valign='top'>

<table width='450' bgcolor='#dddddd' border=0 cellspacing=0 cellpadding=1>
<tr><td> 
<font FACE='ARIAL, HELVETICA' size=-1>
<b>Browse/Delete Ads List</b>
</font></td></tr><tr><td>
<TABLE   bgcolor='#ffffff' border='0' cellspacing=0 cellpadding=0 WIDTH='100%' >
<tr><td> 

<form action='{$admconf_url}md=brdllist' method='post' target='_blank'>
&nbsp; 
  Keyword:<input type='text' name='keyw' class='formst' > &nbsp; 
  last: <input type='text' name='lads' value='100' size='5' class='formst' >ads 
&nbsp; <input type='submit' value='Submit' class='formst'>
<br><font FACE='ARIAL, HELVETICA' size=-2>
Keyword will be searched in the titles, descriptions, e-mails and IP addresses.
<br> When   keyword is empty then  last posted ads will be dispayed.
</font>
</form>

</td></tr></table>
</td></tr></table>
<p>
 
<table width='450' bgcolor='#dddddd' border=0 cellspacing=0 cellpadding=1>
<tr><td> 
<font FACE='ARIAL, HELVETICA' size=-1>
<b>Browse Moderated Folders</b>
</font></td></tr><tr><td>
<TABLE   bgcolor='#ffffff' border='0' cellspacing=0 cellpadding=0 WIDTH='100%' >
<tr><td> 
<font FACE='ARIAL, HELVETICA' size='-1' color='#000088'>
<span style='line-height: 1.8;'>
<a href='{$admconf_url}md=moderate&vis=0'  >New Posted Ads</a> (".get_moder_num("0").")
<br><a href='{$admconf_url}md=moderate&vis=3'  >Ads Edited by Users</a> (".get_moder_num("3").")
<br><a href='{$admconf_url}md=moderate&vis=2'  >Invisible Ads</a> (".get_moder_num("2").")  
</span>
</font>
</td></tr></table>
</td></tr></table>
<p>

<table width='450' bgcolor='#dddddd' border=0 cellspacing=0 cellpadding=1>
<tr><td> 
<font FACE='ARIAL, HELVETICA' size=-1>
<b>Make Ads Visible/Invisible</b>
</font></td></tr><tr><td>
<TABLE   bgcolor='#ffffff' border='0' cellspacing=0 cellpadding=0 WIDTH='100%' >
<tr><td> 
<table><tr><td>
<form action='{$admconf_url}md=make_ads_visible' method='post'>
 
  Ads ID#:
 <input type='text' name='list_id' class='formst'>
</td><td>
<input type='submit' value='Make Ads Visible' class='formst'>
</form>
</td></tr><tr><td> 
<form action='{$admconf_url}md=make_ads_unvisible' method='post'>
 
  Ads ID#: <input type='text' name='list_id' class='formst'>
</td><td>
<input type='submit' value='Make Ads Invisible' class='formst'>
</td></tr></table>
</form>

<font FACE='ARIAL, HELVETICA' size=-2> 
separate by comma, e.g. if you input 12,22,34-37,40  then ads with the following id will be
submitted: 12,22,34,35,36,37,40 
</font>
</center>
 

</td></tr></table>
</td></tr></table>

</td><td width='50%' valign='top'>

<table width='450' bgcolor='#dddddd' border=0 cellspacing=0 cellpadding=1>
<tr><td> 
<font FACE='ARIAL, HELVETICA' size=-1>
<b>Delete Ads</b>
</font></td></tr><tr><td>
<TABLE   bgcolor='#ffffff' border='0' cellspacing=0 cellpadding=0 WIDTH='100%' >
<tr><td> 

<form action='{$admconf_url}md=adm_delete' method='post'>
 
  Ads ID#: <input type='text' name='list_id' class='formst'>
<input type='submit' value='Delete' class='formst'>
 
<p><font FACE='ARIAL, HELVETICA' size=-2> 
Separate by comma, e.g. if you input 12,22,34-37,40  then ads with the following id will be
deleted: 12,22,34,35,36,37,40 
 </font>
 
</form>

</td></tr></table>
</td></tr></table>

<p>
<table width='450' bgcolor='#dddddd' border=0 cellspacing=0 cellpadding=1>
<tr><td> 
<font FACE='ARIAL, HELVETICA' size=-1>
<b>Edit Ad</b>
</font></td></tr><tr><td>
<TABLE   bgcolor='#ffffff' border='0' cellspacing=0 cellpadding=0 WIDTH='100%' >
<tr><td>  
<form action='{$indadm_url}md=editform' method='post' target='_blank'>
 Ad ID#: <input type='text' name='ed_id' class='formst' size=5>
<input type='hidden' name='ed_passw' value='$adm_passw' class='formst'>
<input type='submit' value='Edit' class='formst'>
 
</form>
</td></tr></table>
</td></tr></table>

 
<p>
<table width='450' bgcolor='#dddddd' border=0 cellspacing=0 cellpadding=1>
<tr><td> 
<font FACE='ARIAL, HELVETICA' size=-1>
<b>Move Ad to other category</b>
</font></td></tr><tr><td>
<TABLE   bgcolor='#ffffff' border='0' cellspacing=0 cellpadding=0 WIDTH='100%' >
<tr><td> 
<form action='{$admconf_url}md=replcad&' method='post'>
 
Ad with ID:
<input type='text' name='adid' size='3' class='formst'>
move to: <br>
<select name='ct' size=1  class='formst'>
<option value='' selected>Choose category
";
foreach ($categories as $key => $value)
{
$aa2=split("_",$key);
if($aa2[0] != 'title'){
echo "<option value='$key'>".$categories[$key][0];
}
}
echo "
</select>
<input type='submit' value='Submit' class='formst'>
</center>
</form>
</td></tr></table>
</td></tr></table>


<p>
<table width='450' bgcolor='#dddddd' border=0 cellspacing=0 cellpadding=1>
<tr><td> 
<font FACE='ARIAL, HELVETICA' size=-1>
<b>Print E-Mail Lists</b>
</font></td></tr><tr><td>
<TABLE   bgcolor='#ffffff' border='0' cellspacing=0 cellpadding=0 WIDTH='100%' >
<tr><td> 

<form action='{$admconf_url}md=getemails&' method='post' >
<input type='hidden' name='md' value='getemails'>
<select name='ct' size=1 class='formst'>
<option value='' selected>All Categories
";
foreach ($categories as $key => $value)
{
$aa2=split("_",$key);
if($aa2[0] != 'title'){
echo "<option value='$key'>".$categories[$key][0];
}
}
echo "
</select>
<input type='submit' value='Submit' class='formst'>
</center>
</form>
 
</td></tr></table>
</td></tr></table>

</td></tr></table>
 
<p>
<font FACE='ARIAL, HELVETICA' size=-1>
<a href='{$admconf_url}md=logout'>Log Out</a>
</font>
<p>";
echo $html_footer;
return;
}

function print_cat_name2($key)
{
global $categories;
echo "
<nobr><a href='{$admconf_url}ct=$key&md=getemails'>".$categories[$key][0]."</a></nobr>&nbsp;&nbsp;
"; 
}

function get_array_id($string_id)
{
$i=0;
$string1=split(",",$string_id);
foreach ($string1 as $value1)
{
$string2=split("-", $value1);
if ($string2[1]!="")
{
for ($j=$string2[0];$j<=$string2[1];$j++)
{$array_id[$i]=$j; $i++;}
}
else{$array_id[$i]=$value1; $i++;}
}
return $array_id;
}

 
function admin_message($message)
{
global $cat_fields, $photos_count, $html_header, $html_footer, $id,
$ct, $categories, $ad_second_width, $left_width_sp, $exp_period, $admconf_url;
echo $html_header;
echo "
<center><table width='600' style='text-align: left;'><tr><td>
<font FACE='ARIAL, HELVETICA' COLOR='BLACK'> <font size=-1>
 <a href='$admconf_url'><b>Top:</b></a> </font>
&nbsp; 
<hr size='1'><p>
$message
<p>
</tr></td></table>
</center>
";
echo $html_footer;
return;
}
 
?>
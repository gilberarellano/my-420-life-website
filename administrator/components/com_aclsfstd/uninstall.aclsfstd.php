<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function com_uninstall()
{ 
global $mainframe, $mosConfig_dbprefix, $dbprefix;

# name of database table for ads:
$table_ads1="aclsfstdt";
# name of mysql table for logs
$table_logs1="aclsfstdlg";

if (defined('_JEXEC')){
$jmcfg_var = new JConfig;
$jmldbprfx=$jmcfg_var->dbprefix;}
else { $jmldbprfx=$mosConfig_dbprefix;}

global $mosConfig_db;
if($mosConfig_db==""){
$jmvar5 = new JConfig;
$jhost_name5=$jmvar5->host;
$jdb_user5=$jmvar5->user;
$jdb_password5=$jmvar5->password;
mysql_connect("$jhost_name5","$jdb_user5","$jdb_password5"); 
$jdb_name5=$jmvar5->db;
mysql_select_db("$jdb_name5");
}

$table_ads=$jmldbprfx.$table_ads1;
$table_logs=$jmldbprfx.$table_logs1;

$sqlq="DROP TABLE IF EXISTS  $table_ads ";
mysql_query("$sqlq");
$sqlq="DROP TABLE IF EXISTS  $table_logs ";
mysql_query("$sqlq");


echo "
<font style='FONT-WEIGHT: bold; color: #000099; FONT-SIZE: 16px;'>
The script Almond Classifieds has been uninstalled successfully !
</font>
";
}
?>
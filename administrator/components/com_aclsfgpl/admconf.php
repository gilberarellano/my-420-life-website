<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

if (!defined('_JEXEC')){defined( '_VALID_MOS' ) or die( 'access denied' );}
 

global $_REQUEST, $dlmtr2, $opt_post, $admconf_url, $opt_header, $conf_dir, $opt_footer,  $intlang;

$intlang='eng';
$msgs_fl="msg1_".$intlang.".php"; include_once("{$compnt_path}$msgs_fl");
$msgs_fl="msg2_".$intlang.".php"; include_once("{$compnt_path}$msgs_fl");

require("{$compnt_path}funcs1.php");

$conf_dir=$compnt_path."conf/";

$opt_header="
<STYLE type=text/css>
BODY {  BACKGROUND: #ffffff;
SCROLLBAR-FACE-COLOR: #9999cc; SCROLLBAR-HIGHLIGHT-COLOR: #eeeeff;
SCROLLBAR-SHADOW-COLOR: #000099; SCROLLBAR-3DLIGHT-COLOR: #000099; 
SCROLLBAR-ARROW-COLOR: #eeeeff; SCROLLBAR-TRACK-COLOR: #ddeeee; 
 FONT-FAMILY: ARIAL, HELVETICA, san-serif ; font-size: 12px;  text-align: left;
}
a:link, a:visited {color: #0000ff; text-decoration: underline;  }
 
A:hover{COLOR: #aa0000; BACKGROUND-COLOR: #eeeeee; TEXT-DECORATION: none}
FONT {FONT-FAMILY: ARIAL, HELVETICA, san-serif ; COLOR: #000077; font-size: 12px; text-align: left;} 
TD{FONT-FAMILY: ARIAL, HELVETICA, san-serif ; font-size: 12px; text-align: left;}
.formst
{margin-TOP: 2px;  
BORDER-RIGHT: #5555dd 0.1em solid; BORDER-TOP: #5555dd 0.1em solid;   BACKGROUND: #eeeeff; 
BORDER-LEFT: #5555dd 0.1em solid; COLOR: #000077; BORDER-BOTTOM: #5555dd 0.1em solid;
font-family:Verdana,Geneva,Arial; font-size: 13px;  
}
table{text-align: left;}
.formst1
{margin-TOP: 2px;  
BORDER-RIGHT: #5555dd 0.1em solid; BORDER-TOP: #5555dd 0.1em solid;   BACKGROUND: #cccccc; 
BORDER-LEFT: #5555dd 0.1em solid; COLOR: #000066; BORDER-BOTTOM: #5555dd 0.1em solid;
font-family:Verdana,Geneva,Arial; font-size: 14px; font-weight: bold;
}
.fnts1 {COLOR: #000000; font-size: 11px;}
.fnt10 {font-size: 12px;  color: #000099;  font-weight: bold;}
.fnt2 {font-size: 13px;  color: #000099;  font-weight: bold;}
.fnt2p {font-size: 14px;  color: #000099;   }
.fnt21d {font-size: 13px;  color: #0000ee; TEXT-DECORATION: underline; cursor: pointer; font-weight: bold  }
.fnt2m {font-size: 13px;  color: #555599;  font-weight: bold}
.fnt3 {font-size: 11px;  color: #000099;}
.fnt31 {font-size: 12px;  color: #333399;}
.fntmsg1 {font-size: 14px;  color: #009999; font-weight: bold; }

.fntmsg2 {font-size: 14px;  color: #990000; font-weight: bold; }

.frmft1 { background: #777799; color: #ffffff; FONT-SIZE: 16px;  FONT-WEIGHT: bold;}
.fnt4 {font-size: 12px;  color: #0000ee; TEXT-DECORATION: underline; cursor: pointer;}
 .tbl1 { BORDER-RIGHT: #5555dd 1px solid; BORDER-TOP: #5555dd 1px solid;  
BORDER-LEFT: #5555dd 1px solid; BORDER-BOTTOM: #5555dd 1px solid; }
</style>
<div id=admcfgid1 style='text-align: left;'>
<font class=fnt10>
Setting Up Almond Classifieds Options 
</font>
<TABLE BORDER=0 WIDTH='100%'   cellspacing=0 cellpadding=0 >
<TR><td  style='BORDER-BOTTOM: #777799 2px solid;' cellspacing=0 cellpadding=0  >&nbsp;</td></tr></table>
<p>
<script language='JavaScript'>
<!--
 function displ(nmdiv){if (document.getElementById){
if (document.getElementById(nmdiv).style.display=='none') document.getElementById(nmdiv).style.display ='block';
else document.getElementById(nmdiv).style.display ='none';} else if (document.all)
{ if(document.all[nmdiv].style.display=='none') document.all[nmdiv].style.display ='block';
else document.all[nmdiv].style.display ='none';}}
-->
</script>
 
";

$opt_footer ="
</div>
<p> 
<TABLE BORDER=0 WIDTH='100%'   cellspacing=0 cellpadding=0 >
<TR><td  style='BORDER-BOTTOM: #777799 2px solid;' cellspacing=0 cellpadding=0  >&nbsp;</td></tr></table>
<font class='fnt3'>Copyright   2011 <a href='http://www.almondsoft.com'>AlmondSoft.Com</a> All rights reserved.
</font>
";

$dlmtr="|"; $dlmtr2="\|";

require("{$admcomp_path}cfgincms.php");
require("{$admcomp_path}cfginc1.php");
require("{$admcomp_path}cfginc2.php");
require("{$admcomp_path}cfginc5.php");

if ($_REQUEST['mdopt']=='ctrcform'){$slctcntr="yes";}
 

 
$opt_post=$_REQUEST;
if(get_magic_quotes_gpc()){ foreach ($_REQUEST as $key => $value ) 
{$opt_post[$key]=stripslashes($opt_post[$key]);}}
 
####### check adm password

if ($_REQUEST['mdopt']=="logout") {  logout(); return;}
if ($_REQUEST['mdopt']=="print_menu") 
{
if($admpassw1 != $adm_passw){
$inc_passw="incorrect password !"; 
printpasswordform($inc_passw);
return; 
}
setcookie("admcookpassw", $admpassw1);
echo $opt_header;
 
print_menu();
echo $opt_footer;
return;
}
if($admcookpassw != $adm_passw){
if ($admcookpassw != ""){
$inc_passw= "Incorrect password or cookies does not work"; 
}
printpasswordform($inc_passw);
return;
}


echo $opt_header;

if ($_REQUEST['mdopt']=="saveopts"){saveoptset(); $opt_file="conf_opt.php";include("{$compnt_path}cfgset.php");}



# create mysql tables

$md=$opt_post['md'];
 
if ($md=="mncrtb") {print_menu_crtb();}
if ($md=="create_db") {create_db();}
if ($md=="create_tb") {create_tb();}
if ($md=="sql_tb") {print_sql_tb();}


###########################

 

if ($_REQUEST['mdopt']==""){print_menu();  }
if ($_REQUEST['mdopt']=="print_menu"){print_menu();  }
if ($_REQUEST['mdopt']=="delopt"){del_opt(); print_menu();  }

 

if ($_REQUEST['mdopt']=="optform"){opt_form(); }
if ($_REQUEST['mdopt']=="saveopts"){print_menu();}

if ($_REQUEST['mdopt']=="ctfldform"){ctfld_form();  }
if ($_REQUEST['mdopt']=="savectfld"){savectfld(); print_menu();}

if ($_REQUEST['mdopt']=="ctrcform"){ctrc_form();  }
if ($_REQUEST['mdopt']=="savectrc"){savectrc(); print_menu();}

if ($_REQUEST['mdopt']=="mbrform"){include("{$compnt_path}mb_conf.php"); mbr_form();  }
if ($_REQUEST['mdopt']=="savembr"){savembr(); print_menu();}

if ($_REQUEST['mdopt']=="msgform1"){include("{$compnt_path}msg1_eng.php"); msgf_form("1");  }
if ($_REQUEST['mdopt']=="savemsg1"){ savemsgf("conf_m1.php", "1"); print_menu();}
if ($_REQUEST['mdopt']=="msgform2"){include("{$compnt_path}msg2_eng.php"); msgf_form("2");  }
if ($_REQUEST['mdopt']=="savemsg2"){ savemsgf("conf_m2.php", "2"); print_menu();}


echo $opt_footer;




function  printpasswordform($inc_passw)
{
global  $admconf_url, $opt_header, $opt_footer;

  
echo " 
  $opt_header 
<font class='fntmsg2' >
  $inc_passw  
</font>
<p>  
<form action=\"{$admconf_url}mdopt=print_menu\" method='post'>
<font class='fnt2p'>
Input admin password :</font>
<input type='text' name='admpassw1' class='formst'>
<input type='submit' value='Submit' class='formst1'>
</form>
<font class='fnt3'>
<br>(Cookies must be set up. Initial password: 'adm')
</font>
$opt_footer
";
 
}

function print_menu()
{
global $cspvar1, $cspvar3, $admconf_url ;
echo "
<table width='700'    border='0' cellspacing='10' cellpadding='10'> 
<tr><td valign='top' >
<p>
<font class='fnt2'><a href='{$admconf_url}mdopt=optform' >Set Up Main Options</a></font>
&nbsp;&nbsp;&nbsp; <font class='fnt3'><a href='{$admconf_url}mdopt=delopt&fl=conf_opt.php' >(Reset)</a></font>
<p>
<font class='fnt2'><a href='{$admconf_url}mdopt=ctfldform'  >Set Up Categories/Fields</a></font>
&nbsp;&nbsp;&nbsp; <font class='fnt3'><a href='{$admconf_url}mdopt=delopt&fl=conf_cat.php' >(Reset)</a></font>
<p>
<font   class='fnt21d'   OnClick=\"displ('trmsg1');\">
Translate Messages  for User's interface<br>
</font>
<DIV id='trmsg1' style='DISPLAY: none'><p>
<font class='fnt2m'> 
<a href='{$admconf_url}mdopt=msgform1'>Message set 1</a></font>&nbsp;&nbsp;&nbsp;<font class='fnt3'><a href='{$admconf_url}mdopt=delopt&fl=conf_m1.php' >(Reset)</a></font>
<br><font class='fnt2m'><a href='{$admconf_url}mdopt=msgform2'>Message set 2</a></font>
&nbsp;&nbsp;&nbsp; <font class='fnt3'><a href='{$admconf_url}mdopt=delopt&fl=conf_m2.php' >(Reset)</a></font>
</font>
</div>
<p><font class='fnt2'>
<a href='index.php?option=com_aclsfgpl&task=admin'>Admin Panel</a>
</font>

<p><font class='fnt2'>
<a href='../index.php?option=com_aclsfgpl&'>View Classifieds Front End</a>
</font>

<p>
$cspvar3

</td><td valign='top' width='300'>
 
<font class='fnt31'>

$cspvar1

<p align='justify'>
&nbsp;&nbsp; To delete custom config settings and set up initial default 
options click link 'Reset'.
</font>
</td></tr></table>
<p>
<font class='fnt3'><a href='{$admconf_url}mdopt=logout'>Log Out</a></font>
";
}

function logout()
{
setcookie("admcookpassw", "0");
printpasswordform($inc_passw);
return;
} 

function del_opt()
{global $conf_dir, $_REQUEST;

$dl_fl=$conf_dir.$_REQUEST['fl'];
if (file_exists($dl_fl)){unlink($dl_fl);} 
echo "
<p>&nbsp;<p><font class='fntmsg1'> Config file ".$_REQUEST['fl']." with customized options has been deleted successfully !</font>
<p>
 
";
}

function checkendslash($strv)
{
$strv=ereg_replace(" ","", $strv);
if ($strv!=""){
preg_match("/(.)$/", $strv, $matches);
if ($matches[1]!="/"){$strv=$strv."/";}
}
else {$strv="";}

return $strv;
}

function prn_hash($var_hsh)
{
global $dlmtr;

$vr_str="";
foreach ($var_hsh as $key => $value )
{
 
$var_t="
";
$value=ereg_replace ($var_t , "", $value);

$vr_str=$vr_str.$key.$dlmtr.$value."
";
}

$vr_str=opt_qt($vr_str);

return $vr_str;
}

function save_hash($var_hsh)
{ 
global $opt_post, $dlmtr2;
 
$ereg_vl="
";
$ctg_mss=split($ereg_vl, $opt_post[$var_hsh] );

$res="
$"."$var_hsh= array(
";

foreach ($ctg_mss as  $value )
{
$value = preg_replace ("/^\s+/", "", $value);
$value = preg_replace ("/\s+$/", "", $value);

 
$ctlnms=split($dlmtr2, $value);

if ($ctlnms[0]!=""){
$ctlnms[0]=ereg_replace (" ", "", $ctlnms[0]);

$res=$res." \"".check_opt($ctlnms[0])."\" => \"".check_opt($ctlnms[1])."\", 
";
}
}

$res = preg_replace ("/,\s+$/", "", $res);

$res=$res."
);
";
return $res;
}

function save_opt2($opt_name)
{
global $opt_post;

$opt_post[$opt_name]=ereg_replace(" ", "", $opt_post[$opt_name]);

$res1=save_opt($opt_name);

return $res1;
}

function save_opt($opt_name)
{

global $opt_post;
$opt_indvar="";
$opt_vl=$opt_post[$opt_name];

$opt_rslt="$".$opt_name."=\"".check_opt($opt_vl)."\";
";

return $opt_rslt;
}

function check_opt($opt_val)
{
$opt_val=ereg_replace('"', '\\"', $opt_val);
$opt_val=ereg_replace('&#039;', '\'', $opt_val);

$opt_val = preg_replace ("/^\s+/", "", $opt_val);
$opt_val = preg_replace ("/\s+$/", "", $opt_val);

return $opt_val;
}


function save_arr($opt_name)
{

global $opt_post;
$opt_vl=$opt_post[$opt_name];

$opt_rslt="$".$opt_name."=array( ";

$opt_arr=split(",",$opt_vl);
foreach ($opt_arr as $value){
$value=ereg_replace("'","", $value);
$opt_rslt=$opt_rslt."'".check_opt($value)."', ";
}

$opt_rslt = preg_replace ("/,\s+$/", "", $opt_rslt);

$opt_rslt =$opt_rslt.");
";

return $opt_rslt;
}

function opt_arr($optval)
{
 
foreach ($optval as $value)
{  
$res=$res."'".opt_qt($value)."', ";
}
$res = preg_replace ("/,\s+$/", "", $res);

 
return $res;
}

function opt_qt($optval)
{
$optval=ereg_replace('\'', '&#039;', $optval);
$optval=ereg_replace('&nbsp;', '&#038;nbsp;', $optval);
return $optval;
}

function savecfl($cstrvar, $flname)
{
global $conf_dir, $opt_header, $opt_footer;



$cf_file=$conf_dir."tmp.php";

if(!($flnm=@fopen("$cf_file","w"))){flsverr("tmp.php");}

fwrite($flnm, $cstrvar);
fclose($flnm);

include($cf_file);

unlink($cf_file);

 $cf_file=$conf_dir.$flname; 
 
if (!($flnm=fopen("$cf_file","w"))){flsverr($cf_file);}

fwrite($flnm, $cstrvar);
fclose($flnm);
 
}

function flsverr($cf_file)
{
global $conf_dir; 
echo "
   
<font class='fntmsg2'>

Script cannot modify config  file '$cf_file'. <br>

</font>

<font class='fntmsg1'>
Create the directory '$conf_dir' if it is not created or 
<br> try to set up permission 777 for directory '$conf_dir' and file '$cf_file'. 
</font>

$opt_footer; 
";

}
?>
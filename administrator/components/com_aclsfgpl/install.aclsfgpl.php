<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function com_install()
{
 
global  $mainframe, $mosConfig_absolute_path, $mosConfig_dbprefix,  $table_ads, $table_logs, $jmlconfpath;

if (defined('JPATH_ROOT')){$jmlconfpath=JPATH_ROOT;}else{$jmlconfpath=$mosConfig_absolute_path;}

$comp_pathvl=$jmlconfpath."/components/com_aclsfgpl/";
$config_path="{$comp_pathvl}config.php";
include($config_path); require("{$comp_pathvl}funcs1.php"); prnlg();

global $mosConfig_db;
if($mosConfig_db==""){
$jmvar5 = new JConfig;
$jhost_name5=$jmvar5->host;
$jdb_user5=$jmvar5->user;
$jdb_password5=$jmvar5->password;
mysql_connect("$jhost_name5","$jdb_user5","$jdb_password5"); 
$jdb_name5=$jmvar5->db;
mysql_select_db("$jdb_name5");
}


$sql=sql_create_tb();

$sql_rs1=mysql_query("$sql");
if (!($sql_rs1)){
echo "
<font style='FONT-WEIGHT: bold; color: #990000; FONT-SIZE: 16px;'> 
Error in creating   MySQL table '$table_ads'. 
";
} else {
echo "
<font style='FONT-WEIGHT: bold; color: #000099; FONT-SIZE: 16px;' >
The script Almond Classifieds has been installed successfully ! <br>
<a href='../index.php?option=com_aclsfgpl'>Click here</a> to view  classifieds front end.
</font>
";
}

$sqlcrtblog="CREATE TABLE IF NOT EXISTS $table_logs (ltime integer, ltype char(5), lip char(20), lidnum integer ) ";
mysql_query("$sqlcrtblog");

$kwfldsrch="title, brief, moredetails, city, country, company, jobtype, goal, name";
$sql_query="ALTER TABLE $table_ads ADD FULLTEXT {$table_ads}_ind ($kwfldsrch)";
$sql_res=mysql_query("$sql_query"); 

set_fchmod();

}

function set_fchmod()
{ global $mosConfig_absolute_path, $jmlconfpath;

$comp_dir=$jmlconfpath."/components/com_aclsfgpl";

chmod ("$comp_dir", 0755);
$comp_dir=$comp_dir."/";

#chmod ("{$comp_dir}config.php", 0777);
#chmod ("{$comp_dir}conf_cat.php", 0777);
#chmod ("{$comp_dir}conf_opt.php", 0777);
#chmod ("{$comp_dir}ads_ind.php", 0777);
chmod ("{$comp_dir}sph.php", 0755);
chmod ("{$comp_dir}vrfcd.php", 0755);
#chmod ("{$comp_dir}forms.php", 0777);
#chmod ("{$comp_dir}details.php", 0777);
#chmod ("{$comp_dir}t_ads_list2.html", 0777);
#chmod ("{$comp_dir}t_details.html", 0777);
#chmod ("{$comp_dir}t_index2.html", 0777);
#chmod ("{$comp_dir}t_menu.html", 0777);
#chmod ("{$comp_dir}t_msg.html", 0777);
#chmod ("{$comp_dir}t_sbm.html", 0777);
#chmod ("{$comp_dir}t_top.html", 0777);
#chmod ("{$comp_dir}top.php", 0777);
#chmod ("{$comp_dir}style.css", 0777);
#chmod ("{$comp_dir}fsturl.php", 0777);
}

function sql_create_tb()
{
global $ads_fields, $table_ads,$oldvrsn;
$table_name=$table_ads;
$db_t_fields['idnum']="integer not NULL primary key";
$db_t_fields['time']="integer";
$db_t_fields['exptime']="integer";
$db_t_fields['catname']="text";
$db_t_fields['visible']="integer";
$db_t_fields['adphotos']="char(5)";
$db_t_fields['login']="text";
$db_t_fields['adrate']="integer";
$db_t_fields['ipaddr1']="char(20)";
$db_t_fields['afstip1']="char(20)";
$db_t_fields['afprtid1']="char(20)";
$db_t_fields['cntemll']="integer";
$db_t_fields['cntvstr']="integer";
$db_t_fields['ratevtcn']="integer";
$db_t_fields['ratevtrt']="real";
$db_t_fields['replyid']="integer";
$db_t_fields['replcnts']="integer";
$db_t_fields['adcommkey']="integer";
$db_t_fields['attflext']="char(10)";
$db_t_fields['attfltl']="text";
$db_t_fields['flsrvtl1']="text";
$db_t_fields['flsrvur1']="text";
$db_t_fields['flsrvtl2']="text";
$db_t_fields['flsrvur2']="text";
$db_t_fields['embdcod']="text";

foreach ($ads_fields as $key => $value)
{
$db_t_fields[$key]=$ads_fields[$key][6];
}

$create_string="";
foreach ($db_t_fields as $db_key => $value)
{
$create_string=$create_string.$db_key." ".$db_t_fields[$db_key].", ";
}
$create_string=corr_sqlstring($create_string);
$sql="CREATE TABLE IF NOT EXISTS $table_name ( $create_string ) ";
return $sql;
}

 
?>
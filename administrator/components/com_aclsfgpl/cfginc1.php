<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function opt_form()
{
global $admconf_url, $db_name, $host_name, $db_user, $db_password, $table_ads, 
$urlclscrpt, $opt_header, $opt_footer, $adm_passw, $adm_email,
 $photos_path, $usevrfcode, $plcntdtl,$vstminnmbr,
$plcntpml,$voterate,$maxvalrate,$uselogsvmv, $exprlogs, $table_logs, $use_spmg,
$exp_period, $adsonpage, $sndadmnotif, $moderating, $hltadsf,$exp_perdhlt,$dsplhltpht,
 $pradsdupl,$tmltads, $privacy_mail, $pmailtp, $sendcopytoadm, $redirtoadm, $sndexpmsg1,
$photos_count,$phptomaxsize, $incl_mtmdfile, $mtmdfile_maxs, $flattchext, $plsflsrvrs, $infldflsrv,
$prphscnd, $maximgswith, $resphdb, $maxpixph, $ppactv, $pladddp,$mbac_sndml, $mbac_addad,$mbac_second,
$jsfrmch, $schallusrads, $ch_nmusr, $usrads_max, $usrads_chcktime, $titleclpg, $ht_header, $ht_footer,
$moderating_ct, $moderating_vl, $mbac_second_vl, $mbac_second_ct, 
$mbac_addad_vl, $mbac_addad_ct, $mbac_sndml_vl, $mbac_sndml_ct,
$adsontoppage, $colr_hltads, $capt_hltads, $tpadsindd, $bc2adsfrm, $width_2tpf, $adbnrcll, $fntclr_3, 
$fntclr_2, $tbclr_5, $fntclr_1, $tbclr_4, $tbclr_3, $tbclr_2, $tbclr_1, $prphscrwdth, $incl_prevphoto,
$ad_ind_width, $top_page_width, $maximgswith, $prphscnd, $ad_second_width, $detl_leftcol, $ind_leftcol, 
$btmtophtml, $top_rightcol, $top_leftcol, $prwdly, $photos_url, $displaflusr,$waflnscr, 
$dtl_rightcol, $ind_rightcol, $ht_leftcol, $cnt_htl_det, $cnt_htl_ind, $ordhltrate,$jloginfo, $sturlvar,
$use_ajax, $jstoplads, $jstphlads, $maxttlsz ;

echo "
<p>
<a href='{$admconf_url}'><b>Top:</b></a><br>
<form action='{$admconf_url}' method='post'>
<input type='hidden' name='mdopt' value='saveopts'>
<p>
<table width='700'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp; General  Options
</td></tr></table>

<table width='700'    border='0' cellspacing='10' cellpadding='10'> 
<tr><td valign='top' width='50%'>
<p align='justify'> 
Admin password:<br>
<input type='text' name='adm_passw' value='".opt_qt($adm_passw)."' class='formst'><p>
Admin e-mail:<br>
<input type='text' name='adm_email' value='".opt_qt($adm_email)."' class='formst'>
<p>

Expiration period for ads (days):<br>
<input type='text' name='exp_period' value='".opt_qt($exp_period)."' class='formst'>


<p align='justify'> 
 Use verification code for ads submitting, sending privacy mail:<br>
<select name='usevrfcode' class='formst'>
<option>".opt_qt($usevrfcode)."<checked><option>yes<option>no</select>


<p>
Number of ads displayed on the index page:<br>
<input type='text' name='adsonpage' value='".opt_qt($adsonpage)."' class='formst'>

<p>
Max ads title size for displaying on ads index pages (chars):<br>
<input type='text' name='maxttlsz' value='".opt_qt($maxttlsz)."' class='formst'>
 
<p align='justify'>
Protection for ads duplication:<br>
<select name='pradsdupl' class='formst'>
<option>".opt_qt($pradsdupl)."<checked><option>yes<option>no</select>

</td><td valign='top' width='50%'>

<p align='justify'>
Time (days) for latest ads link   on the top page:<br>
<input type='text' name='tmltads' value='".opt_qt($tmltads)."' class='formst'> 

<p align='justify'>
Set up JavaScript checking of  ads fields on submitting form:<br>
<select name='jsfrmch' class='formst'>
<option>".opt_qt($jsfrmch)."<checked><option>yes<option>no</select>

 

<p align='justify'> 
Place 'visits counter' on ads details pages:<br>
<select name='plcntdtl' class='formst'>
<option>".opt_qt($plcntdtl)."<checked><option>yes<option>no</select>



<p align='justify'> 
Use IP logs to count only one visit
  of the same user for the same ad:<br>
<select name='uselogsvmv' class='formst'>
<option>".opt_qt($uselogsvmv)."<checked><option>yes<option>no</select>

<p align='justify'> 
Expiration period for logs (days):<br>
<input type='text' name='exprlogs' value='".opt_qt($exprlogs)."' class='formst'> 

</td></tr></table>



<p>
<table width='700'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
&nbsp; Privacy Mail, Messages
</td></tr></table>

<table width='700'    border='0' cellspacing='10' cellpadding='10'> 
<tr><td valign='top' width='50%'>
Set up privacy mail: <br>
<select name='privacy_mail' class='formst'>
<option>".opt_qt($privacy_mail)."<checked><option>yes<option>no</select>

<p align='justify'>
Privacy mail template:<br>

<textarea rows='8' cols='40' name='pmailtp' wrap=off class=formst>$pmailtp</textarea> 
 
</td><td valign='top' width='50%'>

 If privacy mail is set up, send copy of privacy messages 
 to admin: <br>
<select name='sendcopytoadm' class='formst'>
<option>".opt_qt($sendcopytoadm)."<checked><option>yes<option>no</select>
 
<p align='justify'>
 If privacy mail is set up, redirect privacy messages 
 to admin:<br>
<select name='redirtoadm' class='formst'>
<option>".opt_qt($redirtoadm)."<checked><option>yes<option>no</select>

</td></tr></table>

<p>
<table width='700'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp;Ads Photos
</td></tr></table>

<table width='700'    border='0' cellspacing='10' cellpadding='10'> 
<tr><td valign='top' width='50%'>

Max number of photos allowed:<br>
<input type='text' name='photos_count' value='".opt_qt($photos_count)."' class='formst'> 

<p align='justify'>
Set max size for all photos on the second ad page (bytes):<br>
<input type='text' name='phptomaxsize' value='".opt_qt($phptomaxsize)."' class='formst'> 

<p align='justify'>
Width of preview photos on the second ad page (pixels):<br>
<input type='text' name='prphscnd' value='".opt_qt($prphscnd)."' class='formst'> 

<p align='justify'>
Max width of photos on the second ad page (pixels):<br>
<input type='text' name='maximgswith' value='".opt_qt($maximgswith)."' class='formst'> 
 
<p align='justify'>
Set up resizing of large photos before saving in the database:<br>
<select name='resphdb' class='formst'>
<option>".opt_qt($resphdb)."<checked><option>yes<option>no</select>
 
<p align='justify'>
Max width or height (pixels) of photos for resizing:<br>
<input type='text' name='maxpixph' value='".opt_qt($maxpixph)."' class='formst'>
 
</td><td valign='top' width='50%'>
 
</td></tr></table>
  


<p><table width='700'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp;Layout Headers/Footers
</td></tr></table>


<table width='700'    border='0' cellspacing='10' cellpadding='10'> 
<tr><td valign='top' width='50%'>

Set up html header   for pages generated by the script:<br>
<textarea rows='15' cols='90' name='ht_header' wrap=off class=formst>".opt_qt($ht_header)."</textarea>
 
<p align='justify'>
Set up html footer for pages generated by the script:<br>
<textarea rows='15' cols='90' name='ht_footer' wrap=off class=formst>".opt_qt($ht_footer)."</textarea>

 </td></tr></table>
<p>

&nbsp;
<input type='submit' value='Save Options' class='formst1'>
</form>
 
";
 
}

function saveoptset()
{
global $opt_post, $conf_dir, $opt_header, $opt_footer;

$opt_post['photos_path']=checkendslash($opt_post['photos_path']);
$opt_post['photos_url']=checkendslash($opt_post['photos_url']);
$opt_post['urlclscrpt']=checkendslash($opt_post['urlclscrpt']);
 
$cfgstr=$cfgstr.save_opt('adm_passw');
$cfgstr=$cfgstr.save_opt('adm_email');
$cfgstr=$cfgstr.save_opt('usevrfcode');
$cfgstr=$cfgstr.save_opt('plcntdtl');
$cfgstr=$cfgstr.save_opt('vstminnmbr');
$cfgstr=$cfgstr.save_opt('plcntpml');
$cfgstr=$cfgstr.save_opt('voterate');
$cfgstr=$cfgstr.save_opt('maxvalrate');
$cfgstr=$cfgstr.save_opt('uselogsvmv');
$cfgstr=$cfgstr.save_opt('exprlogs');
$cfgstr=$cfgstr.save_opt('use_spmg');
$cfgstr=$cfgstr.save_opt('exp_period');
$cfgstr=$cfgstr.save_opt('adsonpage');
$cfgstr=$cfgstr.save_opt('maxttlsz');
$cfgstr=$cfgstr.save_opt('moderating');
$cfgstr=$cfgstr.save_opt('hltadsf');
$cfgstr=$cfgstr.save_opt('ordhltrate');
$cfgstr=$cfgstr.save_opt('exp_perdhlt');
$cfgstr=$cfgstr.save_opt('cnt_htl_ind');
$cfgstr=$cfgstr.save_opt('cnt_htl_det');
$cfgstr=$cfgstr.save_opt('dsplhltpht');
$cfgstr=$cfgstr.save_opt('pradsdupl');
$cfgstr=$cfgstr.save_opt('sturlvar');
$cfgstr=$cfgstr.save_opt('use_ajax');
$cfgstr=$cfgstr.save_opt('jstoplads');
$cfgstr=$cfgstr.save_opt('jstphlads');
$cfgstr=$cfgstr.save_opt('tmltads');
$cfgstr=$cfgstr.save_opt('privacy_mail');
$cfgstr=$cfgstr.save_opt('pmailtp');
$cfgstr=$cfgstr.save_opt('sendcopytoadm');
$cfgstr=$cfgstr.save_opt('redirtoadm');
$cfgstr=$cfgstr.save_opt('sndexpmsg1');
$cfgstr=$cfgstr.save_opt('photos_count');
$cfgstr=$cfgstr.save_opt('phptomaxsize');
$cfgstr=$cfgstr.save_opt('incl_mtmdfile');
$cfgstr=$cfgstr.save_opt('mtmdfile_maxs');
$cfgstr=$cfgstr.save_opt('plsflsrvrs');
$cfgstr=$cfgstr.save_opt('infldflsrv');
$cfgstr=$cfgstr.save_opt('maximgswith');
$cfgstr=$cfgstr.save_opt('resphdb');
$cfgstr=$cfgstr.save_opt('maxpixph');
$cfgstr=$cfgstr.save_opt('ppactv');
$cfgstr=$cfgstr.save_opt('mbac_sndml');
$cfgstr=$cfgstr.save_opt('mbac_addad');
$cfgstr=$cfgstr.save_opt('mbac_second');
$cfgstr=$cfgstr.save_opt('jsfrmch');
$cfgstr=$cfgstr.save_opt('schallusrads');
$cfgstr=$cfgstr.save_opt('ch_nmusr');
$cfgstr=$cfgstr.save_opt('usrads_max');
$cfgstr=$cfgstr.save_opt('usrads_chcktime');
$cfgstr=$cfgstr.save_opt('ht_header'); 
$cfgstr=$cfgstr.save_opt('ht_footer');
$cfgstr=$cfgstr.save_opt('jloginfo');
$cfgstr=$cfgstr.save_opt('prphscnd');
$cfgstr=$cfgstr.save_opt2('moderating_ct');
$cfgstr=$cfgstr.save_opt('moderating_vl');
$cfgstr=$cfgstr.save_opt2('mbac_second_ct');
$cfgstr=$cfgstr.save_opt('mbac_second_vl');
$cfgstr=$cfgstr.save_opt2('mbac_addad_ct');
$cfgstr=$cfgstr.save_opt('mbac_addad_vl');
$cfgstr=$cfgstr.save_opt2('mbac_sndml_ct');
$cfgstr=$cfgstr.save_opt('mbac_sndml_vl');

$cfgstr="<?php
$cfgstr
?>";

savecfl($cfgstr, "conf_opt.php");
echo "
<p>&nbsp;<p><font class='fntmsg1'> Options have been saved successfully !</font>
<p>
 
";
}
?>
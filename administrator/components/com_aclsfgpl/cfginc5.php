<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function msgf_form($msgnm)
{
global $msg, $msg2, $months_short, $months_nm, $weekdays, $admconf_url;

echo "
<p>
<a href='$admconf_url'><b>Top:</b></a><br>
<form action='{$admconf_url}' method='post'>
<input type='hidden' name='mdopt' value='savemsg{$msgnm}'>

<table width='100%'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp; Modify/Translate Messages for User's Interface (set $msgnm)
</td></tr></table>
<br>
Some text info can be translated in the templates t_...html in the directory
joomla_dir/components/com_aclsfgpl . 
<br>
<table width='100%'    border='0' cellspacing='5' cellpadding='5'> 
<tr><td>
"; 

if($msgnm=="1"){$msg_ms=$msg;}else{$msg_ms=$msg2;}

if($msgnm=="1"){
$months_short_str="";
foreach ($months_short as  $value ){ $months_short_str= $months_short_str."$value, ";}
$months_short_str = preg_replace ("/,\s+$/", "", $months_short_str);
echo "
<p>
Short names for months:
<input type='text' size='120' name='months_short' value='$months_short_str' class=formst>
";


$months_nm_str="";
foreach ($months_nm as  $value ){ $months_nm_str= $months_nm_str."$value, ";}
$months_nm_str = preg_replace ("/,\s+$/", "", $months_nm_str);
echo "
<p>
Full names for months:
<input type='text' size='120' name='months_nm' value='$months_nm_str' class=formst>
";


$weekdays_str="";
foreach ($weekdays as  $value ){ $weekdays_str= $weekdays_str."$value, ";}
$weekdays_str = preg_replace ("/,\s+$/", "", $weekdays_str);
echo "
<p>
Names of Week Days:
<input type='text' size='120' name='weekdays' value='$weekdays_str' class=formst>
";

}

echo "
<p>
<input type='submit' value='Save Options' class='formst1'>
<p> Messages and words of user's interface which can be modified/translated: 
";

foreach ($msg_ms as $key => $value )
{
$msg_ms[$key]=preg_replace ("/\s+/", " ", $msg_ms[$key]);

echo "
<p>
<input type='text' size='120' name='m_{$key}' value='".opt_qt($msg_ms[$key])."' class=formst>
";
}

echo "
<p>
<input type='submit' value='Save Options' class='formst1'>
</form>
</td></tr></table>
";

}


function savemsgf($msgfl, $msgnm)
{
global $msg, $msg2, $opt_post;

if ($msgnm=="1"){$msg_ms=$msg; $msgvr="msg"; } else {$msg_ms=$msg2; $msgvr="msg2";}

$msgstr="$".$msgvr."= array(
";

foreach ($msg_ms as $key => $value )
{
$postvl=$opt_post["m_{$key}"];

$msgstr=$msgstr."\"".$key."\" => \"".check_opt($postvl)."\", 
"; 
}

$msgstr = preg_replace ("/,\s+$/", "", $msgstr);

$msgstr=$msgstr."
);";

 

if ($msgnm=="1"){$msg_ms=$msg; $msgvr="msg";  
$msgstr=$msgstr.save_arr("months_short");
$msgstr=$msgstr.save_arr("months_nm");
$msgstr=$msgstr.save_arr("weekdays");
}


$msgstr="<?php
$msgstr
?>";

savecfl($msgstr, $msgfl);
echo "
<p>&nbsp;<p><font class='fntmsg1'>Messages for user's interface have been saved successfully !</font>
<p>
";

}

?>
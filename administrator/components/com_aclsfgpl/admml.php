<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function eml_to_adm($ad_id,$add_ed_tp)
{
global $notmsbj, $urlclscrpt, $ct, $adm_passw, $moderating, 
$mbac_second, $adm_email, $ntfmlfrmt;

$notmsbj="";
if ($add_ed_tp=="add") {
$notmsbj="New ad has been places to classifieds";
}

if ($add_ed_tp=="edit") {
$notmsbj="Ad with ID# $ad_id has been edited";
}


$row=get_edit_info($ad_id);

$time1=$row['time'];
$date_posted=get_date($time1);
$ad_idnm=$row['idnum'];

$scndpgapss="";
if ($mbac_second=="m"){$scndpgapss="ammlk=$adm_passw";}

$details_lnk=$urlclscrpt."index.php?ct=$ct&md=details&id=$ad_idnm&$scndpgapss";
$deletad_lnk=$urlclscrpt."index.php?md=editform&edit_delete=delete&ammlk=$adm_passw&ed_id=$ad_idnm";
$editad_lnk=$urlclscrpt."index.php?md=editform&edit_delete=edit&ammlk=$adm_passw&ed_id=$ad_idnm";
$hilightt_lnk=$urlclscrpt."admin.php?md=admrate_ads&ammlk=$adm_passw&list_id=$ad_idnm&ads_rt=1";
$mdrtappr_lnk=$urlclscrpt."admin.php?md=make_ads_visible&ammlk=$adm_passw&list_id=$ad_idnm&";
if ($moderating=="yes"){
$modrlnkv="Approve this ad: $mdrtappr_lnk";
$modrlnkv_htm="<a href='$mdrtappr_lnk'>Approve this ad</a>";
}

$mail_mssg= "

$notmsbj:

ID#: $ad_idnm; date posted: $date_posted;  IP: ".$row['ipaddr1'].";
Title: ".$row['title']."
Brief description: ".$row['brief']."
Contact e-mail: ".$row['email']."

Details: $details_lnk

Delete this ad: $deletad_lnk

Edit this ad: $editad_lnk

$modrlnkv
";

if ($ntfmlfrmt=="2")
{
$mail_mssg="
<html>
<body bgcolor='#ffffff'>
<font FACE='ARIAL, HELVETICA' size='-1'>
<font COLOR='#000099'><b>$notmsbj:</b></font>
<p>
<font COLOR='#000099'>ID#:</font> $ad_idnm; &nbsp; <font COLOR='#000099'>date posted:</font> $date_posted; &nbsp; 
<font COLOR='#000099'>IP:</font> ".$row['ipaddr1'].";
<br>
<font COLOR='#000099'>Title:</font> ".$row['title']."
<br>
<font COLOR='#000099'>Brief description:</font> ".$row['brief']."
<br>
<font COLOR='#000099'>Contact e-mail:</font> ".$row['email']."

<br>
<b><a href='$details_lnk'>Details</a></b>
<p>
<a href='$deletad_lnk'>Delete this ad</a>  
&nbsp;&nbsp;
<a href='$editad_lnk'>Edit this ad</a>
&nbsp;&nbsp;
&nbsp;&nbsp;
$modrlnkv_htm
</font>
<p>
</body>
</html>
";
}

sndmail($adm_email, $notmsbj, $mail_mssg, $adm_email);
}

?>
<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function ctfld_form()

{
global   $admconf_url, $ads_fields, $fields_sets, $categories, $dlmtr, $hghltcat, $fields_comm, 
$reply_catg, $allcatfields, $tbclhlads, $mtg_keywrds, $mtg_descr, $mtfldkwrs,
$fields_layer, $use_adslayer, $slctcntr, $fld_dim, $fld_dimd, $slctcntr, $ctgroups_sets;


echo "
<p>
<a href='$admconf_url'><b>Top:</b></a><br>
<form action='{$admconf_url}' method='post'>
<input type='hidden' name='mdopt' value='savectfld'>

<table width='100%'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp; Settings Ads Fields Options
</td></tr></table><p>
<input type='submit' value='Save Options' class='formst1'>
<p>
 
<font   class='fnt4'   OnClick=\"displ('fldopt');\">
Fields options description</font>
<DIV id='fldopt' style='DISPLAY: none'>

<p>
<b>Short Name</b> - simple one word field name   for using  in the script functions and mysql database fields (not changed);
<p><b>Field Name</b> -  name of the field which is displayed on the ads indexes and forms;
<p> <b>Field   places</b> - parameter which shows how this field will be displayed:
 <br>      '1' - only at the first page (ads index);
  <br>     '2' - only at the second page ( 'detailed info');
  <br>     '12' - on both first and second pages;
  <br>     '00' - is not displayed on pages (hidden fields). 
<p> <b>Search Form   Option</b> - search parameter that sets the principle of search, it must have
      one of three different values explained below: 
    <br>       'keyword' - defines this as  search field. Users will be able to search
                for keys in this field; 
    <br>       'minmax' - when the field has numerical value (it is aplied for fields with mysql types 'real' or 'integer'),
 it will be searched in the range [minvalue, maxvalue]; 
   <br>       'nosearch' - not  search field. Fields 'title' and 'brief' are already inlcuded on search 
form in field with name 'keyword'.

<p>
<b>Sizes</b> - defines [m:n] the length of the input field on ad submitting form (m) and 
               max length of the info in this field (n); <br> For 'select' type parameter (m) means 
              vertical size of the field on the forms.  <br> Type 
size format for 'textarea' field   will be [m:n:k] -  where m - number of cols, n - number of rows, 
k- max size of the field. 
<p>
<b>Field type on forms</b> -  type of  the field on the submitting and search forms, 
          it can be 'text', 'select', 'checkbox', 'textarea'.        

<p> 
<b>should be filled out</b>  - means this field reqired to be filled out ('0' is not required).

<p>
<b>Mysql type</b> - parameter means MySQL type for this field (not changed).

<p>
<b>Choices</b>  - this last parameter can appear only for fields types
                      'select' and 'checkbox', this parameter means default values for 
                      users' choice.
<p> Some fields options e.g. short name, mysql type, options for specific fields (e.g. passw, title, 
email, country...) cannot be modified.  

</div>
<p>
<table width='100%'    border='0' cellspacing='1' cellpadding='3' > 
<tr>
<td valign='top' bgcolor='#eeeeee' class=tbl1>Short Name</td>
<td valign='top' bgcolor='#eeeeee'  class=tbl1>Field   Name</td>
<td valign='top'  bgcolor='#eeeeee' class=tbl1 >Field <br> places</td>
<td valign='top'  bgcolor='#eeeeee' class=tbl1>Search Form <br>(does not active <br>in this version)</td>
<td valign='top'  bgcolor='#eeeeee' class=tbl1>Sizes</td>
<td valign='top' bgcolor='#eeeeee'  class=tbl1>Field type <br> on forms</td>
<td valign='top'  bgcolor='#eeeeee' class=tbl1>should be <br> filled out (1)</td>
<td valign='top'  bgcolor='#eeeeee' class=tbl1>Mysql <br> Type</td>
<td valign='top'  bgcolor='#eeeeee' class=tbl1>Choices (for select type)</td>
</tr>
";


foreach ($ads_fields as $key => $value )
{

if ($ads_fields[$key][4]!="select" and $ads_fields[$key][4]!="checkbox"){$dfltvl="-";}
else{

$ereg_vl="
";

$fldvl7= ereg_replace ($ereg_vl, "", $ads_fields[$key][7]);

$fldvl7= ereg_replace ("<option>", $ereg_vl,  $fldvl7);

$fldvl7= preg_replace ("/\n$/", "", $fldvl7);

$dfltvl="
<textarea rows='4' cols='25' name='{$key}_7' wrap=off class=formst>".$fldvl7."</textarea>
";

}
echo "
<tr>
<td class=tbl1>$key</td> 
<td class=tbl1><input type='text' name='{$key}_0' value='".opt_qt($ads_fields[$key][0])."' size='20' class=formst></td>
";

 
if (($key=="passw") or ($key=="email") or ($key=="homeurl") ){

echo "
<td class=tbl1>".opt_qt($ads_fields[$key][1])."<input type='hidden' name='{$key}_1' value='".opt_qt($ads_fields[$key][1])."' class=formst></td>
";

}
else {
echo "
<td class=tbl1><select name='{$key}_1' class=formst><option>".opt_qt($ads_fields[$key][1])."<checked><option>12<option>2<option>1<option>00</select></td>
";
}

if (($key=="title") or ($key=="brief") ){
echo "
<td class=tbl1>".opt_qt($ads_fields[$key][2])."<input type='hidden' name='{$key}_2' value='".opt_qt($ads_fields[$key][2])."' class=formst></td>
";
}
else{
echo "
<td class=tbl1><select name='{$key}_2' class=formst><option>".opt_qt($ads_fields[$key][2])."<checked><option>nosearch<option>keyword<option>minmax</select></td>
";
}

echo "
<td class=tbl1><input type='text' name='{$key}_3' value='".opt_qt($ads_fields[$key][3])."' size='5'class=formst></td>
";

if (($ads_fields[$key][4]=="date") or ($ads_fields[$key][4]=="select2") or ($ads_fields[$key][4]=="select3") ){
 
echo "
<td class=tbl1> ".opt_qt($ads_fields[$key][4])." <input type='hidden' name='{$key}_4' value='".opt_qt($ads_fields[$key][4])."' class=formst></td>
";
}

else {
echo "
<td class=tbl1><select name='{$key}_4' class=formst><option>".opt_qt($ads_fields[$key][4])."<checked><option>text<option>textarea<option>select<option>checkbox</select></td>
";
}

echo "
<td class=tbl1><select name='{$key}_5' class=formst><option>".opt_qt($ads_fields[$key][5])."<checked><option>1<option>0</select></td>
<td class=tbl1 align='center'>".opt_qt($ads_fields[$key][6])."</td>
<td class=tbl1 align='center'>$dfltvl</td>

</tr>
";

}
echo "</table>";

echo "
<p>
<table width='100%'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp; Fields Sets for Classifieds Categories
</td></tr></table>
&nbsp; Set up lists of fields  sets which will be used in the categories (defined below).
 These  lists should contain short names of ads fields which are described in the table above.
 The standard ad fields with the  short names 'passw','email', 'urlpage', 'title', 'brief' should be included into each fields set.  
<p>
";

foreach ($fields_sets as $key => $value )
{
$st_mss=$fields_sets[$key];

$st_str=""; if ($st_mss!=""){foreach ($st_mss as $vl2){$st_str=$st_str.$vl2.", "; }}
$st_str = preg_replace ("/,\s+$/", "", $st_str);
echo "$key:<br>
<input type='text' name='{$key}_st' value='".opt_qt($st_str)."' class='formst' size=130>
<p>
";

} 

echo "
<p>
<table width='100%'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp; Set Up Categories
</td></tr></table>
";

$ctg_str="";
foreach ($categories as $key => $value )
{
$aa1=split("_",$key);
if ($aa1[0]=='newcolumn'){$ctg_str=$ctg_str.'newcolumn'."
";} else {
if($aa1[0] == 'title'){$ctg_str=$ctg_str.'title'.$dlmtr.$categories[$key]."
";} else {
$ctg_str=$ctg_str.$key.$dlmtr.$categories[$key][0].$dlmtr.$categories[$key][1].$dlmtr.$categories[$key][2]."
";
}}}

echo "
<table width='100%' border='0' cellspacing='4' cellpadding='4'><tr><td>
<textarea rows='20' cols='60' name='ctglst' wrap=off class=formst>".opt_qt($ctg_str)."</textarea>
</td><td valign='top'>
Categories format:<br>

shortcatname{$dlmtr}category name{$dlmtr}fields set{$dlmtr}field1,field2{$dlmtr}hide_param
<br>
where 
<br><i> shortcatname</i> - unique one word for simple short name of the category which will be used in the mysql database;
<br><i> category name</i> - full name of the category which will be displayed on the user's interface;
<br><i> fields set</i> - name of fields set which will be used for this category and which are described above;
<br><i> field1,field2</i> - short names of ads fields which will create
additional subcategories.
<p> Names of  categories group (e.g. 'Personals', 'Careers, Jobs', 'Real Estate') can be set up 
in the following format:
<br> 
title{$dlmtr}Name of  categories group
<p>
One word 'newcolumn' in the line means starting new categories column on the top page.
 
</td></tr></table>
";


echo "
<p>
<table width='100%'    border='0' cellspacing='1' cellpadding='0'> 
<tr><td  class='frmft1'>
 &nbsp; Categories Options
</td></tr></table>
 
";

echo "
<p>
 Set up name of fields sets which will be used  for displaying ads when user 
clicks on the link of  the group with several  categories; 
<br> e.g. for category group with key 'title_1' you can set up fields set  'set1' as the following:
  '1' => 'set1' <br>
<textarea rows='10' cols='40' name='ctgroups_sets' wrap=off class=formst>".prn_hash($ctgroups_sets)."</textarea>
"; 

 

echo "
 
<p>
Dimensions for ads fields:<br>
Format:<br>
field_short_name{$dlmtr}dimension<br>
<textarea rows='7' cols='40' name='fld_dim' wrap=off class=formst>".prn_hash($fld_dim)."</textarea>

<p>
Dimensions for ads fields which will be printed before value e.g. \$:<br> 
Format:<br>
field_short_name{$dlmtr}dimension<br>
<textarea rows='7' cols='40' name='fld_dimd' wrap=off class=formst>".prn_hash($fld_dimd)."</textarea>

<p>
Set up short names of the fields which will be displayed on the ads index 
<br> when search (e.g. Latest Ads)  is conducted through all categories: <br>
<input type='text' name='allcatfields' value=\"".opt_arr($allcatfields)."\" class='formst' size=40> 
";

echo "
<p>
<input type='submit' value='Save Options' class='formst1'>
</form>
";
 
}


function savectfld()
{
global $opt_post, $conf_dir, $opt_header, $opt_footer;

$cfgstr=$cfgstr.save_fldvr();
$cfgstr=$cfgstr.save_fld_sets();
$cfgstr=$cfgstr.save_catgs();
$cfgstr=$cfgstr.save_hash("fields_comm");
$cfgstr=$cfgstr.save_hash("reply_catg"); 
$cfgstr=$cfgstr.save_hash("fld_dim");
$cfgstr=$cfgstr.save_hash("ctgroups_sets"); 
$cfgstr=$cfgstr.save_hash("fld_dimd");
$cfgstr=$cfgstr.save_arr("allcatfields"); 
$cfgstr=$cfgstr.save_opt("use_adslayer"); 
$cfgstr=$cfgstr.save_opt("tbclhlads"); 
$cfgstr=$cfgstr.save_fldlr("fields_layer"); 
$cfgstr=$cfgstr.save_opt("slctcntr");

$cfgstr="<?php
$cfgstr
?>";

savecfl($cfgstr, "conf_cat.php");
echo "
<p>&nbsp;<p><font class='fntmsg1'>Ads fields/categories options  have been saved successfully!</font> 
<p>
 
";
}

function save_fldlr()
{
global $opt_post, $dlmtr2;

$lrstr=ereg_replace (" ", "", $opt_post['fields_layer']);
$lr_mss=split(",", $lrstr );

$res="
$"."fields_layer=array( ";

foreach  ($lr_mss as  $value ){ $res=$res."'$value' => 'yes', "; }

$res = preg_replace ("/,\s+$/", "", $res);

$res=$res.");
";
return $res;

}

function save_hltctg()
{
global $opt_post, $dlmtr2;
 
$ereg_vl="
";
$ctg_mss=split($ereg_vl, $opt_post['ctghltlst'] );

$res="

$"."hghltcat= array(
";

foreach ($ctg_mss as  $value )
{

$value = preg_replace ("/^\s+/", "", $value);
$value = preg_replace ("/\s+$/", "", $value);

 
$ctlnms=split ($dlmtr2, $value);

if ($ctlnms[0]!=""){
 
$ctlnms[1]=ereg_replace (" ", "", $ctlnms[1]);
$ctlnms[1]= preg_replace ("/,\s+$/", "", $ctlnms[1]);
 
$res=$res."
\"".check_opt($ctlnms[0])."\" => array( ".$ctlnms[1]." ), ";
}
}

$res = preg_replace ("/,\s+$/", "", $res);

$res=$res."
);";
return $res;
}

function save_catgs()
{
global $opt_post, $dlmtr2;
 
$ereg_vl="
";
$ctg_mss=split($ereg_vl, $opt_post['ctglst'] );

$res="

$"."categories= array(
";

$newcol_cnt=0;
$title_cnt=0;
foreach ($ctg_mss as  $value )
{

$value = preg_replace ("/^\s+/", "", $value);
$value = preg_replace ("/\s+$/", "", $value);

$ctlnms=split ($dlmtr2, $value);

if ($ctlnms[0]!=""){
$ctlnms[0]=ereg_replace (" ", "", $ctlnms[0]);
$ctlnms[2]=ereg_replace (" ", "", $ctlnms[2]);
$ctlnms[3]=ereg_replace (" ", "", $ctlnms[3]);

if ($ctlnms[0]=='newcolumn'){ $newcol_cnt++; $res=$res."
'newcolumn_".$newcol_cnt."' => \"".$ctlnms[1]."\", "; 
} else {

if($ctlnms[0] == 'title'){$title_cnt++; $res=$res."
'title_".$title_cnt."' => \"".$ctlnms[1]."\", "; 
} else {
$res=$res."
\"".check_opt($ctlnms[0])."\" => array( \"".check_opt($ctlnms[1])."\", \"".check_opt($ctlnms[2])."\", \"".check_opt($ctlnms[3])."\"), ";
}}
} 

}

$res = preg_replace ("/,\s+$/", "", $res);

$res=$res."
);";

return $res;

}


function save_fld_sets()
{
global $fields_sets, $opt_post;

$res="

$"."fields_sets= array(
";

foreach ($fields_sets as $key => $value )
{
$st_str = preg_replace ("/,\s+$/", "", $opt_post["{$key}_st"]);
$st_str= ereg_replace (" ", "", $st_str);

$res=$res."'".$key."' => array( $st_str ), 
";
}

$res = preg_replace ("/,\s+$/", "", $res);

$res=$res.");";

return $res;
}


function save_fldvr()
{
global $ads_fields, $opt_post;

$res="$"."ads_fields= array(
";

if (($opt_post["slctcntr"]=="no") and ($opt_post["country_4"]=="select2") and ($opt_post["city_4"]=="select3") ){
$opt_post["country_4"]="text"; $opt_post["country_3"]="40:50";
$opt_post["city_4"]="text"; $opt_post["city_3"]="40:50";
}

foreach ($ads_fields as $key => $value )
{

if ($opt_post["{$key}_4"]=="checkbox"){$opt_post["{$key}_5"]="0";}

if ($opt_post["{$key}_4"]!="select" and $opt_post["{$key}_4"]!="checkbox"){$dfltvl="";}
else{

$ereg_vl="
";

$dfltvl_arr=split($ereg_vl, $opt_post["{$key}_7"]);
$dfltvl="";
foreach ($dfltvl_arr as $value){ 
$value = preg_replace ("/^\s+/", "", $value);
$value = preg_replace ("/\s+$/", "", $value);

if ($value!=""){$dfltvl=$dfltvl."<option>".$value;}
}
 
$dfltvl=", \"".check_opt($dfltvl)."\"";


}

$res=$res."\"".$key."\" => array(\"".check_opt($opt_post["{$key}_0"])."\",\"".check_opt($opt_post["{$key}_1"])."\",\""
.check_opt($opt_post["{$key}_2"])."\",\"".check_opt($opt_post["{$key}_3"])."\",\"".check_opt($opt_post["{$key}_4"])."\",\""
.check_opt($opt_post["{$key}_5"])."\",\"".check_opt($ads_fields[$key][6])."\"".$dfltvl."),
";
}

$res=preg_replace ("/,\s+$/", "", $res);

$res=$res."
);";

return $res;

}

?>
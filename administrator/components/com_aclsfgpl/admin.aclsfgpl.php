<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

if (!defined('_JEXEC')){defined( '_VALID_MOS' ) or die( 'access denied' );}

$phpvrsn=explode('.',phpversion()); if($phpvrsn[0]*10+$phpvrsn[1] >= 53 ){ error_reporting(E_ALL  & ~E_NOTICE & ~E_DEPRECATED);}
else {error_reporting(E_ALL & ~E_NOTICE);}

if (defined('JPATH_ROOT')){$jmlconfpath=JPATH_ROOT;}else{$jmlconfpath=$mosConfig_absolute_path;}
$config_path=$jmlconfpath."/components/com_aclsfgpl/config.php";
include($config_path);

$ht_header=preg_replace("/<div class='aclassf'>$/","",$ht_header);
$ht_footer=preg_replace("/^<\/div>/","",$ht_footer);

global $_REQUEST, $html_footer, $html_header, $admcookpassw, $adm_passw ;

$admconf_url="index.php?option=com_aclsfgpl&task=".$_REQUEST['task']."&"; 
$indadm_url="../index.php?option=com_aclsfgpl&";

if($_REQUEST['task']=='config'){include("{$admcomp_path}admconf.php");}
if($_REQUEST['task']=='admin'){include("{$admcomp_path}admin.php");}

if($_REQUEST['task']==''){
echo "

<a href='index.php?option=com_aclsfgpl&task=admin'
 style='FONT-WEIGHT: bold; color: #000099; FONT-SIZE: 13px; text-align: left;'>Admin Panel</a>
<p>
<a href='index.php?option=com_aclsfgpl&task=config' 
style='FONT-WEIGHT: bold; color: #000099; FONT-SIZE: 13px; text-align: left;' >Configuration</a>
<p>
<hr size=1>
";
 
}

?>
<?php
/**
 * $Id: stalker.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Entry to Stalker Component
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

// Require the base controller
require_once(JPATH_COMPONENT . DS . 'controller.php');

// Require specific controller if requested
if($controller = JRequest::getWord('controller')) {
    $path = JPATH_COMPONENT . DS . 'controllers' . DS . $controller . '.php';

    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}
 
// Create the controller
$classname    = 'StalkersController' . $controller;
$controller   = new $classname();
 
// Perform the Request task
$controller->execute(JRequest::getVar('task'));
 
// Redirect if set by the controller
$controller->redirect();

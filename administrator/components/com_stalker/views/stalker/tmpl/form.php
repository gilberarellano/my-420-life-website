<?php
/**
 * $Id: form.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Stalker View for Stalker Component
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */
 
// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="width-100">
    <fieldset class="adminform">
        <legend><?php echo JText::_('COM_STALKER_DETAILS'); ?></legend>
        <ul class="adminformlist">
            <li>
                <label for="socnetid">
                    <?php echo JText::_('COM_STALKER_SOCNET'); ?>:
                </label>
   				<?php echo (is_null($this->lists['socnet'])) ? JText::_('COM_STALKER_ERROR_NOSOCNETS') : $this->lists['socnet']; ?>
            </li>
            <li>
                <label for="socnetid">
                    <?php echo JText::_('COM_STALKER_GROUP'); ?>:
                </label>
				<?php echo $this->lists['stalkgrp']; ?>
            </li>
            <li>
                <label for="username">
                    <?php echo JText::_('COM_STALKER_IDENT'); ?>:
                </label>
                <input class="text_area" type="text" name="username" id="username" size="32" maxlength="250" value="<?php echo $this->stalker->username; ?>" />
            </li>
            <li>
                <label for="linktitle">
                    <?php echo JText::_('COM_STALKER_LINKTITLE'); ?>:
                </label>
                <input class="text_area" type="text" name="linktitle" id="linktitle" size="32" maxlength="200" value="<?php echo $this->stalker->linktitle; ?>" />
            </li>
            <li>
                <label for="imagealt">
                    <?php echo JText::_('COM_STALKER_IMAGEALT'); ?>:
                </label>
                <input class="text_area" type="text" name="imagealt" id="imagealt" size="32" maxlength="200" value="<?php echo $this->stalker->imagealt; ?>" />
            </li>
            <li>
                <label for="target">
                    <?php echo JText::_('COM_STALKER_TARGET'); ?>:
                </label>
                <input class="text_area" type="text" name="target" id="target" size="22" maxlength="20" value="<?php echo $this->stalker->target; ?>" />
            </li>
    		<li>
				<label for="image">
					<?php echo JText::_('COM_STALKER_IMAGE'); ?>:
				</label>
				<?php echo $this->lists['image']; ?>
    		</li>
    		<li>
				<label for="preview">
					<?php echo JText::_('COM_STALKER_PREVIEW'); ?>:
				</label>
				<script language="javascript" type="text/javascript">
    				if (getSelectedValue( 'adminForm', 'image' )!=''){
    					jsimg='<?php echo JURI::root(true) ?>/media/stalker/icons/<?php echo $this->imageset ?>/' + getSelectedValue( 'adminForm', 'image' );
    				} else {
    					jsimg='<?php echo JURI::root(true) ?>/images/M_images/blank.png';
    				}
    				document.write('<img src=' + jsimg + ' name="imagelib" width="64" height="64" border="2" alt="<?php echo JText::_('COM_STALKER_PREVIEW'); ?>" />');
				</script>
    		</li>
    		<li>
    		    <label for="published">
				    <?php echo JText::_('COM_STALKER_PUBLISHED'); ?>:
			    </label>
				<fieldset class="radio">
				    <?php echo $this->lists['published']; ?>
				</fieldset>
    		</li>
    		<li>
				<label for="ordering">
					<?php echo JText::_('COM_STALKER_ORDER'); ?>:
				</label>
				<?php echo $this->lists['ordering']; ?>
    		</li>
        </ul>
    </fieldset>
</div>
 
<div class="clr"></div>
 
<input type="hidden" name="option" value="com_stalker" />
<input type="hidden" name="id" value="<?php echo $this->stalker->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="stalker" />
</form>

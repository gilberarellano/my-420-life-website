<?php
/**
 * $Id: view.html.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Import/Export View for Stalker Component
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */
 
// no direct access
defined('_JEXEC') or die('Restricted access');

require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'stalker.helper.php' );

jimport('joomla.application.component.view');

class StalkersViewImport extends JView
{
    /**
     * Import/Export view display method
     * @return void
     **/
	function display($tpl = null)
	{
        StalkerHelper::add_submenu ($this->getName());
	    JToolBarHelper::title(JText::_('COM_STALKER_IMPORTTITLE'), 'generic.png');
   	    JToolBarHelper::cancel();
   	    
		parent::display($tpl);
	}
}

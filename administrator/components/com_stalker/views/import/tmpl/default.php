<?php
/**
 * $Id: default.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Import/Export View for Stalker Component
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */
 
// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<script type="text/javascript">
function settask(task)
{
	var form = document.adminForm;

	form.task.value = task;
	form.submit();
}
</script>

<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="width-100">
	<fieldset class="adminform">
    	<legend><?php echo JText::_( 'COM_STALKER_STALKERIMPORT' ); ?></legend>
		<ul class="adminformlist">
    		<li>
				<label for="xmlfile">
					<?php echo JText::_( 'COM_STALKER_STALKERIMPORTFILE' ); ?>:
				</label>
				<input type="file" name="xmlfile" value="" size="30">
    		</li>
    		<li>
                <label for="import"></label>
				<input class="button" type="button" id="import" name="import" value="<?php echo JText::_('COM_STALKER_IMPORT_NOW'); ?>"  onclick="settask('import');"/>
    		</li>
		</ul>
	</fieldset>

	<fieldset class="adminform">
		<legend><?php echo JText::_( 'COM_STALKER_STALKEREXPORT' ); ?></legend>
        <ul class="adminformlist">
    		<li>
    		    <label for="export">
                    <?php echo JText::_( 'COM_STALKER_STALKEREXPORTXML' ); ?>
                </label>
				<input class="button" type="button" id="export" name="export" value="<?php echo JText::_('COM_STALKER_EXPORT_NOW'); ?>"  onclick="settask('export');"/>
    		</li>
		</ul>
	</fieldset>
</div>

<input type="hidden" name="option" value="com_stalker" />
<input type="hidden" name="view" value="import" />
<input type="hidden" name="controller" value="import" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="id" value="" />

</form>

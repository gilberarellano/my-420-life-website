<?php
/**
 * $Id: view.html.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Stalkgrps View for Stalker Component
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */

// no direct access

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class StalkersViewStalkgrp extends JView
{
    /**
     * Stalker Group view display method
     * @return void
     **/
    function display($tpl = null)
    {
	    //get the Stalker record
    	$stalkgrp      	=& $this->get('Data');
    	$isNew       	= ($stalkgrp->id < 1);
 
	    $text 			= $isNew ? JText::_('COM_STALKER_NEW') : JText::_('COM_STALKER_EDIT');
        StalkerHelper::add_submenu ($this->getName());
	    JToolBarHelper::title(JText::_('COM_STALKER_STALKGRPTITLE') . ': <small><small>[ ' . $text . ' ]</small></small>');
   	 	JToolBarHelper::save();

		if ($isNew) {
        	JToolBarHelper::cancel();
	    } else {
	        // for existing items the button is renamed `close`
    	    JToolBarHelper::cancel('cancel', 'Close');
	    }

	    $this->assignRef('stalkgrp', $stalkgrp);

	    parent::display($tpl);
    }
}
<?php
/**
 * $Id: view.html.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Socnets View for Stalker Component
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */
 
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class StalkersViewSocnets extends JView
{
    /**
     * Stalkers view display method
     * @return void
     **/
    function display($tpl = null)
    {
        $app =& JFactory::getApplication();
        $option =  JRequest::getCmd('option');
        
		$varcon = $option . 'socnets.';

        StalkerHelper::add_submenu ($this->getName());
		JToolBarHelper::title(JText::_('COM_STALKER_SOCNETSTITLE'), 'generic.png');
		JToolBarHelper::custom('import', 'default', 'default', JText::_('COM_STALKER_IMP_EXP'), false);
		JToolBarHelper::deleteList('COM_STALKER_CONFIRMDELETE');
		JToolBarHelper::editListX();
		JToolBarHelper::addNewX();

		// Set up the default ordering
		$filter_order	    = $app->getUserStateFromRequest($varcon . 'filter_order',		'filter_order',		'name',	'cmd');
		$filter_order_Dir	= $app->getUserStateFromRequest($varcon . 'filter_order_Dir',	'filter_order_Dir',	'',		'word');

        // Get data, total & pagination from the model
        $items 		=& $this->get('Data');
        $total 		=& $this->get('Total');
		$pagination =& $this->get('Pagination');

		$lists['order']     = $filter_order;
		$lists['order_Dir'] = $filter_order_Dir;

		$this->assignRef('lists', 		$lists);
        $this->assignRef('items', 		$items);
		$this->assignRef('total',		$total);
        $this->assignRef('pagination',	$pagination);

        parent::display($tpl);
    }
}

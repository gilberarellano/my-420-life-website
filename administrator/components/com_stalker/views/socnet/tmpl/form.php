<?php
/**
 * $Id: form.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Socnet View for Stalker Component
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */
 
// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="width-100">
    <fieldset class="adminform">
        <legend><?php echo JText::_('COM_STALKER_DETAILS'); ?></legend>
        <ul class="adminformlist">
            <li>
                <label for="name">
                    <?php echo JText::_('COM_STALKER_NETNAME'); ?>:
                </label>
                <input class="text_area" type="text" name="name" id="name" size="32" maxlength="40" value="<?php echo $this->socnet->name; ?>" />
                <div class="clr"></div>
            </li>
            <li>
                <label for="url">
                    <?php echo JText::_('COM_STALKER_URL'); ?>:
                </label>
                <input class="text_area" type="text" name="url" id="url" size="60" maxlength="250" value="<?php echo $this->socnet->url; ?>" /><br />
                <?php echo JText::_('COM_STALKER_IDHELP'); ?>:
                <div class="clr"></div>
            </li>
        </ul>
    </fieldset>
</div>
 
<div class="clr"></div>
 
<input type="hidden" name="option" value="com_stalker" />
<input type="hidden" name="id" value="<?php echo $this->socnet->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="socnet" />
</form>

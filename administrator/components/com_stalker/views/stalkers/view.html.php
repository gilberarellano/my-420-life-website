<?php
/**
 * $Id: view.html.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Stalkers View for Stalker Component
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class StalkersViewStalkers extends JView
{
    /**
     * Stalkers view display method
     * @return void
     **/
    function display($tpl = null)
    {
    	$app =& JFactory::getApplication();
    	$option =  JRequest::getCmd('option');
    	
		$varcon = $option . 'stalkers.';

        StalkerHelper::add_submenu ($this->getName());
		JToolBarHelper::title(JText::_('COM_STALKER_STALKERSTITLE'), 'generic.png');
//		JToolBarHelper::media_manager( '/' );
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
        JToolBarHelper::deleteList('COM_STALKER_CONFIRMDELETE');
        JToolBarHelper::editListX();
        JToolBarHelper::addNewX();
        JToolBarHelper::preferences('com_stalker', '500');

		$filter_order     	= $app->getUserStateFromRequest($varcon . 'filter_order',		'filter_order',		'ordering',	'cmd');
		$filter_order_Dir 	= $app->getUserStateFromRequest($varcon . 'filter_order_Dir',	'filter_order_Dir',	'',			'word');

        // Get data, total & pagination from the model
        $items 		=& $this->get('Data');
        $total 		=& $this->get('Total');
		$pagination =& $this->get('Pagination');

		$lists['order']     = $filter_order;
		$lists['order_Dir'] = $filter_order_Dir;

		$params = &JComponentHelper::getParams('com_stalker');
		$imageset = $params->get('globalimageset', 'default');

		$this->assignRef('lists', 		$lists);
        $this->assignRef('items', 		$items);
		$this->assignRef('total',		$total);
        $this->assignRef('pagination',	$pagination);
        $this->assignRef('imageset',	$imageset);
 
        parent::display($tpl);
    }
}

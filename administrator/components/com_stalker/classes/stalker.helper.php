<?php
/**
 * $Id: stalker.helper.php 197 2011-07-07 22:58:10Z meloman $
 * 
 * Help class for Stalker Component
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */

defined( '_JEXEC' ) or die( 'Restricted access' );

Class StalkerHelper
{
    function add_submenu ($view_name = '')
    {
		JSubMenuHelper::addEntry(JText::_('COM_STALKER_MYLINKS'), 'index.php?option=com_stalker&view=stalkers', $view_name == 'stalkers');
        JSubMenuHelper::addEntry(JText::_('COM_STALKER_STALKGRP'), 'index.php?option=com_stalker&view=stalkgrps', $view_name == 'stalkgrps');
        JSubMenuHelper::addEntry(JText::_('COM_STALKER_SOCNETS'), 'index.php?option=com_stalker&view=socnets', $view_name == 'socnets');
    }    
}
?>
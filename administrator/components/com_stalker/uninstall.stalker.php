<?php
/**
 * $Id: uninstall.stalker.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Custom Uninstaller for Stalker Component
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

function com_uninstall()
{
?>

	<p>Thank you for using the Stalker Component! 
	And thank you to <a href='http://www.nicktexidor.com' target='_blank'>Nick Texidor</a> for creating this great extension!</p>
    <p>Alain Rivest - <a href='http://aldra.ca' target='_blank'>http://aldra.ca</a></p>

<?php	
}

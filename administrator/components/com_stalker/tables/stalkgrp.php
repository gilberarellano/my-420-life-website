<?php
/**
 * $Id: stalkgrp.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Stalkgrp table class
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */
 
// No direct access
defined('_JEXEC') or die('Restricted access');

class TableStalkgrp extends JTable
{
    /**
     * Primary Key
     *
     * @var int
     */
    var $id = null;
 
    /**
     * @var string
     */
    var $name = null;
 
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function TableStalkgrp(&$db) {
        parent::__construct('#__stalker_groups', 'id', $db);
    }
}

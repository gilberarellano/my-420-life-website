<?php
/**
 * $Id: stalker.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Stalker table class
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

class TableStalker extends JTable
{
    /**
     * Primary Key
     *
     * @var int
     */
    var $id = null;
 
    /**
     * @var string
     */
    var $socnetid = null;
 
    /**
     * @var string
     */
    var $groupid = null;
 
    /**
     * @var string
     */
    var $username = null;
 
    /**
     * @var string
     */
    var $target = null;
 
    /**
     * @var string
     */
    var $image = null;
 
     /**
     * @var string
     */
    var $linktitle = null;
 
    /**
     * @var string
     */
    var $imagealt = null;
 
   /**
     * @var boolean
     */
    var $published = true;
 
    /**
     * @var int
     */
    var $ordering = 0;
 
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function TableStalker(&$db) {
        parent::__construct('#__stalker', 'id', $db);
    }
}

#!/usr/bin/php
<?php

/**
* FeedGator - Aggregate RSS newsfeed content into a Joomla! database
* @version 2.3.2
* @package FeedGator
* @author Original author Stephen Simmons
* @now continued and modified by Matt Faulds, Remco Boom & Stephane Koenig and others
* @email mattfaulds@gmail.com
* @Joomla 1.5 Version by J. Kapusciarz (mrjozo)
* @copyright (C) 2005 by Stephen Simmons - All rights reserved
* @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
*
**/


define( '_JEXEC', 1 );

define('DS', DIRECTORY_SEPARATOR);
define('JPATH_ROOT', substr(__FILE__,0,strrpos(__FILE__, DS.'administrator')));
define('JPATH_BASE', JPATH_ROOT.DS.'administrator');
define('JPATH_COMPONENT', JPATH_BASE.DS.'components'.DS.'com_feedgator');

@require_once( JPATH_BASE.DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE.DS.'includes'.DS.'framework.php' );
require_once( JPATH_BASE.DS.'includes'.DS.'helper.php' );
require_once( JPATH_BASE.DS.'includes'.DS.'toolbar.php' );

$mainframe = &JFactory::getApplication('site');
$mainframe->initialise();

define('FG_VERSION', '2.3.2');

$version = new JVersion();
define('J_VERSION', $version->getShortVersion());

require_once ( JPATH_COMPONENT.DS.'controller.php');
require_once ( JPATH_COMPONENT.DS.'factory.feedgator.php');
require_once ( JPATH_COMPONENT.DS.'helpers'.DS.'feedgator.helper.php');
require_once ( JPATH_COMPONENT.DS.'helpers'.DS.'feedgator.utility.php');
if(file_exists(JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'addkeywords.php')) {
	require_once( JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'addkeywords.php' );
}
FeedgatorUtility::profiling('Start cron');

define('SPIE_CACHE_AGE', 60*10);

require_once( JPATH_ROOT.DS.'libraries'.DS.'simplepie'.DS.'simplepie.php');
require_once(JPATH_COMPONENT.DS.'inc'.DS.'simplepie'.DS.'overrides.php');
FeedgatorUtility::profiling('Loaded SimplePie');

JRequest::setVar('task','cron','get');
JRequest::setVar(JUtility::getToken(),'1','get');

$controller = new FeedgatorController();
$controller->import('all');

FeedgatorUtility::profiling('End');
echo 'Import finished';
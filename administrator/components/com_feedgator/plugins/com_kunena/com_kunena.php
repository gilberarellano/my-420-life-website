<?php
/**
* FeedGator Content Importing Plugin for Kunena Content
* @version 0.5.2
* @package FeedGator
* @author Matt Faulds
* @email mattfaulds@gmail.com
* @copyright (C) 2010 Matthew Faulds - All rights reserved
* @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
*
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JTable::addIncludePath(JPATH_BASE.DS.'components'.DS.'com_feedgator'.DS.'tables');

class FeedgatorPluginKunena
{
	// Title for use in menus etc
	var $title = 'Kunena Content';
	// Name of extension that plugin enable Feed Gator to save to
	var $extension = 'com_kunena';
	// DB table to above extension
	var $table = '#__kunena_messages';
	// Name for "published' column in content items - com_content uses 'state'
	var $state = 'hold';
	// Name for section column in content items or alias for section
	var $section = '<i>Kunena Category</i>';
	// Section ID over-ride for content components without sections
	var $sectionid = null;
	// Object containing plugin data
	var $data = null;
	// JParameter object with plugin parameters
	var $params = null;
	
	function __construct()
	{
		$this->model = &FGFactory::getPluginModel();
		$this->model->setExt($this->extension);
		$this->_db = &JFactory::getDBO();
	}
	
	function setData($data)
	{
		$this->data = $data;
		$this->sectionid = -1*$this->data->id;
	}
	
	function &getData()
	{
		if(!$this->data) {
			$this->model->setExt($this->extension);
			$this->setData($this->model->getPluginData());
		}
		return $this->data;
	}
	
	function &getParams($feedId = -1)
	{
		if(!$this->params) {
			$this->params = new JParameter( $this->model->getParams($feedId), $this->model->getXmlPath() );
		}
		return $this->params;
	}
	
	function componentCheck()
	{
		$components = JComponentHelper::_load();
		
		return (boolean)isset($components[$this->extension]);
	}
	
	function countContentItems($where)
	{
		$this->_buildWhere($where);
		// Get the total number of records specified by where clause
		$query = '(SELECT COUNT(*)' .
				' FROM ' . $this->table .' AS c' .
				' LEFT JOIN #__kunena_categories AS cc ON cc.id = c.catid' .
		//		' LEFT JOIN #__sections AS s ON s.id = c.sectionid' .
				' LEFT JOIN #__feedgator_imports AS fi ON fi.content_id = c.id AND fi.plugin = '. $this->_db->Quote($this->extension) .
				' LEFT JOIN #__feedgator AS fg ON fg.id = fi.feed_id' .
				$where.')';
		return $query;
//		$this->_db->setQuery($query);
//		return $this->_db->loadResult();
	}
	
	function countContentQuery()
	{
		// Get the total number of records in range (added later)
		$query = 'SELECT COUNT(*)' .
				' FROM ' . $this->table .
				' WHERE id IN (%s)';
		return $query;
	}
	
	function getContentItem($id)
	{
		$query = 	'SELECT *' .
					' FROM ' . $this->table .
					' WHERE id = '. $this->_db->Quote($id);
		$this->_db->setQuery( $query );
		if(!$content = $this->_db->loadAssoc()) {
		    return false;
		}
				
		return $content;
	}
	
	function getContentLink($id)
	{
	    return JRoute::_( 'index.php?option='.$this->extension.'&task=edit&cid[]='. $id );
	}
	
	function getContentItemsQuery($where)
	{ // access through categories table, featured as sticky, ?kunena groups, kunena users,  c.'.$this->state.' AS state,c.access AS access, g.name AS groupname, u.name AS editor

		$this->_buildWhere($where);
		$this->getData();
		// Get the articles
		$query = '(SELECT c.id AS id, c.subject AS title, \'1\' AS state, c.time AS created, c.ordering AS ordering,'.$this->_db->Quote($this->sectionid).' AS sectionid,c.catid AS catid,c.time AS publish_up,\'0000-00-00 00:00:00\' AS publish_down,c.name AS created_by_alias,c.userid AS created_by,\'0\' AS access, \'0\' AS checked_out,\'Public\' AS groupname, cc.name AS cat_name,\'Someone\' AS editor, c.ordering AS frontpage, '.$this->_db->Quote($this->section).' AS section_name, v.name AS author, fi.feed_id AS feedid, fg.title AS feed_title, fg.content_type AS content_type' .
				' FROM ' . $this->table . ' AS c' .
				' LEFT JOIN #__kunena_categories AS cc ON cc.id = c.catid' .
		//		' LEFT JOIN #__sections AS s ON s.id = c.sectionid' .
		//		' LEFT JOIN #__groups AS g ON g.id = c.access' .
		//		' LEFT JOIN #__users AS u ON u.id = c.checked_out' .
				' LEFT JOIN #__users AS v ON v.id = c.userid' .
		//		' LEFT JOIN #__content_frontpage AS f ON f.content_id = c.id' .
				' LEFT JOIN #__feedgator_imports AS fi ON fi.content_id = c.id AND fi.plugin = '. $this->_db->Quote($this->extension) .
				' LEFT JOIN #__feedgator AS fg ON fg.id = fi.feed_id' .
				$where.')';
	
		return $query;
	}
	
	function getFeedItems($where)
	{
		$this->_buildWhere($where);
		$query ='SELECT fg.*,cc.name AS cat_name, '.$this->_db->Quote($this->section).' AS section_name, u.name AS editor FROM #__feedgator fg'.
				' LEFT JOIN #__kunena_categories AS cc ON cc.id = fg.catid'.
				' LEFT JOIN #__users AS u ON u.id = fg.checked_out '.
				$where;
		$this->_db->setQuery($query);

		return $this->_db->loadObjectList();
	}
	
	function findDuplicates($type,$string)
	{		
		// type can be id, alias, title or internal
		if($type == 'internal') { // for com_content we use the alias to find internal duplicates
			$this->getParams();
			$query =	'SELECT '. $this->_db->Quote($this->extension) .' AS content_type, fg.title AS feed_title,c.subject AS title,\'No Alias\' AS alias,COUNT(*) AS num,' .
						' GROUP_CONCAT(CONCAT_WS(\'|\',CONVERT(c.id,CHAR(11)),'.$this->_db->Quote($this->section).',CONVERT(c.catid,CHAR(11)),c.subject) ORDER BY c.id ASC SEPARATOR \'||\') AS results' .
						' FROM ' . $this->table . ' AS c' .
						' LEFT JOIN #__feedgator_imports AS fi ON fi.content_id = c.id AND fi.plugin = '. $this->_db->Quote($this->extension) .
						' LEFT JOIN #__feedgator AS fg ON fg.id = fi.feed_id' .
						' WHERE (c.'.$this->state.' = 1 OR c.'.$this->state.' = 0)' .
						($this->params->get('ignore') ? ' AND c.id NOT IN ('.$this->params->get('ignore').')' : '' ).
						' GROUP BY title' .
						' HAVING ( COUNT(*) > 1 )';
			return '('.$query.')';
		} else {
			$query = 	'SELECT id' .
						' FROM ' . $this->table .
						' WHERE '. $type .' = '. $this->_db->Quote($string) .
						' AND ('.$this->state.' = 1 OR '.$this->state.' = 0)';
		}
		$this->_db->setQuery( $query );
		
		return $this->_db->loadResult();
	}
	
	function getSectionList(&$fgParams)
	{
		$options[] = JHTML::_('select.option', $this->sectionid, '- '.JText::_('Select Kunena Category Below').' -', 'id', 'title');
		
		return $options;
	}
	
	function getCategoryList(&$fgParams)
	{
		//Kunena categories
		$query = 	'SELECT id, name AS title' .
					' FROM #__kunena_categories' .
					' WHERE published = 1' .
					' ORDER BY ordering';
		$this->_db->setQuery( $query );
		$categories = $this->_db->loadObjectList();

	 	if(!$categories) {
			$options = array(JHTML::_('select.option', -1, JText::_( 'Kunena is not installed' ), 'id', 'title'));
	 	} else {
			$options = array(JHTML::_('select.option', -1, JText::_( 'Select Kunena Category' ), 'id', 'title'));
			$options = array_merge( $options, $categories );
	 	}
	 	
	 	return $options;
	}
	
	function getCatSelectLists($filter,&$fgParams)
	{
		$this->getData(); // ensure plugin data loaded
		$prefix = $this->data->id.'_';
		
		$categories[] = JHTML::_('select.option', $prefix.'0', '- '.JText::_('Select Kunena Category').' -');
		
		// get list of categories for dropdown filter
		$query = 'SELECT CONCAT(\''.$prefix.'\', cc.id) AS value, cc.name AS text, \''.$this->sectionid.'\' as section' .
				' FROM #__kunena_categories AS cc' .
				$filter . // this is null except for Joomla sections
				' ORDER BY cc.ordering';
		$this->_db->setQuery($query);
		$categories = array_merge($categories,$this->_db->loadObjectList());
		
		return $categories;
	}
	
	function getSecSelectLists(&$fgParams)
	{
		$this->getData(); // ensure plugin data loaded

		$sections[] = JHTML::_('select.option', $this->sectionid, '- '.JText::_('Kunena Section').' -', 'value', 'text');

		return $sections;
	}
	
	function getFieldNames(&$content)
	{		
		//consider expanding this to contain parent categories also
		$query = "SELECT name FROM #__kunena_categories WHERE id = ". $this->_db->Quote($content['catid']);
		$this->_db->setQuery( $query );
		
		return $this->_db->loadResult();	
	}
	
	function getSectionCategories(&$fgParams)
	{
		// needs to have key set as the -ve plugin id if there are no sections
		return array($this->sectionid => $this->getCategoryList($fgParams));
	}
	
	function save(&$content,&$fgParams)
	{
		require_once(JPATH_ADMINISTRATOR . DS .'components'.DS.'com_kunena'.DS.'api.php');
		require_once (JPATH_ROOT.DS.'components'.DS.'com_kunena'.DS.'lib'.DS.'kunena.posting.class.php');
		if($autoTweet = file_exists(JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'autotweetkunena.php')) {
			require_once (JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'autotweetkunena.php');
		}
		$app = &JFactory::getApplication();
		$my = &JFactory::getUser($content['created_by']);
		
		kimport('session');
		$kunena_session = KunenaSession::getInstance(true,$content['created_by']); // start a session for the "user"
		
		$dispatcher = &JDispatcher::getInstance();
		JPluginHelper::importPlugin('system');

		$fields ['name'] = 'FG';
		$fields ['email'] = $my->get('email');
		$fields ['subject'] = $content['title'];
		$fields ['message'] = $content['fulltext'] ? $this->html2bb($content['fulltext']) : $this->html2bb($content['introtext']);
		$fields ['message'] = html_entity_decode($fields ['message'], ENT_QUOTES, 'UTF-8');
		$fields ['topic_emoticon'] = JRequest::getInt ( 'topic_emoticon', null );
	
		$message = new CKunenaPosting ($content['created_by']);
		if($message->post ( $content['catid'], $fields, $options = array())) {
			if($message->save ()) {
				FeedgatorHelper::saveImport($fgParams->get('hash'),$fgParams->get('id'),$message->get('id'),$this->extension,$fgParams);
				if($autoTweet) {
					$plug = new plgSystemAutotweetKunena($dispatcher,array());
					JRequest::setVar('option','com_kunena');
					JRequest::setVar('func', 'post');
					JRequest::setVar('do', 'approve');
					JRequest::setVar('id', $message->get('id'));
					$plug->onAfterRoute();
					JRequest::setVar('option','');
				}		
				return true;
			}
		}
		return false;	
	}
	
	function reorder($catid)
	{
		return true;
	}
	
	function _buildWhere(&$where)
	{
		$where = str_replace(array('AND c.state = -1','AND c.state = 0'),'',$where);
		$where = str_replace('c.state = -2','c.hold = 1',$where); // deleted
		$where = str_replace(array('c.state != -2','c.state = 1'),'c.hold = 0',$where);
		$where ? $where .= ' AND fg.content_type = '.$this->_db->Quote($this->extension) : $where = 'WHERE fg.content_type = '.$this->_db->Quote($this->extension);
	}

	function html2bb($data)
	{
		$this->getParams();
		$size = $this->params->get('image_size') ? ' size='.$this->params->get('image_size') : '';
		
		$data = str_replace(array('<br />','<br>'), "\n", $data);
		$data = str_replace(array('</p>'), "\n\n", $data);

		$bbcode = array(
		       '[b]$1[/b]',
		       '[b]$1[/b]',
		       '[i]$1[/i]',
		       '[i]$1[/i]',
		       '[u]$1[/u]',
		       '[img'. $size .']$1[/img]',
		       '[url=$1]$2[/url]'
		       );
		
		$html = array(
		       '#<strong>(.*?)<\/strong>#is',
		       '#<b>(.*?)<\/b>#is',
		       '#<em>(.*?)<\/em>#is',
		       '#<i>(.*?)<\/i>#is',
		       '#<u>(.*?)<\/u>#is',
		       '#<img.*?src\="([^?^"]*)[^/]*\/>#is', //Kunena cannot support images with query strings in SRC so removed here
		       '#<a.*?href\="([^"]*)[^>]*>([^<]*)<\/a>#is'
		       );
		
		return strip_tags(preg_replace($html, $bbcode, $data));
	}
}
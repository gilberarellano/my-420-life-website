<?php/*** FeedGator - FLEXIcontent plugin* @version 1.6.5* @package Feedgator* @author Orchid One Software Studio LLC / Trafalgar Design Ltd with code from Manuel Dannan* @email admin@orchid1software.com* @copyright (C) 2010 Orchid One Software Studio LLC - All rights reserved* @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html***//*** FeedGator Content Importing Plugin for Joomla Content
* @version 1.6.2
* @package FeedGator* @author Matt Faulds* @email mattfaulds@gmail.com* @copyright (C) 2010 Matthew Faulds - All rights reserved* @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html***/// Check to ensure this file is included in Joomla!defined('_JEXEC') or die('Restricted access');JTable::addIncludePath(JPATH_BASE.DS.'components'.DS.'com_flexicontent'.DS.'tables');

class FeedgatorPluginFlexicontent{	// Title for use in menus etc
	var $title = 'Flexicontent';		// Name of extension that plugin enables Feed Gator to save to
	var $extension = 'com_flexicontent';	// DB table to above extension
	var	$table = '#__content';
	// Name for 'published' column in content items - com_content uses 'state'
	var $state = 'state';	// Object containing plugin data - id,extension,published,params(INI)
	var $data = null;
	// Name for section column in content items or alias for section
	var $section = 's.title';
	// Section ID over-ride for content components without sections
	var $sectionid = null;   	// JParameter object with plugin parameters
    var $params = null;
 	
 	var $_flexiparams = null;
 	var $_flexisection = null;
 	var $_db = null;
 	var $_itemStatus = null;
 	var $feedId = null;    	function __construct()    {        $this->model = &JModel::getInstance('Plugin','FeedgatorModel');        $this->model->setExt($this->extension);
		$this->_db = &JFactory::getDBO();
    }		function setData($data)	{		$this->data = $data;		
//		$this->sectionid = -1*$this->data->id;
		$this->_flexiparams = &JComponentHelper::getParams( 'com_flexicontent' );
		$this->_flexisection = $this->_flexiparams->get('flexi_section');
	}
	
	function &getData()
	{
		if(!$this->data) {
			$this->model->setExt($this->extension);
			$this->setData($this->model->getPluginData());
		}
		return $this->data;
	}
	
	function &getParams($feedId = -1) // defaults to the global plugin parameters
	{
		if(!$this->params) {
			$this->params = new JParameter( $this->model->getParams($feedId), $this->model->getXmlPath() );
		}
		return $this->params;
	}
	
	function componentCheck() // method to check if component is installed
	{		$components = JComponentHelper::_load();
		return (boolean)isset($components[$this->extension]);
	}
	
	function countContentItems($where)
	{
		$this->_buildWhere($where);
		// Get the total number of records specified by where clause
		$query = '(SELECT COUNT(*)' .
				' FROM ' . $this->table .' AS c' .
				' LEFT JOIN #__categories AS cc ON cc.id = c.catid' .
				' LEFT JOIN #__sections AS s ON s.id = c.sectionid' .
				' LEFT JOIN #__feedgator_imports AS fi ON fi.content_id = c.id' .
				' LEFT JOIN #__feedgator AS fg ON fg.id = fi.feed_id' .
				$where.')';
		return $query;
	}
	
	function countContentQuery()
	{
		// Get the total number of records in range (added later)
		$query = 'SELECT COUNT(*)' .
				' FROM ' . $this->table .
				' WHERE id IN (%s)';
		return $query;
	}
	
	function getContentItem($id)
	{
		$query = 	'SELECT *' .
					' FROM ' . $this->table .
					' WHERE id = '. $this->_db->Quote($id);
		$this->_db->setQuery( $query );
		if(!$content = $this->_db->loadAssoc()) {
			return false;
		}
		
		return $content;
	}
	
	function getContentLink($id)
	{
	    return JRoute::_( 'index.php?option='.$this->extension.'&controller=items&task=edit&cid[]='. $id );
	}
	
	function getContentItemsQuery($where)
	{
		$this->_buildWhere($where);
		// Get the articles
		$query = '(SELECT c.id AS id, c.title AS title, c.'.$this->state.' AS state, c.created AS created, c.ordering AS ordering,c.sectionid AS sectionid, c.catid AS catid,c.publish_up AS publish_up,c.publish_down AS publish_down,c.created_by_alias AS created_by_alias,c.created_by AS created_by,c.access AS access,c.checked_out AS checked_out, g.name AS groupname, cc.title AS cat_name, u.name AS editor, f.content_id AS frontpage, '.$this->section.' AS section_name, v.name AS author, fi.feed_id AS feedid, fg.title AS feed_title, fg.content_type AS content_type' .
				' FROM ' . $this->table . ' AS c' .
				' LEFT JOIN #__categories AS cc ON cc.id = c.catid' .
				' LEFT JOIN #__sections AS s ON s.id = c.sectionid' .
				' LEFT JOIN #__groups AS g ON g.id = c.access' .
				' LEFT JOIN #__users AS u ON u.id = c.checked_out' .
				' LEFT JOIN #__users AS v ON v.id = c.created_by' .
				' LEFT JOIN #__content_frontpage AS f ON f.content_id = c.id' .
				' LEFT JOIN #__feedgator_imports AS fi ON fi.content_id = c.id' .
				' LEFT JOIN #__feedgator AS fg ON fg.id = fi.feed_id' .
				$where.')';
					
		return $query;
	}
	
	function getFeedItems($where)
	{
		$this->_buildWhere($where);
		$query =	'SELECT fg.*, cc.title AS cat_name, '.$this->section.' AS section_name, u.name AS editor FROM #__feedgator fg'.
					' LEFT JOIN #__categories AS cc ON cc.id = fg.catid' .
					' LEFT JOIN #__sections AS s ON s.id = fg.sectionid' .
					' LEFT JOIN #__users AS u ON u.id = fg.checked_out '.
					$where;
		$this->_db->setQuery($query);
		
		return $this->_db->loadObjectList(); 
	}
	
	function findDuplicates($type,$string)
	{
		// type can be id, alias, title or internal
		if($type == 'internal') { // for com_content we use the alias to find internal duplicates
			$this->getParams();
			$query =	'SELECT '. $this->_db->Quote($this->extension) .' AS content_type, fg.title AS feed_title,c.title,c.alias,COUNT(*) AS num,' .
						' GROUP_CONCAT(CONCAT_WS(\'|\',CONVERT(c.id,CHAR(11)),CONVERT(c.sectionid,CHAR(11)),CONVERT(c.catid,CHAR(11)),c.title) ORDER BY c.id ASC SEPARATOR \'||\') AS results' .
						' FROM ' . $this->table . ' AS c' .
						' INNER JOIN #__feedgator_imports AS fi ON fi.content_id = c.id AND fi.plugin = '. $this->_db->Quote($this->extension) .
						' INNER JOIN #__feedgator AS fg ON fg.id = fi.feed_id' .
						' WHERE (c.'.$this->state.' = 1 OR c.'.$this->state.' = 0)' .
						($this->params->get('ignore') ? ' AND c.id NOT IN ('.$this->params->get('ignore').')' : '' ).
						' GROUP BY alias' .
						' HAVING ( COUNT(*) > 1 )';
			return '('.$query.')';
		} else {
			$query = 	'SELECT id' .
						' FROM ' . $this->table .
						' WHERE '. $type .' = '. $this->_db->Quote($string) .
						' AND ('.$this->state.' = 1 OR '.$this->state.' = 0)';
			$this->_db->setQuery( $query );
			return $this->_db->loadResult();
		}
	}
		function getSectionList(&$fgParams)	{		$query = 	'SELECT s.id, s.title ' .					'FROM #__sections AS s WHERE s.id = ' . "'$this->_flexisection'" . 					'ORDER BY s.ordering';		$this->_db->setQuery( $query );		$sections = $this->_db->loadObjectList();	//	$options[] = JHTML::_('select.option', -1, '- '.JText::_('Select FLEXIcontent Section').' -', 'id', 'title');	//	$options = array_merge( $options, $sections );		return $sections;	}		function getCategoryList(&$fgParams)	{		$sectionid = $fgParams->get('sectionid');				if ( $sectionid == 0 ) {			$where = 'WHERE section NOT LIKE \'%com_%\' ';		} else {			$where = 'WHERE section = '.(int)$sectionid.' ';		}		//Joomla categories		$query = 	'SELECT id, title ' .					'FROM #__categories ' .					$where .					'ORDER BY ordering';		$this->_db->setQuery( $query );		$categories = $this->_db->loadObjectList();				if($sectionid == 0) {			$options[] = JHTML::_('select.option', -1, JText::_( 'Select Joomla Category' ), 'id', 'title');		} else {									$options = array();		}				$options = array_merge( $options, $categories );				return $options;	}
	
	function getCatSelectLists($filter,&$fgParams)
	{
		$this->getData(); // ensure plugin loaded
		$prefix = $this->data->id.'_';

		$categories[] = JHTML::_('select.option', $prefix.'0', '- '.JText::_('Select Joomla Category').' -');

		// get list of categories for dropdown filter
		$query = 'SELECT CONCAT(\''.$prefix.'\', cc.id) AS value, cc.title AS text, section' .
				' FROM #__categories AS cc' .
				' INNER JOIN #__sections AS s ON s.id = cc.section ' .
				$filter . // this is null except for Joomla sections
				' ORDER BY s.ordering, cc.ordering';
		$this->_db->setQuery($query);
		$categories = array_merge($categories,$this->_db->loadObjectList());

		return $categories;
	}
	
	function getSecSelectLists(&$fgParams)
	{
		$this->getData(); // ensure plugin loaded

		$sections[] = JHTML::_('select.option',  $this->_flexisection, '- '. JText::_( 'Flexicontent Section' ) .' -' );

		return $sections;
	}
		function getFieldNames(&$content)	{				$query = 	"SELECT CONCAT_WS(', ', s.title, c.title) " .					"FROM #__categories AS c " .					"LEFT JOIN #__sections AS s ON s.id = c.section " .					"WHERE c.id = ". $this->_db->Quote($content['catid']);		$this->_db->setQuery( $query );				return $this->_db->loadResult();	}		function getSectionCategories(&$fgParams)	{				$sectionid = $fgParams->get('sectionid');				if ( $sectionid == 0 ) {			$where = 'WHERE section NOT LIKE \'%com_%\' ';		} else {			$where = 'WHERE section = '.(int)$sectionid.' ';		}		$query = 	'SELECT s.id, s.title ' .					'FROM #__sections AS s ' .					'ORDER BY s.ordering';		$this->_db->setQuery( $query );		$sections = $this->_db->loadObjectList();		$sectioncategories 			= array();		$sectioncategories[-1] 		= array();		$sectioncategories[-1][] = JHTML::_('select.option', -1, JText::_( 'Select Category' ), 'id', 'title');				foreach($sections as $section) {			$sectioncategories[$section->id] = array();			$query = 	'SELECT id, title ' .						'FROM #__categories ' .						'WHERE section = '.(int)$section->id .' '.						'ORDER BY ordering';			$this->_db->setQuery( $query );			$rows2 = $this->_db->loadObjectList();			foreach($rows2 as $row2) {				$sectioncategories[$section->id][] = JHTML::_('select.option', $row2->id, $row2->title, 'id', 'title');			}		}				return $sectioncategories;	}
		function save(&$content,&$fgParams)	{
		$app = &JFactory::getApplication();		$user = &JFactory::getUser();
		$dispatcher = &JDispatcher::getInstance();
		JPluginHelper::importPlugin('system');
		JPluginHelper::importPlugin('content');

// <--- Start of FlexiContent (hybrid) code ---> //

		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_flexicontent'.DS.'models'.DS.'item.php');		$flexiModel = JModel::getInstance('Item','FlexicontentModel');		
		$item  	= &$flexiModel->getTable('flexicontent_items', '');
		
//		$details		= JRequest::getVar( 'details', array(), 'post', 'array');
//		$tags 			= JRequest::getVar( 'tag', array(), 'post', 'array');
//		$cats 			= JRequest::getVar( 'cid', array(), 'post', 'array');
//		$post 			= JRequest::get( 'post', JREQUEST_ALLOWRAW );
//		$post['vstate'] = (int)$post['vstate'];

		// bind it to the table
		if (!$item->bind($content)) {
			$content['mosMsg'] = $title . '***ERROR: bind' . $this->_db->getErrorMsg();			return false;
		}
//		$item->bind($details);

		// sanitise id field
		$item->id = (int) $item->id;
		$isNew = ($row->id < 1);

		$this->getParams($fgParams->get('id'));		$item->type_id = $this->params->get('flexiType',1);
		
		//set to hide introtext when viewing full article unless only making introtext	
		$item->attribs = 'show_intro=0';	
		if($fgParams->get('onlyintro')) {
			$item->attribs = '';
		}

		$item->ordering = $item->getNextOrder();

		$use_versioning = $this->_flexiparams->get('use_versioning', 1);
		$item->version = 1;
		
		$item->search_index = $this->_db->Quote($item->title.' | '. ((JString::strlen($item->fulltext) > 1) ? $item->introtext . '<hr id="system-readmore" />' . $item->fulltext : $item->introtext) );

		// set language
		$languages =& JComponentHelper::getParams('com_languages');        $item->language = $languages->get('site', 'en-GB');
				
		$mdate =& JFactory::getDate();
		$item->modified 	= $mdate->toMySQL();
		$item->modified_by 	= (int)$user->id;
			
		// Make sure the data is valid
		/*if (!$item->check()) {
			$e = '';			foreach ($item->getErrors() as $error) {				$e .= $error.'<br/>';			}			$content['mosMsg'] = '***ERROR*(check)*  Feed - '.$content['title'].':' . $this->_db->getErrorMsg().'<br/>'.$e;
			return false;
		}*/
		
		//Trigger OnBeforeContentSave
		$result = $dispatcher->trigger('onBeforeContentSave', array(&$item, $isNew));
		
		// Store it in the db
		if (!$item->store()) {
			$content['mosMsg'] = $item->title . '***ERROR:' . $this->_db->stderr();			return false;
		}

		$content['id'] = $item->id;
		
//		if (FLEXI_ACCESS) {
//			FAccess::saveaccess( $item, 'item' );
//		}

		$flexiModel->_item	= &$item;

//		$tags = array_unique($tags); ? use keywords (metakeys)
//		foreach($tags as $tag)
//		{
//			$query = 'INSERT INTO #__flexicontent_tags_item_relations (`tid`, `itemid`) VALUES(' . $tag . ',' . $item->id . ')';
//			$this->_db->setQuery($query);
//			$this->_db->query();
//		}

		// Store categories to item relations
		$query 	= 'INSERT INTO #__flexicontent_cats_item_relations (`catid`, `itemid`)'
				.' VALUES(' . $content['catid'] . ',' . $item->id . ')'
				;
		$this->_db->setQuery($query);
		$this->_db->query();

		
//		$post['categories'][0] = $cats;
//		$post['tags'][0] = $tags;

		///////////////////////////////
		// store extra fields values //
		///////////////////////////////
		
		// get the field object
		$flexiModel->_id = $item->id;	
		$fields		= $flexiModel->getExtrafields();
//		$dispatcher = & JDispatcher::getInstance();
		
		// NOTE: This event isn't used yet but may be useful in a near future
//		$results = $dispatcher->trigger('onAfterSaveItem', array( $item ));
		
		// versioning backup procedure
		// first see if versioning feature is enabled
		if ($use_versioning) {
			$v = new stdClass();
			$v->item_id 		= (int)$item->id;
			$v->version_id		= (int)$version+1;
			$v->modified		= $item->modified;
			$v->modified_by		= $item->modified_by;
			$v->created 		= $item->created;
			$v->created_by 		= $item->created_by;
		}
		
	// I'm not familiar with fields so not sure if they are useful. Left the code for you to look at...
	/*	if ($fields) {
			$files	= JRequest::get( 'files', JREQUEST_ALLOWRAW );
			$files = null;
			$searchindex = '';
			$jcorefields = flexicontent_html::getJCoreFields();
			foreach($fields as $field) {
				// process field mambots onBeforeSaveField
				$results = $app->triggerEvent('onBeforeSaveField', array( $field, &$post[$field->name], &$files[$field->name] ));

				// add the new values to the database 
				if (is_array($post[$field->name])) {
					$postvalues = $post[$field->name];
					$i = 1;
					foreach ($postvalues as $postvalue) {
						$obj = new stdClass();
						$obj->field_id 		= $field->id;
						$obj->item_id 		= $item->id;
						$obj->valueorder	= $i;
						$obj->version		= (int)$version+1;
						// @TODO : move to the plugin code
						if (is_array($postvalue)) {
							$obj->value			= serialize($postvalue);
						} else {
							$obj->value			= $postvalue;
						}
						if ($use_versioning)
							$this->_db->insertObject('#__flexicontent_items_versions', $obj);
						if(
							($isnew || ($post['vstate']==2) )
							&& !isset($jcorefields[$field->name])
							&& !in_array($field->field_type, $jcorefields)
							&& ( ($field->field_type!='categories') || ($field->name!='categories') )
							&& ( ($field->field_type!='tags') || ($field->name!='tags') )
						) {
							unset($obj->version);
							$this->_db->insertObject('#__flexicontent_fields_item_relations', $obj);
						}
						$i++;
					}
				} else if ($post[$field->name]) {
					//not versionning hits field => Fix this issue 18 http://code.google.com/p/flexicontent/issues/detail?id=18
					if ($field->id != 7) {
						$obj = new stdClass();
						$obj->field_id 		= $field->id;
						$obj->item_id 		= $item->id;
						$obj->valueorder	= 1;
						$obj->version		= (int)$version+1;
						// @TODO : move in the plugin code
						if (is_array($post[$field->name])) {
							$obj->value			= serialize($post[$field->name]);
						} else {
							$obj->value			= $post[$field->name];
						}
						if($use_versioning) $this->_db->insertObject('#__flexicontent_items_versions', $obj);
					}
					if(
						($isnew || ($post['vstate']==2) )
						&& !isset($jcorefields[$field->name])
						&& !in_array($field->field_type, $jcorefields)
						&& ( ($field->field_type!='categories') || ($field->name!='categories') )
						&& ( ($field->field_type!='tags') || ($field->name!='tags') )
					) {
						unset($obj->version);
						$this->_db->insertObject('#__flexicontent_fields_item_relations', $obj);
					}
				}
				// process field mambots onAfterSaveField
				$results		 = $dispatcher->trigger('onAfterSaveField', array( $field, &$post[$field->name], &$files[$field->name] ));
				$searchindex 	.= @$field->search;
			}
	
			// store the extended data if the version is approved
			$item->search_index = $searchindex;
			// Make sure the data is valid
			if (!$item->check()) {
				$e = '';				foreach ($row->getErrors() as $error) {					$e .= $error.'<br/>';				}				$content['mosMsg'] = '***ERROR*(check)*  Feed - '.$content['title'].':' . $this->_db->getErrorMsg().'<br/>'.$e;
				return false;
			}
			// Store it in the db
			if (!$item->store()) {
				$content['mosMsg'] = $title . '***ERROR:' . $this->_db->stderr();				return false;
			}
//				dump($searchindex,'search');
//				dump($item,'item');
		}*/
		if ($use_versioning) {
			if ($v->modified   != $this->_db->getNullDate()) {
				$v->created 	= $v->modified;
				$v->created_by 	= $v->modified_by;
			}
			
			$v->comment		= '';
			unset($v->modified);
			unset($v->modified_by);
			$this->_db->insertObject('#__flexicontent_versions', $v);
		}
		
// <--- End of FlexiContent (hybrid) code ---> //				// Check the article and update item order//		$row->checkin();//		$row->reorder('catid = '.(int) $row->catid.' AND state >= 0');				require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_frontpage'.DS.'tables'.DS.'frontpage.php');		$fp = new TableFrontPage($this->_db);		if ($fgParams->get('front_page')) {								// Is the item already viewable on the frontpage?			if (!$fp->load($item->id)) {				// Insert the new entry				$query = 	'INSERT INTO #__content_frontpage' .							' VALUES ( '. (int) $item->id .', 1 )';				$this->_db->setQuery($query);								if (!$this->_db->query()) {					$content['mosMsg'] = $title . '***ERROR:' . $this->_db->getErrorMsg();					return false;				}				$fp->ordering = 1;			}
			$fp->reorder();		}
		
		//Trigger OnAfterContentSave
		$dispatcher->trigger('onAfterContentSave', array(&$item, $isNew));
				$cache = & JFactory::getCache('com_content');
		$cache->clean();
		
		FeedgatorHelper::saveImport($fgParams->get('hash'),$fgParams->get('id'),$content['id'],$this->extension,$fgParams);
				
		return true;	}
	
	function _buildWhere(&$where)
	{
		if($this->state != 'state' AND strpos($where,'state') !== false) {
			$where = str_replace('state',$this->state,$where);
		}
		$where ? $where .= ' AND fg.content_type = '.$this->_db->Quote($this->extension) : $where = 'WHERE fg.content_type = '.$this->_db->Quote($this->extension);
	}}
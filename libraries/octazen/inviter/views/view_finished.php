<?php
/********************************************************************************
DO NOT EDIT THIS FILE!

Unified Inviter Component

You may not reprint or redistribute this code without permission from Octazen Solutions.

Copyright 2009 Octazen Solutions. All Rights Reserved.
WWW: http://www.octazen.com
********************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Parameters
//	oz_recipients
//		Array of contacts.
//////////////////////////////////////////////////////////////////////////
if (!defined('_OZ_INVITER')) exit();
?>
<div class="oz_header">
<div class="oz_header_label"><?php echo oz_text('FINISHED_TITLE'); ?></div>
<div class="oz_header_startagain"><a href="#" onclick="ozStartAgain2();return false;"><?php echo oz_text('START_AGAIN'); ?></a></div>
</div>
<div id="ozpanel_finished">
  <?php
$recipients = isset($_REQUEST['oz_recipients']) ? $_REQUEST['oz_recipients'] : array();
if (count($recipients)>0)
{
	echo oz_text('FINISHED_SENT_TO');
	echo '<ul>';
	foreach ($recipients as $r) 
		echo '<li>'.htmlspecialchars($r).'</li>';
	echo '</ul>';
}
else
{
	echo oz_text('FINISHED_SENT');
}

?>
  <br/>
  <br/>
  <?php echo oz_text('FINISHED_HAVE_MORE_FRIENDS'); ?> <a href="#" onclick="ozStartAgain2();return false;"><?php echo oz_text('FINISHED_INVITE_THEM'); ?></a>
</div>
<script type="text/javascript">
//<![CDATA[
ozNotifyViewChange('finished');
//]]>
</script>
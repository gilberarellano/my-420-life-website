<?php
/********************************************************************************
JomSocial 1.x contacts importer & invite module

Copyright 2009 Octazen Solutions. All Rights Reserved
You may not use, reprint or redistribute this code without permission from Octazen Solutions.
WWW: http://www.octazen.com
Version: 1.1
********************************************************************************/

global $mainframe;
$my =& CFactory::getUser();
if( $my->id == 0 )
{
	$mainframe->enqueueMessage(JText::_('CC PLEASE LOGIN'), 'error');
	return;
}
		
jimport('octazen.abimporter.abi');
jimport('octazen.inviter.ozinviter');

//True to enable detection of users who are already members of the social network)
define('_OZ_CHECK_EXISTING_USER',true);
//define('_OZ_EXCLUDE_PENDING_INVITES',true);

oz_set_config('captcha_file_path', dirname(__FILE__).DS.'inviter'.DS.'captcha');
oz_set_config('captcha_uri_path', JURI::base().'libraries/octazen/inviter/captcha');
ozi_set_config('use_add_as_friend',FALSE);

//Uncomment this line to use Joomla's mailer instead of JomSocial's mailq
//ozi_set_config('mailer', 'joomla');

//Use JomSocial's FBConnect key if available
$config =& CFactory::getConfig();
$fbkey = $config->get('fbconnectkey');
//$secret = $config->get('fbconnectsecret');
$fbkey2 = ozi_get_config('facebook_connect.api_key','');
if (!empty($fbkey) && empty($fbkey2)) {
	ozi_set_config('facebook_connect.api_key',$fbkey);
	$url = CRoute::emailLink('index.php?option=com_community&view=connect&task=receiver');
	ozi_set_config('facebook_connect.receiver_path',$url);
}


ozi_set_member_id($my->id);

//Include CSS
$respath = JURI::base().'libraries/octazen/inviter/res/';
$xhtml = '<link type="text/css" href="'.$respath.'style.css" rel="stylesheet" />';
$xhtml .= '<!--[if lte IE 6]><link type="text/css" href="'.$respath.'style_ie6.css" rel="stylesheet" /><![endif]-->';
$xhtml .= '<style type="text/css">';
//$html .= 'body, select, input, textarea {font-family:Arial, Helvetica, sans-serif;font-size: 12px;}';
$xhtml .= '#oz_inviter {border: solid 1px #999999;padding: 0px;margin: 0px; width: 600px}';
$xhtml .= '.oz_contacts_table {height: 350px;}';
$xhtml .= '#oz_inviter a {text-decoration:none; }';
$xhtml .= '#oz_inviter a:hover {text-decoration:underline}';
if (!ozi_get_config('show_branding',TRUE)) $xhtml .= '#oz_footer {display:none}';
//$html .= '.oz_logo {border: dotted 1px #D0D0D0;}';
$xhtml .= '</style>';

//Render the information block
$xhtml .= '<div id="oz_callback" style="margin:10px; padding: 10px;"></div>';
$xhtml .= '<script>';
$xhtml .= 'function ozOnViewChanged(view)';
$xhtml .= '{';
$xhtml .= '	var ele = document.getElementById("oz_callback");';
$xhtml .= '  ele.style.display="block";';
$xhtml .= '	if (view=="selector") ele.innerHTML="'.htmlentities(ozi_get_config('msg.welcome','Invite your friends to join this site! Select where your contacts are...'),ENT_QUOTES,'UTF-8').'";';
$xhtml .= '	else if (view=="upload") ele.innerHTML="'.htmlentities(ozi_get_config('msg.upload',''),ENT_QUOTES,'UTF-8').'";';
$xhtml .= '	else if (view=="manual") ele.innerHTML="'.htmlentities(ozi_get_config('msg.manual',''),ENT_QUOTES,'UTF-8').'";';
$xhtml .= '	else if (view=="login") ele.innerHTML="'.htmlentities(ozi_get_config('msg.login',''),ENT_QUOTES,'UTF-8').'";';
$xhtml .= '	else if (view=="contacts") ele.innerHTML="'.htmlentities(ozi_get_config('msg.contacts',''),ENT_QUOTES,'UTF-8').'";';
$xhtml .= '	else if (view=="captcha") ele.innerHTML="'.htmlentities(ozi_get_config('msg.captcha',''),ENT_QUOTES,'UTF-8').'";';
$xhtml .= '	else if (view=="bookmark") ele.innerHTML="'.htmlentities(ozi_get_config('msg.bookmark',''),ENT_QUOTES,'UTF-8').'";';
$xhtml .= '	else if (view=="finished") ele.innerHTML="'.htmlentities(ozi_get_config('msg.finished',''),ENT_QUOTES,'UTF-8').'";';
$xhtml .= '	else ele.style.display="none";';
$xhtml .= '}';
$xhtml .= '</script>';
$xhtml .= '<div id="oz_inviter">';
$xhtml .= oz_render_inviter($respath);
$xhtml .= '<div id="oz_footer">Powered by <a href="http://www.octazen.com" target="_blank">Octazen Solutions Invite Engine</a></div>';
$xhtml .= '</div>';

echo $xhtml;



/*
// Process the Suggest Friends
// Load required filterbar library that will be used to display the filtering and sorting.
CFactory::load( 'libraries' , 'filterbar' );
$id			= JRequest::getCmd('userid', $my->id);
$user		=& CFactory::getUser($id);
$sorted		= JRequest::getVar( 'sort' , 'suggestion' , 'GET' );
$filter		= JRequest::getVar( 'filter' , 'suggestion' , 'GET' );
$friends 	=& CFactory::getModel( 'friends' );

$rows 		= $friends->getFriends( $id , $sorted , true , $filter );
$resultRows = array();
*/
/*	
foreach($rows as $row)
{
	$user =& CFactory::getUser($row->id);
	$obj = clone($row);
	$obj->friendsCount  = $user->getFriendCount(); 
	$obj->profileLink	= cUserLink($row->id);
	$obj->isFriend		= true;
	$resultRows[] = $obj;
}
unset($rows);
// Finished Process the Suggest Friends

*/	


/////////////////////////////////////////////////////////////
//FRIEND SUGGESTION BEGIN
/////////////////////////////////////////////////////////////
$friends =& $tmpl->vars['friends'];
//echo '<pre>.htmlentities(var_export($friends,true)).'</pre>';
?>
<br /><br /><br />
<div class="suggest-friends">
	<?php if( !empty( $friends ) ) : ?>
	<h3><?php echo JText::_('CC FRIEND SUGGESTION'); ?></h3>
		<?php foreach( $friends as $user ) : ?>
	<div class="mini-profile">
		<div class="mini-profile-avatar">
			<a href="<?php echo $user->profileLink; ?>">
				<img class="avatar" src="<?php echo $user->getThumbAvatar(); ?>" alt="<?php echo $user->getDisplayName(); ?>" />
			</a>
		</div>
		<div class="mini-profile-details">
			<h3 class="name">
				<a href="<?php echo $user->profileLink; ?>"><strong><?php echo $user->getDisplayName(); ?></strong></a>
			</h3>
		
			<div class="status">
				<?php echo $user->getStatus() ;?>
				<!--
				<a class="remove" onclick="if(!confirm('<?php echo JText::_('CC CONFIRM DELETE FRIEND'); ?>'))return false;" href="<?php echo CRoute::_('index.php?option=com_community&view=friends&task=remove&fid='.$user->id); ?>">
					<?php echo JText::_('CC REMOVE'); ?>
				</a>
				-->
			</div>
	
			<div class="icons">
				<!--
			    <span class="icon-group">
			    	<a href="<?php echo CRoute::_('index.php?option=com_community&view=friends&userid=' . $user->id );?>"><?php echo JText::sprintf( (cIsPlural($user->friendsCount)) ? 'CC FRIENDS COUNT MANY' : 'CC FRIENDS COUNT' , $user->friendsCount);?></a>
			    </span>
			    -->
			    <?php //if(!$isFriend && !$isMine) { ?>
			    <span class="btn-add-friend">
					<a class="icon-add-friend" href="javascript:void(0)" onclick="joms.friends.connect('<?php echo $user->id;?>')"><span><?php echo JText::_('CC ADD AS FRIEND'); ?></span></a>
				</span>
				<?php //} ?>
			    
			    <?php if($user->isOnline()): ?>
				<span class="icon-online">
			    	<?php echo JText::_('CC ONLINE'); ?>
			    </span>
			    <?php else: ?>
			    <span class="icon-offline">
			    	<?php echo JText::_('CC OFFLINE'); ?>
			    </span>
			    <?php endif; ?>
			    
				<?php if( $my->id != $user->id && $config->get('enablepm') ): ?>
		        <span class="icon-write">
		            <a onclick="joms.messaging.loadComposeWindow(<?php echo $user->id; ?>)" href="javascript:void(0);">
		            <?php echo JText::_('CC WRITE MESSAGE'); ?>
		            </a>
		        </span>
		        <?php endif; ?>
		        
			</div>
		</div>
		<div class="clr"></div>
	</div>
	
		<?php endforeach; ?>
	<?php endif; ?>
</div>


<div style="clear: both;"></div>	
<?php
/////////////////////////////////////////////////////////////
//FRIEND SUGGESTION END
/////////////////////////////////////////////////////////////


function oz_jomsocial_build_profile_url($memberid,$inviteurl=true) {
	$url = CRoute::emailLink('index.php?option=com_community&view=profile&userid='.$memberid.($inviteurl?'&invite='.$memberid:''));

	//$url = trim(JURI::base(),'/').CRoute::_('index.php?option=com_community&view=profile&userid='.$memberid.($inviteurl?'&invite='.$memberid:''),false);
	//$url = JURI::base().'index.php?option=com_community&view=profile&userid='.$memberid.($inviteurl?'&invite='.$memberid:'');
	return $url;
}


function oz_add_friends ($member_ids) {

	$model =& CFactory::getModel('friends');
	$my =& CFactory::getUser();
	foreach ($member_ids as $mid) {
		$model->addFriend($mid, $my->id);
	}
}


//--------------------------------------------------
//Generate the invite message. You can modify this 
//to generate messages with personalized links, etc
//--------------------------------------------------
function oz_get_invite_message ($from_name=NULL,$from_email=NULL,$personal_message=NULL)
{
	$my =& CFactory::getUser();
	$config =& CFactory::getConfig();
	
	//You can obtain your current avatar image here (useful only if you're not using JomSocial's mailq, as it does not support HTML mails) 
	//$my =& CFactory::getUser();
	//$avatarModel =& CFactory::getModel('avatar');
	//$imageurl = $avatarModel->getLargeImg($my->id); 
	// OR
	//$imageurl = $avatarModel->getSmallImg($my->id);
	
	$subject = ozi_get_config('subject');
	$text_body = ozi_get_config('text_body');
	$html_body = ozi_get_config('html_body');
	$fbml_body = ozi_get_config('fbml_body');
	$invite_url = ozi_get_config('invite_url');
	if (empty($invite_url)) $invite_url = oz_jomsocial_build_profile_url($my->id);
	if (empty($subject)) {
		//Some version of JomSocial seems to have the subject taking in only 1 parameter, some 2
		$subject = JText::_('CC INVITE EMAIL SUBJECT');
		if (strpos($subject,'%2$s')===FALSE) $subject = JText::sprintf('CC INVITE EMAIL SUBJECT', $config->get('sitename') );
		else $subject = JText::sprintf('CC INVITE EMAIL SUBJECT', $my->getDisplayName(), $config->get('sitename') );
	}
	if (empty($text_body)) {
		$text_body = JText::sprintf('CC INVITE EMAIL MESSAGE', $my->getDisplayName() , $config->get('sitename') );
		$text_body .= "\r\n";
		$text_body .= empty($personal_message) ? '' : $personal_message;
		$text_body .= "\r\n";
		$text_body .= "---------------------------------------------------\r\n";
		$text_body .= JText::_('CC INVITE EMAIL MESSAGE VIEW FRINEDS').' '.$invite_url;
	}
	if (empty($html_body)) $html_body = nl2br(htmlentities($text_body,ENT_COMPAT,'UTF-8'));
	if (empty($fbml_body)) $fbml_body = htmlentities($text_body,ENT_COMPAT,'UTF-8');
	$sa = array('{PERSONALMESSAGE}','{URL}','{USERNAME}');
	$name = $my->getDisplayName();
	$subject = str_replace($sa,array($personal_message,$invite_url,$name),$subject);
	$text_body = str_replace($sa,array($personal_message,$invite_url,$name),$text_body);
	$html_body = str_replace($sa,array(htmlentities($personal_message,ENT_COMPAT,'UTF-8'),htmlentities($invite_url,ENT_COMPAT,'UTF-8'),htmlentities($name,ENT_COMPAT,'UTF-8')),$html_body);
	$fbml_body = str_replace($sa,array(htmlentities($personal_message,ENT_COMPAT,'UTF-8'),htmlentities($invite_url,ENT_COMPAT,'UTF-8'),htmlentities($name,ENT_COMPAT,'UTF-8')),$fbml_body);
	$msg = array(
		'subject'=>$subject,
		'text_body'=>$text_body,
		'html_body'=>$html_body,
		'fbml_body'=>$fbml_body,
		'url'=>$invite_url
	);

	return $msg;
}



//--------------------------------------------------
//Send email to multiple recipients. 
//
//You customize this if your web framework already 
//provides mechanisms for sending invitation emails.
//--------------------------------------------------
function oz_send_emails ($from_name,$from_email,&$contacts, $personal_message=NULL)
{
	$msg = oz_get_invite_message($from_name,$from_email,$personal_message);

	$mailer = ozi_get_config('mailer');
	if (empty($mailer)) {
		$mailqModel =& CFactory::getModel( 'mailq' );
		foreach ($contacts as $c) {
			$mailqModel->add( $c['email'], $msg['subject'], $msg['text_body']);
	//		echo '<hr>EMAIL TO '.$c['email'].', subject='.$msg['subject'].', msg='.$msg['text_body'].'<br/>';
		}
	}
	elseif ($mailer=='joomla') {
		$subject = $msg['subject'];
		$text_body = $msg['text_body'];
		$html_body = $msg['html_body'];
		global $mainframe;
		if (empty($from_name)) $from_name = ozi_get_config('from_name','');
		if (empty($from_email)) $from_email = ozi_get_config('from_email','');
		if (empty($from_name)) $from_name = $mainframe->getCfg('fromname');
		if (empty($from_email)) $from_email = $mainframe->getCfg('mailfrom');
		if ($from_name=='{OWNER_EMAIL}') $from_name = ozi_get_owner_email();
		if ($from_email=='{OWNER_EMAIL}') $from_email = ozi_get_owner_email();		
		
		foreach ($contacts as $c) {
			$mailer =& JFactory::getMailer();
			// Build e-mail message format
			$mailer->setSender(array($from_email,$from_name));
			$mailer->setSubject($subject); 
			$mailer->setBody($html_body);
			$mailer->IsHTML(true);
			$email = $c['email'];
			//$name = isset($c['name']) ? $c['name'] : '';
			$mailer->addRecipient($email);
			$res = $mailer->Send();
		}
	}
	else {
		oz_inviter_sendmail ($from_name,$from_email,$contacts, $msg['subject'], $msg['text_body'], $msg['html_body']);
	}
	
	if (count($contacts) > 0) {
		//add user points
		CFactory::load( 'libraries' , 'userpoints' );		
		CUserPoints::assignPoint('friends.invite');	
	}


}

//Sort to show existing members first, followed by normal email contacts
function ozi_compare_contacts(&$c1, &$c2) 
{
	//Sort by existing members first, followed by name
	if (isset($c1['member_id'])) {
		if (isset($c2['member_id'])) {
			$k1 = $c1['member_id'];
			$k2 = $c2['member_id'];
		 	return strcasecmp($k1,$k2);
		}
		else {
			return -1;
		}
	}
	else {
		if (isset($c2['member_id'])) {
			return 1;
		}
		else {
			$k1 = (isset($c1['name']) ? $c1['name'] : '').'_';
			$k2 = (isset($c2['name']) ? $c2['name'] : '').'_';
			return strcasecmp($k1,$k2);
		}
	}
}


//--------------------------------------------------
//Run through imported contacts list and perform any filtration if required.
//
//$contacts is a reference to an array of Contacts, where each Contact is an associative array containing the following associations:
//
//	'name' => Name of the contact (may be blank)
//	'email' => Email address of the contact (only for email contacts)
//	'id' => An identifier for the contact (email address or social network user ID)
//	'uid' => Social network user ID of the contact (optional)
//	'image' => Absolute url to the thumbnail image of the contact (optional)
//
//This function can then perform the following
//	1) Remove/inject contacts into $contacts
//	2) For each contact, inject special attributes, as follows
//
//		'x-nocheckbox' => If set, disables the ability for the user to select the contact. Must be set to true if hyperlinks are present in contact row.
//		'x-namehtml' => If set, this is html snippet used in place of the name for display. You may modify the html code to generate hyperlinks, etc.
//		'x-emailhtml' => If set, this is html snippet used in place of the email for display. 
//
//This is useful in cases where the contact is already a member of the website, and we would prefer to provide
//the user the option to add the contact as a friend rather than sending an invitation emai.
//--------------------------------------------------
function oz_filter_contacts (&$contacts)
{
	$my =& CFactory::getUser();
	$db =& JFactory::getDBO();
	
	//Get friend list of current user
	$sql = "SELECT `connect_to` FROM #__community_connection where `connect_from`=".$db->Quote($my->id);
	$db->setQuery($sql);
	$result = $db->loadResultArray();
	if($db->getErrorNum()) JError::raiseError( 500, $db->stderr());
	$friendids = array();
	foreach ($result as $r) $friendids[$r]=TRUE;
	

	$avatarModel =& CFactory::getModel('avatar');
	
	$cl = array();
	foreach($contacts as $contact) {
		if (isset($contact['uid'])) {
			//It's a social network contact
			$cl[] = $contact;
		}
		else {
			//It's an email contact
			$e = trim(strtolower($contact['email']));
			
			//Is it an existing user?
			if (_OZ_CHECK_EXISTING_USER) {
				$userModel =& CFactory::getModel('user');
				
//				$sql = "SELECT `id`,`name` FROM #__users WHERE `email`=".$db->Quote($e);
				$sql = "SELECT `id`,`name`,`avatar`,`thumb` FROM #__users LEFT JOIN #__community_users ON id=userid WHERE `email`=".$db->Quote($e);
				$db->setQuery($sql);
				$result = $db->loadAssoc();
				if($db->getErrorNum()) {
					JError::raiseError( 500, $db->stderr());
				}
				if ($result!==NULL) {
					//User exists.
					$uid = $result['id'];
					$name = $result['name'];
					$thumb = $result['thumb'];
					$avatar = $result['avatar'];
					
					//If it's myself, then skip this contact
					if ($my->id==$uid) continue;
					
					//Skip existing friends
					if (isset($friendids[$uid])) continue;
					
					
					//Is this person a friend? If it is, then skip too.
					$url = oz_jomsocial_build_profile_url($uid,false);
					$contact['member_id']=$uid;
					$contact['name']=$name;
					$contact['x-nocheckbox']=true;
					$namehtml = '<a href="'.$url.'" target="_blank">'.htmlentities($name,ENT_COMPAT,'UTF-8').'</a>';
					$contact['x-namehtml'] = $namehtml;

					//$contact['x-emailhtml'] = '<a href="javascript:void(0)" class="icon-add-friend" onclick="cFriendConnect('.$uid.')"><span>'.JText::_('CC ADD AS FRIEND').'</span></a>';
					if (!isset($friendids[$uid])) {
						//$emailhtml=htmlentities($e).'<br/><a href="javascript:void(0)" class="icon-add-friend" onclick="cFriendConnect('.$uid.')"><span>'.JText::_('CC ADD AS FRIEND').'</span></a>';
						$emailhtml=htmlentities($e).'<br/><a href="javascript:void(0)" class="icon-add-friend" onclick="joms.friends.connect('.$uid.')"><span>'.JText::_('CC ADD AS FRIEND').'</span></a>';
						$contact['x-emailhtml'] = $emailhtml;
					}
					
					//$imageurl = $avatarModel->getLargeImg($uid);
					$imageurl = JURI::base().$thumb;
					//echo "[UID=$uid]";
					//echo "[IMG=$imageurl]";
					$contact['image'] = $imageurl;
					$contact['name'] = $name;
					$contact['member_id'] = $uid;
					
				}
				else {
					//Doesn't exist. Normal user. Have we invited this user before?
					//todo: remove user if user already sent invitation
				}
			}
			$cl[] = $contact;
		}
	}
	//Sort contacts list
	usort($cl, "ozi_compare_contacts");	
	$contacts = $cl;	
}

?>

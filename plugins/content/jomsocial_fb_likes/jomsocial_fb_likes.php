<?php
/**
 * @category	Plugins
 * @package		JomSocial
 * @copyright (C) 2008 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );
require_once( JPATH_ROOT . DS . 'components' . DS . 'com_content' . DS . 'helpers' . DS . 'route.php' );
class plgContentJomsocial_FB_Likes extends JPlugin
{
	function plgContentJomsocial_FB_Likes( &$subject, $params )
	{
		parent::__construct( $subject, $params );
	}

	function onPrepareContent( &$row, &$params, $limitstart )
	{
		$view	= JRequest::getVar( 'view' , '' );

		if( $view != 'frontpage' )
		{
			$document	=& JFactory::getDocument();
			$uri	=& JURI::getInstance();
			$base	= $uri->toString( array('scheme', 'host', 'port'));
			
			// XB 
			if(!function_exists("curPageURL")) {
			function curPageURL() {
				$pageURL = 'http';
				if(isset($_SERVER["HTTPS"])) {
					if ($_SERVER["HTTPS"] == "on") {
						$pageURL .= "s";
					}
				}
				$pageURL .= "://";
				if ($_SERVER["SERVER_PORT"] != "80") {
					$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
				} else {
					$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
				}
				return $pageURL;
			}
			}
			if(!isset($url)) {
				$url = curPageURL();
			}
			
			// XB
			if(!isset($url)) {
			// XB 
				$url	= $base.JRoute::_( ContentHelperRoute::getArticleRoute( $row->slug, $row->catslug, $row->sectionid) , false );
			// XB
			}
			// XB
			
			$document->addCustomTag( '<meta property="og:title" content="' . $row->title . '" />' );
			$document->addCustomTag( '<meta property="og:url" content="' . $url . '" />' );
			
			$url		= urlencode( $url );

			$plugin 		=& JPluginHelper::getPlugin('content','jomsocial_fb_likes');
			$pluginParams 	= new JParameter( $plugin->params );
			$row->text	.= '<div>';
			$html		= '<iframe ';
			$html		.= 'src="http://www.facebook.com/plugins/like.php';
			$html		.= '?href=' . $url;
			$html		.= '&layout='.$pluginParams->get('style');
			$html		.= '&show_faces=' . $pluginParams->get('faces');
			$html		.= '&width=' . $pluginParams->get('width');
			$html		.= '&action='.$pluginParams->get('verb');
			$html		.= '&font='. $pluginParams->get('font');
			$html		.= '&colorscheme='.$pluginParams->get('scheme');
			$html		.= '&height='.$pluginParams->get('height');
			$html		.= '" scrolling="no" frameborder="0" style="border:none; overflow:hidden;height:80px;" allowTransparency="true"></iframe>';
			$row->text	.= $html . '</div>';
		}
	}

	function _getLink($id, $alias, $catid, $catAlias, $sectionid , $myblog, $permalink, $myblogItemId)
	{
	
		if(!$myblog)
		{
			$link	= ContentHelperRoute::getArticleRoute( $id . ':' . $alias , $catid . ':' . $catAlias , $sectionid );
			$link	= JRoute::_( $link );
		}
		else
		{	
			$link = JRoute::_( "index.php?option=com_myblog&show=".$permalink."&Itemid=".$myblogItemId );
		}
		
		return $link;
	}
	
}

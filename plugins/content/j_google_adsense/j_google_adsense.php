<?php
/*
* @package J - Google AdSense Plugin for Joomla 1.6/1.7 from tridentsystemsgroup.com
* @copyright Copyright (C) 2011 tridentsystemsgroup.com All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see license.txt
* Joomla! is free software.
* This extension is made for Joomla! 1.6/1.7;
** THE BEST CMS : JOOMLA! **
*/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.plugin.plugin' );
class plgContentJ_google_adsense extends JPlugin {
	public function __construct( $subject, $params ) {		
		parent::__construct( $subject, $params );		
	}	
    public function onContentPrepare($context, &$article, &$params, $limitstart){
        if(!isset($article) OR empty($article->id) OR !isset($this->params)) {
            return "";            
        }        
        $ad_content    = $this->get_ad_Content($article, $params);
        $ad_position   = $this->params->get('position');    
		$article->text = "\n<!-- google_ad_section_start -->\n" . $article->text . "\n<!-- google_ad_section_end -->\n";
        switch($ad_position){
            case 1:
                $article->text = $ad_content . $article->text;
                break;
            case 2:
                $article->text = $article->text . $ad_content;
                break;
            default:
                $article->text = $ad_content . $article->text . $ad_content;
                break;
        }       
        return true;
    }	
	function get_ad_Content(&$article, &$params){
        $app =& JFactory::getApplication();     
        if($app->isAdmin()) {
            return;
        }      
        $j_adcss = $this->params->get('j_ad_css');
		$j_clientID = $this->params->get('j_ad_client');
		$j_adtype = $this->params->get('j_ad_type');
		$j_adchannel = $this->params->get('j_ad_channel');
		$j_uifeatures = $this->params->get('j_ad_uifeatures');
		$j_ad_format = $this->params->get('j_ad_format');
		$j_format = explode("-", $j_ad_format);
		$j_ad_width = explode("x", $j_format[0]);
		$j_ad_height = explode("_", $j_ad_width[1]);
		$j_border = $this->params->get('j_color_border');
		$j_bg = $this->params->get('j_color_bg');
		$j_link = $this->params->get('j_color_link');
		$j_text = $this->params->get('j_color_text');
		$j_url = $this->params->get('j_color_url');			
		$j_demo_ch = "";	
		$j_ip_block = 1;	
		$j_code = "";		
		if ($_SERVER["REMOTE_ADDR"] == $params->get('j_ip_block1')) $j_ip_block = 0;
		if ($_SERVER["REMOTE_ADDR"] == $params->get('j_ip_block2')) $j_ip_block = 0;
		if ($_SERVER["REMOTE_ADDR"] == $params->get('j_ip_block3')) $j_ip_block = 0;
		if ($_SERVER["REMOTE_ADDR"] == $params->get('j_ip_block4')) $j_ip_block = 0;
		if ($_SERVER["REMOTE_ADDR"] == $params->get('j_ip_block5')) $j_ip_block = 0;
		if ($_SERVER["REMOTE_ADDR"] == $params->get('j_ip_block6')) $j_ip_block = 0;
		if ($_SERVER["REMOTE_ADDR"] == $params->get('j_ip_block7')) $j_ip_block = 0;
		if ($_SERVER["REMOTE_ADDR"] == $params->get('j_ip_block8')) $j_ip_block = 0;
		if ($j_ip_block) {
			$html         =   "";
			$view         =   $this->params->get('view');
			$currentView  =   JRequest::getWord("view");		
			$showInArticles     = $this->params->get('showInArticles');
			$showInCategories   = $this->params->get('showInCategories');
			$showInFrontPage    = $this->params->get('showInFrontPage');
			$currentView = JRequest::getWord("view");			
			if(!$showInArticles AND (strcmp("article", $currentView) == 0)){
            return "";
			}        
			if(!$showInCategories AND (strcmp("category", $currentView) == 0)){
				return "";
			}			
			if(!$showInFrontPage AND (strcmp("frontpage", $currentView) == 0)){
				return "";
			}			
			$excludedCats =   $this->params->get('excludeCats');
			$excludeArticles =   $this->params->get('excludeArticles');
			if ( !empty( $excludedCats ) ) {
				$excludedCats = explode(',',$excludedCats);
			}
			settype($excludedCats,'array');
			if ( !empty( $excludeArticles ) ) {
				$excludeArticles = explode(',',$excludeArticles);
			}
			settype($excludeArticles,'array');
			if(in_array($article->catid, $excludedCats) OR in_array($article->id, $excludeArticles)){
				return '';
			}			
			if ($j_adcss) { 
				$j_code = "<div style=\"" . $j_adcss . "\">\r\n"; 
			}
			$j_code = $j_code . "<script type=\"text/javascript\"><!--\r\n";
			if ($j_clientID) { 
				$j_code = $j_code . "google_ad_client = \"" . $j_clientID . "\";\r\n";
			} 
			else {
				require_once(JPATH_PLUGINS.DS."content".DS."j_google_adsense".DS."elements".DS."helper.php");
				$j_demo_id = plgJGAHelper::getDemoId();
				$j_code = $j_code . "google_ad_client = \"pub-" . $j_demo_id . "\";\r\n";
			}			
			$j_code = $j_code . "google_ad_width = " .  $j_ad_width[0] . "; \r\n"
				. "google_ad_height = " . $j_ad_height[0] . "; \r\n"
				. "google_ad_format = \"" . $j_format[0] . "\"; \r\n";				
			if ($j_adtype) { 
				$j_code = $j_code . "google_ad_type = \"" . $j_adtype . "\"; \r\n";
			}
			if ($j_clientID == NULL) { 
				$j_code = $j_code . "google_ad_channel = \"" . $j_demo_ch . "\"; \r\n";
			} 
			else {
				$j_code = $j_code . "google_ad_channel = \"" . $j_adchannel . "\"; \r\n";
			}
			$j_code = $j_code . "google_color_border = \"" . $j_border . "\"; \r\n"
				. "google_color_bg = \"" . $j_bg . "\"; \r\n"
				. "google_color_link = \"" . $j_link . "\"; \r\n"
				. "google_color_text = \"" . $j_text . "\"; \r\n"
				. "google_color_url = \"" . $j_url . "\"; \r\n";
			if ($j_uifeatures != NULL) {
				$j_code = $j_code . "google_ui_features = \"rc:" . $j_uifeatures . "\"; \r\n";
			}
			$j_code = $j_code . "//--> \r\n"
				. "</script>\r\n"
				. "<script type=\"text/javascript\" src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">\r\n"
				. "</script></br>\r\n";
			if ($j_adcss) { 
				$j_code = $j_code . '</div>'; 
			}
		} 
		else {
			$j_code = $j_code . '<div style="float: none; margin: 0px 0px 0px 0px;">' . $params->get('j_ip_block_alt_code') . '</div>';
		}
		return $j_code;
    }
}
?>
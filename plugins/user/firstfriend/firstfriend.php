<?php
/**
 * @version $Id: header.php 789 2009-03-23 15:56:03Z elkuku $
 * @package    Jomsocial
 * @subpackage FirstFriend
 * @author     Webmaster {@link http://www.Sociables.com}
 * @author     Created on 25-Mar-2009
 * @license http://www.sociables.com Copyright (C) 2009. All rights reserved. Copyrighted Commercial Software
 * @license No portion of this software may be reproduced without the explicit written permission of the author.
 * @license In purchasing this software you are granted the use on one domain.
 */

// no direct access

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );

class plgUserFirstFriend extends JPlugin
{
	//var $_my		= null;
	var $_user		= null;
	function plgUserFirstFriend (& $subject, $config)
	{	
		//$this->_my		=& CFactory::getUser();
		$this->_user		=& JFactory::getUser();
		parent::__construct($subject, $config);
		
		// Check whether the plugin table exists or not and create it if necessary						
	}
	
	
	function onAfterStoreUser($user, $isnew, $success, $msg)
		{
			if ($isnew)
				{
					require_once( JPATH_SITE . DS . 'components' . DS . 'com_community' . DS . 'libraries' . DS . 'core.php');
					require_once( JPATH_SITE . DS . 'components' . DS . 'com_community' . DS . 'libraries' . DS . 'notification.php');
		
					//JPlugin::loadLanguage('plg_firstfriend', JPATH_ADMINISTRATOR);
				
					$sendreq 	= $this->params->get( 'sendreq' , 1 );		
					$sendletter 	= $this->params->get( 'sendletter' , 1 );
					//@todo add multiple first friends & conditions such as male/female
					$firstfriend 	= $this->params->get( 'firstfriend' , 42 );
					$friends 	= explode(',', $firstfriend);
					$method 	= $this->params->get( 'method' , 3 );
					$massfriends 	= $this->params->get( 'massfriends' , 0 );
					$checkmass 	= $this->params->get( 'checkmass' , 0 );							
					if ($user['id'] != $firstfriend) $notself = 1;
								
					if (($sendreq !=0) && ($notself == 1))
					{
						$method = $this->params->get( 'method' , 3 );			
						
								if ($massfriends == 1 && $checkmass == 1)
									{	
										$userids = $this->_getAllUsers();
										//parse through all id's
										foreach ($userids AS $row)
											{
												//for each user send a friend request to the user from the new member
												$this->_addFriendR($method, $row->userid, $user['id']);
																					
												//This one approves all the pending requests of the Member receiving the friend request
												if ($method != 3)$this->_autoApprove($row->userid, $user['id'], 1);
					
											}		
									}
								else
									{
										if ($friends[1])
											{	
												foreach ($friends as $friend)
												{
													$this->_addFriendR($method, $user['id'], $friend);		
													
													//This one approves all the friend requests of the new person sending the requests
													if ($method != 3)$this->_autoApprove($user['id'], $friend, 1);
												}
											}
										else
											{
												$this->_addFriendR($method, $user['id'], $firstfriend);		
										
												//This one approves all the friend requests of the new person sending the requests
												if ($method != 3)$this->_autoApprove($user['id'], $firstfriend, 1);	
											}
									}
									
						
						
					}
					
					if ($sendletter !=0 && $notself == 1)
						{
			
									$notify = new CNotificationLibrary();
						
									$notify->add( 'system.member.welcome' , $this->params->get( 'senderid' , 42 ) , $user['id'] , $this->params->get( 'subject' , '' ) ,
									 	JText::sprintf($this->params->get( 'body' , '' ) , JURI::base()), '' , array() );
									 	
						}
					
				}
		}
	
	
	function onLoginUser($user, $options)
	{
		$base = JPATH_BASE;
		preg_match("/administrator/", $base, $load);
		if (!$load[0])
		{
		require_once( JPATH_SITE . DS . 'components' . DS . 'com_community' . DS . 'libraries' . DS . 'core.php');
		$ffupdate = $this->params->get( 'ffupdate' , 0 );	
		if (($this->params->get( 'cfcounts' , 1 )==1) && ($this->params->get( 'masscfc' , 0 )!=1))$this->_correctCount($this->_user->id);
		if (($this->params->get( 'cfcounts' , 1 )!=1) && ($this->params->get( 'masscfc' , 0 )==1))$this->massCorrect();
		$massfriends = $this->params->get( 'massfriends' , 0 );$checkmass = $this->params->get( 'checkmass' , 0 );$updateall =$this->params->get( 'updateall' , 0 );		
		if (($updateall==1)&&($this->_getUpdate('updateall')==0))$this->_massUpdate();	
		$ffrunupdate = $this->_getUpdate('ffupdate');								
		if (($ffupdate == 1)&&($ffrunupdate != 1))$this->_ffUpdate();
		if ($this->params->get( 'undomass' , 0 )==1)$this->_getUpdate('','restore');
		$resetrun = $this->params->get( 'resetrun' , 0 );				
		if ($resetrun == 1)
					{
						$this->_getUpdate('' , 'reset');
						//these set params do not work not sure why
						$this->params->set( 'resetrun' , '0' );
					}
		$clearfriends = $this->params->get( 'clearfriends', 0 );			
		if ($clearfriends == 1 )
					{
						$this->_getUpdate('' , 'clear');
						//these set params do not work not sure why
						$this->params->set( 'clearfriends' , '0' );
					}
			$installed = $this->_getUpdate('id');
			if(!$installed) {
			$this->_getUpdate('backup','backup');
			$db = &JFactory::getDBO();		
			// The query needed to create the table if it really does not exist
			// It might exist if the plugin was previously installed, removed and newly installed
			$query = "CREATE TABLE IF NOT EXISTS `#__community_plg_firstfriend` (  ".
			  		 "`id` tinyint(4) DEFAULT 0, ".
					 "`updateall` tinyint(4) DEFAULT 0, ".
					 "`ffupdate` tinyint(4) DEFAULT 0, ".
					 "`backup` tinyint(4) DEFAULT 0, ".
					 "`BackupDate` datetime NOT NULL, ".
					 "`RunDate` datetime NOT NULL, ".
					 "PRIMARY KEY  (`id`), ".
					 "KEY `RunDate` (`RunDate`) ".
					 ") ENGINE=MyISAM;";			
			$db->setQuery($query);
			$db->query();			
			$this->_getUpdate('', 'install');
		}
		}//end load
	}

	function _ffUpdate()
		{
			$updateall =$this->params->get( 'updateall' , 0 );
			if (!$updateall){
			$this->_getUpdate('backup','backup');
			//$this->_getUpdate('','clear');
				$firstfriend = $this->params->get( 'firstfriend' , 42 );
				$friends = explode(',', $firstfriend);
				$userids = $this->_getAllUsers();
				$method = $this->params->get( 'method' , 3 );
				if ($friends[1])
					{	
						foreach ($friends as $friend)
						{					
							//parse through all id's
							foreach ($userids AS $row)
									{
										//for each user send a friend request to the user from the firstfriend
										$this->_addFriendR($method, $row->userid, $friend);																			
										//This one approves all the pending requests of the Member receiving the friend request
										if ($method != 3)$this->_autoApprove($row->userid, $friend, 1);							
									}
							$this->_getUpdate('ffupdate',1);
							$this->massCorrect();
						}
					}
				else
					{
												//parse through all id's
						foreach ($userids AS $row)
								{
									//for each user send a friend request to the user from the firstfriend
									$this->_addFriendR($method, $row->userid, $firstfriend);																			
									//This one approves all the pending requests of the Member receiving the friend request
									if ($method != 3)$this->_autoApprove($row->userid, $firstfriend, 1);							
								}
						$this->_getUpdate('ffupdate',1);
						$this->massCorrect();
					}
			}			
		}
	
	function _massUpdate()
		{
			$this->_getUpdate('backup','backup');
			$this->_getUpdate('','clear');			
			$method = $this->params->get( 'method' , 3 );			
				//This sends a friend request from the new person to everyone		
							//Get Id's of all Users
								$userids = $this->_getAllUsers();
											$updaterunall = $this->_getUpdate('updateall');
											if (!$updaterunall)
												{	
													//this should cover the new member as this routine is run after the user is stored
														//for each member
														$i=0;														
													foreach ($userids as $allrows)
															{	
																//add everyone as their friend
																$uids = $this->_getAllUsers();																	
																$output = array_slice($uids, $i);															
																foreach ($output as $all)
																		{	//start with the +1 of the current row							//to friend		from friend																			
																			if ($all->userid != $allrows->userid)$this->_addFriendR($method , $all->userid, $allrows->userid);
																		}	
																$i++;				
																//$this->_addFriendR($method, $row->userid, $this->_user->id);
																//if ($method != 3)$this->_autoApprove($allrows->userid);	
																																					
															}
														$this->_getUpdate('updateall',1);
														$this->massCorrect();
												}				
										
		}
	
	function  massCorrect()
			{
				$correctallcounts = $this->_getAllUsers();
				foreach ($correctallcounts as $count)
					{
						$this->_correctCount($count->userid);
					}
			}
	
	function _getAllUsers()
	{
		$db = &JFactory::getDBO();

			$query="SELECT ". $db->nameQuote( 'userid' ) . " FROM ". $db->nameQuote( '#__community_users' );
				$db->setQuery($query);
				$result = $db->loadObjectList();

		return $result;		
	}	

	function _autoApprove($userid, $approve=NULL, $oneuser=NULL)
	{			
		$model 	=& CFactory::getModel('friends');
		if ($this->params->get( 'autoapprove', 0 ) == 1)
			{
				$pid = $model->getPending($userid);						
					if (!$oneuser)
						{
							foreach ($pid AS $row)
									{							
										$model->approveRequest($row->connection_id);															
									}
						}			
					else
						{
							$model->approveRequest($pid->$approve);
						}								
			}
	}
	
	function _addFriendR($method, $to, $from)
	{
		$model 	=& CFactory::getModel('friends');
		$add = '';
		$fadd = '';
		$tadd = '';
		//1st lets make sure sender and receiver are not the same
		if (($to != $from)&&($to != $this->_user->id)&&($from != $this->_user->id))
			{		
				switch ($method)
				{
					case 0://Two Way
								//this is a check to see if the friend is already on their list if so dont add them again
								$tid = $model->getFriendIds($to);	
								foreach ($tid AS $ctid)if ($ctid == $from) $tadd = 1;
								//if friend check does not return true go ahead and send the request.
								if (!$tadd) $model->addFriend($to, $from);
									
								$fid = $model->getFriendIds($from);
								foreach ($fid AS $cfid)if ($cfid == $to) $fadd = 1;
								if (!$fadd) $model->addFriend($from, $to);	
						break;
					case 1://one way to
								//send a request to a user from the current user
								$tid = $model->getFriendIds($to);
								foreach ($tid AS $ctid)if ($ctid == $from) $add = 1;
								if (!$add) $model->addFriend($to, $from);		
						break;
					case 2://one way from
								$fid = $model->getFriendIds($from);
								foreach ($fid AS $cfid)if ($cfid == $to) $add = 1;
								if (!$add) $model->addFriend($from, $to);
						break;
						
					case 3:
							$tid = $model->getFriendIds($to);	
							foreach ($tid AS $ctid)if ($ctid == $from) $tadd = 1;
							if (!$tadd) 
								{
									if (($from != $this->_user->id))$model->addFriendRequest($to, $from);
									$model->addFriendCount($from);
								}
						break;
					default://default two way autoapprove
							$tid = $model->getFriendIds($to);	
							foreach ($tid AS $ctid)if ($ctid == $from) $tadd = 1;
							if (!$tadd) 
								{
									if (($from != $this->_user->id))$model->addFriendRequest($to, $from);
									$model->addFriendCount($from);
								}
						break;
				}
			}	
	}
	
	function _correctCount($id)
	{
		$model 	=& CFactory::getModel('friends');
		$fcount = $model->getFriendsCount($id);
		$ccount = $this->_currentCount($id);
		//if currentcount is less that actual friend count we need to add friends
		if ($ccount < $fcount)
		{
			//1st we need to know how many to add so lets find the difference
			$fcount -= $ccount;
			for ($i=0; $i < $fcount; $i++)
			{
				$model->addFriendCount($id);
			}
		}
		
		if ($ccount > $fcount)
		{
			$ccount -= $fcount ;
				for ($i=0; $i < $ccount; $i++)
				{
					$model->substractFriendCount($id);
				}
		}
		// if the count is correct return true otherwise do it again, this could be a dangerous loop.
		if ($ccount == $fcount)return true;
		//else $this->_correctCount($id);
	}
	
	function _currentCount( $userId )
	{
		$db = &JFactory::getDBO();
		
		$query	= 'SELECT ' . $db->nameQuote( 'friendcount' ) . ' '
				. 'FROM ' . $db->nameQuote( '#__community_users' )
				. 'WHERE ' . $db->nameQuote( 'userid' ) . '=' . $db->Quote( $userId );
		$db->setQuery( $query );
		
		$count	= $db->loadResult();
		return $count;
	}
	
	function _getUpdate( $what, $add=NULL )
	{
				$db = &JFactory::getDBO();
				$restore = '';	
				$update = '';	
		if (!$add)
			{

				$query = "SELECT". $db->nameQuote($what). "FROM #__community_plg_firstfriend";
				$db->setQuery($query);
				$update = $db->loadresult();	
				return $update;
			}
		else 	
			{	
				switch ($add)
				{
					case 'install':
							$ffupdate = $this->params->get( 'ffupdate' , 0 );
							$updateall =$this->params->get( 'updateall' , 0 );
							if ($updateall && $ffupdate)$query = "INSERT INTO #__community_plg_firstfriend VALUES (1,1,1,1,'0000-00-00 00:00:00',NOW())";
							else if ($updateall)$query = "INSERT INTO #__community_plg_firstfriend VALUES (1,1,0,1,'0000-00-00 00:00:00',NOW())";
							else if($ffupdate) $query = "INSERT INTO #__community_plg_firstfriend VALUES (1,0,1,1,NOW(),'0000-00-00 00:00:00')";							
							else $query = "INSERT INTO #__community_plg_firstfriend VALUES (1,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00')";
						break;
						case 'reset':
							$query = "UPDATE #__community_plg_firstfriend SET updateall=0, ffupdate=0, backup=0, BackupDate='0000-00-00 00:00:00', RunDate='0000-00-00 00:00:00'";
						break;
					case 'clear':
							$query = "TRUNCATE TABLE `#__community_connection`";
						break;
					case 'backup':
							$backup = $this->_getUpdate('backup');
							$backup++;
							if ($backup < 1)$backup = 1;
							$query = "CREATE TABLE IF NOT EXISTS #__community_connection_backup_".$backup." SELECT * FROM #__community_connection";
								$update = 1;						
						break;
					case 'update':
							$backup = $this->_getUpdate('backup');
							$backup++;
							$query = "UPDATE #__community_plg_firstfriend SET ". $db->nameQuote($what). "=".$backup.", BackupDate=NOW()";
						break;
					case 'drop':
							$backup = $this->_getUpdate('backup');
							$query = "DROP TABLE IF EXISTS #__community_connection_temp_".$backup;
						break;
					case 'rename':
							$backup = $this->_getUpdate('backup');
							//$backup--;
							$query = "RENAME TABLE #__community_connection TO #__community_connection_temp_".$backup.", #__community_connection_backup_".$backup." TO #__community_connection";
							$rename == 1;
						break;
					case 'restore':
						$backup = $this->_getUpdate('backup');
						$backup++;					
							$query = "CREATE TABLE IF NOT EXISTS #__community_connection_backup_".$backup." SELECT * FROM #__community_connection";
								$restore = 1;
								$update = 1;			
						break;
					default:
							$query = "UPDATE #__community_plg_firstfriend SET ". $db->nameQuote($what). "=1, RunDate=NOW()";
						break;
				}
				$db->setQuery($query);
				$db->query();
				
				
				if ($restore)
				{
					$this->_getUpdate('','rename');
					$this->_getUpdate('','drop');;						
				}
				if ($update)$this->_getUpdate('backup','update');

			}
	}
}

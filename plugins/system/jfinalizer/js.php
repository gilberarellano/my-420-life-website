<?php
/**
 * @version		2.0
 * @package		jFinalizer
 * @copyright	(c) 2010 farbfinal.de
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 *
 * php gzipped js delivery
 *
*/

ini_set ('allow_url_fopen', 0);

foreach ($_REQUEST as $key => $val){
	$f = $key;
	break;
}

$f = str_replace('_','.', $f).'.gz';

header('Content-type: text/javascript; charset: UTF-8');
header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 2592000) . ' GMT');
header("Content-Encoding: gzip");
header('Last-Modified: '.gmdate('D, d M Y H:i:s',filemtime($f)).' GMT');

readfile($f);
?>
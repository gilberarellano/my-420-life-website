<?php
/**
 * @version		2.0.1
 * @package		jFinalizer
 * @copyright	(c) 2010, 2011 farbfinal.de
 * @license		GNU General Public License version 2 or later
 *
 * joomla! 1.6+ system plugin for PHP5
 * 
 * documentation & support: www.farbfinal.de/jfinalizer
 *
 * Merge CSS & JS
 * compact CSS & JS
 * compact html
 *
*/

defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

class plgSystemJFinalizer extends JPlugin {

	var $version			= '2.0.1';
	var $poweredBy			= '<!-- accelerated by www.farbfinal.de/jfinalizer -->';  // remove this if you wish - or keep it. thanks :)!

	var $cssFiles;
	var $jsFiles;
	var $base;
	var $outPath;
	var $deliverPath;
	var $doDebug;
	var $autoplace;
	var $htmlBytesSaved 	= false;
	var $gzipmode;
	var $tmp;
	var $builderLoaded 		= false;
	var $builder;
	var $_active 			= true;
	var $htmlBypass			= false;
	var $HTDocRoot;
	var $subDir;
	var $_eh				= false;

	var $hasPreTags 		= false;
	var $hasTextArea		= false;
	var $textArea;
	var $_ta;
	
	var $jQueryNoConflict;
	
	var $csp				= 'jFCSSxPL';
	var $jsp				= 'jFJSxPL';
	
	var $patternCSS 		= '/<link(.*?)(type=)("|\')(text\/css)("|\')(.*?)\/>/i';
	var $patternJS 			= '/<script(.*?)(src=)("|\')(.*?)("|\')(.*?)><\/script>/i';
	var $patternExceptCSS	= '/<\!--\[if(.*?)endif\]-->/msi';
	
	var $skipJS;			// historical name. Now used for both skipping
							// JS and CSS
	
	public function onAfterRender() {
		
		if ($this->_active == false) return true;

		if (JFactory::getDocument()->getType() != 'html' ) return true; 
		
		$this->gzipmode   	= $this->params->get( 'gzipmode' );
		$this->base   		= JURI::base();

		if (!$this->checkCreateOutputPath()) {
			$this->closeJF();
			return true;
		}

		$this->cssFiles 	= array();
		$this->jsFiles 		= array();
		$this->csExceptions	= array();

		$buffer = JResponse::getBody();
		
		$doJS 				= $this->params->get( 'processjs', 1 ) ? true : false;
		$doCSS 				= $this->params->get( 'processcss', 1 ) ? true : false;
		$doCheckExcept 		= $this->params->get( 'checkexcept', 1 ) ? true : false;
		$this->incRemote 	= $this->params->get( 'includeremote', 1) ? true : false;
		$this->doDebug 		= $this->params->get( 'debug') > 0 ? true : false;
		$lifetime			= $this->params->get( 'cachelifetime');	
		$this->autoplace	= $this->params->get( 'autoplace', 1 ) ? false : true;
		$this->htmlpre		= $this->params->get( 'htmlpre');
		$this->subDir		= $this->params->get( 'subdir');
		
		if ($this->params->get( 'usecache') == 0) $lifetime = -1;
		
		if ($this->doDebug) {
			$this->loadErrorHandler();
			$time = microtime(true);
		}
		
		if (!$this->getHtDocRoot()) {
			$this->closeJF();
			return true;
		}
		
		preg_match("/<head>(.*?)<\/head>/msi", $buffer, $head);
		
		if ($doCSS || $doJS) {
			$this->skipJS = explode(',', strtolower(str_replace(' ', '', $this->params->get('skipjs'))));
		}
		
		if ($doCSS) {

			if ($doCheckExcept) {
				$this->ccount=0;
				$head[1] = preg_replace_callback($this->patternExceptCSS, array($this, 'recordExceptionCSS'), $head[1]);
			}
			
			$this->ccount			=0;
			$head[1] 				= preg_replace_callback($this->patternCSS, array($this, 'recordCSS'), $head[1]);
			$this->cssFiles 		= $this->removeHTTP($this->cssFiles, $this->base);
			$cssFileOut 			= $this->mkCheckSum($this->cssFiles, 'css');
			
			if (!$this->checkLifeTime($cssFileOut, $lifetime)) {
				$this->loadBuilder();
				$this->builder->readAndMerge($this->cssFiles, $cssFileOut, 'css');
			} 
		}
		
		if ($doJS) {
			$this->ccount=0;
			$head[1] 				= preg_replace_callback($this->patternJS, array($this, 'recordJS'), $head[1]);
			$this->jsFiles  		= $this->removeHTTP($this->jsFiles, $this->base);
			$this->jQueryNoConflict	= $this->params->get( 'jquerynoconflict', 1 ) ? true : false;
			$jsFileOut 				= $this->mkCheckSum($this->jsFiles, 'js');
			
			if (!$this->checkLifeTime($jsFileOut, $lifetime)) {
				$this->loadBuilder();
				$this->builder->readAndMerge($this->jsFiles, $jsFileOut, 'js');
			}
		}


		if ($doCSS) {
			$this->_writeCSS($head[1], $cssFileOut);
			if ($doCheckExcept) {
				$this->_writeExceptions($head[1]);
			}
		}
		
		if ($doJS) $this->_writeJS($head[1], $jsFileOut);	
		if ($doJS || $doCSS ) $buffer = preg_replace("/<head>(.*?)<\/head>/msi", "<head>".$head[1]."</head>", $buffer);
		
		if ( ($md = $this->params->get( 'htmllevel')) > 0) {			
			if ($this->doDebug) $this->htmlBytesSaved = strlen($buffer);
			jFinalizerOutHTMLOptimizer::_compress($buffer, $md, $this);
			if ($this->doDebug) $this->htmlBytesSaved = $this->htmlBytesSaved - strlen($buffer). ' ('.number_format((100-(strlen($buffer)*100)/$this->htmlBytesSaved),1). '%)';		
		}	
		
		$this->_writeNote($buffer);
		
		if ($this->doDebug) { 
			$this->loadBuilder();
			$this->builder->showDebugInfo($time, $doJS, $doCSS);
		}

		$this->closeJF($buffer);
		
		return true;
	}

	/* major difference starting from 2.0.1
	 *
	 * NOTE: In joomla 1.5, jFinalizer could handle late requests by manually triggering
	 * a new store() call of joomla's cache plugin. In joomla 1.6, that does not
	 * work anymore. So, starting from joomla 1.6, jFinalizer must be BEFORE cache in the
	 * joomla system plugin chain.
	 *
	 * Perfect system plugin order (numbers are fiction):
	 * 01 SYSTEM PLUGIN A
	 * 02 SYSTEM PLUGIN B
	 * ... so on
	 * 98 jFinalizer
	 * 99 System.Cache (last plugin)
	 *
	*/
	public function onAfterInitialise() {		
	
		$mainframe = JFactory::getApplication();
		
		if ($mainframe->getName() != 'site') {
			$this->_active = false;
			return true;
		}
		
		$mainframe->jFinalizer =& $this;
		
		return true;
	}

	public function doHTML() {
		if ($this->_active == true && $this->params->get( 'compacthtml') != 1) return true;
		return false;
	}
	
	public function protect($input) {
		if ($this->_active == false || $this->params->get( 'compacthtml') != 1) return $input;
		return '{jfbypass} PROTECT:'.$input.'{/jfbypass}';
	}
	
	public function startProtect($input) {
		if ($this->_active == false || $this->params->get( 'compacthtml') != 1) return $input;
		return '{jfbypass}';
	}
	
	public function endProtect($input) {
		if ($this->_active == false || $this->params->get( 'compacthtml') != 1) return $input;
		return '{/jfbypass}';
	}
	
	function recordCSS($matches){
		$merge = implode($matches);
		if (strstr($matches[0], '?')) return $matches[0];

		if ($this->isSkipped($this->skipJS, $matches[0])){
			return $matches[0];
		}

		preg_match('/(.*?)href=("|\')(.*?)\.css("|\')/', $merge, $fl);
		
		if (@strlen($fl[3])<2) {
			return $matches[0];
		}
		
		// if (@strstr(strtolower($merge), 'media="print')){
		//	return $matches[0];
		// }

		$fl[3] = $fl[3].'.css';
		array_push($this->cssFiles, $fl[3]);
		$this->ccount++;
		$ret = '';
		if ($this->autoplace && $this->ccount<2) return $ret .= $this->csp;
		if ($this->doDebug) $ret .= '<!-- REMOVED CSS: '.$fl[3].' -->'; 
		return $ret;
	}
	
	function recordExceptionCSS($matches){
		array_push($this->csExceptions, $matches[0]);
		return '<!-- EXEPT__CSS: '.count($this->csExceptions).' -->';
	}
	
	function recordJS($matches){
		
		if (strstr($matches[0], '?')) return $matches[0]; 
	
		$merge = implode($matches);
		if (!strstr($merge, 'text/javascript')){
			return $matches[0];
		}
		preg_match('/(.*?)src=("|\')(.*?)("|\')(.*?)/', $merge, $fl);
		if ( (@strlen($fl[3])<2) || ($this->isSkipped($this->skipJS, $fl[3])) ) {
			return $matches[0];
		}

		array_push($this->jsFiles, $fl[3]);
		$this->ccount++;
		if ($this->autoplace && $this->ccount<2) return $this->jsp;
		if ($this->doDebug) return '<!-- REMOVED JS: '.$fl[3].' -->'; return '';
	}
	
	public function recordPre($matches){
		$this->_pre[0][$this->tmp] = $matches[0];
		$this->_pre[1][$this->tmp] = "__xxsup".$this->tmp;
		$this->tmp++;
		return $this->_pre[1][$this->tmp-1];
	}
	
	public function recordTextArea($matches){
		$this->_ta[0][$this->tmp] = $matches[0];
		$this->_ta[1][$this->tmp] = "__xxasup".$this->tmp;
		$this->tmp++;
		return $this->_ta[1][$this->tmp-1];
	}
	
	public function recordBypass($matches){
		$this->_bypass[0][$this->tmp] = $matches[1];
		$this->_bypass[1][$this->tmp] = "__xxbyp".$this->tmp;
		$this->tmp++;
		return $this->_bypass[1][$this->tmp-1];
	}
	
	private function _writeCSS(&$buffer, $cssFile){
		$fly = $this->gzipmode == 1 ? 'css.php?' : '';
		$l = '<link rel="stylesheet" href="'.$this->deliverPath.$fly.$cssFile.'" type="text/css" />';
		$buffer = str_replace($this->csp, $l, $buffer);
	}
	
	private function _writeJS(&$buffer, $jsFile){
		$fly = $this->gzipmode == 1 ? 'js.php?' : '';
		$l = '<script type="text/javascript" src="'.$this->deliverPath.$fly.$jsFile.'"></script>'.PHP_EOL;
		$buffer = str_replace($this->jsp, $l, $buffer);
	}
	
	private function _writeExceptions(&$buffer){
		$c = @count ($this->csExceptions);
		for ($i=0; $i<$c; $i++){
			$j=$i+1;
			$buffer = str_replace('<!-- EXEPT__CSS: '.$j.' -->', $this->csExceptions[$i], $buffer);
		}
	}
	
	function mkCheckSum($dataArr, $type){
		return md5(implode($dataArr)).'.'.$type;
	}


	function removeHTTP($l, $base){
		return $l;
	}

	function checkCreateOutputPath(){
		$this->outPath = JPATH_CACHE.DS.'assets'.DS;	

		if (strlen($dp = $this->params->get( 'outpath'))>0){
			$this->deliverPath = $dp;
			if (!file_exists($this->deliverPath)){
				$this->_E(101, $this->deliverPath);
				return false;
			}
		} else {
			$livebase = JURI::base(true);
			if (strlen($livebase)<2) $livebase='';
			$this->deliverPath = $livebase.DS.substr(JPATH_CACHE, strlen(JPATH_BASE)+1).DS.'assets'.DS;		
		}	
		
		/* Windows machine patch - thanks to Hari Karam Singh */
		$this->deliverPath = str_replace("\\", '/', $this->deliverPath);	
		
		if ($this->params->get( 'usecache') == 0){
			jimport('joomla.filesystem.folder');
			JFolder::delete($this->outPath);
		}
		
		if (!file_exists($this->outPath)){
			if (!mkdir($this->outPath)) {
				$this->_E(100, $this->outPath);
				return false;
			}
		}

		if ($this->gzipmode == '2'){
			if (!file_exists($this->outPath.'.htaccess')){
				$this->loadBuilder();
				jFinalizerTools::writeHtaccessFile($this->outPath, $this->deliverPath);
			}
		}
		
		if ($this->gzipmode == '1'){
			if (!file_exists($this->outPath.DS.'css.php')){
				$this->loadBuilder();
				jFinalizerTools::writeFlyFiles($this->outPath, JPATH_PLUGINS.DS."system".DS."jfinalizer".DS);
			}
		}
		return true;
	}
	
	function checkLifeTime($fn, $lifetime){
		$fn = $this->outPath.$fn;
		$ftime = @filemtime($fn);
		if ($lifetime == -1 || $ftime == false) return false;
		if ($lifetime == 0) return true;
		if ( (time() - $ftime >= $lifetime)) return false;
		return true;
	}

	function _writeNote(&$buffer){
		$buffer = str_replace('</body>', $this->poweredBy.'</body>', $buffer);
	}
	
	function getHtDocRoot(){
		$this->HTDocRoot = substr(JPATH_BASE, 0, (strlen(JPATH_BASE) - strlen(trim($this->subDir))));
		if (!file_exists($this->HTDocRoot)){
			if (strlen($this->subDir)>0) $this->_E(200, $this->subDir);
			$this->_E(200, $this->subDir);
			$this->_E(201, $this->HTDocRoot);
			$this->_E(202);
			return false;
		}
		return true;
	}
	
	function _E($id, $var1='', $var2=''){
		$this->loadErrorHandler();
		$this->_eh->Show($id, $var1, $var2);	
	}
	
	function _msg($msg){
		$this->_eh->msg(htmlentities($msg));	
	}
	
	function _report($msg){
		if ($this->doDebug) $this->_msg($msg);
	}
	
	function isSkipped($list, $file){
		if (@count($list)<1) return false;
		foreach (@$list as $l){
			if (@strstr(strtolower($file), $l)) return true;
		}
		return false;
	}

	function closeJF($buffer=false){
		if ($buffer==false) $buffer = JResponse::getBody();
		if ($this->_eh != false){	
			$buffer = str_replace('</body>', $this->_eh->display().'</body>', $buffer);
		}
		JResponse::setBody($buffer);
	}
	
	function loadBuilder(){
		if (!$this->builderLoaded){
			require_once(JPATH_PLUGINS.DS."system".DS."jfinalizer".DS."helper.php");
			$this->builder = new jFinalizerBuilder($this);
			$this->builderLoaded = true;
		}
	}
	
	private function loadErrorHandler(){
		if ($this->_eh == false){
			require_once(JPATH_PLUGINS.DS."system".DS."jfinalizer".DS."errorhandler.php");	
			$this->_eh = new jFinalizerErrorHandler();	
		}	
	}
}


/* built in html optimizer class. Put in here cause its small and 
 * we save one more file open
 *
*/
class jFinalizerOutHTMLOptimizer {

	public static function _compress(&$buffer, $mode, &$plugin) {	
		/* pre check for {jfbypass} and <pre> tags */
		if ($plugin->htmlpre > 0) jFinalizerOutHTMLImplement::_getPre($buffer, $plugin);
		jFinalizerOutHTMLImplement::_getBypass($buffer, $plugin);
		if (jFinalizerOutHTMLImplement::checkTextArea($buffer, $plugin) == true) jFinalizerOutHTMLImplement::_getTextArea($buffer, $plugin);
		
		/* do the compression */
		jFinalizerOutHTMLImplement::_compress($buffer, $plugin);
		
		/* insert {jfbypass} and <pre> tags if found before */
		if ($plugin->htmlBypass) jFinalizerOutHTMLImplement::_putBypass($buffer, $plugin);
		if ($plugin->htmlpre > 0) jFinalizerOutHTMLImplement::_putPre($buffer, $plugin);
		
		if ($plugin->hasTextArea == true) jFinalizerOutHTMLImplement::_putTextArea($buffer, $plugin);
		
	}
}

class jFinalizerOutHTMLImplement{

	public static function _compress(&$buffer, &$plugin){
		
		$level = $plugin->params->get('htmllevel'); 
		
		if ($plugin->params->get( 'htmlcomments') == 1) {
			$buffer = preg_replace('/<!--.*?-->/', '', $buffer);		
		}	

		if ($plugin->params->get( 'htmlgenerator') == 1) {
			$buffer = preg_replace('/<meta name="generator.*?\/>/', '', $buffer, 1);		
		}
				
		if ($level == 1) {
			/* MODERATE (old strong) mode */
			$buffer = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "", $buffer);
			$buffer = str_replace(array("\r\n", "\r", "\t"), '', $buffer);	
			$buffer = str_replace(array('  ', '    ', '     ', '      '), ' ', $buffer);
			$buffer = preg_replace("/^[ \t]+|[ \t]+$/msi", "", $buffer);
			$buffer = str_replace(array(">\n"), '>', $buffer);	
		} else if ($level == 0){
			/* SAFE mode A */
			$buffer = preg_replace('/^\n+|^[\t\s\r]*\n+/m','',$buffer);
			$buffer = str_replace(array("\t"), '', $buffer);	
			$buffer = preg_replace("/^\s+/msi", "", $buffer);
			$buffer = preg_replace("/>[\s\n\r]+</msi", "><", $buffer);
		} else if ($level == 2){
			/* ULTRA mode */
			$buffer = str_replace(array("\r\n", "\r", "\t", "\n"), '', $buffer);
			$buffer = str_replace(array('  ', '    ', '    '), ' ', $buffer);
		} else if ($level == 3){
			/* SIMPLE mode */
			$buffer = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "", $buffer);
			$buffer = str_replace(array("\t", '  '), '', $buffer);	
		} else if ($level == 4){
			/* SAFE MODE B */
			$buffer = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "", $buffer);
			$buffer = str_replace(array("\r\n", "\r", "\t"), '', $buffer);	
			$buffer = str_replace(array('  ', '    ', '    '), ' ', $buffer);
		} else if ($level == 5){
			/* SIMPLE B mode */
			$buffer = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "", $buffer);
			$buffer = str_replace(array("\t", '  '), '', $buffer);	
			$buffer = preg_replace("/>[\n\r]+</msi", "><", $buffer);
		} else if ($level == 6){
			/* SAFE MODE C */
			$buffer = preg_replace('/^\n+|^[\t\s\r]*\n+/',' ',$buffer);
			$buffer = str_replace(array("\r\n", "\r", "\n", "\t"), '', $buffer);
			$buffer = str_replace(array("\t"), '', $buffer);	
			$buffer = preg_replace("/^\s+/msi", "", $buffer);
			$buffer = preg_replace("/>[\n\r]+</msi", "><", $buffer);
			$buffer = str_replace('<![CDATA[', "<![CDATA[\r\n", $buffer);
 			$buffer = preg_replace('/[\"\']text\/javascript[\"\']\>[\s]*\<\!--/i',"'text/javascript'><!--\r\n",$buffer);
			$buffer = str_replace(array('  ', '   ', '    '), ' ', $buffer);
		} else if ($level == 7){
			/* SAFE MODE C+ */
			$buffer = preg_replace('/^\n+|^[\t\s\r]*\n+/','',$buffer);
			$buffer = str_replace(array("\r\n", "\r", "\n"), ' ', $buffer);
			$buffer = str_replace(array("\t"), '', $buffer);
			$buffer = preg_replace("/^\s+/msi", "", $buffer);
			$buffer = preg_replace("/>[\n\r]+</msi", "><", $buffer);
			$buffer = str_replace('<![CDATA[', "<![CDATA[\r\n", $buffer);
 			$buffer = preg_replace('/[\"\']text\/javascript[\"\']\>[\s]*\<\!--/i',"'text/javascript'><!--\r\n",$buffer);
			$buffer = str_replace(array('  ', '   ', '    '), ' ', $buffer);
		} else if ($level == 8){
			/* DeTab only - very clean, lightning fast, super effective and compatible */
			$buffer = str_replace(array("\t"), '', $buffer);
			$buffer = preg_replace("/^\s+/msi", "", $buffer);
		} 
		
	}
	
	public static function checkTextArea(&$buffer, &$plugin){
		if (strstr($buffer, '<textarea')) {
			$plugin->hasTextArea = true;
			return true;
		}
		return false;
	}
	
	public static function _getTextArea(&$buffer, &$plugin) {
		$plugin->tmp=0;
		$plugin->_ta = array();
		$buffer = preg_replace_callback("/<textarea(.*?)<\/textarea>/msi", array($plugin, 'recordTextArea'), $buffer);
	}
	
	public static function _putTextArea(&$buffer, &$plugin) {
		$buffer = str_replace($plugin->_ta[1], $plugin->_ta[0], $buffer);	
	}	

	public static function _getPre(&$buffer, &$plugin) {
		if ($plugin->htmlpre == 1) {
			$found = strstr($buffer, "<pre");
			if ($found == false) {
				$plugin->htmlpre = 0;
				return;
			}
		}
		$plugin->tmp=0;
		$plugin->_pre = array();
		$buffer = preg_replace_callback("/<pre(.*?)<\/pre>/msi", array($plugin, 'recordPre'), $buffer);
	}
	
	public static function _putPre(&$buffer, &$plugin) {
		$buffer = str_replace($plugin->_pre[1], $plugin->_pre[0], $buffer);	
	}
	
	public static function _getBypass(&$buffer, &$plugin) {
		if (!strstr($buffer, "{jfbypass}")) return;
		$plugin->htmlBypass = true;
		$plugin->tmp=0;
		$plugin->_bypass = array();
		$buffer = preg_replace_callback("/\{jfbypass\}(.*?)\{\/jfbypass\}/msi", array($plugin, 'recordBypass'), $buffer);
	}
	
	public static function _putBypass(&$buffer, &$plugin) {
		$buffer = str_replace($plugin->_bypass[1], $plugin->_bypass[0], $buffer);	
	}
	
}

?>
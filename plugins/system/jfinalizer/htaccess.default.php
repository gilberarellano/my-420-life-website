<?php
/**
 * @version		2.0
 * @package		jFinalizer
 * @copyright	(c) 2010 farbfinal.de
 * @license		GNU General Public License version 2 or later;
 *
 * joomla! 1.6+ system plugin
 *
 * documentation & support: www.farbfinal.de/jfinalizer
 *
 * .htaccess file content for gZip .htaccess delivery
 * edit the $hta string depending on your server configuration
 *
 * IMPORTANT NOTE:
 * before you edit the .htaccess code, copy the original file
 * htaccess.default.php to htaccess.custom.php
 *
 * this way, your changes remain after you install a jFinalizer upgrade
 *
 *
*/

defined('_JEXEC') or die;

$hta = 'RewriteEngine On
RewriteBase '.$deliverPath.' 
RewriteCond %{HTTP:Accept-Encoding} .*gzip.*
RewriteRule (.*)\.css $1.css.gz
RewriteRule (.*)\.js $1.js.gz
RemoveType application/x-gzip .gz
AddEncoding x-gzip .gz
ExpiresActive on
ExpiresDefault A604800
ExpiresByType text/css "A604800"
ExpiresByType application/x-javascript "A604800"
<IfModule mod_deflate>
RemoveOutputFilter DEFLATE
</IfModule>
';


?>
<?php
/**
 * @version		2.0.0
 * @package		jFinalizer
 * @copyright	(c) 2010 farbfinal.de
 * @license		GNU General Public License version 2 or later;
 *
 * joomla! 1.6+ system plugin
 *
 * documentation & support: www.farbfinal.de/jfinalizer
 *
 * Helper classes only needed for creation & write, not read
 *
*/

defined('_JEXEC') or die;

/* class jFinalizerBuilder
 * build & compress tools
*/

class jFinalizerBuilder {

	var $jf;
	var $tmp;
	
	function __construct(&$jf){
		$this->jf =& $jf;
	}

	function readAndMerge($list, $name, $type=''){
		jimport('joomla.filesystem.file');
		$buffer = '';
		$fn  = $this->jf->outPath.$name;
		$i=1;
		foreach ($list as $l){
			if ($type=='css') {
				$cs =$this->optimizeCSS($this->replaceURLs(PHP_EOL.$this->openAssetFile($l), $l));
				$buffer .= PHP_EOL.'/* FILE# '.$i.' '.$l.' */'.$cs;
			} else if ($type=='js'){
				$js = $this->optimizeJS(PHP_EOL.$this->openAssetFile($l));
				$buffer .= PHP_EOL.'/* FILE# '.$i.' '.$l.' */'.$js;				
			}
			$i++;
		}
		if ($type=='js' && $this->jf->jQueryNoConflict) $buffer .= "\njQuery.noConflict();\n";
		
		if (!jFinalizerTools::fileWrite($fn, $buffer)) {
			$this->jf->_E(403, $fn);
		};
		if ($this->jf->gzipmode > 0) jFinalizerTools::fileWrite($fn.'.gz', gzencode($buffer));
		return $name;	
	}
	
	function optimizeJS($in){
		if ($this->jf->params->get( 'compactjs', 1)) {
			require_once(JPATH_PLUGINS.DS."system".DS."jfinalizer".DS."jsoptimizer.php");
			return jFinalizerJSOptimizer::_compress($in);
       	}
       	return $in;
	}
	
	function optimizeCSS($in){
		if ($this->jf->params->get( 'compactcss', 1 ) ) {
			require_once(JPATH_PLUGINS.DS."system".DS."jfinalizer".DS."cssoptimizer.php");
			return jFinalizerOutCSSOptimizer::_compress($in);
       	}
       	return $in;
	}

	private function replaceURLs($in, $fn){
		$this->tmp = substr($fn, 0, strrpos($fn, '/')).DS;
		$in = preg_replace_callback('/url\([\'"]?(?![a-z]+:|\/+)([^\'")]+)[\'"]?\)/i', array($this, 'rebuildCssURL'), $in);
		return $in;
	}

	private function rebuildCssURL($matches) {
		$fp = $this->tmp . $matches[1];
		$cp = '';
		while ($fp != $cp) {
			$cp = $fp;
			$fp = preg_replace('`(^|/)(?!\.\./)([^/]+)/\.\./`', '$1', $fp);
		}
		return 'url('. $fp .')';
	}
	
	function openAssetFile($file){
		$fnc = parse_url($file);	
		if (@isset($fnc['host'])) {
			if ($_SERVER['SERVER_NAME'] != $fnc['host']) {	
				$this->_report('<strong>remote file detected</strong>: '.$file);			
				if ($this->jf->incRemote){
					$c = file_get_contents($file);
					if ($c == false){
						$this->jf->_E(402, $file);
					} else {
						return $c;
					}
				}
				return false;	
			}
		}		
		$fn = $this->jf->HTDocRoot.'/'.ltrim($fnc['path'], '/');
		if (file_exists($fn)){
			$this->_report('process: '.$fn);
			return JFile::read($fn);
		} else {
			$this->jf->_E(401, $file);
		}
	}
	
	function _msg($msg){
		if ($this->jf->_eh == false){
			require_once(JPATH_PLUGINS.DS."system".DS."jfinalizer".DS."errorhandler.php");	
			$this->jf->_eh = new jFinalizerErrorHandler();	
		}
		$this->jf->_eh->msg($msg);	
	}
	
	function _report($msg){
		if ($this->jf->params->get('debug') > 1) $this->_msg($msg);
	}
	
	function showDebugInfo($time, $doJS, $doCSS){
		if ($this->jf->params->get( 'debug') < 2) return;

		$time2 	= microtime(true);
		$time3 	= $time2 - $time;
		$mode	= $this->jf->params->get( 'usecache', 1) ? '<strong>LIVE MODE (production)</strong>' : '<strong>DESIGN MODE (develop - slow!)</strong>';
		$js 	= $doJS  ? count($this->jf->jsFiles) : '(JS processing disabled)';
		$css 	= $doCSS ? count($this->jf->cssFiles) : '(CSS processing disabled)';
		$html	= $this->jf->htmlBytesSaved ? $this->jf->htmlBytesSaved : '(HTML processing disabled)';
		$msg = '<br /><strong>jFinalizer runtime Information:</strong><br />'
			.'<strong>plugin total runtime:</strong> '.substr($time3,0,7).'s<br />'
			.'<strong>JS files merged: </strong>'.$js.'<br />'
			.'<strong>CSS files merged: </strong>'.$css.'<br />'
			.'<strong>HTML Bytes saved: </strong>'.$html.'<br />'
			.'<br />'.$mode;
		$this->_msg($msg);
	}

}


/* class jFinalizerTools
 * some tools we dont need instanced
 *
*/
class jFinalizerTools{

	public static function writeHtaccessFile($path, $deliverPath){
		$sPath = JPATH_PLUGINS.DS."system".DS."jfinalizer".DS;
		if (file_exists($sPath.'htaccess.custom.php')){
			include ($sPath.'htaccess.custom.php');
		} else {
			include ($sPath.'htaccess.default.php');
		}
		jFinalizerTools::fileWrite($path.".htaccess", $hta);
	}
	
	public static function writeFlyFiles($dpath, $spath){
		jimport('joomla.filesystem.file');
		JFile::copy($spath.'css.php', $dpath.'css.php');
		JFile::copy($spath.'js.php', $dpath.'js.php');
	}
	
	public static function fileWrite($fn, $buffer){
		$buffer = $buffer.' ';
		if (!$handle = fopen($fn, "w")) {
			echo "jFinalizer Error: cannot create file: ".$fn;
			die;
    	}
		if (!fwrite($handle, $buffer)) {
			echo "jFinalizer Error: cannot write to file: ".$fn;
			exit;
    	}
    	fclose($handle);
    	return true;
    }
}

?>
<?php
/**
 * @version		2.0.1
 * @package		jFinalizer
 * @copyright	(c) 2010 farbfinal.de
 * @license		GNU General Public License version 2 or later;
 *
 * joomla! 1.6+ system plugin
 *
 * documentation & support: www.farbfinal.de/jfinalizer
 *
 * Error Handler Class, 
 * only loaded if errors occour
 *
*/

defined('_JEXEC') or die;

/* class jFinalizerErrorHandler
 * simply a collection of Errors
*/

class jFinalizerErrorHandler {

	var $errors 	= array();
	var $records 	= array();
	var $version;

	function __construct() {
	
		$this->errors[200] = "Subdirectory path set but incorrect:";
		$this->errors[201] = "Resulting physical path from given subdir does not exit: "; 
		$this->errors[202] = "<strong> Tip</strong>: if your joomla installation subdirectory is example.com/joomladir, use <strong>/joomladir</strong>, not joomladir/ or /joomladir/ or joomladir as subdirectory entry"; 
		$this->errors[100] = "cannot create cache path: ";
		$this->errors[101] = "Output Path Alias is set but does not exist: ";		
		$this->errors[102] = "";	
		$this->errors[103] = "";	
		$this->errors[104] = "";	
		$this->errors[401] = "asset file not found: ";	
		$this->errors[402] = "remote file inclusion enabled but file could not be loaded: ";	
		$this->errors[403] = "cannot store asset cache file to disk: ";	
		$this->errors[501] = "cannot open or create file for writing: ";	
		
		$mainframe = JFactory::getApplication();
		$this->version = $mainframe->jFinalizer->version;
		
	}

	public function show($id, $var){
		$this->records[] = "<strong>jFinalizer Error ".$id.":</strong> ".$this->errors[$id].$var."<br />";
	}
	
	public function msg($msg){
		$this->records[] = $msg."<br />";
	}

	public function display(){
		$dump='';
		if (count($this->records)>0){		
			$headText = "<p style='border:0; margin:0;padding:0;border-bottom: 1px solid #888;margin-bottom: 5px; color: #bbb; text-align: center;'><strong>jFINALIZER ".$this->version." :: DEBUG WINDOW</strong></p>";
			$dump = "<div id='jfdebugwin' style='position:absolute;top:0px;right:5px;padding:0px;color:#fff;text-align: left; background: #444; padding: 5; border: 10px solid #444; float: left; font-size: small; z-index: 99999;/*--CSS3 Box Shadows--*/ -webkit-box-shadow: 0px 0px 10px #000; -moz-box-shadow: 0px 0px 10px #000; box-shadow: 0px 0px 10px #000; /*--CSS3 Rounded Corners--*/ -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; opacity: 0.9;'>";
			$dump .= $headText;
			foreach ($this->records AS $r){
				$dump .= $r;
			}
			$dump .= "<p style='border:0; margin:0;padding:0;color: #bbb; text-align: center;'><a href='#' onclick='javascript:document.getElementById(\"jfdebugwin\").style.visibility=\"hidden\";' style='color: #fff; text-decoration:none;'><input type='button' value='CLOSE' /></a><br /><span style='color: #aaa; font-size: xx-small; margin-top: 5px;'>DISABLE THIS WINDOW BY TURNING OF DEBUGGING IN jFINALIZER</span></p>";
			$dump .= "</div>";
			
		}
		return $dump;
	}

}

?>
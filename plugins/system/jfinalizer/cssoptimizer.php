<?php
/**
 * @version		2.0
 * @package		jFinalizer
 * @copyright	(c) 2010 farbfinal.de
 * @license		GNU General Public License version 2 or later;
 *
 * www.farbfinal.de/jfinalizer
 *
 * CSS compressor
 *
 * this routine might not be the most efficient but it works.
 * you can replace it with whatever you want.
 *
 * let us know if you found something for efficient
 *
 *
*/

// no direct access
defined('_JEXEC') or die;

/* wrapper function called by plugin
 * input JS string, output compressed JS string
 * 
*/

class jFinalizerOutCSSOptimizer {

	public static function _compress($buffer) {
		/* code by drupal.org */
		$buffer = preg_replace('<\s*([@{}:;,]|\)\s|\s\()\s* | /\*([^*\\\\]|\*(?!/))+\*/ |[\n\r]>x', '\1', $buffer);
		return $buffer;
	}

}
?>
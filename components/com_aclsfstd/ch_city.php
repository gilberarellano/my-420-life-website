<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

include_once("top.php");
function ch_city()
{ 
global $msg, $msg2, $templ,$indx_url;
 
$thtml= "
<font class='stfntb'> 
<a href='$indx_url'><b>".$msg['top'].":</b></a>
<br>&nbsp;<p><b>
".$msg['choosecity2'].":
</b>
</font><p>
<table width='100%' cellpadding='2' cellspacing='5' class='tb3'>
<tr><td valign='top'> <p align='justify' style='line-height: 1.8;'>
".ch_city2()."
</td></tr></table>
";
 
include($templ['msg']); 
return;

}

function ch_city2()
{
$locations=print_location();

return $locations;
}

?>
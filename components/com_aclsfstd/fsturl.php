<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function sturl_top()
{
global $sturlvar, $urlclscrpt1, $indx_url;
$sturl="$indx_url";
return $sturl;
}


function sturl_ct_top($ct,$getcityval, $page)
{
global $_REQUEST, $sturlvar, $urlclscrpt1, $categories, $sturlctname, $indx_url, $wrtrtdr;
$cityvar=$_REQUEST['city'];
$sturl="{$indx_url}ct=$ct&{$getcityval}";
return $sturl;
}

function sturl_ads($ct, $id, $title)
{
global $_REQUEST, $sturlvar, $urlclscrpt1, $categories, $sturlctname, $indx_url,$sturladtitle;
$sturl="{$indx_url}ct=$ct&md=details&id=$id";
return $sturl;
}

function sturl_ct_ind($ct)
{
global $_REQUEST, $sturlvar, $urlclscrpt1, $categories, $sturlctname, $indx_url, $wrtrtdr ;

$sturl="{$indx_url}md=browse&ct=$ct";

return $sturl;
}

function sturl_ct_pgs($ct, $i, $search_str)
{
global $_REQUEST, $sturlvar, $urlclscrpt1, $categories, $sturlctname, $indx_url, $wrtrtdr;
$sturl="{$indx_url}ct=$ct&md=browse&page=$i&$search_str";
return $sturl;
}

?>
<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

include('config.php');
$id=$_REQUEST['id'];
$wd=$_REQUEST['wd'];
$ht=$_REQUEST['ht'];
$np=$_REQUEST['np'];
 
$photofile=$photos_path."p".$id."n".$np.".jpg";
if ($np=='0'){$photofile=$photos_path."p".$id."prw".".jpg";}

if ($ph_size = @getimagesize($photofile) )
{
$width = $ph_size[0]; 
$height = $ph_size[1]; 

$new_width=$wd;
$new_height=$height/$width*$new_width;

if ($ht!=""){
$new_height=$ht;
$new_width=$width/$height*$new_height;
 
}

}

if ($im1 = @imagecreatefromjpeg($photofile) )
{  
$im2 = imagecreatetruecolor($new_width,$new_height); 
#$im2 = imagecreate($new_width,$new_height); 
@imagecopyresized($im2, $im1, 0, 0, 0, 0, $new_width,$new_height,$width,$height); 
header('Content-type: image/jpeg'); 
imagejpeg($im2); 
imagedestroy($im1); 
imagedestroy($im2);
} 

elseif  ($im1 = @ImageCreateFromGIF ($photofile) )
{  
$im2 = imagecreatetruecolor($new_width,$new_height); 
#$im2 = imagecreate($new_width,$new_height); 
@imagecopyresized($im2, $im1, 0, 0, 0, 0, $new_width,$new_height,$width,$height); 
header('Content-type: image/gif'); 
imagegif($im2); 
imagedestroy($im1); 
imagedestroy($im2);
} 

elseif  ($im1 = @imagecreatefrompng ($photofile) )
{  
$im2 = imagecreatetruecolor($new_width,$new_height); 
#$im2 = imagecreate($new_width,$new_height); 
@imagecopyresized($im2, $im1, 0, 0, 0, 0, $new_width,$new_height,$width,$height); 
header('Content-type: image/gif'); 
imagegif($im2); 
imagedestroy($im1); 
imagedestroy($im2);
} 

?>
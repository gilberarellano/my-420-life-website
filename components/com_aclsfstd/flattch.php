<?php 
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function get_fl_ext($id)
{
global $md, $userfile_name,  $userfile, $photos_count, $table_ads, $HTTP_POST_VARS;

$num_attch=$photos_count;
if (($md=="submitad" or $md=="edit") and file_exists($userfile[$num_attch])) {
$fl_mss=split("\.",$userfile_name[$num_attch]);

if ($fl_mss[1]!=""){
foreach ($fl_mss as $value){$multimext1=".".$value;}
}

$flattextn="flattext".$num_attch;
if ($HTTP_POST_VARS[$flattextn]!=""){$multimext1=".".$HTTP_POST_VARS[$flattextn];}

if(!check_fl_ext($multimext1)){return;}
}
else {
if ($id!=""){
$sql_query="select * from $table_ads where idnum=$id";
$sql_res=mysql_query("$sql_query");
$row = mysql_fetch_array ($sql_res); $multimext1=$row['attflext'];
}}

return $multimext1;
}

function del_edtfl($id)
{
global $md, $userfile_name,  $userfile, $photos_count, $table_ads, $photos_path;

if ($id!=""){
$sql_query="select * from $table_ads where idnum=$id";
$sql_res=mysql_query("$sql_query");
$row = mysql_fetch_array ($sql_res); $multimext1=$row['attflext'];
}

$multimedia_path=$photos_path."mtmd".$id.$multimext1;
 
if (file_exists($multimedia_path)){unlink($multimedia_path);}

}

function check_fl_ext($flext)
{
global $flattchext, $msg2;

$k="";
foreach ($flattchext as $value_ext){if ($value_ext==$flext){$k="1";} }
if ($k!="1"){
$fltps1="";
foreach ($flattchext as $value_ext){$fltps1=$fltps1."$value_ext, ";}
$message="
<center>
<font FACE='ARIAL, HELVETICA' COLOR='#880000' size='-1'> <b>
".$msg2['type_attch']."  $flext  ".$msg2['does_not_match']." <br> $fltps1 
<br>".$msg2['place_corr_fl']." 
 
</b></font>
</center>
";
output_mssg_ex($message); 
return;

}

return 1;
} 


function flattsbv()
{
global $HTTP_POST_VARS;
$fllext1=get_fl_ext(0);
$value=" '$fllext1',  '".$HTTP_POST_VARS['attfltl']."', '".$HTTP_POST_VARS['flsrvtl1']."', 
'".$HTTP_POST_VARS['flsrvur1']."', '".$HTTP_POST_VARS['flsrvtl2']."', '".$HTTP_POST_VARS['flsrvur2']."',
 '".$HTTP_POST_VARS['embdcod']."', ";

return $value;
}

function flattsbf()
{
$value=" attflext, attfltl, flsrvtl1, flsrvur1, flsrvtl2, flsrvur2, embdcod, ";
return $value;
}

function updflattv($ed_id)
{
global $HTTP_POST_VARS;
$fllext1=get_fl_ext($ed_id);

$value=" attflext='$fllext1',  attfltl='".$HTTP_POST_VARS['attfltl']."', flsrvtl1='".$HTTP_POST_VARS['flsrvtl1']."', 
flsrvur1='".$HTTP_POST_VARS['flsrvur1']."', flsrvtl2='".$HTTP_POST_VARS['flsrvtl2']."', flsrvur2='".$HTTP_POST_VARS['flsrvur2']."',
embdcod='".$HTTP_POST_VARS['embdcod']."', ";
return $value;
}
?>
<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function adscounter($idnm, $cnttype)
{
global $table_ads;

$checkres=checkcntlogs($cnttype, $idnm);
if ($checkres=="1"){
if ($cnttype=='vis'){
$sql_query="update $table_ads set cntvstr=cntvstr+1 where idnum=$idnm ";
}

if ($cnttype=='pml'){
$sql_query="update $table_ads set cntemll=cntemll+1 where idnum=$idnm ";
}

mysql_query("$sql_query");
}

return $checkres;
}

function checkcntlogs($cnttype, $idnm)
{ 
global $table_logs, $REMOTE_ADDR, $exprlogs, $uselogsvmv,$waflnscr, $_REQUEST;

if ($waflnscr=="yes"){$remtadrip=$_REQUEST['afflusrip'];} else {$remtadrip=$REMOTE_ADDR;}

$checkres="1";
 
if ($uselogsvmv=='yes'){
$timen1=time();

$checkres="0";
$sql_query="select count(lidnum) from $table_logs 
where ltype='$cnttype' and lip='$remtadrip' and  lidnum=$idnm ";

$sql_res=mysql_query("$sql_query");
$row=mysql_fetch_row($sql_res);
$lcount=$row[0];
if ($lcount > 0){
$checkres="0";
}
else { $checkres="1";

$sql_query="insert into $table_logs (ltime, ltype, lip, lidnum)
values ($timen1, '$cnttype', '$remtadrip', $idnm) ";
mysql_query("$sql_query");
}
$time1hlt=$timen1 -  $exprlogs*86400;
$sql_query="delete from $table_logs where (ltime < $time1hlt) ";
mysql_query("$sql_query");
}
return $checkres;
}

function output_message($message)
{
global $cat_fields, $photos_count, $html_header, $html_footer, $id,
$ct, $categories, $ad_second_width, $left_width_sp, $exp_period, $msg, $templ, $indx_url;

$thtml= "
<center><table width='600'><tr><td>
<font class='stfnt' >
<b><a href='$indx_url'><b>".$msg['top'].":</b></a></b>
&nbsp; &nbsp; 
<a href='{$indx_url}ct=$ct'><b>".$categories[$ct][0]."
</b></a> 
</font>  
<p class='pst1'> &nbsp; <p class='pst1'>
$message
<p class='pst1'> &nbsp; <p class='pst1'>
</tr></td></table>
</center>
";
include($templ['msg']); 
return;
}

function output_mssg_ex($message)
{
global $cat_fields, $photos_count, $html_header, $html_footer, $id, $ht_footer, $ht_header,
$ct, $categories, $ad_second_width, $left_width_sp, $exp_period, $msg, $templ, $indx_url, $style_css_url;

$thtml= "
<html><body>
<link href='$style_css_url' rel='stylesheet' type='text/css'>
$ht_header 
<center><table width='600'><tr><td>
<font class='stfnt' >
<b><a href='$indx_url'><b>".$msg['top'].":</b></a></b>
&nbsp; &nbsp; 
<a href='{$indx_url}ct=$ct'><b>".$categories[$ct][0]."
</b></a> 
</font> 
<p class='pst1'>
$message
<p> &nbsp; <p>
</tr></td></table>
</center>
$ht_footer
</body></html>
";
#include($templ['msg']); 
echo "$thtml";
exit;
}

function getuseradsnum($searchemail)
{
global  $table_ads;
$sql_query="select count(idnum) from $table_ads where email='$searchemail' and visible=1";
$sql_res=mysql_query("$sql_query");
$row = mysql_fetch_row($sql_res);
$mcount=$row[0];

return $mcount;
}

function getmbadsnum($mb1_login)
{
global  $table_ads;
$sql_query="select count(idnum) from $table_ads where login='$mb1_login' and visible=1";
$sql_res=mysql_query("$sql_query");
$row = mysql_fetch_row($sql_res);
$mcount=$row[0];

return $mcount;
}

function check_email($email)
{
global $msg2;
$email=ereg_replace(' ', '', $email);
$a1=split('@', $email);
$a2=split('\.',$email);

if (($a1[0] == "") or ($a1[1] == "") or ($a2[0] == "") or ($a2[1] == ""))
{
$message="
<font class='msgf1' >
 ".$msg2['email_3']." <font color='#AA0000'> $email</font> ".$msg2['has_incr_format']."  
 <font class='stfntb' >
<p> ".$msg2['return_and_fillfl']."
</font></font>
";
output_message($message);
return;
}
return $email;
}


function sendpassw($ps_email)
{
global $cat_fields, $table_ads,$adm_email, $ct, $msg2;
#$ps_email=check_email($ps_email);
$sql_query="select idnum, passw, title from $table_ads where email='$ps_email'";
$sql_res=mysql_query("$sql_query");
 
 
$kll221="0";
while ($row = mysql_fetch_array ($sql_res))
{
$kll221="1";
$p_message="
".$msg2['title_t3'].": ".$row['title'].";
".$msg2['Ad_id1']." : ".$row['idnum'].";
".$msg2['Your_password'].": ".$row['passw'].";
";
$p_subject=$msg2['Your_password'];
 
sndmail($ps_email, $p_subject, $p_message, $adm_email);
} 
 
if ($kll221=="0")
{
$info_message="
<font class='msgf2' >
".$msg2['email_3']." <font class='msgf1' > $ps_email </font> ".$msg2['doesnt_found']."
</font>
";
output_message($info_message);
return;
}

$info_message="
<font class='msgf1' >
".$msg2['login_pass_sent_em']."
</font>
";
output_message($info_message);
return;
}

function send_mail($idnum)
{
global $pm_message, $pm_email, $pm_subject, $msg2,
$sendcopytoadm, $redirtoadm, $adm_email, $categories, $cook_login, $plcntpml, 
$emltp, $emllogin, $pmailtp, $urlclscrpt, $ct, $indx_url;

global $usevrfcode;
if ($usevrfcode=="yes"){include_once("vrfuncs.php"); if(!ch_vrcode()){return;}}

if ($plcntpml=="yes"){ adscounter($idnum, 'pml');}

$fields_val=get_edit_info($idnum); 

$id_email=$fields_val['email'];
$id_num1=$fields_val['idnum'];
$id_title1=$fields_val['title'];
$id_cat1=$fields_val['catname'];
$id_categor1=$categories[$id_cat1][0];

if($emltp=='mblg'){
include("mb_conf.php");
$sql_query="select * from $table_mb where login='$emllogin'";
$sql_res=mysql_query("$sql_query");
$row = mysql_fetch_array ($sql_res);
$id_email=$row['email']; 
} 

if ($cook_login!='')
{
$mb_login_inf="
".$msg2['message_sent_by_member']." '$cook_login'
";
}

global $REMOTE_ADDR;
if (($redirtoadm=='yes') or ($sendcopytoadm=='yes'))
{
$mail_message="
".$msg2['message_from']." $pm_email ".$msg2['to_t2']." $id_email .
".$msg2['Category_3'].":$id_categor1; ".$msg2['Ad_id1'].": $id_num1; ".$msg2['title_t3'].":$id_title1
IP: $REMOTE_ADDR

$pm_message

$mb_login_inf
";
sndmail($adm_email, $pm_subject, $mail_message, $pm_email);
}

if ($redirtoadm!='yes') {
$ad_url="{$indx_url}ct=".$ct."&md=details&"."id=".$id_num1; 
$pmailtp=preg_replace("/--ad_url--/", $ad_url,$pmailtp);
$pmailtp=str_replace("--title--", $id_title1,$pmailtp);
$pmailtp=str_replace("--message--", $pm_message,$pmailtp);
$pmailtp=str_replace("--mb_login_inf--", $mb_login_inf,$pmailtp);
$pmailtp=preg_replace("/--r_email--/", $pm_email, $pmailtp);

global $fb_intrfc; if($fb_intrfc=="yes"){$fbpsndv=fb_prfl_snd();}else{$fbpsndv="";}
$pmailtp=preg_replace("/--facebook_user_profile--/", $fbpsndv, $pmailtp);

$mail_message=$pmailtp;
sndmail($id_email, $pm_subject, $mail_message, $pm_email);
}


$info_message="
<font class='msgf1'>
  ".$msg2['mssg_sent_succssfl']."  
</font>
";

output_message($info_message);
return;
}

function sndmail_bak($email, $subject, $message, $emailfrom) 
{
echo "
<p>mail= $email
<br>subject=$subject
<br>message=$message
<br>From: $emailfrom

";
}

function sndmail($email, $subject, $message, $emailfrom) 
{ global $charsetcd, $adm_email;
$ml_header = "From: $emailfrom\r\n";
$ml_header .= "Content-type: text/plain; charset=utf-8\r\n";
mail($email, $subject, $message,$ml_header);
}

function sndmail_bak3 ($email, $subject, $message, $emailfrom) 
{
mail($email, $subject, $message,"From: $emailfrom");
}

function check_expadsp()
{global $photos_path;
$expdl_file=$photos_path."expdl.dat";
if (file_exists("$expdl_file")){
$tmfl=file("$expdl_file"); 
if($tmfl[0]+86400<time()){write_expdl(); delete_expired_ads();}
}else {write_expdl();}
} 

function write_expdl()
{
global $photos_path;
$time2=time();
$expdl_file=$photos_path."expdl.dat";
if($flnm=@fopen("$expdl_file","w")){ fwrite($flnm, $time2); fclose($flnm);}
}

function delete_expired_ads()
{
global $cat_fields, $table_ads, $exp_period, $exp_perdhlt, $sndexpmsg1;
$time1=time() - $exp_period*86400;
$sql_query="select * from $table_ads where (time < $time1) and (catname!='evntcl') and ((adrate=0) or (ISNULL(adrate)=1)) ";
$sql_res=mysql_query("$sql_query");
while ($row = mysql_fetch_array ($sql_res))
{  
$del_id=$row['idnum'];  
if ($sndexpmsg1=="yes"){include("expmsg.php");}
delete_photos($del_id);
$sql_query="delete from $table_ads where idnum=$del_id";
mysql_query("$sql_query");
 
}

$time1hlt=time() - $exp_perdhlt*86400;
$sql_query="select * from $table_ads where (time < $time1hlt) and (adrate > 0)";
$sql_res=mysql_query("$sql_query");
while ($row = mysql_fetch_array ($sql_res))
{
$del_id=$row['idnum']; 
if ($sndexpmsg1=="yes"){include("expmsg.php");}
delete_photos($del_id);
$sql_query="delete from $table_ads where idnum=$del_id";
mysql_query("$sql_query");
 
}
}

function delete_photos($id)
{
global $photo_path, $multimedia_path, $previewphoto_path, $photos_count;

get_jpg_path($id);
get_att_path($id);

for ($i=1; $i<=$photos_count; $i++)
{
 if (file_exists($photo_path[$i])) {unlink($photo_path[$i]);}
}
if (file_exists($previewphoto_path)) {unlink($previewphoto_path);}
if (file_exists($multimedia_path)) {unlink($multimedia_path);}

}

function get_date($time1)
{
global $months_nm;
$months=$months_nm;
$d=getdate($time1);
$d2=$d['mon'];  
$date_string=$months[$d2-1]." ".$d['mday'].", ".$d['year'];
return $date_string;
 
}

function get_ad_details($id)
{
global $cat_fields, $table_ads, $msg, $msg2, $sqlaflcl;


$sql_query="select * from $table_ads where idnum=$id";
$sql_res=mysql_query("$sql_query");
$row = mysql_fetch_array ($sql_res);
if ($row['idnum']==""){
$message="
<font class='msgf2' >
".$msg['no_ad_with_id']." $id
</font>
";
output_message($message);
return "no_ads";
}
if ($row['visible']!=1){
$message="
<font class='msgf2'>
 <center>
".$msg['inaccessible_ad']."
</center> 
</font>
";   
output_message($message);
return "no_ads";
}
return $row; 
}

function get_edit_info($ed_id)
{
global $cat_fields, $table_ads;
$sql_query="select * from $table_ads where idnum=$ed_id";
$sql_res=mysql_query("$sql_query");
$row = @mysql_fetch_array ($sql_res);

if ($row['idnum']!=""){
foreach ($row as $key => $value)
{
$row[$key]=ereg_replace("'", "&#039;", $row[$key]);
}
}
return $row; 
}

function delete_ad($ed_id)
{
global  $table_ads, $msg2;

delete_photos($ed_id);
$sql_query="delete from $table_ads where idnum=$ed_id";
mysql_query("$sql_query");
 

$message="
<font class='msgf1'> 
  ".$msg2['your_ad_deleted']."  
</font>
";
output_message($message);
return;
}



function get_cat_count($ct)
{
global $cat_fields, $table_ads,  $page, $adsonpage, $schopt, $_REQUEST; 
 if ($_REQUEST[$schopt]!=''){$wrvl="and $schopt='".$_REQUEST[$schopt]."'";};

global $sqlaflcl; 

$sql_query="select count(idnum) from $table_ads where 
catname='$ct' and $sqlaflcl visible=1 $wrvl ";
 
$sql_res=mysql_query("$sql_query");
$row=mysql_fetch_row($sql_res);
$count=$row[0];
return $count;
}

function get_date_update($ct)
{
global $cat_fields, $table_ads,  $page, $adsonpage, $schopt, $_REQUEST; 
 if ($_REQUEST[$schopt]!=''){$wrvl="and $schopt='".$_REQUEST[$schopt]."'";};
 
$sql_query="select time from $table_ads where 
catname='$ct' and visible=1 $wrvl order by time desc ";
$sql_res=mysql_query("$sql_query");
$row=mysql_fetch_row($sql_res);
$time1=$row[0];
$date_update=get_short_date($time1);
if ($time1==0) {$date_update="";}
return $date_update;
}

?>
<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

# Set up categories list

$categories=array(
'title_1' => "Personals",
'manw' =>array("Man Looking for Woman","set1",'age,goal'),
'womenm' =>array("Woman Looking for Man","set1",'age,goal'),
'manm' =>array("Man Looking for Man","set1",'age,goal'),
'womenw' =>array("Woman Looking for Woman","set1",'age,goal'),
'title_2' => "Merchandise",
'merch1' =>array("Appliances","set3",'type'),
'merch2' =>array("Books / Magazine","set3",'type'),
'merch3' =>array("Clothing / Accessories","set3",'type'),
'merch4' =>array("Collectibles","set3",'type'),
'merch5' =>array("Computers","set3",'type'),
'merch6' =>array("Electronics","set3",'type'),
'merch7' =>array("Furniture","set3",'type'),
'merch8' =>array("Sports","set3",'type'),
'merch9' =>array("Jewelries","set3",'type'),
'merch10' =>array("Tickets","set3",'type'),
'merch11' =>array("CD / DVD","set3",'type'),
'merch13' =>array("Music Related","set3",'type'),
'merch14' =>array("Bicycles","set3",'type'),
'merch16' =>array("Other Merchandise","set3",'type'),

'title_3' => "Real Estate",
'rlest1' =>array("Commercial/Office","set3",'type'),
'rlest2' =>array("Farm and Land","set3",'type'),
'rlest3' =>array("Garage / Storage","set3",'type'),
'rlest4' =>array("Residential","set3",'type'),
'rlest5' =>array("Real Estate for Rent","set3",'type'),
'rlest6' =>array("Vacation Rentals","set3",'type'),
'rlest7' =>array("Apartments","set3",'type'),
'rlest8' =>array("Other Real Estate Ads","set3",'type'),

'title_4' => "Community",
'commun1' =>array("Activity Partners","set5"),
'commun2' =>array(" Announcements","set5"),
'commun3' =>array("Events/Parties","set5"),
'commun4' =>array("Friends","set5"),
'commun5' =>array("Lost & Found Items","set5"),
'commun6' =>array("Birthdays","set5"),
'commun7' =>array("Weddings","set5"),
'commun8' =>array("Volunteers ","set5"),

'newcolumn_1' => "",

'title_5' => "Careers, Jobs",
'job1' =>array("Accounting Jobs","set2","jobtype"),
'job2' =>array("Advertising","set2","jobtype"),
'job3' =>array("Administrative/Office","set2","jobtype"),
'job4' =>array("Agriculture ","set2","jobtype"),
'job5' =>array("Arts/Media","set2","jobtype"),
'job6' =>array("Automotive","set2","jobtype"),
'job7' =>array("Banking/Finance","set2","jobtype"),
'job8' =>array("Pharmaceutical","set2","jobtype"),
'job9' =>array("Computer Engineering","set2","jobtype"),
'job11' =>array("Computer Tech. Support","set2","jobtype"),
'job14' =>array("Education Jobs ","set2","jobtype"),
'job15' =>array("Engineering Jobs","set2","jobtype"),
'job16' =>array("Fitness/Sports","set2","jobtype"),
'job17' =>array("Work from Home","set2","jobtype"),
'job18' =>array("Healthcare Jobs","set2","jobtype"),
'job20' =>array("Human Resources","set2","jobtype"),
'job21' =>array("Legal","set2","jobtype"),
'job22' =>array("Manufacturing Jobs","set2","jobtype"),
'job23' =>array("Marketing","set2","jobtype"),
'job24' =>array("General Management","set2","jobtype"),
'job25' =>array("Non Profit Jobs  ","set2","jobtype"),
'job27' =>array("Part-time Jobs","set2","jobtype"),
'job28' =>array("Publishing","set2","jobtype"),
'job29' =>array("Retail Jobs","set2","jobtype"),
'job30' =>array("Transportation","set2","jobtype"),
'job31' =>array("Veterinary Services","set2","jobtype"),
'job32' =>array("Other Jobs","set2","jobtype"),

'title_6' => " Vehicles",
'veh1' =>array("Cars","set3",'type'),
'veh2' =>array("Vans / Minivans","set3",'type'),
'veh3' =>array("Motorcycles","set3",'type'),
'veh4' =>array("Trailers & Other","set3",'type'),
'veh5' =>array("Auto Parts","set3",'type'),
'veh6' =>array("Buses","set3",'type'),
'veh7' =>array("Trucks","set3",'type'),
'veh9' =>array("Watercrafts","set3",'type'),
'veh10' =>array("Other Vehicles","set3",'type'),

'newcolumn_2' => "",

'title_7' => "Software",
'soft1' =>array("Perl Programming","set3",'type'),
'soft2' =>array("PHP Programming","set3",'type'),
'soft3' =>array("ASP Programming","set3",'type'),
'soft4' =>array("Web Design","set3",'type'),
'soft5' =>array("E-Commerce","set3",'type'),
'soft6' =>array("Java, JavaScript","set3",'type'),
'soft7' =>array("C++ Development","set3",'type'),
'soft8' =>array("Delphi Programming","set3",'type'),
'soft9' =>array("Visual Basic","set3",'type'),
'soft10' =>array("UNIX Programming","set3",'type'),
'soft11' =>array("Databases","set3",'type'),
'soft12' =>array("Networking","set3",'type'),

'title_8' => "Services",
'servs1' =>array("Childcare","set3" ),
'servs2' =>array("Elderly Care","set3" ),
'servs3' =>array("Construction","set3" ),
'servs4' =>array("Household Help","set3" ),
'servs5' =>array("Labor Offered","set3" ),
'servs6' =>array("Landscape","set3" ),
'servs7' =>array("Other Services","set3" ),

'title_9' => "Music",
'mus1' =>array("Announcements","set5"),
'mus2' =>array("Musicians","set5"),
'mus3' =>array("Bands","set5"),
'mus4' =>array("Musical Instruments","set5"),
 
'mus5' =>array("Services","set5"),
'mus6' =>array("Collectibles","set5"),
'mus7' =>array("Other Music Ads","set5"),

'title_10' => "Pets",
'pet1' =>array("Birds","set5"),
'pet2' =>array("Cats","set5"),
'pet3' =>array("Dogs","set5"),
'pet4' =>array("Exotic Pets","set5"),
'pet5' =>array("Fish","set5"),
'pet6' =>array("Horses","set5"),
'pet7' =>array("Reptiles","set5"),
'pet8' =>array("Pet Services","set5"),
 
);
 

# Set up the sets of the fields for categories
$fields_sets=array (
'set1' => array (title,name,age,weight,height,smoker,goal,country,city,brief,moredetails,contact_name,contact_phone,email,
homeurl,passw,addrmap),
'set2' => array (title,jobtype,salary,company,country,city,specloc,brief,contact_name,contact_phone,email,homeurl,passw,addrmap),
'set3' => array (title,type,price,company,country,city,specloc,brief,contact_name,contact_phone,email,homeurl,passw,check1),
'set4' => array (title,initialad,comment,email,homeurl,passw),
'set5' => array (title,country,city,specloc,brief,contact_name,contact_phone,email,homeurl,passw),
'set6' => "",
'set7' => ""
);

 

#  Set up description of ad fields used in all categories:
$ads_fields= array(
'title' => array('Title','00','nosearch','40:50','text','1','text'),
'brief' => array('Brief Description','2','nosearch','60:5:200','textarea','1','text'),
'initialad' => array('In the initial ad written','2','nosearch','40:4:400','textarea','0','text'),
'comment' => array('Comment','2','nosearch','40:4:200','textarea','1','text'),
'contact_name' => array('Contact Name','2','nosearch','40:50', 'text', '0','text'),
'contact_phone' => array('Contact Phone','2','nosearch','20:50', 'text', '0','text'),
'email' => array('Contact e-mail','00','nosearch','20:50', 'text', '1','text'),
'homeurl' => array('Home Page URL', '00','nosearch','40:50', 'text','0','text'),
'passw' => array('Edit/delete password','00','nosearch','20:50', 'text','1','text'),
'name' => array('Name','2','nosearch','40:50','text','0','text'),
'country' => array('Country','2','nosearch','40:50','text','0','text'),
'city' => array('City','12','nosearch','1:30','select','1','text', '<option>Akron OH<option>Albany NY<option>Albuquerque NM<option>Allentown PA<option>Anchorage AK<option>Atlanta GA<option>Atlantic City NJ<option>Austin TX<option>Baltimore MD<option>Baton Rouge LA<option>Billings MT<option>Birmingham AL<option>Bismarck ND<option>Boise ID<option>Boston MA<option>Buffalo NY<option>Burlington VT<option>Casper WY<option>Charleston SC<option>Charleston WV<option>Charlotte NC
<option>Chattanooga TN<option>Cheyenne WY<option>Chicago IL<option>Cincinnati OH<option>Cleveland OH<option>Columbia SC<option>Columbus OH<option>Dallas TX<option>Dayton OH<option>Denver CO<option>Des Moines IA<option>Detroit MI<option>El Paso TX<option>Erie PA<option>Eugene OR<option>Fargo ND<option>Fort Smith AR<option>Grand Rapids MI<option>Green Bay WI<option>Greensboro NC<option>Hartford CT<option>Honolulu HI<option>Houston TX<option>Indianopolis IN<option>Jackson MS<option>Jacksonville FL<option>Jersey City NJ<option>Kansas City MO
<option>Knoxville TN<option>Las Vegas NV<option>Lexington KY<option>Little Rock AR<option>Los Angeles CA<option>Louisville KY<option>Madison WI<option>Manchester NH<option>Memphis TN<option>Miami FL<option>Milwaukee WI<option>Minneapolis MN<option>Missoula MT<option>Mobile AL<option>Montgomery AL<option>Nashville TN<option>New Haven CT<option>New Orleans LA<option>New York<option>Norfolk VA<option>Okla. City OK<option>Omaha NE<option>Orlando FL<option>Philadelphia PA<option>Phoenix AZ<option>Pittsburgh PA<option>Portland ME<option>Portland OR<option>Providence RI<option>Raleigh NC<option>Rapid City SD
<option>Reno NV<option>Richmond VA<option>Rochester NY<option>Sacramento CA<option>Saint Louis MO<option>Salt Lake UT<option>San Antonio TX<option>San Diego CA<option>S F Bay Area CA<option>Seattle WA<option>Shreveport LA<option>Sioux Falls SD<option>Spokane WA<option>Stamford CT<option>Syracuse NY<option>Tampa Bay FL
<option>Toledo OH<option>Topeka KS<option>Tucson AZ<option>Tulsa OK<option>Washington DC<option>Wichita KS<option>Wilmington DE<option>Yonkers NY<option>other'),
'goal' => array('Goal','12','keyword','1:30','select', '1', 'text',
        '<option>Marriage<option>Sex<option>Virtual Romance<option>Penpal<option>Other'),	
'check1' => array('checkbox field','2','nosearch','1:30','checkbox','0','text', 
        '<option>checkoption 1<option>checkoption 2<option>checkoption 3' ),
'type' => array('Type','12','keyword','1:30','select', '0','text',
'<option>Wanted<option>Offered' ),	
'jobtype' => array('Type','12','keyword','1:30','select', '1','text',
'<option>Job Offered<option>Job Wanted' ),
'specloc' => array('Specific Location',"2",'nosearch','50:100','text', '0','text'),
'price' =>  array('Price','2','minmax','10:30','text','0','float(10,2)'),
'salary' => array('Salary','2','minmax','10:30','text','0','integer'),
'company' => array('Company',"2",'keyword','20:30','text', '0','text'),
'smoker' => array('Smoker','2','nosearch','1:30','select', '0', 'text',
'<option>Yes<option>No' ),
'age' =>  array('Age','12','keyword','1:30','select', '1', 'text',
        '<option>18-20<option>20-25<option>25-30<option>30-35<option>35-40<option>40-50<option>more 50'),
'weight' =>  array('Weight,kg','2','minmax','10:30','text','0','integer'),
'height' =>  array('Height,cm','2','minmax','10:30','text','0','integer'),	
'moredetails' => array('More Details','2','nosearch','60:10:2000','textarea','0','text'),
'addrmap' => array('Map Address','00','nosearch','40:50','text','0','text'),
'adfld1' => array('reserved field 1 (integer type)','00','nosearch','40:50','text','0','text'),
);


# Dimensions for ads fields
$fld_dim = array(
'weight' => 'kg',
'height' => 'cm',
);

# Dimensions for ads fields which will be printed before value e.g. $
$fld_dimd = array(
'price' => '$',
'salary' => '$'
);

# Set up name of fields sets which will be used  for displaying ads when user clicks on the link of  the group with several  categories; 
# e.g. for category group with key 'title_1' you can set up fields set 'set1' as the following:
#  '1' => 'set1'
$ctgroups_sets=array('1' => 'set1', '2' => 'set3', '3' => 'set3', '4' => 'set5', 
'5' => 'set2','6' => 'set3', '7' => 'set3', '8' => 'set3', '9' => 'set5', '10' => 'set5' );


# set up comments for fields on ads submitting form
$fields_comm=array(
'title' => 'place title of ads here (any comment for ad field)',
'brief' => ' main idea of your ad (any comment for ad field) ',
'age' => '(any comment for ad field)',
'homeurl' => '(any comment for ad field)',
'email' => '(any comment for ad field)',
'moredetails' => '(any comment for ad field)',
'contact_name' => '(any comment for ad field)',
'contact_phone' => '(any comment for ad field)',
'addrmap' => "(address format: <font COLOR='#007700'>number street, city, country</font>)",
'keywords' => "Input several most popular for your ad keywords or phrases, separated by commas. <br>
<a href='index.php?option=com_aclsfstd&md=pclkeyw' target='_blank'>View most popular keywords</a>"
);

 
# set up short names for categories into which reply comments(bids, reply messages)
# can be places 

$reply_catg=array(
'manw' => 'manw_repl',
'womenm' => 'wlkm_repl',
'databas1' => 'databas1',
'evntcl' => 'evcl_rpl'  
);


# Set up short names of the fields which will be displayed 
# on the search form on the top page (it is necessary when search through all categories ) .
$allcatfields=array('title','city','brief');

# Using ads layer when user place mouse over details link on index page (yes, no)
$use_adslayer="yes";
# Set up fields which will be placed on ads layers
$fields_layer=array('eventdt'=>'yes', 'brief'=>'yes', 'weight'=>'yes', 'height'=>'yes', 'name'=>'yes',
'country'=>'yes', 'city'=>'yes', 'goal'=>'yes', 'company'=>'yes', 'type'=>'yes', 'price'=>'yes', 'salary'=>'yes', 'age'=>'yes',
 'height'=>'yes', 'comment'=>'yes');

# table bgcolor for layers of highlighted ad
$tbclhlads="#aa5555";
 



?>
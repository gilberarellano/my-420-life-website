<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

$msg=array(

# top categories list
'categories' => 'Categories:',
'subcategories' => 'more...',
'choosecity' =>'Choose',
'choosecity2' =>'Choose City',
'all_cities' => "All Cities",
'ads_t' => 'ads',
'updated_t' => 'updated',
'choose_category' => 'Choose Category',
'all_categories' => 'All Categories',
'top_ads' => 'Top Ads:',
'events' => 'Events',
'rememb_loc' => 'Remember City',
'next_start_loc' => 'Your next classifieds sessions  will start <br> with  city',
'js_choose_corr_cat' =>" Sorry, you have selected a group title or empty item. Please choose the correct category!",
'pictvl' => 'pic', 
'all_ads1' => "All",
'Please_choose_one_sl' => 'Please choose one',

'no_favorites' => 'No ads have been chosen as favorites at present time',

'top_loading' => "<font class='stfnt' style='color: #777777'>Loading...</font>",
'no_favorites' => 'No ads have been chosen as favorites at present time',
'tp_highlighted_ads' => 'Highlighted Ads:',
'tp_latest_ads' => 'Latest Ads:',


# ads index 
'top' => 'Top',
'ads_match_your_query' => 'ads match your query',
'entries' => 'entries',
'post_new_ad' => 'Post New Ad',
'edit_ad' => 'Edit Ad',
'top_contacted_ads' => 'Top contacted ads',
'top_commented_ads' => 'Top commented ads',
'top_rated_ads' => 'Top rated ads',
'top_visited_ads' => 'Top visited ads',
'ads_rated_by_admin' => 'Ads rated by admin:',
'Best_Ads' => 'Best Ads',
'No_prev_photo' => 'No preview photo',
'view_all_ads' => '(View All Ads)',

# random ads & ads photos gallery
'higlighted_rg' => 'Highlighted',
'random_ads' => 'Random Ads:',
'more_rand_ads' => 'More random ads...',
'view_ads_list' => 'View Ads List',
'view_photos_gallery' => 'View Photos Gallery',
'ads_photos_gallery' => 'Ads Photos Gallery:',
'no_ads_photos_match_q' => 'No ads with photos match this query',

# Search form
'Search' => 'Search',
'Keyword' => 'Keyword',
'only_for_last' => 'Only  for last ',
'days' => 'days',
'only_with_photos' => 'With photos',
# Submit button value 
'searchsubm' => 'Search',
'search_fields_optional' => 'All search fields are optional',

# keywords search
'k_fast_search' => 'Fast Search',
'k_click_to_choose' => 'Click on the item to choose:',
'k_search_help_inf' => 'Search keywords, locations, companies, job vacancies, models...',
'k_more_search_options' => 'More search options',
'k_click_to_choose_category' => '<font class=\'fsrch_3\' style=\'TEXT-DECORATION: underline;\'>Click here</font>  to choose category for more specific search options',
'k_click_to_change_category' => '<font class=\'fsrch_3\' style=\'TEXT-DECORATION: underline;\'>Click here</font>  to change category for search',
'k_hide_form' => 'Hide this form and open fast search',
'k_View_keywords' => 'View classifieds keywords',
'k_subcategories' => 'Subcategories:',
'k_no_ads_try_separately' => 'No ads found for this query. <p> Try to search keywords separately through all ads database :',
'k_no_ads_try_all_db' => 'No ads found for this query. <p>  Try to search keywords through all ads database:',
'k_no_ads_try_other_kw' => 'No ads found for this query. Try other keywords',
'k_search_keywords_all_db' => '&nbsp; &nbsp; &nbsp; Also try to search keywords through all ads database :',
'k_View_keywords_alphabet' => 'View classifieds keywords in alphabet order',
'k_keywords_pop_order' => 'Classifieds keywords in popularity order:',
'k_keywords_index_not_created' => 'Keywords index is not created',
'k_View_keywords_pop' => 'View classifieds keywords in popularity order',
'k_keywords_alphabet_order' => 'Keywords in alphabet order:',

'flag_as_spam' => 'Flag as Spam',
'flag_ad_as_spam' => 'Flag this ad as spam',
'thanks_for_flagging' => 'Thanks for flagging the ad as spam !',

# ads layout
'category' => 'Category',
'adsid' => 'ID#',
'preview_photo' => ' Preview ',
'photoind' => 'Photo',
'posted' => 'Posted',
'no_preview_photo' => 'No preview photo',
'View_initia_ad' => 'View initial ad',
'photos2' => 'photos',
'Details' => 'Details',

# Comments
'ads_posted_by_user' => 'Ads posted by the same user',
'ads_posted_by_member' => 'Ads posted by member ',
'replies_for' => 'Replies for', 
'ad_rp' => 'ad',

# Ads pages listing
'listingads' => 'Listing',
'page_of' => 'Page',
'of_pg' => 'of',
'previous_pg' => 'Previous',
'next_pg' => 'Next',

# Ad details page
'photo_gallery' => 'Photo Gallery',
'photo_d' => 'Photo',
'multimedia_file' => 'Attachment file',
'dtlp_link' => 'Link',
'dtlp_video' => 'Video',
'Click_ph_to_enlarge' => 'Click the photos to enlarge',
'click_ph_to_hide' => 'click the photo to hide',

'privacy_mail' => 'Privacy Mail',
'members_information' => 'Ad owner\'s information',
'browse_ads_by_user' => 'Browse all ads posted by this user',
'ads_d' => 'Ads',
'visits_d' => 'visits',
'no_one_email_sent' => 'no one e-mail was sent to ad owner',
'was_sent_to_owner' => 'was sent to ad owner',
'email_snt' => 'e-mails',
'email_snt_one' => 'e-mail',
'browse_ads_by_member' => 'Browse all ads posted by member',
'Home_Page_d' => 'Home Page',
'contact_email' => 'Contact e-mail',
'Highlight_this_ad' => 'Highlight this ad (for ad owner)',
'ad_posted_member' => 'Ad posted by member',
'date_posted_d' => 'Date posted',
'expire_date_d' => 'Expire date',
'press_to_bookmark' => 'Press Ctrl+D to bookmark this page',
'close_window_d' => 'Close Window',
'not_rated_yet'  => 'Not rated yet. Be first to rate this ad !', 
'current_rate' => 'Current rate',
'votes_v' => 'votes',
'rate_this_ad' => 'Rate this ad',
'no_ad_with_id' => 'No Ad with ID#',

'inaccessible_ad' => 'This ad is inaccessible at present time <br> and will apear 
                      in the index after approving by admin.',

# Comments on detailed page
'Comments_d' => 'Comments',
'post_your_comment' => 'Post your comment',
'posted_c' => 'posted',
'More_c' => 'More...',
'view_all_comments' => 'View all comments',
'your_favorite_ads' => 'Your favorite ads',
'add_to_favorites' => 'Add to favorites',
'remove_this_ad' => 'Remove this ad from favorites',
'remove_all_favorites' => 'Remove all favorites',
'add_ad_to_favorite' => 'Add this ad to favorite list',

'Incorect_operation_v' => 'Incorect operation !',

# login info
'Welcome_m' => 'Welcome',
'Your_ads_m' => 'Your ads',
'Your_Profile_m' => 'Your Profile',
'Log_out_m' => 'Log out',

'first_sel_country' => 'first select the country',
'opt_choose_one' => '<option>Please choose city',
'Choose_Country2' => 'Choose Country',


# Google Map Module
'map_gm' => 'map',
'check_map_your_location' => 'Check map of your location !',
'if_map_not_found_leave_field_blank' => '(if the map is not found then leave the field "Map Address" blank)',
'view_map_gm'=> '<b>View Map</b>',
'you_can_move_map_with_the_cursor' => '(You can move map with the cursor)',
'Find_driving_direction_from_your_location' => 'Find driving direction from your location:',
'address_format_street_city' => '(address format: number street, city, country)',
'go_gm' => 'Go!',
'Map_for_ad_address' => 'Map for ad address:',
'does_not_found_gm' => 'does not found',
'Close_gm' => 'Close',

# PayPal Module

'pp_success_processed' => 'Your PayPal payment was successfully processed  with the following parameters:',
'pp_not_processed' => 'Your PayPal payment was not  processed due to the following reason:',
'pp_http_error' => 'HTTP error of the web server occurred.',
'pp_not_correrct_amount' => 'Payment amount is not correct',
'pp_not_correrct_currency' => 'Payment currency is not correct',
'pp_email_not_correct' => 'PayPal e-mail is not correct',
'pp_item_name' => 'item name',
'pp_item_number' => 'item number',
'pp_payment_status' => 'payment status',
'pp_payment_amount' => 'payment amount',
'pp_payment_currency' => 'payment currency',
'pp_receiver_email' => 'receiver email',
'pp_click_button_proceed_member_subscr' => 'Click on the button to proceed membership subscribing (on-line payment) or  update membership period:',
'pp_click_button_proceed_highlighting' => 'Click on the button to proceed highlighting of  your ad:',
'pp_total_ads_including_activated_ads' => 'Total ads (including activated ads):',
'pp_Discount' => 'Discount: ',
'pp_Price_for_one_ad_activation' => 'Price for one ad activation: ',
'pp_Total_price_to_pay_including_discount' => 'Total price to pay (including discount): ',
'pp_To_proceed_ads_activating_payment_please_click' => 'To proceed  ads activating payment please click on the button:',
'pp_ad_submitted_proceed_activation' => '<font class=\'msgf1\' > 
		 Your ad has been submitted successfully.</font>  <p>
		<font class=\'stfntb\' ><b> 
		To activate your ad you have  to proceed activating payment via PayPal:
 		 </b></font> ',
'pp_you_can_activate_ad_later' => 'You can also  activate your ad later via <a href=\'components/com_aclsfstd/ppact.php\' target=\'_blank\'>Ads Activation Service</a>. 
	When you post more than one ad you will be able to activate
	them by one payment with discount.',
'pp_Ads_Activating_Service' => 'Ads Activating Service',
'pp_Input_your_email' => 'Input your e-mail:',
'pp1_Submit' => 'Submit',
'pp_Non_activated_ads_with_contact_email' => 'Non-activated ads with contact e-mail',
'pp1_are_not_found' => 'are not found',
'pp_Your_non_activated_ads' => 'Your non-activated ads:',
'pp_Edit_Delete_Ads' => 'Edit/Delete Ads',
'pp_Your_payment_was_canceled_by_PayPal' => 'Your payment was canceled by PayPal <br> All details were sent to your e-mail account.',
'pp_Thank_you_for_payment' => 'Thank you for payment. <br> All details were sent to your e-mail account.',

# facebook interface

'logged_as_fb_usr' => 'You are logged as Facebook user',
'fb_your_profile' => 'Your Profile',
'fb_usrclmn' => 'Facebook',
'fb_login_intofb_acc' => 'Please log in into your Facebook account.',
'only_fb_users' => 'View only ads of facebook users',
'fb_view_all_ads' => 'View all ads',
'fb_profile_ad_owner' => 'Facebook profile of ad owner:',
'fb_click_place_comm' => 'You can click Like button and/or place your comment about this ad:',
'fb_or_lgfrm' => 'or',
'fb_frm_click_here' => 'Click here',
'fb_proceed_ad_placing' => 'to proceed  placing of ad without facebook users\' profile info',
'place_ad_login_fb' => 'To place ad please log in into your Facebook account:',
'fb_proceed_pm_snd' => 'to proceed   sending of privacy mail without facebook users\' profile info',
'send_pm_login_fb' => 'To send privacy mail to ad owner please log in into your Facebook account: ',
'fb_soc_interface' => 'Facebook social interface:',
'fb_pm_snd_profile' => 'This privacy mail was sent by the following facebook user:',
'fb_userind' => 'facebook'
);

# Short names for months
$months_short =array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
# full names for month
$months_nm=array('January', 'February', 'March', 'April', 'May', 'June',  
'July', 'August', 'September', 'October', 'November','December');

$weekdays=array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
?>
<?php

/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

include("funcs2.php");

function ad_details()
{
global $cat_fields, $photos_count, $html_header, $html_footer, $id,
$ct, $categories, $ad_second_width, $left_width_sp, $exp_period,$exp_perdhlt, $privacy_mail,
$schallusrads, $detl_leftcol, $tbclr_1, $tbclr_2, $tbclr_3, $tbclr_4, $fntclr_1, $evnt_cat,
$fntclr_3, $plcntdtl, $plcntpml, $reply_catg, $pladddp, $hltadsf, $msg, $det_cnt, 
$prphscnd, $templ, $javastl, $use_gl_map, $row, $fld_dim, $fld_dimd, $indx_url, $jpath_url;

global $clmnmap; $clmnmap="no";

if($ct!=$row['catname']){$ct="";}

if ($plcntdtl=='yes'){adscounter($id, 'vis');}

$row=get_ad_details($id);
if ($row=="no_ads"){return;}

if($ct==""){$ct=$row['catname']; $cat_fields=""; getfldlst();}
$row=check_row($row);
 

$repl_cmminf="";

if ($row['adcommkey']==1){
$view_intad="
<a href='{$indx_url}md=details&id=".$row['replyid']."' >
  ".$msg['View_initia_ad']."(#".$row['replyid'].")</a>
<br>
";
}


if ($schallusrads=='yes')
{
$useradsnum=getuseradsnum($row['email']);
if ($useradsnum > 1)
{
$all_ads_user="
 
<br><a href='{$indx_url}md=browse&idemail=".$row['idnum']."' >
 ".$msg['browse_ads_by_user']."</b></a> ($useradsnum ".$msg['ads_d'].")
 
";
}
}

if ($plcntdtl=='yes'){
$det_cnt="
<nobr>".$row['cntvstr']." ".$msg['visits_d']."</nobr>
";
}

if ($plcntpml=='yes'){
if ($row['cntemll']==0){
$det_pml="&nbsp; <font class='smallfnt'>
(".$msg['no_one_email_sent'].")</font>";
}
else{
if ($row['cntemll']>1){$eml_msg=$msg['email_snt'];}  else {$eml_msg=$msg['email_snt_one'];}
$det_pml="&nbsp;&nbsp; <font class='smallfnt'>(".$row['cntemll']." $eml_msg 
 ".$msg['was_sent_to_owner'].")</font>";
}
}
 

if ($row['login']!='')
{

if ($pladddp=='yes')
{
$addt_info="<p>".getadditinfo($row['login'], $id);
}

$mbadsnum=getmbadsnum($row['login']);

if ($mbadsnum > 1)
{
$all_ads_user="
<br><a href='{$indx_url}md=browse&mblogin=".$row['login']."' >
 ".$msg['browse_ads_by_member']." '".$row['login']."'</a> ($mbadsnum 
".$msg['ads_d'].")
";
}
}

$time1=$row['time'];
$date_posted=get_date($time1); 
$time2=$time1+$exp_period*86400;
if ($row['adrate'] > 0){$time2=$time1+$exp_perdhlt*86400;}
$expire_date=get_date($time2);

if ($privacy_mail=='yes') {
$cont_email="<a href='{$indx_url}ct=$ct&md=privacy_mail&idnum=$id'>
<b> ".$msg['privacy_mail']."</b></a>";}
else{
$cont_email="<a href='mailto:".$row['email']."'><b>".$row['email']."</b></a>";
}

$homepg_info="";
if ($row['homeurl']!="--"){
$homepg_info="<p class='pst1'>".$msg['Home_Page_d'].":<a href='".$row['homeurl']."'>".$row['homeurl']."</a> ";
}

$contemail_info="";
if ($row['email']!="--" and $row['email']!=""){
$contemail_info=$msg['contact_email'].":<b> $cont_email</b> <br>$det_pml";
}

$idnum=$row['idnum'];

$sdtpcol="$tbclr_4";
$sdtpcol1="$tbclr_3";
$ad_sec_rt=$ad_second_width-$left_width_sp-1;

$repltitle="<title>".$row['title'].".  ";
$html_header=ereg_replace('<title>', $repltitle, $html_header);

$hltadsinfo="";

include($templ['details']);
return;
}


function lrg_photo($idnum)
{
global  $_REQUEST, $photos_url, $photos_path, $photo_path, $photo_url, $photos_count,$multim_link;

$lrgphprez="";
 
if ($_REQUEST['lrgphp']=='1')
{
$lnph=$_REQUEST['lnph'];
get_jpg_path($idnum);

global $maximgswith; $imgsize1=GetImageSize($photo_path[$lnph]);
$imgwidth1=""; if ($imgsize1[0] > $maximgswith){ $imgwidth1="width='$maximgswith'";}
 
$lrgphprez="
<center><p>
<img src='$photo_url[$lnph]' $imgwidth1>   
</center>
";
}

return $lrgphprez;
}


function print_photos($idnum, $row)
{
global  $ct, $photos_url, $photos_path, $photo_path, $photo_url, $urlclscrpt, 
$photos_count,$multim_link, $prphscnd, $msg, $indx_url, $jpath_url;
$pho1="";
get_jpg_path($idnum);
for($i=1; $i<=$photos_count; $i++)
{
if (file_exists($photo_path[$i])){$pho1="1";} 
 
}
 
if ($pho1==""){return;}

$html="
<center>
 <font class='smallfnt' style='color: #999999'>
".$msg['Click_ph_to_enlarge']." 
</font>
</center>
";
for($i=1; $i<=$photos_count; $i++)
{
if (file_exists($photo_path[$i])){
$photokey="photocaption$i";
$photocapt=$row[$photokey];

$tmvrld=time();
global $use_ajax, $preloadpht;
if ($use_ajax=="yes"){$lphtpgurl=$photo_url[$i]."?{$tmvrld}";
 $prldphv=$prldphv."im{$i}=new Image; im{$i}.src=\"$lphtpgurl\";
";
}  
else {$lphtpgurl="{$indx_url}ct=$ct&md=details&id=$idnum&lrgphp=1&lnph=$i&";}


$html=$html."
<font class='smallb' >
".$msg['photo_d']." $i 
<center><a href='$lphtpgurl' style='a:hover{text-decoration:none;}'>
<img src='".$urlclscrpt."{$jpath_url}sph.php?id=$idnum&wd=$prphscnd&np=$i&tmv=$tmvrld' border=0 align='top' > 
</a></center>
</font>
";
}
}

global $dsplhltpht, $row;
if (($dsplhltpht=="yes") and (($row['adrate'] < 1) or ($row['adrate']==""))){
if (file_exists($photo_path[1])){
$html="
<center>
<img src='".$urlphtn1."{$jpath_url}sph.php?id=$idnum&wd=$prphscnd&np=1' border=0> 
</center>
";

}
}

if ($use_ajax=="yes" and $preloadpht=="yes" and $dsplhltpht!="yes"){
$html=$html."
<script type=\"text/javascript\">
$prldphv
</script>
";
}

return $html;
}

function print_multimed($idnum)
{
global $incl_mtmdfile,$multimedia_path, $multimedia_url, $multim_link, $msg, $row;
$mm_link="";  
if ($incl_mtmdfile=='yes')
{
get_att_path($idnum);

if (file_exists($multimedia_path)) 
{
$attflsz1=round(filesize($multimedia_path)/1000);
if ($attflsz1==0){$attflsz1=1;}
$mm_link="<font class='stfnt'>
<p class='pst1'>".$msg['multimedia_file'].": 
<a href='$multimedia_url'>".$row['attfltl']." (".$row['attflext'].", ".$attflsz1."Kb)</a> 
</font>
";
 
}

if (($row['flsrvur1']!="") and ($row['flsrvur1']!="http://")){
$mm_link=$mm_link."<p class='pst1'> ".$msg['dtlp_link'].": 
<a href='".$row['flsrvur1']."' target='_blank'>".$row['flsrvtl1']."</a> 
";
}

if (($row['flsrvur2']!="") and ($row['flsrvur2']!="http://")){
$mm_link=$mm_link." <p class='pst1'> ".$msg['dtlp_link'].":    
 <a href='".$row['flsrvur2']."' target='_blank'>".$row['flsrvtl2']."</a> 
";
}
if ($row['embdcod']!=""){
$row_embcod=ereg_replace('<embed', "<embed wmode='opaque' ", $row['embdcod']);

if(preg_match('/youtube/',$row_embcod) and preg_match('/<iframe/',$row_embcod)){
preg_match('/src="(.*)"/U',$row_embcod,$match1);
$row_embcod=str_replace($match1[1], $match1[1]."?wmode=transparent",  $row_embcod); 
}

$mm_link=$mm_link."<p class='pst1'>".$msg['dtlp_video'].":<p class='pst1'> ".$row_embcod;
}

}

global $dsplhltpht;
if (($dsplhltpht=="yes") and (($row['adrate'] < 1) or ($row['adrate']==""))){$mm_link="";}
 
return  $mm_link;
}

function getadditinfo($mbblogin, $id)
{  
global   $privacy_mail, $msg;

$sdtpcol="#ffffff";
$sdtpcol1="#dddddd";

include("mb_conf.php");

$sql_query="select * from $table_mb where login='$mbblogin'";
$sql_res=mysql_query("$sql_query");
$row = mysql_fetch_array ($sql_res);
$row=check_row($row);

if ($privacy_mail=='yes') {
$row['email']="<a href='{$indx_url}ct=$ct&md=privacy_mail&idnum=$id&emltp=mblg&emllogin=$mbblogin'>
".$msg['privacy_mail']."</a>";}
else{
$row['email']="<a href='mailto:".$row['email']."'>".$row['email']."</a>";
}

$rslt="
<font class='stfnt'>
<b>".$msg['members_information'].": 
</font>
<table width='100%'  border=0   cellspacing=3 cellpadding=0>
";

$keyffl="0";
foreach ( $mb_fields as $key => $value )
{
if ((ereg('3', $mb_fields[$key][1])) and ($row[$key]!=""))
{$keyffl="1";
if ($key=="homeurl")
{$row[$key]="<a href='".$row[$key]."'  >$row[$key]</a>";}
$rslt=$rslt."
<tr><td colspan='2' height='1'  bgcolor='#eeeeee'><spacer type='block' height='1' width='1'></td></tr>
<tr><td bgcolor='$sdtpcol' width='25%'>
<font class='stfnt'>
&nbsp; ".$mb_fields[$key][0].":</font>
</td><td bgcolor='$sdtpcol' width='75%'>
<font class='stfnt'>
 ".$row[$key]." &nbsp; </td></tr>
";
}
}
$rslt=$rslt."
<tr><td colspan='2' height='1' bgcolor='#eeeeee'><spacer type='block' height='1' width='1'></td></tr>
</table>
";

if ($keyffl!="1"){$rslt="";}
return $rslt;

}

?>

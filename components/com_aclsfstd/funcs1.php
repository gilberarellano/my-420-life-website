<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function get_short_date($time1)
{ global $months_short;
$months=$months_short;
$d=getdate($time1);
$d2=$d['mon'];  
$date_string=$months[$d2-1]." ".$d['mday'];
return $date_string;
}


function get_date_f($date1)
{ 
global $months_short, $weekdays;

$months1=array('january', 'february', 'march', 'april', 'may', 'june',  
'july', 'august', 'september', 'october', 'november','december');

$months=$months_short;
$dt_mss=split('-',$date1); 
$d2=$dt_mss[1];

$dtstr=$dt_mss[2]." ".$months1[$d2-1]." ".$dt_mss[0];
$evnttime=strtotime($dtstr)+1000;
$evntd=getdate($evnttime);
$evntd2=$evntd['wday'];  

$evntwkd=$weekdays[$evntd2];

$date_string=$months[$d2-1]." ".$dt_mss[2]." $evntwkd";
return $date_string;
}

function slctgetstr($value)
{
$value=ereg_replace(' ', '+', $value);
$value=ereg_replace('@', '%40', $value);
$value=ereg_replace('!', '%21', $value);
return $value;
}


function get_ads_count()
{
global $cat_fields, $table_ads, $ct, $page, $adsonpage, $html_header, 
$html_footer; 
 
$where_string=get_where_string();
$sql_query="select count(idnum) from $table_ads where 
$where_string "; 
if( !($sql_res=@mysql_query("$sql_query")))
{echo $html_header;
echo "
<center>
<font FACE='ARIAL, HELVETICA'  COLOR='#bb0000' size=-1><b>
Error in connecting to ads MySQL table <font color='#000099'>'$table_ads'</font>.
<br> Seems, this table is not created. 
</b></font></center>
";
echo $html_footer;
exit;
}
$row=mysql_fetch_row($sql_res);
$count=$row[0];
return $count; }  function adscnt1() {echo "count=".get_ads_count().";";}

function delete_expevents()
{
global $cat_fields, $table_ads, $exp_period, $exp_perdhlt,$ct, $pstdevc, $flattchext;

if ($flattchext!=""){include_once("flattch.php");}

$time1=time() - $exp_period*86400;
$sql_query="select idnum from $table_ads where (catname='$ct') and 
(CURRENT_DATE > DATE_ADD(eventdt, INTERVAL $pstdevc DAY) or  eventdt='0000-00-00') ";

$sql_res=mysql_query("$sql_query");
while ($row = mysql_fetch_array ($sql_res))
{  
$del_id=$row['idnum'];
delete_events_photos($del_id);
$sql_query="delete from $table_ads where idnum=$del_id";
mysql_query("$sql_query");
 
}
 
}

function delete_events_photos($id)
{
global $photo_path, $multimedia_path, $previewphoto_path, $photos_count;

get_jpg_path($id);
get_att_path($id);

for ($i=1; $i<=$photos_count; $i++)
{
 if (file_exists($photo_path[$i])) {unlink($photo_path[$i]);}
}
if (file_exists($previewphoto_path)) {unlink($previewphoto_path);}
if (file_exists($multimedia_path)) {unlink($multimedia_path);}
}

function get_hltads()
{
global $cat_fields, $table_ads,$ct, $page, $adsonpage, $cnt_htl_ind, $cnt_htl_det, 
$ratedads, $plhltads, $HTTP_GET_VARS, $vstminnmbr,  $evnt_cat, $hltlstk; 

$ctnm_q=""; 

if ($ct!=''){$ctnm_q="catname='$ct' and ";}  

if ($HTTP_GET_VARS['gmct']!= ""){$ctnm_q=get_mctsql($HTTP_GET_VARS['gmct']);}


if ($_REQUEST['md']=='details'){$cnt_limit=$cnt_htl_det; $dstid=" and (idnum!=".$_REQUEST['id'].") ";} 
else {$cnt_limit=$cnt_htl_ind;} 

if ($_REQUEST['city']!=""){$cityvl=ereg_replace("'","\\'",$_REQUEST['city']); $city_qr="and city='$cityvl'";}


global $gladsqr;
if($HTTP_GET_VARS['adsgal']=='1'){$sqlph=" and adphotos='yes' ";} 

$sql_query="select * from $table_ads where  (($ctnm_q adrate>0 $city_qr) or (adrate=2 $city_qr) or (adrate>2)) and 
 visible=1 $sqlph $dstid
order  by  rand()  limit $cnt_limit ";

$sql_res=mysql_query("$sql_query");
 
$hltlstk="h";

return $sql_res;
}

function get_ads()
{
global $cat_fields, $table_ads,$ct, $page, $adsonpage, 
$ratedads, $plhltads, $HTTP_GET_VARS, $vstminnmbr,  $evnt_cat, $hltlstk, $ordhltrate; 

$ord_ratedads="";  
if ($hltadsf='yes' and $ordhltrate=="yes"){$ord_ratedads="adrate desc,";} 

if ($evnt_cat[$ct]=="yes") {delete_expevents($ct); $ord_ratedads="eventdt,";}

if ($HTTP_GET_VARS['adsordtp']=='vote'){$ord_ratedads=$ord_ratedads." ratevtrt desc, ";}
if ($HTTP_GET_VARS['adsordtp']=='vis'){$ord_ratedads=$ord_ratedads." cntvstr desc, "; }
if ($HTTP_GET_VARS['adsordtp']=='pml'){$ord_ratedads=$ord_ratedads." cntemll desc, ";}
if ($HTTP_GET_VARS['adsordtp']=='cmmnt'){$ord_ratedads=$ord_ratedads." replcnts desc, ";}
$html_ads="";
$start_num=($page-1)*$adsonpage; 
if($page=="")$start_num=0;
$where_string=get_where_string();

if ($ct==''){$adcmord='adcommkey,';} else {$adcmord='';}
 
$sql_query="select * from $table_ads where $where_string  
order by $ord_ratedads $adcmord idnum desc limit $start_num, $adsonpage";

$sql_res=mysql_query("$sql_query");
 
$hltlstk=""; 

return $sql_res;
}



function get_mctsql($nmct)
{
global $sql_mct, $categories, $gmct_name;

if ($sql_mct=="")
{
foreach ($categories as $key => $value){
$ms22=split("_",$key); 
if($ms22[0]== 'title'){$kk1=0;}
if($kk1==1 and $key!="evntcl" and  $key!="evcl_rpl" 
and $categories[$key][2]!="h" and $ms22[0]!='newcolumn'){$sql_mct=$sql_mct." catname='$key' or";}
if($ms22[0]== 'title' and $ms22[1]== $nmct){$kk1=1; $gmct_name=$categories[$key];}
}

$sql_mct=preg_replace ("/or$/", "", $sql_mct);
$sql_mct="( $sql_mct ) and ";
}
return $sql_mct;
}




function get_where_string()
{
global $cat_fields, $ct, $table_ads, $HTTP_GET_VARS, $text_userinfo, $fntclr_1, $msg,
$vstminnmbr, $sql_mct, $categories, $indx_url;

if ($HTTP_GET_VARS['brief_key']!=""){
if(get_magic_quotes_gpc()){$brf_rpq="\'";}else{$brf_rpq="'";}
$HTTP_GET_VARS['brief_key']=ereg_replace("&#039;", $brf_rpq, $HTTP_GET_VARS['brief_key']); 
}
if(!get_magic_quotes_gpc()){
foreach ($HTTP_GET_VARS as $key => $value )
{$HTTP_GET_VARS[$key]=addslashes($HTTP_GET_VARS[$key]);
}}

global $sqlaflcl;
$adctnm1="$sqlaflcl visible=1 and ";
if ($HTTP_GET_VARS['visunvis'] == "1") {$adctnm1="";}
if ($HTTP_GET_VARS['ct'] != "")
{
$adctnm1=$adctnm1."catname='$ct' and ";
}

if ($HTTP_GET_VARS['gmct']!= ""){$adctnm1=$adctnm1.get_mctsql($HTTP_GET_VARS['gmct']);}


$where_string=$adctnm1;
$tm_check=time() - $HTTP_GET_VARS['before']*86400;

if ($HTTP_GET_VARS['before'] != "")
{$where_string=$where_string."time > $tm_check and "; 
}

if ($HTTP_GET_VARS['idemail'] != "")
{
$var_idemail=$HTTP_GET_VARS['idemail'];
$sql_query="select email from $table_ads where idnum='$var_idemail'";
$sql_res=mysql_query("$sql_query");
$row = mysql_fetch_row($sql_res);
$searchemail=$row[0];
$where_string=$where_string."email='$searchemail' and "; 
$text_userinfo="
<font class='stfntb'>
<b>".$msg['ads_posted_by_user']."</b>
</font>
";
}

if ($HTTP_GET_VARS['mblogin'] != "")
{$where_string=$where_string."login='".$HTTP_GET_VARS['mblogin']."' and ";
$text_userinfo="
<font  class='stfntb'>
 ".$msg['ads_posted_by_member']."<b>'".$HTTP_GET_VARS['mblogin']."'</b>
</font>
";
}

$rplidval=$HTTP_GET_VARS['replyid'];
if ($rplidval != "")

{$where_string=$where_string."replyid=$rplidval and ";
$text_userinfo="&nbsp;&nbsp; &nbsp;&nbsp; 
<font class='stfntb'>
<b>".$msg['replies_for']." 
<a href='{$indx_url}md=details&id=$rplidval' target='inad$rplidval'>
".$msg['ad_rp']."(#$rplidval) 
</a>
</b>
</font>
";
}


if ($HTTP_GET_VARS['ratedads'] != "")
{$where_string=$where_string."adrate > 0 and ";
}

if ($HTTP_GET_VARS['adsordtp']=='pml')
{$where_string=$where_string."cntemll > 0 and ";
}

if ($HTTP_GET_VARS['adsordtp']=='cmmnt')
{$where_string=$where_string."replcnts > 0 and ";
}
 
if ($HTTP_GET_VARS['adsordtp']=='vote')
{$where_string=$where_string."ratevtcn > 0 and ";
}

global $gladsqr;
if ($HTTP_GET_VARS['onlywithphoto'] != "" or  $HTTP_GET_VARS['adsgal']=='1')
{$where_string=$where_string."adphotos='yes' and ";
}

if ($HTTP_GET_VARS['ipaddr1'] != "")
{$where_string=$where_string."ipaddr1='".$HTTP_GET_VARS['ipaddr1']."' and ";
}

if ($HTTP_GET_VARS['idnum'] != "") 
{$where_string=$where_string."idnum=".$HTTP_GET_VARS['idnum']." and ";
 }

global $use_fltxtind;
if ($HTTP_GET_VARS['brief_key'] != "") 
if ($use_fltxtind=="yes"){$where_string=$where_string.kw_wherestr();}
else{
{$where_string=$where_string.
"(brief like '%".$HTTP_GET_VARS['brief_key']."%' or title like '%".$HTTP_GET_VARS['brief_key']."%') and ";
}
}

if ($HTTP_GET_VARS['email']!=""){
$where_string=$where_string."email='".$HTTP_GET_VARS['email']."' and ";
 $cat_fields['email'][2]='keyword';}


foreach ($cat_fields as $key => $value )
{

if($cat_fields[$key][2] == "keyword" or $key=='city') 
{
 
if($HTTP_GET_VARS['fsloc']=='all'){$HTTP_GET_VARS['city']="";}

if ($HTTP_GET_VARS[$key] != "") {  
if (($key=="city") and 
($HTTP_GET_VARS['mblogin']!="" or $HTTP_GET_VARS['idemail']!="" 
or $HTTP_GET_VARS['replyid']!="" or $HTTP_GET_VARS['email']!="" ))

{$kkttrv=1;} else
{$where_string=$where_string."$key like '%".$HTTP_GET_VARS[$key]."%' and ";}
 }
}
if($cat_fields[$key][2] == "minmax")
{
$flmin=$key."1";
$flmax=$key."2";
if (($HTTP_GET_VARS[$flmin] != "") and ($HTTP_GET_VARS[$flmax] != "")) 
{
$HTTP_GET_VARS[$flmin]=ereg_replace(',', '', $HTTP_GET_VARS[$flmin]);
$HTTP_GET_VARS[$flmax]=ereg_replace(',', '', $HTTP_GET_VARS[$flmax]);

$HTTP_GET_VARS[$flmin]=preg_replace ('/[A-Za-z]|\$|,|\?|\+/', "", $HTTP_GET_VARS[$flmin]);
$HTTP_GET_VARS[$flmax]=preg_replace ('/[A-Za-z]|\$|,|\?|\+/', "", $HTTP_GET_VARS[$flmax]);



$where_string=$where_string."$key >= ".$HTTP_GET_VARS[$flmin]."
 and $key <= ".$HTTP_GET_VARS[$flmax]." and ";
}
}
}
$where_string=corr_wherestring($where_string);

if ($HTTP_GET_VARS['mfvrt']=='1'){$where_string=view_fv_ads();}

$valcntvstr=""; 
if ($HTTP_GET_VARS['adsordtp']=='vis'){$valcntvstr="cntvstr > $vstminnmbr and "; }
$where_string=$valcntvstr.$where_string; 
 
return $where_string;
}

function corr_wherestring($string1)
{
$string1=$string1."fdspkdsanbf";
$db_dcf="and fdspkdsanbf";
$string1=ereg_replace($db_dcf,"",$string1);
return $string1;
}


function get_jpg_path($id_count)
{
global $photo_url, $photo_path,  
$photos_url, $photos_path, $photos_count,
$previewphoto_url, $previewphoto_path, $multimedia_path,
$multimedia_url, $multim_ext;

for ($i=1; $i<=$photos_count; $i++)
{
$photo_url[$i]=$photos_url."p".$id_count."n".$i.".jpg";
$photo_path[$i]=$photos_path."p".$id_count."n".$i.".jpg";
}

$previewphoto_url=$photos_url."p".$id_count."prw".".jpg";
$previewphoto_path=$photos_path."p".$id_count."prw".".jpg";
}

function prnlg(){echo "<img src='http://www.almondsoft.com/logo/logo9.php' height='1' width='1'>";
}

function get_att_path($id_count)
{
global $photo_url, $photo_path,  
$photos_url, $photos_path, $photos_count,
$previewphoto_url, $previewphoto_path, $multimedia_path,
$multimedia_url, $multim_ext, $flattchext, $indx_url;

 
if ($flattchext!=""){$multim_ext=get_fl_ext($id_count);}

$multimedia_url=$photos_url."mtmd".$id_count.$multim_ext;
$multimedia_path=$photos_path."mtmd".$id_count.$multim_ext;
} 

function ads_pages_list()
{ $maxcntplnk=20;
global $ads_count, $adsonpage, $ct, $page, $idemail, $mblogin,  $ratedads, $visunvis, $msg;
$search_str=get_srch_str();
if ($idemail!=""){$search_str="idemail=$idemail";}
if ($mblogin!=""){
$search_str="mblogin=$mblogin";
if ($visunvis=="1"){$search_str="mblogin=$mblogin&visunvis=1";}
}
if ($ratedads=="1"){$search_str="ratedads=1";}
$num_pages=($ads_count-$ads_count%$adsonpage)/$adsonpage;
if ($ads_count%$adsonpage > 0) {$num_pages++;}

 
if($num_pages > $maxcntplnk){
$crntpg=$page;
if($crntpg==""){$crntpg=1;}
$maxcntpl2=round($maxcntplnk/2);
$lstpg_st=$crntpg - $maxcntpl2;
$lstpg_fn=$crntpg + $maxcntpl2;
 
if ($lstpg_st>0){$i_pgst=$lstpg_st;} else {$i_pgst=1;}
if ($lstpg_fn<$num_pages){$i_pgfn=$lstpg_fn;} else { $i_pgst=$num_pages-$maxcntplnk; $i_pgfn=$num_pages;}
if ($lstpg_st<=0 ){$i_pgfn=$maxcntplnk;}

}
else{
$i_pgst=1;
$i_pgfn=$num_pages;
}

$list_pages="";

for ($i = $i_pgst; $i <= $i_pgfn; $i++) 
{
if ($i != $page){
$list_pages=$list_pages." [<a href='".sturl_ct_pgs($ct, $i, $search_str)."'>$i</a>]";
}
else
{
$list_pages=$list_pages." [<font color='#ee0000'>$i</font>]";
}
}
if ($lstpg_st>1){
$list_pages= " [<a href='".sturl_ct_pgs($ct, 1, $search_str)."'>1</a>] ... ".$list_pages;
}

if ( ($lstpg_fn<$num_pages) and ($num_pages > $maxcntplnk) ){
$list_pages= $list_pages." ... [<a href='".sturl_ct_pgs($ct, $num_pages, $search_str)."'>$num_pages</a>]";
}


$list_pages=$msg['listingads'].":".$list_pages;
if ($ads_count == 0) $list_pages="";
return $list_pages;

}
 

function get_srch_str()
{
global $page, $ads_count, $adsonpage, $ct, $mds, $HTTP_GET_VARS;
$search_str="";
if($HTTP_GET_VARS['gmct'] !== ''){
# $search_str="gmct=".$HTTP_GET_VARS['gmct']."&";
$mds_res=1;
}

 
 

$mds_res=0;
#if($HTTP_GET_VARS['mds'] == 'search')
#{
foreach ($HTTP_GET_VARS as $key => $value)
{
if (($key !='md')  and ($key !='page') and ($key !='option') and 
($key !='ct') and !preg_match('/quot;/',$key))
{$mds_res=1;
$value=ereg_replace(' ', '+', $value);
$value=ereg_replace('@', '%40', $value);
$value=ereg_replace('!', '%21', $value);

if($HTTP_GET_VARS['fsearch']=='1' and $key=='brief_key'){$value="";}
if($HTTP_GET_VARS['fsearch']!='1' and $key=='keyword'){$value="";}

$search_str=$search_str."$key=$value&";
 
}
#}
}
if ($mds_res==0)
{
$mds="";
$HTTP_GET_VARS['mds']="";
}
return $search_str;

}

function pages_next_prev()
{
global $page, $ads_count, $adsonpage, $ct, $HTTP_GET_VARS, $idemail, $mblogin,
 $ratedads, $visunvis, $msg, $indx_url;
$search_str=get_srch_str();
if ($idemail!=""){$search_str="idemail=$idemail";}
if ($mblogin!=""){
$search_str="mblogin=$mblogin";
if ($visunvis=="1"){$search_str="mblogin=$mblogin&visunvis=1";}
}
if ($ratedads=="1"){$search_str="ratedads=1";}
$max_pages=($ads_count-$ads_count%$adsonpage)/$adsonpage;
if ($ads_count%$adsonpage > 0) {$max_pages++;}
$next_prev="";
if ($page>1)
{
$a1_prev=$page-1;
$next_prev=$next_prev.
"<a href='".sturl_ct_pgs($ct, $a1_prev, $search_str)."'>".$msg['previous_pg']."</a>";
}
if($page==0)$page=1;
$next_prev=$next_prev." ".$msg['page_of']." $page ".$msg['of_pg']." $max_pages ";
$a1_next=$page+1;
$a2=$page*$adsonpage;
if ($a2 < $ads_count)
{
$next_prev=$next_prev.
"<a href='".sturl_ct_pgs($ct, $a1_next, $search_str)."'>".$msg['next_pg']."</a>";
}
if ($ads_count == 0) $next_prev="";
return $next_prev;
}



function start()
{
global $ct,$ads_fields,$categories, $ads_count, $cat_fields,$fields_sets,$allcatfields;
$c_res1=0;
foreach ($categories as $key => $value)
{
if ($key == $ct ){$c_res1=1;}
}
if ($c_res1==0){
if ($ct !=""){
echo $html_header;
echo "<h3> Incorrect Category </h3>";
echo $html_footer;
exit;
}
}
 
getfldlst();
 
$ads_count=get_ads_count();
}

function getfldlst()
{
global $ct,$ads_fields, $categories, $ctgroups_sets, $_REQUEST,
$ads_count, $cat_fields,$fields_sets,$allcatfields;

if ($ct!="")
{
$a1=$categories[$ct][1];
$a2=$fields_sets[$a1];
}
if ($a2=="")
{$a2=$allcatfields;}

$gmctvl=$_REQUEST['gmct'];
if ($gmctvl!="" and $ctgroups_sets[$gmctvl]!=""){$a1=$ctgroups_sets[$gmctvl]; $a2=$fields_sets[$a1];}

foreach ($a2 as $a2_value)
{
$cat_fields[$a2_value]=$ads_fields[$a2_value];
}

}

 

function corr_sqlstring($string1)
{
$string1=$string1."fdspkdsnbf";
$db_dcf=", fdspkdsnbf";
$string1=ereg_replace($db_dcf,"",$string1);
return $string1;
}

 
?>
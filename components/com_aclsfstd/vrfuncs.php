<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

global $use_hdn_vrfc, $use_cookie_vrfc;

$use_cookie_vrfc="yes";

function vrchtml()
{
global $urlclscrpt, $msg2, $jpath_url, $use_cookie_vrfc, $use_hdn_vrfc;


$tmvl1=time();
$tmsrt=substr($tmvl1,-5);


if($use_hdn_vrfc=="yes"){
$html="
<input type='hidden' name='verfcode' value=''>
<input type='hidden' name='vcdnumb' value='$tmvl1'>
";
}
else{
$urlimgcd=$jpath_url."vrfcd.php?idcd=$tmsrt";
$html="
<table width='500' border=0 cellspacing=1 cellpadding=1>
<tr><td width='350'>
 ".$msg2['entr_vrfcode']."
<input type='text' name='verfcode' size='5' class=formst>
<input type='hidden' name='vcdnumb' value='$tmvl1'>
 
</td><td width='150' >
<img src='$urlimgcd'> 
</td></tr></table>
<br>
 
";
}

if ($use_hdn_vrfc=='yes' or $use_cookie_vrfc=='yes'){
$vcdshrt=substr($tmvl1,-5);
$crcdv1=cr_vrcd($vcdshrt); 
setcookie("vrfck1", $crcdv1);
}

return $html;
}

function vrfcjs_add()
{
global $use_cookie_vrfc, $use_hdn_vrfc, $msg2;
$html="
var vrfc = document.cookie.match ( '(^|;) ?'+'vrfck1'+'=([^;]*)(;|$)' );
var vrfld = document.forms['f'].elements['verfcode'].value;
if(vrfc[2]!='' && vrfc[2]!=vrfld){
alert(\"".$msg2['vrf_cd_notcor_alrt']."\");
return false; 
}
";

if ($use_hdn_vrfc=='yes'){
$html="
var vrfc = document.cookie.match ( '(^|;) ?'+'vrfck1'+'=([^;]*)(;|$)' );
document.forms['f'].elements['verfcode'].value=vrfc[2];
";
}

if ($use_cookie_vrfc!='yes'){$html="";}
 
return $html;
}

function vrfcjs_chck()
{
global $use_cookie_vrfc, $use_hdn_vrfc, $msg2 ;
$html="
<script language='JavaScript'>
function vrfcjch()
{
";

if ($use_hdn_vrfc!="yes"){
$html=$html."
var vrfc = document.cookie.match ( '(^|;) ?'+'vrfck1'+'=([^;]*)(;|$)' );
var vrfld = document.forms['f'].elements['verfcode'].value;
if(vrfc[2]!='' && vrfc[2]!=vrfld){
alert(\"".$msg2['vrf_cd_notcor_alrt']."\");
return false; 
}
";

} else {
$html=$html."
var vrfc = document.cookie.match ( '(^|;) ?'+'vrfck1'+'=([^;]*)(;|$)' );
document.forms['f'].elements['verfcode'].value=vrfc[2];
";
}
 
$html=$html."
document.forms['f'].submit();
}
</script>
<input type='button' value='".$msg2['Send_p']."' class=formst STYLE='FONT-WEIGHT: bold;'
 onClick=\"vrfcjch();\" >
";
return $html;
}


function cr_vrcd($idcd)
{
$crcodev=crypt($idcd,'aa');
$crcodev=substr($crcodev,2,5);
$crcodev=strtolower($crcodev);

$crcodev=ereg_replace('l','a',$crcodev);
$crcodev=ereg_replace('/','b',$crcodev);
$crcodev=ereg_replace('\.','c',$crcodev);
$crcodev=ereg_replace('0','d',$crcodev);
$crcodev=ereg_replace('o','e',$crcodev);
$crcodev=ereg_replace('1','f',$crcodev);

return $crcodev;
}

function ch_vrcode()
{
global $_REQUEST, $msg2;
 
$vcdshrt=substr($_REQUEST['vcdnumb'],-5);

$tmrslt=time()-$_REQUEST['vcdnumb'];

$crcdv1=cr_vrcd($vcdshrt); 

$vrfcd=strtolower($_REQUEST['verfcode']);

if(($crcdv1!=$vrfcd) or ($tmrslt > 7000) ){
$message=$msg2['vrf_cd_notcor'];
output_message($message);
return 0;

}

return 1;
}


?>
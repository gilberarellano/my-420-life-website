<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

# String of list of fields in which keywords will be searched;
$kwfldsrch="title, brief, moredetails,  specloc, city, company, jobtype, goal, name";

# Count real amount of ads with keyword   for keywords index (yes,no). 
# it requires more time for creating keywords index comparing with aproximate count.
$kwrealamount='yes';

# Max amount of ads for indexing per one clink on the link 'Create keywords index'
$kwind_one_q=10000;

#setlocale(LC_ALL, 'UTF-8' );
#echo system('locale -a'); 

###################

global $brd_notnull;
if(!is_null($_REQUEST['brief_key'])){$brd_notnull=1;}else{$brd_notnull=0;}

global $jm_cmpath;
 
if ($HTTP_GET_VARS['keyword']!="")
{$HTTP_GET_VARS['brief_key']=$HTTP_GET_VARS['keyword'];}
else{ if($HTTP_GET_VARS['brief_key']!=""){
$HTTP_GET_VARS['keyword']=$HTTP_GET_VARS['brief_key'];}}
  
  
function strToLower_kw($str)
{global $charsetcd;
if(@function_exists('mb_strtolower')){return mb_strtolower($str, "utf-8");}else{return strToLower($str); }
}

function strToUpper_kw($str)
{global $charsetcd;
if(@function_exists('mb_strtoupper')){return mb_strtoupper($str, "utf-8");}else{return strToUpper($str); }
}

function ucwords_kw($str)
{global $charsetcd;
if(@function_exists('mb_convert_case')){return mb_convert_case($str, MB_CASE_TITLE, "utf-8");} 
else{return ucwords($str);}
}

function kw_searchf()
{
global $urlclscrpt, $msg, $_REQUEST, $HTTP_GET_VARS, $brd_notnull, $kwsrchfrm, $use_ajax;

if ($kwsrchfrm=="yes"){
if ($use_ajax=="yes"){ $html=kw_searchf1();}
else {$html=kw_searchf2(); }
} else {$html="<br>";}

return $html;
}

function kw_searchf1()
{  
$kwcolor1='#555555';
$kwcolor2='#000077';

global $urlclscrpt, $msg, $_REQUEST, $HTTP_GET_VARS, $brd_notnull, $kwsrchfrm, $use_ajax, $jm_cmpath,
 $indx_url, $jpath_url, $indxjf_get, $urlclscrpt1, $fbpind2;

$fstsrchtxt=$msg['k_fast_search'];

if ($HTTP_GET_VARS['keyword']!=""){ 
$kwfstsrch=$HTTP_GET_VARS['keyword'];
$kwfstsrch=preg_replace('/\'/', '&#039;', $kwfstsrch);
$kwfstsrch=stripslashes($kwfstsrch);
$kwstl="style='color:{$kwcolor2}'";
} else {
$kwfstsrch=$fstsrchtxt;
$kwstl="style='color:{$kwcolor1}'";
 
}

if ($brd_notnull and $_REQUEST['fsearch']!='1'){$fsrchst1="style='display:none;'";}

if ($_REQUEST['kwcity1']=='1'){$kwcitycheck2='checked';}else{$kwcitycheck1="checked";}

if ($_REQUEST['kwmsrch']=='1'){
$kwjmosrchpt="
jq(document).ready(function(){
jq('#srch').show(); 
jq('#kwfstsrch').hide();
jq('#sbctg').hide();
});
";
}

$html="  
<script language='JavaScript'>

$kwjmosrchpt

jq(document).click(function() {
jq('#srchd').hide();});

function rplc(value1) {
jq('#srchstrf').val(value1);
jq('#srchd').hide(''); 
}
function hdsrchd() {
jq('#srchd').hide(); 
}

function kwhideform(){
jq('#srch').hide('');
jq('#sbctg').show('2000');
jq('#kwfstsrch').show('');
}
</script> 
<table width='100%' cellpadding='0' cellspacing='0' height='55' ><tr><td > 
<table width='600' cellpadding='8' cellspacing='8'>
<form action='{$urlclscrpt1}index{$fbpind2}.php'  method='get' id='fs'>
<tr><td>
<div id='kwfstsrch' $fsrchst1>
$indxjf_get
<input type='hidden' name='md' value='browse'>
<input type='hidden' name='mds' value='search'>
<input type='hidden' name='ct' value='".$_REQUEST['ct']."'>
<input type='hidden' name='fsearch' value='1'>
";
if($_REQUEST['gmct']!= ''){$html=$html."<input type='hidden' name='gmct' value='".$_REQUEST['gmct']."'>";} 
$html=$html."
<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td width='200' style='padding:1px;'>
<input type='text'  class='kwfsrch' style='width:300px;'    id='srchstrf'    name='keyword'    $kwstl
value='$kwfstsrch' 
 onFocus=\"if (this.value=='$fstsrchtxt'){this.value=''; this.style.color='{$kwcolor2}';};\"
onBlur=\"if (this.value==''){this.value='$fstsrchtxt';this.style.color='{$kwcolor1}';};   \"   
><br>

<div id='srchd' STYLE=\"DISPLAY: none; position: absolute;\"> 
<table   class='srchdtb' border='0' cellpadding='0' cellspacing='0'>
<tr><td valign='top'> 
<font class='kwsgfnt'>".$msg['k_click_to_choose']."</font>
<div id='srchd2'></div>
</td></tr></table>
</div> 
</td>
<td width='50' style='padding:0px;'>
<input type='button' value='Search' class='kwfsrch' onClick=\"if(jq('#srchstrf').val()!='$fstsrchtxt'){document.forms['fs'].submit();}\"> 
</td><td>
&nbsp <nobr>
<font  OnClick=\"jq('#srch').show('1000'); jq('#kwfstsrch').hide(); jq('#sbctg').hide();\"  
style='color: #3333aa; cursor: pointer;TEXT-DECORATION: underline;'>
".$msg['k_more_search_options']."</font></nobr>
</td></tr></form></table> 
</div>   
</td></tr></table>
".kwprn_search_form()." 
".kwsrchmsg($ads_count)."
</td></tr></table>  
";

return $html;

}

function kw_searchf2()
{  
global $urlclscrpt, $msg, $_REQUEST, $HTTP_GET_VARS, $brd_notnull, $kwsrchfrm, 
$use_ajax, $indx_url, $indxjf_get, $urlclscrpt1, $fbpind2;

$kwfstsrch=$HTTP_GET_VARS['keyword'];
$kwfstsrch=preg_replace('/\'/', '&#039;', $kwfstsrch);
$kwfstsrch=stripslashes($kwfstsrch);
$html="
<table width='300' cellpadding='10' cellspacing='10'  >
<form action='{$urlclscrpt1}index{$fbpind2}.php'  method='get' id='fs'>
<tr><td style='padding:4px;'>
$indxjf_get
<input type='hidden' name='md' value='browse'>
<input type='hidden' name='mds' value='search'>
<input type='hidden' name='ct' value='".$_REQUEST['ct']."'>
<input type='hidden' name='fsearch' value='1'>
";
if($_REQUEST['gmct']!= ''){$html=$html."<input type='hidden' name='gmct' value='".$_REQUEST['gmct']."'>";} 
$html=$html."
<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td width='50' style='padding:1px;'>
<input type='text'  style='width:300px;'   id='srchstrf' class='kwfsrch' name='keyword' value='$kwfstsrch'>
<td width='50' style='padding:0px;'>
<input type='submit' value='Search' class='kwfsrch'> 
</td></tr></form></table> 
</td></tr></table>
".kwsrchmsg($ads_count)."
";

return $html;

}

function kwprn_search_form()
{
global $_REQUEST, $msg, $kwsrchfrm, $ct, $msg, $HTTP_GET_VARS, $brd_notnull, $jm_cmpath, $indx_url, $urlclscrpt1;
$html="";

if ($kwsrchfrm=="yes"){

if ($_REQUEST['kwcity1']=='1'){$kwcitycheck2='checked';}else{$kwcitycheck1="checked";}
if($_REQUEST['city']!="" and $ct==""){$kwcityv="<font style='color: #5555cc;'>(".$_REQUEST['city'].")</font>";}
 
if ($ct=="" and $_REQUEST['gmct']==""){ 
$tiptxtv=$msg['k_click_to_choose_category'];}
else{
$tiptxtv=$msg['k_click_to_change_category'];
}

$kwgt_keyword=preg_replace('/\'/', '&#039;', $HTTP_GET_VARS['keyword']);
$kwgt_keyword=stripslashes($kwgt_keyword);

$tiptxt="
<script language='JavaScript'>
function fkwchct(){
jq('#kwchct').html(\"".$msg['top_loading']."\").load('{$urlclscrpt1}{$jm_cmpath}keywsearch.php', 
{md: 'kwcl', brief_key: '$kwgt_keyword'}); 
return false; 
};
</script>
<div id='kwchct'>
<font   OnClick='fkwchct();' class='fsrch_3' 
style='cursor: pointer;'>
<nobr>$tiptxtv</nobr>
</font>
</div>
";

if(($_REQUEST['mds']=='search') and ($brd_notnull and $_REQUEST['fsearch']!='1')
 and ($_REQUEST['adsordtp']=='')){$dislpval='block';} else {$dislpval='none';} 

$html=$html."<DIV id='srch' style='DISPLAY: {$dislpval}'> 
<table width='600' cellpadding='1' cellspacing='1' class='srchtb'  >
<tr><td valign='top' style='padding:4px;'> 
<table width='100%' cellpadding='0' cellspacing='2' class='srchtb2'><tr><td style='margin:1px;padding:2px;'>
<font class='fnt4'>".$msg['Search']."</font> &nbsp; &nbsp; $kwcityv
</td>$kwshcity<td align='right' style='text-align: right; padding:4px;'>&nbsp; 
<font  OnClick='kwhideform();' class='fsrch_3' 
style='cursor: pointer;TEXT-DECORATION: underline;'>
".$msg['k_hide_form']."</font>
</td></tr></table>
".get_search_form()."
<p class='pst1'>
<table width='100%'><tr><td style='padding:4px;'>
 $tiptxt 
</td></tr></table>
</td></tr></table>
&nbsp;  
</div>
";
}
return $html;
}


function prn_subcategories()
{ global $_REQUEST, $check_subcateg, $categories, $ct, $kwsrchfrm, $msg, $brd_notnull;
 
if($categories[$ct][2]!="" and $kwsrchfrm=="yes"){ 

if(($_REQUEST['mds']=='search') and ( $brd_notnull  and  $kwsrchfrm=="yes")
 and ($_REQUEST['adsordtp']=='')){$dislpval='none';} else {$dislpval='block';} 
$html=$html."<DIV id='sbctg' style='DISPLAY: {$dislpval}'>";
$html=$html."
<table width='100%' cellpadding='0' cellspacing='0'><tr><td valign='top' width='90'> 
<font class='stfnt' style='line-height: 1.8;'>".$msg['k_subcategories']."</font>
</td><td valign='top' align='left'>
<span style='line-height: 1.8;'>".print_subcat($ct)."
</span> </td></tr></table>
</div> 
"; 
}
return $html;
 
}

function kwsrchmsg($ads_count)
{ 
global $HTTP_GET_VARS, $_REQUEST, $kwsrchfrm, $ads_count, $msg, $kw_no_ads;
$kwcount=0;
$kwadscount=0;
$kwstr="";

$keywordv=$HTTP_GET_VARS['keyword'];

if($_REQUEST['mds']=='search' and $kwsrchfrm=='yes' and $keywordv!="")
{

$keywordv=stripslashes($keywordv);

$kwqr=" $keywordv ";
preg_match_all('/"(.*)"/U',$kwqr,$matches);
$kwstr="";
foreach ($matches[1] as $value){
if ($value!=""){
#$tcnt=get_kwcnt($value);
 
$tcnt=get_realkwcnt($value,'');
if($tcnt >0){$kwadscount=$kwadscount+$tcnt; $kwcount++; $kwstr=$kwstr.print_kwlink($value,$tcnt,"0");}
}}

$kwqr=preg_replace('/"(.*)"/U',"", $kwqr);

#preg_match_all("/(\b[\w|']+\b)/s",$kwqr,$matches);
preg_match_all("/([\w|\pL|\pN|']+)/su",$kwqr,$matches);

foreach ($matches[1] as $value){
if ($value!=""){
#$tcnt=get_kwcnt($value);
  
$tcnt=get_realkwcnt($value,'');
if($tcnt >0){$kwadscount=$kwadscount+$tcnt; $kwcount++; $kwstr=$kwstr.print_kwlink($value,$tcnt,"0");}
}
}

if($ads_count==0 and $kwadscount >0 and $kwcount>1){
$res_html=" ".$msg['k_no_ads_try_separately']." $kwstr";
}

if($ads_count==0 and $kwadscount >0 and $kwcount==1){
$res_html=" ".$msg['k_no_ads_try_all_db']." $kwstr";
}

if($ads_count==0 and $kwadscount==0){
$res_html=$msg['k_no_ads_try_other_kw'];
}

global $keywords_search_info;
if($ads_count>0 and $kwadscount > $ads_count ){
$keywords_search_info=" ".$msg['k_search_keywords_all_db']." $kwstr";
}

if($res_html!=""){$res_html="<p class='pst1'><font class='kwmsgf1'> $res_html </font><p class='pst1'>";} 

if($ads_count==0){$kw_no_ads=1;}
 
}
 
return $res_html;
}

function get_realkwcnt($keywrd, $adid)
{
global $table_ads, $kwfldsrch;
$keywrd=stripslashes($keywrd);
$keywrd=preg_replace('/\&\#039\;/', '\'', $keywrd);
$keywrd=addslashes($keywrd);
if ($adid!=""){$whrvl=" and idnum!=$adid ";}
$sql_query="select count(idnum) from $table_ads where 
            MATCH ($kwfldsrch)  AGAINST ('\"$keywrd\"' IN BOOLEAN MODE) and visible=1 $whrvl ";
$sql_res=mysql_query("$sql_query");
$row=mysql_fetch_row($sql_res);
return $row[0];
}


function get_kwcnt($keywrd)
{
global $keywrdtable;

$sql_query="select * from $keywrdtable where keyword='$keywrd' ";
$sql_res=mysql_query("$sql_query");
$row = mysql_fetch_array($sql_res);

return $row['kwcnt'];
}

function kw_wherestr()
{
global $HTTP_GET_VARS, $kwfldsrch;

$keywordv=$HTTP_GET_VARS['keyword'];

if ($keywordv!=""){
$keywordv=stripslashes($keywordv);
$keywordv=preg_replace('/\&\#039\;/', '\'', $keywordv); 
 
$keywordstr=get_kwqrstr($keywordv); 

$where_str="MATCH ($kwfldsrch)   AGAINST ('$keywordstr' IN BOOLEAN MODE) and ";
}

return $where_str;
}

function get_kwqrstr($kwqr)
{
$kwqr=" $kwqr ";
preg_match_all('/"(.*)"/U',$kwqr,$matches);
$kwstr="";
foreach ($matches[1] as $value){ $value=addslashes($value); $kwstr=$kwstr."+\"$value\" ";}
$kwqr=preg_replace('/"(.*)"/U',"", $kwqr);

$kwqr=" $kwqr ";

#preg_match_all("/(\b[\w|']+\b)/s",$kwqr,$matches);
preg_match_all("/([\w|\pL|\pN|']+)/su",$kwqr,$matches);

foreach ($matches[1] as $value){
if (strlen($value)>2)
{$value=preg_replace('/\"/','&quote;',$value); $value=addslashes($value); $kwstr=$kwstr."+{$value} ";}
}

 
return $kwstr;
}


function get_kwflds()
{
global $kwlst_fields1, $kwlst_fields2;
$kwflds="";
foreach ($kwlst_fields1 as $value) { $kwflds=$kwflds.$value. ", ";}
foreach ($kwlst_fields2 as $value) { $kwflds=$kwflds.$value. ", ";}
$kwflds=preg_replace("/, $/", " ", $kwflds);

return $kwflds;
}


function get_kwrds($row,$mwrd)
{ 
global $kwlst_fields1, $kwlst_fields2, $kwminln, $kwlst_prdfields, 
 $kwfrcharc, $kwcmmdlmt, $kwaddrdlmt, $kwsplcdlmt;
$mwrd1="";
 
foreach ($kwlst_fields1 as $value) {if($row[$value]!="")
{$rval=$row[$value]; 

if(!in_array($value, $kwlst_prdfields))
{
$rval=strToLower_kw($rval);
  
if($kwfrcharc!='1'){
#$rval=preg_replace('/\b(\w)/e',"strToUpper('\\1')",$rval);
$rval=ucwords_kw($rval);
}

}

$mwrd1[$rval]++;
}}


foreach ($kwlst_fields2 as $value) 
{
if($row[$value]!=""){ 
if((($value=='keywords'  and $kwcmmdlmt=='yes') or ($value=='addrmap' and $kwaddrdlmt=='yes')
 or ($value=='specloc' and $kwsplcdlmt=='yes') ) and preg_match('/,/',$row[$value]))
{$wrds = preg_split ("/[,]+/",$row[$value]);}
else{$rvarval=" ".$row[$value]." "; 

#preg_match_all("/(\b[\w|']+\b)/s",$rvarval,$matches); 
preg_match_all("/([\w|\pL|\pN|']+)/su",$rvarval,$matches); 

$wrds=$matches[1];}
foreach ($wrds as $value1)
{$value1=trim($value1); 
if($value1!="" and !preg_match('/^[\d,-]+$/',$value1) ){ 
if(!in_array($value, $kwlst_prdfields)){

$value1=strToLower_kw($value1);
if($kwfrcharc!='1'){
#if(($value=='keywords'  and $kwcmmdlmt=='yes') or ($value=='addrmap' and $kwaddrdlmt=='yes')
#or ($value=='specloc' and $kwsplcdlmt=='yes')){ 
#$value1=preg_replace('/\b(\w)/e',"strToUpper('\\1')",$value1); 

$value1=ucwords_kw($value1);
} 
$mwrd1[$value1]++;
}
}
}
}
} 

if($mwrd1!=""){
foreach ($mwrd1 as $key => $value){ if (strlen($key)>=$kwminln){$mwrd[$key]++; }}
}

return $mwrd;
}

function print_kwlink($key,$value,$lnkundrl)
{global $indx_url;
if ($lnkundrl=="1"){$kwklnk1="class='kwlnk'";}

#if(preg_match('/\W/',$key))
if(preg_match('/\PL/u',$key))
{$key1="&quot;".$key."&quot;";}else{$key1=$key;}
#$key1=addslashes($key1);
$key1=preg_replace('/\'/', '&#039;', $key1);
$key1=stripslashes($key1);
$html=" &nbsp; <nobr><a href='{$indx_url}md=browse&mds=search&fsearch=1&fsloc=all&keyword=$key1' $kwklnk1>$key ($value)</a></nobr> &nbsp; ";

return $html;
}

?>
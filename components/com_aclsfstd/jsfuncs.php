<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

# preload photos on ads details pages
$preloadpht="yes";


$urlclscrpt2=$jm_cmpath;

global $itemidvl; $itemidvl=$_REQUEST['Itemid'];

global $sturlvar; if ($sturlvar=="yes"){$urlclscrpt2=$urlclscrpt1.$jm_cmpath;}

function jqueryld()
{
global $use_ajax, $urlclscrpt2, $urlclscrpt1;
if ($use_ajax=="yes"){
$urlclscrpt3=$urlclscrpt2;
echo "<script src=\"{$urlclscrpt3}jquery-1.4.2.min.js\" type=\"text/javascript\"></script> 
<script>var jq = jQuery.noConflict(); </script>
"; 
}
}

function jqphtgal()
{
global $use_ajax, $urlclscrpt, $msg;
if ($use_ajax=="yes"){
echo "
<script language='JavaScript'>
jq(document).ready(function(){ 
        jq('#lrimg').hide();
	jq('#pht1 a').click(function(){
        jq('#lrimg').hide(); jq('#dvlimg').show(); 
        jq('#dvlimg2').html(\"<p>&nbsp;<p>".$msg['top_loading']."<p>&nbsp;<p>\");
         var imgpth = jq(this).attr('href'); 
	jq('#lrimg').attr({ src: imgpth });  
        jq('#lrimg').fadeIn('1000');
        jq('#dvlimg2').html(\"<font class='smallfnt' style='color: #999999'>".$msg['click_ph_to_hide']."</font>\");
          return false;
}); 

jq('#lrimg').load(function(){jq('#lrimg').fadeIn('1000'); 
jq('#dvlimg2').html(\"<font class='smallfnt' style='color: #999999;text-align:center;'>".$msg['click_ph_to_hide']."</font>\");
}); 

jq('#dvlimg').click(function(){jq('#dvlimg').fadeOut('');
});
});
</script>
";
}
}

function js_lrimg()
{
global $use_ajax, $msg;
if ($use_ajax=="yes"){

echo "<br>
<div id='dvlimg' STYLE=\"DISPLAY: none; position: absolute; z-index: 5; \">
<table   width='450' bgcolor='#ffffff' border=0 cellspacing=10 cellpadding=0 class='tb4'>
<tr><td style='padding:10px;'>
<center>  
<img   src='' id='lrimg'>

<br><div id='dvlimg2'  style='width:100%; text-align: center;padding: 5px;'></div>

</center>
</td></tr></table>
<p>
 
</div>
";
}
}

function js_displ2()
{
$js_res="
<script language='JavaScript'>
function displ(nmdiv){
jq('#' + nmdiv).slideToggle('2000'); }
</script>
";
return $js_res;
}


function js_displ()
{
$js_res="
<script language='JavaScript'>
function displ(nmdiv){
var nmdiv1 = '#' + nmdiv;
 var atrvr = jq(nmdiv1).css('display'); 
if(atrvr=='none') jq(nmdiv1).slideDown('slow');  else jq(nmdiv1).fadeOut(500);
}

 
 
</script>
";
return $js_res;
}
 


function js_layerv()
{

$htmlvar="
<script language='JavaScript'>
function showlayer(nlayer) { jq('#'+nlayer).show('500'); } 
function hidelayer(nlayer) { jq('#'+nlayer).fadeOut(100); } 
</script>
";

return $htmlvar;
}

function  print_top_ads()
{
global $city, $use_ajax, $urlclscrpt2, $jstoplads, $jstphlads, $msg, $_REQUEST, $itemidvl;
if ($use_ajax=="yes"){
echo "
<script type=\"text/javascript\">
jq(document).ready(function(){
   jq('#hltdtads').html(\"".$msg['top_loading']."\").load('{$urlclscrpt2}jsload.php', {md: 'topads', nhlads: '$jstphlads', nltads: '$jstoplads', Itemid: '$itemidvl'}); return false;         
 });    
</script>

<div id='hltdtads'>
</div>

";

}
}


function js_top()
{
global $use_ajax, $urlclscrpt2, $msg, $itemidvl;
if ($use_ajax=="yes"){
echo "
<script type=\"text/javascript\">
jq(document).ready(function(){
   jq('#top_cities').html(\"".$msg['top_loading']."\").load('{$urlclscrpt2}jsload.php', { md: 'loc', Itemid: '$itemidvl' }); return false;         
 });    
</script>
";
}
}

function js_locations()
{ global $use_ajax, $locations;
if ($use_ajax=="yes"){
echo "
<div id='top_cities' style='text-align: justify'>
</div>
"; } else { echo $locations; }
}
?>
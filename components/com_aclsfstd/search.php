<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function get_search_form()
{
global $cat_fields, $ct, $HTTP_GET_VARS, $bgschcol, $fslct, $adrghtcol, $choose_one1, $choosecntr,
$tbclr_1, $tbclr_2, $tbclr_3, $tbclr_4, $fntclr_1, $msg, $_REQUEST, $schopt,$slctcntr, $indxjf_get,$fbpind2 ;

#if ($ct==""){$targtsch="target=tschf";}

foreach ($HTTP_GET_VARS as $key2 => $value2 )
{
$HTTP_GET_VARS[$key2]=stripslashes($HTTP_GET_VARS[$key2]);
$HTTP_GET_VARS[$key2]=ereg_replace('\'', '&#039;', $HTTP_GET_VARS[$key2]);
}

$bgschcol=$tbclr_1;
if ($HTTP_GET_VARS['onlywithphoto']=='yes'){$onlyphocheck="checked";}
 $srch_html=$srch_html."
<table width='100%' border='0' cellpadding='1' cellspacing='1' style='padding:4px;' >
<tr><td  style='text-align: right;padding:4px;'>
<FORM action='index{$fbpind2}.php' method='get' $targtsch>
$indxjf_get
<input type='hidden' name='ct' value='$ct' >
<input type='hidden' name='md' value='browse'>
<input type='hidden' name='mds' value='search'>
 
<font class='fntsrch'>".$msg['Keyword'].":</font>
</font>
</td><td bgcolor='$bgschcol' style='padding:4px;'>
<input type='text' name='brief_key' class=formst value='".$HTTP_GET_VARS['brief_key']."' size=15 class='frmsrch' >
</font>
</td>
<td colspan='2' style='padding:4px;'>
<font class='fntsrch'>".$msg['only_for_last']."</font>
<select name='before' class='frmsrch'>
<option>".$HTTP_GET_VARS['before']."<checked><option><option>1<option>3<option>5<option>10<option>20<option>40<option>60<option>100</select>
".$msg['days']." &nbsp; 
<input type='checkbox' name='onlywithphoto' value='yes'   class='frmsrch' $onlyphocheck> 
<font class='fntsrch'>".$msg['only_with_photos']."</font>
</font> </td></tr>
";
$s346='0';
foreach ($cat_fields as $keysch => $value )
{
if (($cat_fields[$keysch][2] == 'keyword') or ($cat_fields[$keysch][2] == 'minmax'))
{
if ($_REQUEST['ct']!= '' or $_REQUEST['gmct']!= ''){
if($s346 != '1')
{ 
$s346='1';
$srch_html=$srch_html."<TR><TD style='text-align: right;' style='padding:4px;'>";
$srch_html=$srch_html.print_sch_field($keysch);
$srch_html=$srch_html."</TD>\n";
}
else
{ $s346='0';
$srch_html=$srch_html."<TD style='text-align: right;' style='padding:4px;'>";
$srch_html=$srch_html.print_sch_field($keysch);
$srch_html=$srch_html."</TD></TR>\n";
}
}
}
 
}
if($s346 == '1'){ 
$srch_html=$srch_html."<td   colspan=2 style='padding:4px;'>.</TD></TR>";
}
$srch_html=$srch_html."
</table> 
";

$srch_html=$srch_html."
&nbsp; <INPUT type='submit' value=' ".$msg['searchsubm']." ' class='frmsrch'>
<font class='fsrch_2'>
".$msg['search_fields_optional']."  
</font>
</FORM>
";

global $kwsrchfrm;
#if ($HTTP_GET_VARS['idemail']!="" and $kwsrchfrm!="yes") {$srch_html="";}
if ($HTTP_GET_VARS['ratedads']=="1") {$srch_html="";}
return  $srch_html;
}



function print_sch_field($keysch)
{
global  $cat_fields, $HTTP_GET_VARS,$bgschcol, $jpath_url,
$tbclr_1, $tbclr_2, $tbclr_3, $tbclr_4, $fntclr_1, $choosecntr, $adrghtcol;
$bgschcol=$tbclr_1;
if ( ( $cat_fields[$keysch][4] == 'select') and ($cat_fields[$keysch][2] == 'keyword'))
{ 
 $srch_html=$srch_html."
<font class='fntsrch'>
 ".$cat_fields[$keysch][0].": 
</font>
</td><td bgcolor='$bgschcol' style='padding:4px;'>
<font class='fntsrch'>
<select name='$keysch' size='".$cat_fields[$keysch][5]."' class='frmsrch'   >
<option>".$HTTP_GET_VARS[$keysch]."<checked>
<option>".$cat_fields[$keysch][7]."</select>
</font>
";
}


if ( ( $cat_fields[$keysch][4] == 'select2') and ($cat_fields[$keysch][2] == 'keyword'))
{ 

$fld3v=$cat_fields[$keysch][7];

 $srch_html=$srch_html."
<font class='fntsrch'>
 ".$cat_fields[$keysch][0].": 
</font>
</td><td bgcolor='$bgschcol' style='padding:4px;'>
<font class='fntsrch'>
<select name='$keysch'  class='frmsrch' 
onChange=\"document.getElementById('f$fld3v').src='{$jpath_url}indctr.php?md=slct2&fld=$keysch&fld3=$fld3v&vl='+this.value;\"
>
<option>".$HTTP_GET_VARS[$keysch]."<checked>".sllist($keysch)."</select>
</font>
";
}

if ( ( $cat_fields[$keysch][4] == 'select3') and ($cat_fields[$keysch][2] == 'keyword'))
{ 

$dflt_value=slctgetstr($HTTP_GET_VARS[$keysch]);
$vlslct=$HTTP_GET_VARS[$cat_fields[$keysch][7]];

 $srch_html=$srch_html."
<font class='fntsrch'>
 ".$cat_fields[$keysch][0].": 
</font>
</td><td bgcolor='$bgschcol' style='padding:4px;'>
<input type='hidden' name='$keysch' id='t$keysch' value=''> 
<font class='fntsrch'>

<iframe src='{$jpath_url}indctr.php?md=slct2&fld=".$cat_fields[$keysch][7]."&fld3=$keysch&dvalue=$dflt_value&vl=$vlslct' id='f$keysch' frameborder=0   scrolling=no 
marginwidth='0' marginheight='0'  height='24' width='100%'></iframe>
</font>
";
}



if ( ( $cat_fields[$keysch][4] == 'checkbox') and ($cat_fields[$keysch][2] == 'keyword'))
{ 
 $srch_html=$srch_html."
<font class='fntsrch'>
 ".$cat_fields[$keysch][0].": 
</font>
</td><td bgcolor='$bgschcol' style='padding:4px;'>
<font class='fntsrch'>
<select name='$keysch' size='".$cat_fields[$keysch][0]."'  class='frmsrch'>
<option>".$HTTP_GET_VARS[$keysch]."<checked>
<option>".$cat_fields[$keysch][7]."</select>
</font>
";
}

if ( ( ($cat_fields[$keysch][4] == 'text') or ($cat_fields[$keysch][4] == 'textarea'))

 and ($cat_fields[$keysch][2] == 'keyword'))
{  
$srch_html=$srch_html."
<font class='fntsrch'>
 ".$cat_fields[$keysch][0].": 
</font>
</td><td bgcolor='$bgschcol' style='padding:4px;'>
<font class='fntsrch'>
<input type='text' name='$keysch' size=15 value='".$HTTP_GET_VARS[$keysch]."' 
class='frmsrch' >
</font>
";
}

if ( ($cat_fields[$keysch][4] == 'text') and ($cat_fields[$keysch][2] == 'minmax'))
{
$flmin=$keysch.'1';
$flmax=$keysch.'2';
$srch_html=$srch_html."
<font class='fntsrch'>
 ".$cat_fields[$keysch][0].": 
</font>
</td><td bgcolor='$bgschcol' style='padding:4px;'>
 <font class='fntsrch'>
min:<input type='text' name='$flmin' size='5' value='".$HTTP_GET_VARS[$flmin]."' class='frmsrch' >
max:<input type='text' name='$flmax' size='5' value='".$HTTP_GET_VARS[$flmax]."'  class='frmsrch'>
</font>
";
}

 
return  $srch_html;
}
?>
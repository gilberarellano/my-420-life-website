<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

$msg2=array(

'please_choose_rate' => 'Please choose rate from 1 to',
'already_voted' => 'Our logs shows that you already voted for this ad.',
'thank_for_voting' => 'Thank you for voting !',

'email_3' => 'E-mail', 'has_incr_format' => 'has incorrect format',
'return_and_fillfl' => 'Please use your browser\'s <b> back button </b> to return to the form
                       and fill in these fields.',

# Send password 
'title_t3' => 'Title',
'Ad_id1' => 'Ad id#',
'Edit_delete_passw' => 'Edit/delete password',
'Your_password' => 'Your password',
'doesnt_found' => 'is not found in the database',
'login_pass_sent_em' => 'login/password info has been sent to your e-mail account',

# Search your ads
'search_yr_ads2' => 'Search Your Ads',
'search_ads_cont_em' => 'Search ads with your contact e-mail:',
'input_eml3' => 'Input  e-mail:',

# Send privacy mail
'message_sent_by_member' => 'This message has been sent by member',
'message_from' => 'This message from', 'to_t2' => 'to',
'Category_3' => 'Category',  
'For_your_ad' => 'For your ad',
'following_messg_sent' => 'the following  message was sent',
'For_detl_cont_admin' => 'For more details please contact with admin via e-mail:',
'mssg_sent_succssfl' => ' Your message has been sent successfully ! ',
'your_msg_not_sent' => 'Your message was not sent to ad owner.<br> Please contact with admin',
 
'your_ad_deleted' => 'Your ad has been deleted !',

# Membership subscribing form
'membship_subscr' => 'Membership Subscribing',
'fill_out_the_form' => 'Fill out the following form below:',
'Date_Posted_m' => 'Date Posted',
'Expire_date_m' => 'Expire date',
'not_more_then' => 'Not more than', 'chars_ms' => 'chars',
'chars_left' => 'characters left',
'fields_marked_ms' => 'All fields marked by <font color=#cc0000> * </font> should be filled out ',
'Submit_ms' => 'Submit',
'Thanks_for_subscribing' => 'Thanks for subscribing ! <br>
                          Your memberships information <br> 
                          have been submited successfully!',
'thanks_activate_3days' => ' Thanks for subscribing ! <br>
                To finish subscribing and activate your  
                <br> membership account click on 
                the activating link <br> which is sent to your e-mail. <br> 
                You should activate your account within 3 days.',
'thanks_activated_asap' => 'Thanks for subscribing !
         Your membership account will be activated as soon as possible
         after approving by admin.',
'Thanks_for_subscribing_2' => 'Thanks for subscribing !',
'click_here_to_activate' => 'Click here</a> to activate your membership account.',
'Your_membership_info' => 'Your membership info',
 
'Incorrect_activating' => 'Incorrect activating of membership account !',
'mb_act_successfully' => 'Membership account has been activated successfully !
<br><a href=index.php>
       <b>Welcome to classifieds!</b></a>',
'Incorrect_membership_nick' => 'Incorrect membership Username/Password !',
'view_edit_mb_profile' => 'View/Edit Membership Profile',
'Nickname_mbe' => 'Username',
'Password_mbe' => 'Password',
'Submit_mbe' => 'Submit',
'Forgot_mb_password' => 'Forgot membership <br> password ?',
'View_edit_mb_profile' => 'View/Edit Membership Profile:',
'Delete_unnecessary_options' => 'Delete unnecessary options and/or add yours',
'Save_changes_mb' => 'Save changes',
'incorrect_password_mb' => 'incorrect password',
'memb_info_edited_success' => 'Your memberships information
                                    <br> have been edited successfully!',
'click_on_activat_link' => 'To finish editing and activate your  <br> membership account click on the activating link <br> which is sent to your e-mail. <br> 
       You should activate your account within 3 days.',
'mbacc_activ_asap' => 'Your membership account will be activated as soon as possible
        after approving by admin.',
'log_out_success' => 'You have log out from membership area successfully !',
'your_mb_not_activated' => 'Your membership account is not activated',
'your_mb_acc_expired' => 'Your membership account has been expired',
'click_to_update' => 'Click here</a> to update your account',
'exceeded_max_number' => 'You have exceeded max number of ads',
'allowed_to_place_per' => 'allowed to place per time period ',
'Try_again_later_1' => 'Try again later !',
'Click_to_see_ads' => 'Click here</a>to see your ads',
'Membership_Area' => 'Membership Area',
'Sign_Up_1' => 'Sign Up !',
'edit_your_profile' => 'View/Edit your profile',
'Send_Password' => 'Send Password',
'inp_email_sent_pssw' => 'Input your contact e-mail:',
'e_mail_mb' => 'E-mail ',
'has_incorr_format' => 'has incorrect format',
'use_back_button_r' => 'Please use your browser\'s <b> back button </b> 
to return to the form  and fill in these fields.',
'Your_mb_login' => 'Your membership login',
'not_found_mb_db' => 'is not found in the our membership database',
'mb_lgpss_sent_to_email' => 'membership username/password info has been sent to your e-mail account',

# highlighting ads
'Your_ad_hl' => 'Your ad',
'highlt_success' => 'has been highlighted successfully !',
'cl_to_finish_s' => 'click here </a> to finish this session',
'Input_ad_id_hl' => 'Input ad ID#',
'Submit_hl' => 'Submit',
'Incorr_processing_hl' => 'Incorrect processing ',
'Your_ad_hl' => 'Your ad',
'already_highlighted' => 'already highlighted  ! ',
'Incorect_operation' => 'Incorect operation ! ',

# forms for submitting and editing ads

'choos_other_categ'=> 'Choose other category',
'choose_ct_for_ad_subm' => 'Choose category for ad submitting',

'choose_correct_categ' => '<font color=\'#cc0000\'><b>Please use your browser\'s   back button  to return to 
                          the top categories page and choose one classifieds category.</b></font>',
'Place_New_Ad' => 'Place New Ad',
'Edit_Ad1'  => 'Edit Ad',   
'Place_your_comment' => 'Place your comment:',
'Highlight_this_ad' => 'Highlight this ad',
'fill_out_the_following_form' => 'Fill out the following form below',
'Not_more_then' => 'Not more than', 'chars_k' => 'chars',
'all_fields_marked' => 'All fields marked by <font color=\'#cc0000\'> * </font>
                        should be filled out ',
'Submit_photos' => 'Submit photos',
'delete_p' => 'delete',
'Photo_size_z' => 'Photo size', 'Kbyte_v' => 'Kbyte',
'Photo_c' => 'Photo',
'Submit_preview_photo' => 'Submit  preview photo',
'pixels_p' => 'pixels',
'Preview_Photo' => 'Preview Photo',
'Submit_v' => 'Submit attachment file',   
'size_f' => 'size',
'Submit_Ad_v' => 'Submit Ad',
'sdate_mm' => "mm",
'sdate_dd' => "dd",
'sdate_yy' => "yyyy",
'incorr_date_f' => "Incorrect date format for field: ", 

'js_please_fill_field' => "Please fill out the field",

'js_sbf_email' => "E-mail", 'js_has_incorr_frm' => "has incorrect format. Please input correct e-mail",

'entr_vrfcode' => "<font FACE='ARIAL, HELVETICA' color='#000077' size='-1'>
   &nbsp; To prevent submitting form by automated programs <br>
   please enter verification code from the box:</font>",

'vrf_cd_notcor' => "<font FACE='ARIAL, HELVETICA' color='#000077' size='-1'>
   <font color='#cc0000'><b> Verification code is not correct </font> <br>
   Please use your browser's   back button  to return to the form </b></font> ",

'vrf_cd_notcor_alrt' => "Verification code is not correct ! Try again.",

'your_ipaddr' => "Your IP address:",

'Input_your_cont_email' => 'Input your contact e-mail',
'Send_Password' => 'Send Password',

'view_ad1' => 'View Ad',
'ph_view_after_highl' => 'You can submit photos and attachments but they will appear on your ad
after ad highlighting, otherwise only one small photo (the first one) will be displayed.
After you submit this ad, you can  click on special link 
for fee based ad highlighting.',

# Privacy mail form
'ID_f' => 'ID#',
'Send_Privacy_Message' => 'Send Privacy Message ',
'Your_e_mail_f' => 'Your e-mail',
'Subject_p' => 'Subject',
'Message_p' => 'Message',
'Send_p' => 'Send',

# Log In form
'No_ads_with_ID' => 'No ads with ID#',
'Login_incorrect' => 'Login password incorrect',
'Log_In' => 'Log In',
'ID_ff' => 'ID #',
'Password_v' => 'Password',
'Edit_m' => 'Edit',
'Delete_m' => 'Delete',
'Submit_m' => 'Submit',
'Forgot_password_m' => 'Forgot password ?',

'Ad_info_in_field' => 'Ad info in the  field ',
'is_too_large_inf' => 'is too large. 
       <font class=\'stfntb\'>
       <p> Please use your browser\'s <b> back button </b> to return to the form
        and make more short info  in this field. </font>',

'Ad_field_c' => 'Ad field',
'was_mising_on_form' => '(marked by * )  
       was missing on your form submission. 
       <font class=\'stfntb\'>
       <p> Please use your browser\'s <b> back button </b> to return to the form
       and fill in this field. </font>',

'Your_photo_n' => 'Your photo',
'is_too_large_ph' => 'is too large  <font class=\'stfntb\'>
           <p> Please use your browser\'s <b> back button </b> to 
            return to the form and submit photo with size',  

'Your_preview_photo_d' => 'Your preview photo ',
'is_too_large_prph' => 'is too large  <font class=\'stfntb\'>
                        <p> Please use your browser\'s <b> back button </b> to 
                        return to the form and submit photo with size',

'Your_multimedia_file' => 'Your attachment file',
'is_too_large_mmf' => 'is too large  <font class=\'stfntb\'>
                      <p> Please use your browser\'s <b> back button </b> 
                      to return to the form and submit file with size',
'youcanlldtypes' => 'You can load file with the following types:',
'att_file' => 'File',
'att_file_title' => 'File title',
'load_video_on_serv' => 'Load large videos or archives  for your ad on file servers !',
'att_url_file' => 'File URL',
'code_to_embed' => 'Code to embed video <br> 
                  (e.g. from <a href=\'http://www.youtube.com\'>youtube.com</a>)<br> into ad',
'type_attch' => 'Your attachment type ',
'does_not_match' => 'does not match to the allowed types:',
'place_corr_fl' => 'Please use your browser\'s <b> back button </b> to return to the form <br> and place the correct 
attachment file.', 


'exceeded_max_n_ads' => 'You have exceeded max number of ads ',
'allowed_per_time_p' => 'allowed to place per time period ',
'days_v' => 'days).  Try again later !',

'Click_here_v' => 'Click here',
'to_see_your_ads' => 'to see your ads',

'Error_subm_new_ad' => 'Error in submitting new ad. Seems, after  modifying fields sets for classifieds  categories
there are  some   short names of fields in these sets which do not exist in the ads fields descriptions 
or which do not exist in the  fields list of  mysql table for ads.',


'ad_edited_successfuly' => 'Your ad has been edited successfully ',
'ad_submitted_successfuly' => 'Your ad has been submitted successfully ',
'will_appear_as_possible' => 'and will appear in the index as soon as possible',
'to_activate_ad_click_link' => 'To activate your ad please click on the activation link <br>
                                in the confirmation message which was sent to your e-mail',
'ad_activated_successfully' => 'Your ad has been activated successfully ',
'act_view_ad1' => 'View Ad',
'ad_was_not_activated_incr' =>   'Your ad was not activated due to incorrect activation code', 


'photos_not_saved' => 'Due to incorrect server configuration your 
         photos are not saved into database ',
'tried_duplicate_ad'=> 'You have tried to duplicate the ad which was already posted:',

# membership sign up
'membership_activated_succ' => 'You membership account has been activated successfully !',
'membership_updated_succ' => 'You membership account has been updated successfully !',
'click_to_finish_session' => 'click here </a> to finish this session',
'your_profile_deleted' => 'Your profile has been deleted !',

'Login_ch' => 'Login ', 'already_exist_z' => 'already exist',
'input_another_login_password' => 'Use your browser\'s <b>Back</b> button and input another login/password',
'Member_acc_edited_succ' => 'Member\'s account has been edited successfully ',
'Member_acc_subm_succ' => 'Member\'s account has been submitted successfully '
 
);


?>
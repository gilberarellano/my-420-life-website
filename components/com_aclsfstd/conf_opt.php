<?php

/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

# Setting up Classified Ads options:

# Admin password  
$adm_passw="adm";

# Admin e-mail
$adm_email="info@almondsoft.com";

# Use verification code for ads submitting, sending privacy mail, 
# membership subscribing (yes, no)
$usevrfcode="no";

# Use hidden verification by analysing users' activity on submitting forms without
# entering verification code (yes, no)
$use_hdn_vrfc="no";

# use AJAX framework (yes, no)
$use_ajax="yes";

# number of latest ads displayed on the top page (works with AJAX option)
$jstoplads='10';
 
# number of highlighted ads displayed on the top page (works with AJAX option)
$jstphlads='5';


# Places visits counter on ads details pages  for most visited ads (yes, no)
$plcntdtl="yes";
# Minimum number of visits that allows to place an ad into 
# top visited list
$vstminnmbr=5;

# Places contacts counter (how many privacy mails sent to ad owner) 
# on ads details pages  (yes, no)
$plcntpml="yes";

# set up an ability for users to vote and rate ads (yes, no)
$voterate='yes';
# set up max value for rate
$maxvalrate=10;

# Use IP logs to count only one visit, contact message and vote 
# of the same user for the same ad (yes, no)
$uselogsvmv="yes";
 
# Expiration period for logs (days);
$exprlogs="10";

# Use fast search features with keywords suggestion (yes/no)
$kwsrchfrm="yes";
 
# Min length of keywords for indexing in the keywords list 
$kwminln=4;
# Min amount of appearance of keyword in the ads for including this keyword 
# into popular keywords list
$kwminadsind='2';
# Min amount of appearance of keyword in the ads for including this keyword 
# into list of keywords suggestions in the fast search form
$kwminsgl='1';
# Max amount of keywords in suggestions list for fast search form
$kwmaxsgst=15;

# Using Spam Guard (yes, no)
$use_spmg="no";

# Place link for flagging ads as spam on the ads details pages (yes,no)
$spamlnkfl="yes";
# Amount of users' spam flags to send spam notification to admin
$spamcntnt=2; 

# Protection for ads duplication (yes, no).
$pradsdupl='no';
 
# Number of ads displayed on the index page
$adsonpage="20";

# Max ads title size on ads index pages (chars)
$maxttlsz=25;

# Expiration period for ads (days)
$exp_period="100";

# time (days) for latest ads link on the top page
$tmltads=20;


# Expiration period for highlighted ads (days)
$exp_perdhlt=200;

# Send expiration message to ads owners when their expired ads have been deleted (yes,no)
$sndexpmsg1="no"; 

# Expiration e-mail subject
$expmsg_subj="Your ad has been expired";

# Expiration e-mail template
$expemltmpl="
Dear User,

Your ad:
Title: --title--
Brief Description: --brief--

which you placed on --date-- in our classifieds ads service
has been expired. 

Welcome to  place new ads into our classifieds ads service !

Thanks,
Classifieds Ads Team.
";

# Create static URLs for classifieds content (yes, no): 
# (if you choose option "yes", you also need to set up SEO option in Joomla! configuration 
#  and place special lines into file .htaccess in Joomla! root directory, more details in readme.txt file ) 
$sturlvar="no";

# Set up highlighting ads feature (yes, no)
$hltadsf='no';
# Number of highlighted ads which will be rotated at the top of   ads index pages
$cnt_htl_ind=2;
# Number of highlighted ads which will be rotated on  ads second  pages with ads details
$cnt_htl_det=1;
# To display  ads list  in the order   according to highlighting rate (yes, no)
# (this rate can be set up in the admin interface) 
$ordhltrate="no";

# All photos and attachments will be displayed only on highlighted ads,
# on other ads only one small photo will be displayed  (yes, no)
$dsplhltpht="no";
# Set up moderating (yes, no)
$moderating="no";

# Activate ads by PayPal payment (yes,no) ($moderating option should have value "yes")
# (this feature works with an additional PayPal module)
$ppactv="no";

# Activating of ads by clicking on the activation link in the confirmation e-mail which was 
# sent to ad owner after ad submitting (yes, no)
$actadoptv="no";

# Confirmation e-mail subject:
$cnfemtsbj="confirmation e-mail";

# Confirmation e-mail template: 
# (--act_link-- will be replaced by activation link) 
$cnfemtmpl="
Thank you for submitting your ad. 
Please click on the following link 
to activate your ad
--act_link--
";


# Set up privacy mail (yes, no) 
$privacy_mail="yes";

# Privacy mail template 
$pmailtp="
For you ad: --title-- (--ad_url--)
the following message was sent:

--message--

--mb_login_inf--  

Reply e-mail: --r_email--

--facebook_user_profile--

AlmondSoft.Com

"; 

# If privacy mail is set up , send copy of privacy messages 
# to admin (yes, no)
$sendcopytoadm="no";
# If privacy mail is set up , redirect privacy messages 
# to admin (yes, no)
$redirtoadm="no";

# Max number of photos allowed
$photos_count=3;

# Set max size for all photos on the second ad page (bytes)
$phptomaxsize=5000000;


# Includes into ads attachment file (yes, no)
$incl_mtmdfile="yes";
# Max size for attachment file ( bytes )
$mtmdfile_maxs="5000000";
# types of extensions for  attachment files
$flattchext=array('.avi', '.mpg', '.txt', '.doc', '.xls', '.zip');
# Allow users to place files for ads on popular file servers ( yes, no)
$plsflsrvrs="yes";
# Info about loading ad files on file servers 
$infldflsrv="
If you have large video files or archives, you can load them on popular file servers:
<a href='http://www.youtube.com'>Youtube.Com</a>, 
<a href='http://www.megaupload.com'>Megaupload.Com</a>, 
<a href='http://www.rapidshare.com'>Rapidshare.Com</a>, 
<a href='http://www.sendspace.com'>Sendspace.Com</a>, 
<a href='http://www.upload2.net'>Upload2.Net</a>, 
<a href='http://www.uploading.com'>Uploading.Com</a>, 
<a href='http://www.rapidupload.com'>Rapidupload.Com</a>  or others and then place URLs and titles for these files into the fields below:
";


# Width of preview photos on the second ad page (pixels)
$prphscnd="100";
# Max width of photos on the second ad page (pixels)
$maximgswith="550";

# Set up resizing of large photos before saving in the database (yes, no)
$resphdb="yes";
# Max width or height (pixels) of photos for resizing
$maxpixph="600";

# Set up javascript checking of  ads submitting form (yes, no)
$jsfrmch="yes";

# Place on detailed ad page special link to search 
# all ads posted by the user ( search with the same contact e-mail )
# if user place more then 1 ad (yes, no)
$schallusrads='yes';


# Membership settings:
# Set up free (f) or membership (m) for access to ad second page  
$mbac_second='f';
# Set up free (f) or membership (m) for submitting new ads 
$mbac_addad='f';
# Set up free (f) or membership (m) for sending privacy mails
$mbac_sndml='f';
#Use Joomla users' login to edit/delete ads (yes,no) (this option is active with the membership  option for submitting new ads)
$lgneditd='no';

# place additional information about ad's owner on ad detailed page (yes, no)
$pladddp="no";


# Check number of placed ads per time period by each user (yes, no)
$ch_nmusr="yes";
# if you set up "yes", set up max number of ads allowed to place  
$usrads_max=1000; 
# and time period (days) for check.
$usrads_chcktime=10;
 
#  !!!! In the customized HTML code which will be inserted into the following config variables all     
#  quotes " should be replaced by quotes ' . E.g. tag <font FACE="ARIAL, HELVETICA" size="-2">
#  should be replaced by <font FACE='ARIAL, HELVETICA' size='-2'>


# set up html header/footer for user's interface
$ht_header=" <p class='pst1'>
  &nbsp; <font style='font-size: 20px; color: #555599; font-weight: bold; '>Almond Classifieds (Standard Edition)</font>
 <p class='pst1'>
";

$ht_footer="
<hr size=1>
<table width='100%'  border='0' cellspacing='0' cellpadding='0' class='tb1'>
<tr><td><font class='smallfnt'>
<center>&nbsp;  <br>
 Copyright  &copy; 2012 <a href='http://www.almondsoft.com'>AlmondSoft.Com</a> All right reserved.
</font></center></td></tr></table>
<p class='pst1'>  &nbsp;   <p class='pst1'>  
";

# Set up message for users about membership log in and terms when they try 
# to go into membership areas
$jloginfo="<font class='lrfnt'>
&nbsp; <p class='pst1'> 
This area is for registered users only
<p class='pst1'>
please log in on the left column.
</font>
";

# To display random ads on the pages (yes, no)
$use_rndads='yes';

# How many times to repeat random ads changing on the top page
$rndadsrpt="5";

# Time delay for random ads changing (ms)
$rndadstmdl=4000;

# Minimum of ads in the classifieds when random ads start appearing
$rndadsmin=10; 

# How many first positions in random ads list are assigned for highlighted ads
$rndadsfphl=3;

# To allow users to view ads photos gallery (yes, no)
$use_adsphgal='yes';

# Facebook settings (facebook interace can be activated with an aditional upgrade module ) 

# Set up facebook Application ID
$fbv_app_id="454695454563";
# Set up Application Secret
$fbv_sectret="6eb1fdd14565466d5ffbf5e65757";

# Use facebook interface (yes, no)
$fb_intrfc="no";

# Ads can be placed by any visitor including facebook users (1), only by facebook users (2)
$fb_adspl="1";

# privacy mail can be posted  by any visitor including facebook users (1), only by facebook users (2) 
$fb_prmail="1"; 

# The domain name for activity social plugin 
$fb_act_domain="almondsoft.com";

# URL of your classifieds application on the facebook: 
$fbappsurl="http://apps.facebook.com/aclsfstdb/";


############################################################
# Congratulations!  You've finished defining the variables.#
############################################################

?>
<?php 
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

include("../../configuration.php"); 

global $jhost_name, $jdb_user, $jdb_password, $jdb_name;

error_reporting(E_ALL ^ E_NOTICE);

if($mosConfig_db!=""){
$jmldbprfx=$mosConfig_dbprefix;
$jhost_name=$mosConfig_host;
$jdb_user=$mosConfig_user;
$jdb_password=$mosConfig_password;
$jdb_name=$mosConfig_db;
}
else{
$jmvar = new JConfig;
$jmldbprfx=$jmvar->dbprefix;
$jhost_name=$jmvar->host;
$jdb_user=$jmvar->user;
$jdb_password=$jmvar->password;
$jdb_name=$jmvar->db;
}
include("config.php"); 
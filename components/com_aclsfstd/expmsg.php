<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

global $expemltmpl, $expmsg_subj;
 
$expmsgv=$expemltmpl;

$expmsgv=preg_replace('/--title--/', $row['title'], $expmsgv);
$expmsgv=preg_replace('/--brief--/', $row['brief'], $expmsgv);
$expmsgv=preg_replace('/--date--/', get_date($row['time']), $expmsgv);

global $adm_email;
sndmail ($row['email'], $expmsg_subj, $expmsgv, $adm_email); 
?>
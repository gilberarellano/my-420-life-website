<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

include("funcs2.php");

global $use_hdn_vrfc, $usevrfcode;
if ($use_hdn_vrfc=="yes"){$usevrfcode="yes";} 

function forgotpassw()
{
global $ct, $msg2,$indx_url;

$message="
<form action='{$indx_url}ct=$ct&md=sendpassw' method='post'>
<font class='stfnt' ><b>
".$msg2['Input_your_cont_email'].":</b></font> <input type='text' name='ps_email' class=formst>
<input type='hidden' name='ct' value='$ct'  >
<input type='submit' value='".$msg2['Send_Password']."' class=formst STYLE='FONT-WEIGHT: bold;'>
</form>
";
output_message($message);
return;
}

function privacy_form($idnum)
{
global $ct, $tbclr_1, $tbclr_2, $tbclr_3, $tbclr_4, $fntclr_1, $emltp, $emllogin, $msg2, $indx_url ;

 
$fields_val=get_edit_info($idnum);
$message= "
<center>
<table width='600' border=0   cellspacing=1 cellpadding=0>
<tr><td>
<a href='{$indx_url}ct=$ct&md=details&id=$idnum'>
".$msg2['ID_f']."".$fields_val['idnum']."<b> &nbsp; ".$fields_val['title']."</b>
</a></font>
</td></tr></table>
 
<p class='pst1'>
<table width='600'  class='frmtb1' border=0 cellspacing=1 cellpadding=0>
<tr><td  > 
<font class='frmcp'>  
&nbsp; ".$msg2['Send_Privacy_Message']." </font>
 
<table width='100%'   border=0 cellspacing=1 cellpadding=0>
<tr><td class='frmtb2'>
<form action='{$indx_url}ct=$ct&md=send_mail' method='post' id='f' >
<input type='hidden' name='idnum' value='$idnum'>
<input type='hidden' name='emltp' value='$emltp'>
<input type='hidden' name='emllogin' value='$emllogin'>
&nbsp; <p class='pst1'><center>
<table>
<tr>
<td align='right' >
<font class='frmft1'>
".$msg2['Your_e_mail_f'].": </font>
</td><td>
<input type='text' name='pm_email' size='40' class=formst></td></tr><tr>
<td align='right'>
<font class='frmft1'>
".$msg2['Subject_p'].":
</font>
</td><td>
<input type='text' name='pm_subject' size='40' class=formst value='Re: ".$fields_val['title']."'></td></tr><tr>
<td align='right'>
<font class='frmft1'>
".$msg2['Message_p'].":
</font>
</td><td>
<textarea rows='8' cols='40' name='pm_message' class=formst></textarea>
</td></tr>

</table>
<p class='pst1'>
";
global $usevrfcode;
if ($usevrfcode=="yes"){include_once("vrfuncs.php"); $message=$message.vrchtml();
$submvl1=vrfcjs_chck();
}
else {
$submvl1="
<input type='submit' value='".$msg2['Send_p']."' class=formst STYLE='FONT-WEIGHT: bold;'>
";
}

$message=$message."
 $submvl1
</form>
</center>
</td></tr></TABLE>
</td></tr></TABLE>
</center>
";
output_message($message);
return;
}


 
function edit_form()
{
global $fields_val, $ed_id, $edit_delete, $ed_passw, $rightform_html, 
$html_edit_form, $adm_passw, $admcookpassw, $cat_fields, $ct, $categories,
$fields_sets, $ads_fields, $ammlk, $msg2, $_REQUEST;
if($ed_id==""){$ed_id="0";}
$fields_val=get_edit_info($ed_id);
$var_11=$msg2['No_ads_with_ID']." $ed_id";

global $lgneditd;  $usr_username=gt_usrname();

if ($fields_val['idnum']=="") {edit_login($var_11); return;}
if(($ed_passw==$fields_val['passw']) or ($ed_passw==$adm_passw)
or ($admcookpassw == $adm_passw) or ($ammlk == $adm_passw) 
or (($lgneditd=="yes") and ($usr_username!="")  and  ($usr_username==$fields_val['login']))
)
 {
if ($edit_delete=="delete")
{delete_ad($ed_id);}
else{
 
$rightform_html=$html_edit_form;

$cat_fields="";
$ct=$fields_val['catname'];
$a1=$categories[$ct][1];
$a2=$fields_sets[$a1];
 
foreach ($a2 as $a2_value)
{
$cat_fields[$a2_value]=$ads_fields[$a2_value];

if ($cat_fields[$a2_value][4]=="checkbox")
{
$aa5=split('<option>',$cat_fields[$a2_value][7]);
$i_aa5=0;
foreach ($aa5 as $value1)
{
$i_aa5++;
$namechbx=$a2_value.$i_aa5;
if (($value1!="") and (ereg($value1,$fields_val[$a2_value])))
{$fields_val[$namechbx]="checked";}

}
}
}
ad_form("edit");
}
}
else { 
 edit_login($msg2['Login_incorrect']);
}
}

function edit_login($log_info)
{
global $lgneditd, $jloginfo; 
if ($lgneditd=="yes")
{
output_message($jloginfo);
return;
}
else {edit_login1($log_info);}
}

function edit_login1($log_info)
{
global $html_header, $html_footer, $cat_fields, $ct,
$tbclr_1, $tbclr_2, $tbclr_3, $tbclr_4, $fntclr_1, $msg2, $id, $indx_url,  $indxjf_get, $javastl, $fbpind2;
 
$message= "
<center>
<font class='stfntr' >
<b> $log_info </b>
</font>
<table width='600' border=0 class='frmtb1' cellspacing=1 cellpadding=0>
<tr><td>
<font class='frmcp'>
&nbsp;  ".$msg2['Log_In'].": 
</font>
 
<table width='100%' border=0 cellspacing=1 cellpadding=0>
<tr>
<td class='frmtb2'>
 <form action='{$indx_url}ct=$ct&md=editform' method='post' >
<center>
&nbsp; <p class='pst1'>
<table border=0 cellspacing=1 cellpadding=0>
<tr><td align='right'>
<font  class='frmft1'>
".$msg2['ID_ff'].":
</font>
</td><td>
<input type='text' name='ed_id' value='$id' class=formst>
</td></tr>
<tr><td align='right'>
<font class='frmft1'>
".$msg2['Password_v'].":
</font>
</td><td>
<input type='password' name='ed_passw' class=formst>
</td></tr>
<tr><td align='right'>
<font class='frmft1'>
".$msg2['Edit_m']."
</font>
</td><td>
<input type='radio' name='edit_delete' value='edit' checked class=formst>
</td></tr>
<tr><td align='right'>
<font class='frmft1'>
".$msg2['Delete_m']."
</font>
</td><td>
<input type='radio' name='edit_delete' value='delete' class=formst>
</td></tr></table>
<input type='submit' value='".$msg2['Submit_m']."' class=formst STYLE='FONT-WEIGHT: bold;'>
</form>
</center>


<font class='stfnt'>
&nbsp; <a href='{$indx_url}ct=$ct&md=forgotpassw'>".$msg2['Forgot_password_m']." </a>
</font>


&nbsp; &nbsp; &nbsp;

$javastl

<font   class='stfnt' style='color:#0000ee; TEXT-DECORATION: underline; cursor: pointer;' OnClick=\"displ('srch1');\">
".$msg2['search_yr_ads2']."
</font>
<DIV id='srch1' style='DISPLAY: none'>
 
<p class='pst1'> 
 
<form action='index{$fbpind2}.php' method='get' >
$indxjf_get 
<font class='stfnt' STYLE='color:#000077; FONT-WEIGHT: bold;'>
&nbsp;".$msg2['search_ads_cont_em']."
</font>
<br>
&nbsp;".$msg2['input_eml3']." <input type='text' name='email' class=formst STYLE='FONT-WEIGHT: bold;'>
<input type='hidden' name='md' value='browse'>
<input type='hidden' name='mds' value='search'>

<input type='submit' value='Search' class=formst STYLE='FONT-WEIGHT: bold;'>
</form>
</form>
</div>

<br>

&nbsp;
<br>

</td></tr></table>
</td></tr></table>
</center>

";

output_message($message);
return;
} 

 

function print_add_form()
{ 
global $html_add_form, $rightform_html, $paymgtw, $msg2, $_REQUEST, $templ ;

if($_REQUEST['ct']==''){
$message=$msg2['choose_correct_categ'];
output_message($message);
return;}

$rightform_html=$html_add_form;

if($paymgtw=="yes"){include("ecmc.php"); checkpmstat();}

ad_form("submitad");
}

function ad_form($ed_add)
{
global $html_header, $html_footer, $cat_fields, $fields_val, $ct, $evnt_cat,
$photos_count, $ed_id, $ed_passw, $exp_period, $rightform_html, 
$select_text, $categories, $phptomaxsize, $incl_prevphoto, $prviewphotomax,$multimedia_path,
 $prphotolimits, $pr_lim_height, $pr_lim_width, $incl_mtmdfile, $multim_link,$photo_path,
$multim_ext, $mtmdfile_maxs, $ad_ind_width, $HTTP_POST_VARS, $HTTP_GET_VARS, 
$tbclr_1, $tbclr_2, $tbclr_3, $tbclr_4, $fntclr_1, $fields_comm, $hltadsf, $msg2, $msg, $templ,
$use_gl_map, $fslct, $choose_one1, $choosecntr,$urlclscrpt, $indx_url, $jpath_url, $jpath_url, $jm_cmpath;

global $jsfrmch;

if ($ed_add=="edit")
{
$time1=$fields_val['time'];
$date_posted=get_date($time1); 
$time2=$time1+$exp_period*86400;
$expire_date=get_date($time2);
$jsfrmch="no"; 
}
else{
$time1=time();
$date_posted=get_date($time1); 
$time2=$time1+$exp_period*86400;
$expire_date=get_date($time2);
}

if ($ed_add=="submitad") { $title_inf=$msg2['Place_New_Ad'];}
if ($ed_add=="edit") { $title_inf=" ".$msg2['Edit_Ad1']." ( ID# ".$fields_val['idnum'].") ";

if ($hltadsf=='yes'){
if (($fields_val['adrate'] < 1) or ($fields_val['adrate']=="")){
$hltadsinfo="
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font FACE='ARIAL, HELVETICA' color='#000088' size=-1>
<a href='{$jpath_url}highlight.php?id=".$fields_val['idnum']."' target='_blank'>".$msg2['Highlight_this_ad']."</a></font>
";
}
}
else{$hltadsinfo="";}
$title_inf="
<table width='100%'><tr><td><font class='stfnt'>
<b>$title_inf</b></font></td>
<td align='right'>$hltadsinfo</td></tr></table>
";
}

$replidf_val="";
$replid_val=$HTTP_GET_VARS['replid'];
if ($HTTP_GET_VARS['replid'] !="")  
{ $rowrpl=get_ad_details($replid_val);
$title_inf=$msg2['Place_your_comment'];
$fields_val['title']="Re:".$rowrpl['title'];
$fields_val['initialad']=$rowrpl['brief'];
if ($rowrpl['brief']=="" and $rowrpl['comment']!=""){$fields_val['initialad']=$rowrpl['comment'];}
$replidf_val="
<input type='hidden' name='replidf' value='$replid_val'>
";
}

 

if ($jsfrmch=="yes"){
$frmjs="<script language='javascript'>
<!--
function checkaddform(){
";
foreach ($cat_fields as $key => $value )
{
if (($cat_fields[$key][5] == '1') and ($cat_fields[$key][4] != 'date')){


$chckjsf="";
if (($cat_fields[$key][4] == 'select') || ($cat_fields[$key][4] == 'select2')){$chckjsf=$select_text;}

$frmjs=$frmjs."
if(document.forms['f'].elements['$key'].value=='$chckjsf'){
alert(\"".$msg2['js_please_fill_field']." '".$cat_fields[$key][0]."' \");
return false; 
}
";
}
if ($key=='email' and $cat_fields['email'][5] == '1'){
$frmjs=$frmjs."
emladdr = document.forms['f'].elements['email'].value;
emlobj = new String (emladdr);
msvem1 = emlobj.indexOf(\"@\");
msvem2 = emlobj.indexOf(\".\");
if ((msvem1 < 1) || (msvem2 < 1))
{
alert(\"".$msg2['js_sbf_email']." \"+ emladdr + \" ".$msg2['js_has_incorr_frm']."\");
return false; 
}
";
}

}

global $usevrfcode;
if ($ed_add=='submitad' and $usevrfcode=="yes"){include_once("vrfuncs.php"); $frmjs=$frmjs.vrfcjs_add();}
$frmjs=$frmjs."
document.forms['f'].submit();
}
--></script>";
}


$td_height='30';

$thtml= "
<body  onload='self.focus()'>
<center> 
<table  width='100%'><tr><td valign='top'>

<a href='$indx_url'><font class='stfnt'>
<b>".$msg['top'].":</font></a></b> 
&nbsp; 
 
 &nbsp;<a href='{$indx_url}ct=$ct'><font class='stfnt'><b>".$categories[$ct][0]."
</b></font></a> 
  
";

if($ed_add=="submitad"){
$thtml=$thtml."
 <p class='pst1'>   <a href='{$indx_url}md=chct'>".$msg2['choos_other_categ']."</a> <p class='pst1'> 
";
}

$thtml=$thtml."
<hr size='1'>
<font class='stfntb'> <b> $title_inf </b> </font><br>&nbsp;

$frmjs


<script language='javascript'>
<!--
function checktxtr(txtrid, maxlngth, chrlr)
{
var txtr = document.forms['f'].elements[txtrid];
var txtlngth = txtr.value.length;
var chrlft = document.getElementById(chrlr);

if(txtlngth > maxlngth){txtr.value = txtr.value.substring(0, maxlngth); return;}
chrlft.innerHTML = maxlngth- txtlngth;
}
--></script>


<form action='{$indx_url}ct=$ct&md=$ed_add' method='post' ENCTYPE='multipart/form-data' name='f'>
$replidf_val
<table   width='650' border=0 class='frmtb1' cellspacing=1 cellpadding=0 >
<tr><td>
<font class='frmcp'>
<b>&nbsp;".$msg2['fill_out_the_following_form'].":</b>
</font>
<table width='100%' border=0 cellspacing=1 cellpadding=1 >
<tr><td class='frmtb2a' height='$td_height' width='30%' align='right' >
<font class='frmft1' > 
".$msg['date_posted_d'].":          
</font>
</td><td class='frmtb2' width='70%'  >
<font class='frmft1'>
 &nbsp; $date_posted  
</font>
</td></tr>
";
if ($evnt_cat[$ct] !="yes"){
$thtml=$thtml." 
<tr>
<td  height='$td_height' class='frmtb2a' align='right'>
<font class='frmft1'>
".$msg['expire_date_d'].": 
</font>
</td><td class='frmtb2'>
<font class='frmft1'>
 &nbsp; $expire_date 
</font>
</td></tr>
";
}
 
foreach ($cat_fields as $key => $value )
{  
if ($key=="addrmap"){$key_map="1";}
$commnt_var="";
if ($fields_comm[$key] !="")
{
$commnt_var="
 <font class='smallb'> 
".$fields_comm[$key]."
 </font><br> 
";
}

if (ereg ('url',$key))
{
if ($fields_val[$key]==""){$fields_val[$key]="http://";}
}

if ($cat_fields[$key][5] == '1')
{$cat_fields[$key][0]=$cat_fields[$key][0]."<font class='smallr'>* </font>";}

if ($cat_fields[$key][4] == 'select')
{
$cat_fields[$key][7]=ereg_replace("\n", '', $cat_fields[$key][7]);
$cat_fields[$key][7]=ereg_replace("\r", '', $cat_fields[$key][7]);
$aa4=split(':',$cat_fields[$key][3]);
$t_size=$aa4[0];
$t_max=$aa4[1];
if ($fields_val[$key]!=""){$select_text1=$fields_val[$key];}
else {$select_text1=$select_text;}
$thtml=$thtml. "
<tr>
<td height='$td_height' class='frmtb2a' align='right'>
<font class='frmft1'>
".$cat_fields[$key][0].":
</font>
</td><td class='frmtb2' style='padding: 3px;'>  $commnt_var 
<select name='$key' size='$t_size' class=formst>
 <option value='$select_text1'>$select_text1<checked>
".$cat_fields[$key][7]."</select> </td></tr>
";
}


if ($cat_fields[$key][4] == 'select2')
{


if ($fields_val[$key]!=""){$select_text1=$fields_val[$key];}
else {$select_text1=$select_text;}

$fld3v=$cat_fields[$key][7];

$thtml=$thtml. "
<tr>
<td height='$td_height' class='frmtb2a' align='right'>
<font class='frmft1'>
".$cat_fields[$key][0].":
</font>
</td><td class='frmtb2' style='padding: 3px;'>  $commnt_var 
<select name='$key'  class=formst  
onChange=\"document.getElementById('f$fld3v').src='{$jpath_url}indctr.php?md=slct1&fld=$key&fld3=$fld3v&vl='+this.value;\">
<option value='$select_text1'>$select_text1<checked>".sllist($key)."</select>
</td></tr>
";
}

if ($cat_fields[$key][4] == 'select3')
{
if ($fields_val[$key]!=""){$select_text1=$fields_val[$key];}
else {$select_text1=$select_text;}

$dflt_value=slctgetstr($fields_val[$key]);
$vlslct=$fields_val[$cat_fields[$key][7]];
$thtml=$thtml. "
<tr>
<td height='$td_height' class='frmtb2a' align='right'>
<font class='frmft1'>
".$cat_fields[$key][0].":
</font>
</td><td class='frmtb2' style='padding: 3px;'> 
<input type='hidden' name='$key' id='t$key' value=''> 
<iframe src='{$jpath_url}indctr.php?md=slct1&fld=".$cat_fields[$key][7]."&fld3=$key&dvalue=$dflt_value&vl=$vlslct' id='f$key' frameborder=0   scrolling=no 
marginwidth='0' marginheight='0'  height='26' width='100%'></iframe>
</td></tr>
";
}

 
if ($cat_fields[$key][4] == 'checkbox')
{
$cat_fields[$key][7]=ereg_replace("\n", '', $cat_fields[$key][7]);
$cat_fields[$key][7]=ereg_replace("\r", '', $cat_fields[$key][7]);
$aa5=split('<option>',$cat_fields[$key][7]);
$thtml=$thtml. "
<tr>
<td height='$td_height' class='frmtb2a' align='right'>
<font class='frmft1'>
".$cat_fields[$key][0].":
</font>
</td><td class='frmtb2' style='padding: 3px;'>
$commnt_var 
";
$i_aa5=0;
foreach ($aa5 as $value1)
{
$i_aa5++;
if ($value1!=""){
$namechbx=$key.$i_aa5;
$thtml=$thtml. " 
<input type='checkbox' name='$namechbx' value='$value1' $fields_val[$namechbx]>
<font FACE='ARIAL, HELVETICA' COLOR='#000099' size='-1'> $value1 </font> <br>
";
}
}
echo " </td></tr>";
}



if ($cat_fields[$key][4] == 'text'){
$aa4=split(':',$cat_fields[$key][3]);
$t_size=$aa4[0];
$t_max=$aa4[1];
$thtml=$thtml. "
<tr>
<td height='$td_height' class='frmtb2a' align='right'>
<font class='frmft1'>
".$cat_fields[$key][0].":
</font>
</td><td class='frmtb2' style='padding: 3px;'>
 $commnt_var  
<input type='text' name='$key' size='$t_size' value='".$fields_val[$key]."' maxlength='$t_max' class=formst>
 </td></tr>
";
 
}

if ($cat_fields[$key][4] == 'date'){

$dt_mss=split('-',$fields_val[$key]);
if ($dt_mss[0]=="0000"){$dt_mss[0]=""; } if ($dt_mss[1]=="00"){$dt_mss[1]="";} if ($dt_mss[2]=="00"){$dt_mss[2]="";}

$thtml=$thtml. "
<tr>
<td height='$td_height' class='frmtb2a' align='right'>
<font class='frmft1'>
".$cat_fields[$key][0].":
</font>
</td><td class='frmtb2' style='padding: 3px;'>
 $commnt_var  <font class='frmft1'>
<input type='text' name='$key"."_mm"."' size='2'  value='".$dt_mss[1]."' class=formst> (".$msg2['sdate_mm'].")  &nbsp;
<input type='text' name='$key"."_dd"."' size='2' value='".$dt_mss[2]."' class=formst> (".$msg2['sdate_dd'].")  &nbsp;
<input type='text' name='$key"."_yy"."' size='4' value='".$dt_mss[0]."' class=formst> (".$msg2['sdate_yy'].")  &nbsp;
</font>
 </td></tr>
";
 
}

if ($cat_fields[$key][4] == 'textarea'){
$aa4=split(':',$cat_fields[$key][3]);
$t_rows=$aa4[1];
$t_cols=$aa4[0];
$t_max=$aa4[2];

if ( !($key=='initialad' and $fields_val[$key]=='')){
 
$thtml=$thtml. "
<tr>
<td height='$td_height' class='frmtb2a' align='right'>
<font class='frmft1'>
".$cat_fields[$key][0].":
</font>
<br><font class='frmft2'> ".$msg2['Not_more_then']."  $t_max ".$msg2['chars_k']."</font>
</font>
</td><td class='frmtb2' style='padding: 3px;'>
";
if (($cat_fields[$key][7]!="") and ($ed_add=='submitad'))
{
 $fields_val[$key]=$cat_fields[$key][7];
}
if ($key!='brief'){$varwrap="wrap=off";}else{$varwrap="";}
$thtml=$thtml. "
$commnt_var 
<textarea name='$key' $varwrap  rows='$t_rows' cols='$t_cols' onkeyup=\"checktxtr('$key', $t_max, 'dl$key');\"  class=formst>".$fields_val[$key]."</textarea>
<table><tr><td><font class='smallb'>
<div id='dl$key'></div></font></td><td><font class='smallb'>".$msg2['chars_left']."</font></td></tr></table> 
<script language='javascript'>checktxtr('$key', $t_max, 'dl$key');</script>
</td></tr>
";

}

}
}
 
$thtml=$thtml. "
</td></tr>
</table></td></tr>
</table>
<font class='smallfnt'>
".$msg2['all_fields_marked']."  
</font>
&nbsp; <p class='pst1'> 
";

if ($key_map=="1" and $use_gl_map=="yes"){$thtml=$thtml.checkmpjs();}

global $dsplhltpht;
if ($dsplhltpht=="yes" and (($fields_val['adrate'] < 1) or ($fields_val['adrate']==""))){
$thtml=$thtml. "
<p class='pst1'>
<table width='650' border=0  cellspacing=1 cellpadding=0>
<tr><td>
<font class='stfntb'>
<p align='justify'>
&nbsp;  ".$msg2['ph_view_after_highl']."
</font>
</td></tr></table>

";
} 

if($photos_count > 0) {
$phptomaxsize1=$phptomaxsize/1000;
$thtml=$thtml. "
<p class='pst1'>
<table width='650' border=0 class='frmtb1' cellspacing=1 cellpadding=0>
<tr><td>
<font class='frmcp'>
<b> ".$msg2['Submit_photos'].":</b>
</font>  

<table width='100%' border=0 cellspacing=1 cellpadding=1>
<tr><td class='frmtb2' style='padding: 3px;'>
<font class='frmft1'>
<font size='-2'><b> ".$msg2['Photo_size_z']." < $phptomaxsize1 ".$msg2['Kbyte_v']." </b> </font>
 
";

$tmvrld=time();

for ($i=1; $i<=$photos_count; $i++)
{
$thtml=$thtml. "
<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
".$msg2['Photo_c']." $i: <input type='file' name='userfile[]' class=formst> 
";
$id1_cnt=$fields_val['idnum'];
get_jpg_path($id1_cnt);
 
if( (file_exists($photo_path[$i]))  and ($ed_add=='edit') ){
$thtml=$thtml. "
&nbsp; <img src='".$urlclscrpt."{$jm_cmpath}sph.php?id=$id1_cnt&wd=60&np=$i&tmv=$tmvrld' > 
";
$chbphdl="chdlpht".$i;
$thtml=$thtml. "
&nbsp; <input type='checkbox' name='$chbphdl' value='1'>".$msg2['delete_p']."  
";
}

}
$thtml=$thtml. "
 
</td></tr></table></td></tr></table> 
</b><p class='pst1'> 
";
}

if ($incl_prevphoto=='yes')
{

$prviewphotomax1=$prviewphotomax/1000;
$thtml=$thtml. "
<p class='pst1'>
<table width='650' border=0 class='frmtb1' cellspacing=1 cellpadding=0>
<tr><td>
<font class='frmcp'>
<b> ".$msg2['Submit_preview_photo'].":</b>
</font>  

<table width='100%' border=0 cellspacing=1 cellpadding=1>
<tr><td class='frmtb2'>
<font class='frmft1'>
<font size='-2'><b> ".$msg2['Photo_size_z']." < $prviewphotomax1 ".$msg2['Kbyte_v']."; 
$pr_lim_width x $pr_lim_height ".$msg2['pixels_p']." .<br>

</b> </font>
 
<center>
 
<br>".$msg2['Preview_Photo'].": <input type='file' name='userfile[]' class=formst> 
 
</center>
</td></tr></table></td></tr></table> 
</b><p class='pst1'> 
";
}



global $javastl;
if ($incl_mtmdfile=='yes')
{

$id1_cnt=$fields_val['idnum'];

global $multimedia_url;

get_att_path($id1_cnt);
 
if( (file_exists($multimedia_path))  and ($ed_add=='edit') ){

$chbphdl="chdlpht".$i;
$thtmledf= "<nonbr>
&nbsp;<a href='$multimedia_url'>file</a> &nbsp;<input type='checkbox' name='chdlmmf' value='1'>".$msg2['delete_p']." 
</nobr>";
}

if (($fields_val['flsrvtl1']!="") or  ($fields_val['flsrvtl2']!=""))
{$displv="block";} else {$displv="none";}

$mtmdfile_maxs1=$mtmdfile_maxs/1000;

if ($fields_val['flsrvur1']==""){$fields_val['flsrvur1']="http://";}
if ($fields_val['flsrvur2']==""){$fields_val['flsrvur2']="http://";}

global $flattchext, $infldflsrv;
foreach ($flattchext as $value_ext){$fltps1=$fltps1."$value_ext, ";}
 
$thtml=$thtml. "
<p class='pst1'>
<table width='650' border=0 class='frmtb1' cellspacing=1 cellpadding=0>
<tr><td>
<font  class='frmcp'>
<b> ".$msg2['Submit_v'].":</b>
</font>  

<table width='100%' border=0 cellspacing=1 cellpadding=1>
<tr><td class='frmtb2' style='padding: 3px;'>
<font class='frmft1'>
".$msg2['youcanlldtypes']."  $fltps1 
  ".$msg2['size_f']." < $mtmdfile_maxs1 ".$msg2['Kbyte_v']." </font>
 
<center>
<table width='450'><tr><td>
".$msg2['att_file'].":</td><td> 
<input type='file' name='userfile[]' class=formst> $thtmledf
</td></tr><tr><td>
".$msg2['att_file_title'].":</td><td>
<input type='text' name='attfltl' value='".$fields_val['attfltl']."' class=formst> 
</td></tr></table>
<p class='pst1'>
</center>
";

global $plsflsrvrs;
if ($plsflsrvrs=="yes"){
$thtml=$thtml. "
$javastl 
&nbsp; <font  FACE='ARIAL, HELVETICA' COLOR='#000099' size='-1' 
style='color:#0000ee; TEXT-DECORATION: underline; cursor: pointer;' OnClick=\"displ('flform');\">
<b>".$msg2['load_video_on_serv']."</b>
</font>
<DIV id='flform' style='DISPLAY: $displv'>
<table width='90%'><tr><td>
<p align='justify'> 
&nbsp; $infldflsrv

</td></tr></table>
  
<center>
<table width='450'>
<tr><td width='120'><nobr>".$msg2['att_file_title']." 1 </nobr></td><td>
<input type='text' name='flsrvtl1' value='".$fields_val['flsrvtl1']."' size='35' class=formst> 
</td></tr><tr><td><nobr>".$msg2['att_url_file']." 1</nobr>
</td><td><input type='text' name='flsrvur1' value='".$fields_val['flsrvur1']."' size='35' class=formst> 
</td></tr>
<tr><td><nobr>".$msg2['att_file_title']." 2</nobr></td><td>
<input type='text' name='flsrvtl2' value='".$fields_val['flsrvtl2']."' size='35' class=formst> 
</td></tr><tr><td><nobr>".$msg2['att_url_file']." 2</nobr>
</td><td><input type='text' name='flsrvur2' value='".$fields_val['flsrvur2']."' size='35' class=formst> 
</td></tr>

<tr><td>".$msg2['code_to_embed']."  
</td><td><textarea name='embdcod' rows='3' cols='35'  
 maxsize='1000' class=formst>".$fields_val['embdcod']."</textarea>
</td></tr>

</table>
</div>
 <br>
&nbsp; 
";
}

$thtml=$thtml. " 
</center>
</td></tr></table></td></tr></table> 
</b><p class='pst1'> 
";
}

global $usevrfcode;
if ($ed_add=='submitad' and $usevrfcode=="yes"){include_once("vrfuncs.php"); $thtml=$thtml.vrchtml();}


$thtml=$thtml. "
<table width='100%' border=0 cellspacing=1 cellpadding=1>
<tr><td>
";


if ($jsfrmch=="yes"){
$thtml=$thtml. "
<input type='button' value='".$msg2['Submit_Ad_v']."' class=formst STYLE='FONT-WEIGHT: bold;'
 onClick=\"checkaddform();\">
";
}
else{
$thtml=$thtml. "
<input type='submit' value='".$msg2['Submit_Ad_v']."' class=formst STYLE='FONT-WEIGHT: bold;'>
";}

$thtml=$thtml. "
</td><td align='right'>
 &nbsp; 
";


global $REMOTE_ADDR, $waflnscr, $_REQUEST; 
if ($ed_add=='submitad'){
if($waflnscr=="yes"){$rmt_addrip=$_REQUEST['afflusrip'];} else{$rmt_addrip=$REMOTE_ADDR;} 
$thtml=$thtml. "<font class=stfntb> ".$msg2['your_ipaddr']." $rmt_addrip</font>";

}

$thtml=$thtml."
</td></tr></table>
<input type='hidden' name='ed_id' value='$ed_id'>
<input type='hidden' name='ed_passw' value='$ed_passw'>
</form>
</td><td valign='top'>
$rightform_html   
</td></tr></table> </center>
 
";

include($templ['msg']);
}

 


?>

<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

include("vrfuncs.php");
$idcd1=$_REQUEST['idcd'];

$crcdv=cr_vrcd($idcd1); 

Header("Content-type: image/gif");
$im = imagecreate(65,35);
$colr1 = ImageColorAllocate($im, 200, 200, 255);
$colr2 = ImageColorAllocate($im, 0, 0, 0);
ImageString($im,6,8,8,$crcdv,$colr2);
ImageGif($im);
ImageDestroy($im);

?>
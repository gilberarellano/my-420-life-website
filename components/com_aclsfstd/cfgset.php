<?php
/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

$conf_dir="conf/";

$confopt_fl=$compnt_path.$conf_dir."$opt_file"; 
$opt_file=$compnt_path.$opt_file;
if(file_exists($confopt_fl)){include($confopt_fl);}else{include("$opt_file");} 
?>
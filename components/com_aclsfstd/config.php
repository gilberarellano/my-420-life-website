<?php 

/**
*  This file is part of Almond Classifieds (Standard Edition) Component for Joomla! (http://www.almondsoft.com)
*  Copyright (C) 2008-2012 Almondsoft.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

require("sfmd.php");  

# name of database table for ads:
$table_ads1="aclsfstdtb";
# name of mysql table for logs
$table_logs1="aclsfstdlg";


# the base of the path of the directory where
# photos files will be kept.
$photos_path="photos/";

# the base to the URL of the directory where
# all photos files will be kept. 
$photos_url="photos/";

#########################################
header("Content-Type: text/html; charset=utf-8\n");  

# Joomla options

if (defined('_JEXEC')){
$uri =& JURI::getInstance(); $urlclscrpt1=$uri->root(); 
$jmcfg_var = new JConfig;
$jmldbprfx=$jmcfg_var->dbprefix;
}
if(defined( '_VALID_MOS' )){$urlclscrpt1=$mosConfig_live_site."/"; $jmldbprfx=$mosConfig_dbprefix;}

$jm_cmpath="components/com_aclsfstd/";

if (defined('JPATH_ROOT')){$jmlconfpath=JPATH_ROOT;}
else{$jmlconfpath=$mosConfig_absolute_path;}

if($jmlconfpath==""){
$urlclscrpt1="http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['REQUEST_URI'])."/";
$urlclscrpt1=str_replace ($jm_cmpath, "", $urlclscrpt1);

$compnt_path=dirname($_SERVER['SCRIPT_FILENAME'])."/";
}
 
if($jmlconfpath!=""){
$compnt_path=$jmlconfpath."/".$jm_cmpath;
$admcomp_path=$jmlconfpath."/administrator"."/".$jm_cmpath;
} 

$opt_file="conf_opt.php";include("cfgset.php");
$opt_file="conf_cat.php";include("cfgset.php"); 

$slctcntr="no";


$style_css_url=$urlclscrpt1.$jm_cmpath."style.css";
$table_ads=$jmldbprfx.$table_ads1;
$table_logs=$jmldbprfx.$table_logs1;
$photos_url=$urlclscrpt1.$jm_cmpath.$photos_url;
$jpath_url=$urlclscrpt1.$jm_cmpath;
$photos_path=$compnt_path.$photos_path;

 

$indx_url="{$urlclscrpt1}index.php?option=com_aclsfstd&Itemid=".$_REQUEST['Itemid']."&"; 
$indxjf_get="
<input type='hidden' name='option' value='com_aclsfstd'>
<input type='hidden' name='Itemid' value='".$_REQUEST['Itemid']."'>
";

#######################################

# Set max size for the preview photo (bytes)
$prviewphotomax=15000;
# Include into img tag limits for preview photo (yes, no)
$prphotolimits="yes";
# If "yes" set up limits. If $height="" it means proportional
# photo's size with width equal $pr_lim_width
$pr_lim_height="";  $pr_lim_width="50";
# Create ads preview photos thumbnails by the script on the fly  
# from first photos loaded by users (.jpeg type only) (yes, no)
$prphscr='yes';
# set link to ad details page under "title" field on the index page (yes, no)
$lnkttlfl="yes";

# Place preview photo only on ads layer and do not place on ads index pages (yes)
# Place preview photo  on both ads layer and  on ads index pages (no)
$prwph_layer="yes"; 
 

# Set up default value for fields with type 'select'
$select_text="Please choose one";

# Set up format for displaying fields with real type
$real_format="%01.2f"; 

# Max number of comments which will be displayed on the ads details pages
$maxrepldt="10"; 

# Place number of ads for subcategories and cities on the top page (yes, no)
$subcnmb='yes';

# Width for preview photos   
$prphscrwdth=50;

# Width for preview photos on ad layer
$prphlrwdth=150;


$layout_tmpl['layout_3'] = array(
'top' => 't_top.html',
'index' => 't_index2.html',
'ads_list' => 't_ads_list2.html',
'details' => 't_details.html',
'msg' => 't_msg.html',
'sbm' => 't_sbm.html',
);

# Choose layout for classifieds ad service:
$layoutnb='layout_3';

#include preview photo into ad  (yes, no)
$incl_prevpht1='yes';

# use fulltext index for searching keywords (yes,no)
$use_fltxtind="yes";

# Place number of ads on the top page for groups of categories (yes, no)
# when number of ads is more than 30000, this option can cause delay of top page loading
$nbadsgrpct="yes";

############################################################

if ($mbac_addad=='f'){$lgneditd="no";}; 
 
$plhltads=1; $pladddp="no"; $schopt='city'; $intlang='eng';

if ($moderating_ct!=""){$spoptmss=split(",",$moderating_ct); 
foreach ($spoptmss as $value){if($value==$_REQUEST['ct']){$moderating=$moderating_vl;}}}

if ($mbac_second_ct!=""){$spoptmss=split(",",$mbac_second_ct); 
foreach ($spoptmss as $value){if($value==$_REQUEST['ct']){$mbac_second=$mbac_second_vl;}}}
if ($mbac_addad_ct!=""){$spoptmss=split(",",$mbac_addad_ct); 
foreach ($spoptmss as $value){if($value==$_REQUEST['ct']){$mbac_addad=$mbac_addad_vl;}}}
if ($mbac_sndml_ct!=""){$spoptmss=split(",",$mbac_sndml_ct); 
foreach ($spoptmss as $value){if($value==$_REQUEST['ct']){$mbac_sndml=$mbac_sndml_vl;}}}

global $fslct;
$prphscr='yes'; $topcmmnt='yes'; $pltopratedln="yes"; $pltopcontln="yes"; $pltopvisitln="yes";
$templ=$layout_tmpl[$layoutnb];
if ($slctcntr=="yes"){
$ads_fields['country'][4]='select2'; $ads_fields['country'][5]='1'; $ads_fields['country'][7]='city';
$ads_fields['city'][4]='select3'; $ads_fields['city'][5]='1'; $ads_fields['city'][7]='country'; 
}

require("jsfuncs.php"); 
####################################################################

if(file_exists("{$compnt_path}confmdl.php")){include_once("confmdl.php"); include_once("adsline.php");} 

 
?>
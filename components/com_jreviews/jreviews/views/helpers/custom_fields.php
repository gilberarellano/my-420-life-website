<?php
if (isset($debug) && $debug >= 1)
    echo "<!-- custom_fields.php -->\n";
/**
 * JReviews - Reviews Extension
 * Copyright (C) 2010-2011 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
 **/
defined('MVC_FRAMEWORK')
        or die('Direct Access to this location is not allowed.');
/**
 * New/edit listing form
 * create_form.thtml view is a separate file because it's used in ajax calls when selecting a category
 * on new listing submissions
 */
?>
<?php
/**********************************************************************************
 *                                 JAVASCRIPT
 **********************************************************************************/ 
# Trigger new form if both section and category are selected
?>
<?php if (isset($cat_id) && isset($listing_id)) { ?>
<?php } ?>
<?php
/**
 * JReviews - Reviews Extension
 * Copyright (C) 2006-2010 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
 **/
defined('MVC_FRAMEWORK')
        or die('Direct Access to this location is not allowed.');
class CustomFieldsHelper extends MyHelper
{
    var $helpers = array('html', 'form', 'time');
    var $output = array();
    var $form_id = null; // Used to create unique element ids
    var $types =
            array('text' => 'text', 'select' => 'select',
                    'radiobuttons' => 'radio', 'selectmultiple' => 'select',
                    'checkboxes' => 'checkbox', 'website' => 'text',
                    'email' => 'text', 'decimal' => 'text',
                    'integer' => 'text', 'textarea' => 'textarea',
                    'code' => 'textarea', 'date' => 'date', 'media' => '',
                    'hidden' => 'hidden', 'relatedlisting' => 'text');
    var $legendTypes = array('radio', 'checkbox');
    var $multipleTypes = array('selectmultiple');
    var $multipleOptionTypes =
            array('select', 'selectmultiple', 'checkboxes', 'radiobuttons');
    var $operatorTypes = array('decimal', 'integer', 'date');
    function getFieldsForComparison($listings)
    {
        $groups = array();
        foreach ($listings as $listing) {
            if (!empty($listing['Field']['pairs'])) {
                foreach ($listing['Field']['pairs'] AS $field) {
                    $viewAccess = $field['properties']['access_view'];
                    if ($field['properties']['compareview']
                            && $this->Access->in_groups($viewAccess)) {
                        $groups[$field['group_name']]['fields'][$field['name']]['name'] =
                                $field['name'];
                        $groups[$field['group_name']]['fields'][$field['name']]['title'] =
                                $field['title'];
                        $groups[$field['group_name']]['group']['id'] =
                                $field['group_id'];
                        $groups[$field['group_name']]['group']['name'] =
                                $field['group_name'];
                        $groups[$field['group_name']]['group']['title'] =
                                $field['group_title'];
                        $groups[$field['group_name']]['group']['group_show_title'] =
                                $field['group_show_title'];
                    }
                }
            }
        }
        return $groups;
    }
    /**
     *
     * @param type $name
     * @param type $entry
     * @param type $click2search
     * @param type $outputReformat
     * @return type string
     */
    function field($name, &$entry, $click2search = true, $outputReformat = true)
    {
        $name = strtolower($name);
        if (empty($entry['Field']) || !isset($entry['Field']['pairs'][$name])) {
            return false;
        }
        $viewAccess =
                $entry['Field']['pairs'][$name]['properties']['access_view'];
        if (!$this->Access->in_groups($viewAccess)) {
            return false;
        }
        $values =
                $this->display($name, $entry, $click2search, $outputReformat);
        if (count($values) == 1) {
            return $values[0];
        }
        else {
            return '<ul class="fieldValueList"><li>'
                    . implode('</li><li>', $values) . '</li></ul>';
        }
    }
    function fieldValue($name, &$entry)
    {
        $name = strtolower($name);
        if (isset($entry['Field']['pairs'][$name])) {
            return $this
                    ->onDisplay($entry['Field']['pairs'][$name], false, true,
                            true);
        }
        else {
            return false;
        }
    }
    /**
     * Shows text values for field options even if they have an image assigned.
     */
    function fieldText($name, &$entry, $click2search = true,
            $outputReformat = true, $separator = ' &#8226; ')
    {
        $name = strtolower($name);
        if (empty($entry['Field']) || !isset($entry['Field']['pairs'][$name])) {
            return false;
        }
        $entry['Field']['pairs'][$name]['properties']['option_images'] = 0;
        $output =
                $this
                        ->display($name, $entry, $click2search,
                                $outputReformat, false);
        return implode($separator, $output);
    }
    function display($name, &$element, $click2search = true,
            $outputReformat = true)
    {
        $Itemid = $catid = '';
        $MenuModel = ClassRegistry::getClass('MenuModel');
        $fields = $element['Field']['pairs'];
        if (isset($element['Listing'])) {
            $click2searchUrl =
                    Sanitize::getString($fields[$name]['properties'],
                            'click2searchlink');
            if (Sanitize::getInt($element['Category'], 'menu_id') > 0
                    && strstr(strtolower($click2searchUrl), '{catid}')) {
                $Itemid = $element['Category']['menu_id'];
                $fields[$name]['properties']['click2searchlink'] =
                        str_ireplace(
                                array('/cat:{catid}', '/criteria:{criteriaid}'),
                                '', $click2searchUrl);
            }
            else {
                $MenuModel = ClassRegistry::getClass('MenuModel');
                if (strstr(strtolower($click2searchUrl), '{criteriaid}')) {
                    $Itemid =
                            $MenuModel
                                    ->get(
                                            'jr_advsearch_'
                                                    . $element['Criteria']['criteria_id']);
                    $fields[$name]['properties']['click2searchlink'] =
                            str_ireplace('/criteria:{criteriaid}', '',
                                    $click2searchUrl);
                }
                if (empty($Itemid)) {
                    $Itemid = $MenuModel->get('jr_advsearch');
                }
            }
            if (Sanitize::getInt($element['Listing'], 'cat_id') > 0) {
                $catid = $element['Listing']['cat_id'];
            }
        }
        $criteriaid = $element['Criteria']['criteria_id'];
        $this->output = array();
        if ($fields[$name]['type'] == 'email') {
            $click2search = false;
            $output_format =
                    Sanitize::getString($fields[$name]['properties'],
                            'output_format');
            $output_format == ''
                    and $fields[$name]['properties']['output_format'] =
                            '<a href="mailto:{FIELDTEXT}">{FIELDTEXT}</a>';
        }
        // Field specific processing
        $showImage =
                Sanitize::getInt($fields[$name]['properties'], 'option_images',
                        1);
        $this->onDisplay($fields[$name], $showImage);
        if (Sanitize::getBool($fields[$name]['properties'], 'formatbeforeclick')) {
            # Output reformat
            if ($outputReformat) {
                $this->outputReformat($name, $fields, $element);
            }
            # Click2search
            if (in_array($fields[$name]['properties']['location'],
                    array('listing', 'content'))
                    && ($click2search
                            && $fields[$name]['properties']['click2search'])) {
                $this
                        ->click2Search($fields[$name], $criteriaid, $catid,
                                $Itemid);
            }
        }
        else {
            # Click2search
            if (in_array($fields[$name]['properties']['location'],
                    array('listing', 'content'))
                    && ($click2search
                            && Sanitize::getString(
                                    $fields[$name]['properties'],
                                    'click2search'))) {
                $this
                        ->click2Search($fields[$name], $criteriaid, $catid,
                                $Itemid);
            }
            # Output reformat
            if ($outputReformat) {
                $this->outputReformat($name, $fields, $element);
            }
        }
        return $this->output;
    }
    /**
     * Default display of custom fields  
     * 
     * @param mixed $entry - listing or review array
     * @param mixed $page - detail or list
     * @param mixed $group_names - group name string or group names array
     */
    function displayAll($entry, $page, $group_names = '')
    {
        if (isset($_GET["id"]) && !empty($_GET["id"])) {
            $canEdit = $this->Access->canEditPost($user_id);
        }
        if (isset($_GET["catid"])) {
            $cat_id = $_GET["catid"];
        }
        elseif (isset($_POST["catid"])) {
            $cat_id = $_POST["catid"];
        }
        if (isset($cat_id)) {
            $cat_id = preg_match_all('/\d+/', $cat_id, $m);
            $cat_id = $m[0][0];
        }
        if (isset($_GET["sectionid"])) {
            $section_id = $_GET["sectionid"];
        }
        elseif (isset($_POST["sectionid"])) {
            $section_id = $_POST["sectionid"];
        }
        if (isset($section_id)) {
            $section_id = preg_match_all('/\d+/', $section_id, $m);
            $section_id = $m[0][0];
        }
        if (isset($_GET["id"])) {
            $listing_id = $_GET["id"];
        }
        elseif (isset($_POST["id"])) {
            $listing_id = $_POST["id"];
        }
        if (isset($listing_id)) {
            $listing_id = preg_match_all('/\d+/', $listing_id, $m);
            $listing_id = $m[0][0];
        }
        if ($debug >= 1) {
            echo '<b>cat_id: </b> ' . $cat_id . ' - <b>listing_id: </b> '
                    . $listing_id . '<br>' . "\n";
        }
        if (!isset($entry['Field']['groups']))
            return '';
        $groups = array();
        $showFieldsInView = 0;
        $output = '';
        // Pre-processor to hide groups with no visible fields
        if (isset($entry['Field']['pairs']) && !empty($entry['Field']['pairs'])) {
            foreach ($entry['Field']['pairs'] AS $field) {
                if ($field['properties'][$page . 'view'] == 1
                        && $this->Access
                                ->in_groups($field['properties']['access_view'])) {
                    $showFieldsInView++;
                    $showGroup[$field['group_id']] = 1;
                }
            }
        }
        // Check if group name is passed as string to output only the specified group
        if (is_string($group_names)) {
            $group_name = $group_names;
            if ($group_name != '') {
                if (isset($entry['Field']['groups'][$group_name])) {
                    $groups =
                            array(
                                    $group_name => $entry['Field']['groups'][$group_name]);
                }
            }
            elseif ($showFieldsInView) {
                $groups = $entry['Field']['groups'];
            }
        }
        // Check if group names were passed as array to include or exclude the specified groups
        elseif (is_array($group_names)) {
            if (!empty($group_names['includeGroups'])) {
                foreach ($group_names['includeGroups'] as $group_name) {
                    if (isset($entry['Field']['groups'][$group_name])) {
                        $groups[$group_name] =
                                $entry['Field']['groups'][$group_name];
                    }
                }
            }
            if (!empty($group_names['excludeGroups'])) {
                $groups = $entry['Field']['groups'];
                foreach ($group_names['excludeGroups'] as $group_name) {
                    if (isset($entry['Field']['groups'][$group_name])) {
                        unset($groups[$group_name]);
                    }
                }
            }
        }
        if (empty($groups))
            return '';
        // XB
        $output .=
                '<div id="community-wrap" class="on-my420life ltr">' . "\n";
        $output .= '<div class="jr_customFields">' . "\n";
?>
        
        <script type="text/javascript">
        jQuery(document).ready(function() { jQuery('#jr_address','#jr_listingForm').after('<span id="gm_geocode"> <input class="jrButton" type="button" onclick="geomaps.mapPopupSimple();" value="Map it" />&nbsp; <input class="jrButton" type="button" onclick="geomaps.clearLatLng();" value="Clear LatLng" /> </span>'); });
        </script>
        <script type="text/javascript">
        var plans; jQuery(document).ready(function(){ plans = {"plan0":{"fields":["jr_email","jr_hoursofoperation","jr_phone","jr_web","jr_caviarhashhalf","jr_cottoncandybubblemelthalf","jr_gdpbubblemelthashhalf","jr_hashoilhalf","jr_honeycombwaxhalf","jr_jadekeifhalf","jr_lemonkeifhalf","jr_caviarhashfourth","jr_cottoncandybubblemeltfourth","jr_gdpbubblemelthashfourth","jr_hashoilfourth","jr_honeycombwaxfourth","jr_jadekeiffourth","jr_lemonkeiffourth","jr_caviarhasheight","jr_cottoncandybubblemelteight","jr_gdpbubblemelthasheight","jr_hashoileight","jr_honeycombwaxeight","jr_jadekeifeight","jr_lemonkeifeight","jr_caviarhashgrams","jr_cottoncandybubblemeltgrams","jr_gdpbubblemelthashgrams","jr_hashoilgrams","jr_honeycombwaxgrams","jr_jadekeifgrams","jr_lemonkeifgrams","jr_caviarhashoz","jr_cottoncandybubblemeltoz","jr_gdpbubblemelthashoz","jr_hashoiloz","jr_honeycombwaxoz","jr_jadekeifoz","jr_lemonkeifoz","jr_bananabreadhalf","jr_breathstripshalf","jr_browniescaramelhalf","jr_brownieshalf","jr_chocolatechipcakehalf","jr_crumbcakehalf","jr_giddyupgranolabarshalf","jr_granolahalf","jr_lemonbarshalf","jr_lolipopshalf","jr_popcornhalf","jr_ricecrispiesregularhalf","jr_ricekrispieshalf","jr_bananabreadfourth","jr_breathstripsfourth","jr_browniescaramelfourth","jr_browniesfourth","jr_chocolatechipcakefourth","jr_crumbcakefourth","jr_giddyupgranolabarsfourth","jr_granolafourth","jr_lemonbarsfourth","jr_lolipopsfourth","jr_popcornfourth","jr_ricecrispiesregularfourth","jr_ricekrispiesfourth","jr_bananabreadeight","jr_breathstripseight","jr_browniescarameleight","jr_brownieseight","jr_chocolatechipcakeeight","jr_crumbcakeeight","jr_giddyupgranolabarseight","jr_granolaeight","jr_lemonbarseight","jr_lolipopseight","jr_popcorneight","jr_ricecrispiesregulareight","jr_ricekrispieseight","jr_brownies","jr_granola","jr_lolipops","jr_popcorn","jr_bananabreadgrams","jr_breathstripsgrams","jr_browniescaramelgrams","jr_browniesgrams","jr_chocolatechipcakegrams","jr_crumbcakegrams","jr_giddyupgranolabarsgrams","jr_granolagrams","jr_lemonbarsgrams","jr_lolipopsgrams","jr_popcorngrams","jr_ricecrispiesregulargrams","jr_ricekrispiesgrams","jr_bananabreadoz","jr_breathstripsoz","jr_browniescarameloz","jr_browniesoz","jr_chocolatechipcakeoz","jr_crumbcakeoz","jr_giddyupgranolabarsoz","jr_granolaoz","jr_lemonbarsoz","jr_lolipopsoz","jr_popcornoz","jr_ricecrispiesregularoz","jr_ricekrispiesoz","jr_hazytrainhalf","jr_nycsourdieselhalf","jr_orangecrushhalf","jr_hazytrainfourth","jr_nycsourdieselfourth","jr_orangecrushfourth","jr_hazytraineight","jr_nycsourdieseleight","jr_orangecrusheight","jr_hazytraingrams","jr_nycsourdieselgrams","jr_orangecrushgrams","jr_hazytrainoz","jr_nycsourdieseloz","jr_orangecrushoz","jr_bigbanghalf","jr_bubbahalf","jr_bubblegumhalf","jr_diamondoghalf","jr_gdphalf","jr_godsgifthalf","jr_masterkushhalf","jr_ogkushhalf","jr_skywalkeroghalf","jr_bigbangfourth","jr_bubbafourth","jr_bubblegumfourth","jr_diamondogfourth","jr_gdpfourth","jr_godsgiftfourth","jr_masterkushfourth","jr_ogkushfourth","jr_skywalkerogfourth","jr_bigbangeight","jr_bubbaeight","jr_bubblegumeight","jr_diamondogeight","jr_gdpeight","jr_godsgifteight","jr_masterkusheight","jr_ogkusheight","jr_skywalkerogeight","jr_gdp","jr_bubba","jr_bigbanggrams","jr_bubbagrams","jr_bubblegumgrams","jr_diamondoggrams","jr_gdpgrams","jr_godsgiftgrams","jr_masterkushgrams","jr_ogkushgrams","jr_skywalkeroggrams","jr_bigbangoz","jr_bubbaoz","jr_bubblegumoz","jr_diamondogoz","jr_gdpoz","jr_godsgiftoz","jr_masterkushoz","jr_ogkushoz","jr_skywalkerogoz","jr_adultonly","jr_address","jr_city","jr_country","jr_county","jr_latitude","jr_longitude","jr_name","jr_state","jr_tagline","jr_zipcode","jr_businesstype","jr_dispensarytype","jr_preroll","jr_prerollhalf","jr_prerollfourth","jr_prerolleight","jr_prerollgrams","jr_prerolloz","jr_paymentmethods","jr_bluedreamhalf","jr_calihazehalf","jr_drwhohalf","jr_hawaiinsnowhalf","jr_jackhererhalf","jr_lambsbreathhalf","jr_mauiewowiehalf","jr_missincrediblehalf","jr_sweettoothhalf","jr_bluedreamfourth","jr_calihazefourth","jr_drwhofourth","jr_hawaiinsnowfourth","jr_jackhererfourth","jr_lambsbreathfourth","jr_mauiewowiefourth","jr_missincrediblefourth","jr_sweettoothfourth","jr_bluedreameight","jr_calihazeeight","jr_drwhoeight","jr_hawaiinsnoweight","jr_jackherereight","jr_lambsbreatheight","jr_mauiewowieeight","jr_missincredibleeight","jr_sweettootheight","jr_bluedreamgrams","jr_calihazegrams","jr_drwhograms","jr_hawaiinsnowgrams","jr_jackherergrams","jr_lambsbreathgrams","jr_mauiewowiegrams","jr_missincrediblegrams","jr_sweettoothgrams","jr_bluedreamoz","jr_calihazeoz","jr_drwhooz","jr_hawaiinsnowoz","jr_jackhereroz","jr_lambsbreathoz","jr_mauiewowieoz","jr_missincredibleoz","jr_sweettoothoz","jr_tincturehalf","jr_tincturefourth","jr_tinctureeight","jr_tincture","jr_tincturegrams","jr_tinctureoz","jr_gm_distance"],"images":"6"}}; });
        </script>
        <script type="text/javascript">
        /* <![CDATA[ */
        jQuery(document).ready(function() {
        jreviews.controlFieldListing = new jreviewsControlField('jr_listingForm','catid');
        jreviews.controlFieldListing.loadData({'entry_id':jQuery('#listing_id','#jr_listingForm').val(),'value':false,'page_setup':true,'referrer':'listing'});
        });
        /* ]]> */
        </script>
        <!-- XB -->
        <script language="javascript" type="text/javascript">
        function thanksWeedFade() {
            var t=setTimeout("thanksWeed()",2000);
        }
        function thanksWeed() {
            document.getElementById('msg').innerHTML = "Thanks for using the Weed Menu. Keep adding more!";
        }        
        
        function addElement (divName) {
            // Get the value into the input text field
            var element=document.getElementById('searchword').value;
            
            var price_g    =$("#price_g").val();
            var price_8 =$("#price_8").val();
            var price_4 =$("#price_4").val();
            var price_2 =$("#price_2").val();
            var price_oz=$("#price_oz").val();
            
            var weed_type=$("#weed_type").val();

            // Grab the name
            var weedName = $('#searchboxWeed input[name=searchword]').val();
            
            element='<div><div class="fieldLabel">'+element+'</div><div class="fieldValue"><ul class="fieldValueList"><li>grams</li><li>1/8</li><li>1/4</li><li>1/2</li><li>oz</li></ul></div></div>';
            element=element+'<li>';
            element=element+'<div><div class="jr_fieldDiv jr_prerollgrams"><label class="jrLabel" for="jr_prerollgrams">grams<span class="jr_infoTip jrTipInit">&nbsp;</span><div class="jr_tooltipBox">grams</div></label><input id="jr_prerollgrams" class="jrInteger" type="text" name="data[Field][Listing][jr_prerollgrams]" data-click2add="0" value="'+price_g+'"></div></div>';
            element=element+'<div><div class="jr_fieldDiv jr_prerolleight"><label class="jrLabel" for="jr_prerolleight">1/8<span class="jr_infoTip jrTipInit">&nbsp;</span><div class="jr_tooltipBox">1/8</div></label><input id="jr_prerolleight" class="jrInteger" type="text" name="data[Field][Listing][jr_prerolleight]" data-click2add="0" value="'+price_8+'"></div></div>';
            element=element+'<div><div class="jr_fieldDiv jr_prerollfourth"><label class="jrLabel" for="jr_prerollfourth">1/4<span class="jr_infoTip jrTipInit">&nbsp;</span><div class="jr_tooltipBox">1/4</div></label><input id="jr_prerollfourth" class="jrInteger" type="text" name="data[Field][Listing][jr_prerollfourth]" data-click2add="0" value="'+price_4+'"></div></div>';
            element=element+'<div><div class="jr_fieldDiv jr_prerollhalf"><label class="jrLabel" for="jr_prerollhalf">1/2<span class="jr_infoTip jrTipInit">&nbsp;</span><div class="jr_tooltipBox">1/2</div></label><input id="jr_prerollhalf" class="jrInteger" type="text" name="data[Field][Listing][jr_prerollhalf]" data-click2add="0" value="'+price_2+'"></div></div>';
            element=element+'<div><div class="jr_fieldDiv jr_prerolloz"><label class="jrLabel" for="jr_prerolloz">oz<span class="jr_infoTip jrTipInit">&nbsp;</span><div class="jr_tooltipBox">oz</div></label><input id="jr_prerolloz" class="jrInteger" type="text" name="data[Field][Listing][jr_prerolloz]" data-click2add="0" value="'+price_oz+'"></div></div>';
            element=element+'<div class="separator_micro"></div>';
            element=element+'</li>';
            
            
            if (element=="") {
                // Show an error message if the field is blank;
                document.getElementById('msg').style.display="block";
                document.getElementById('msg').innerHTML = "Error! Insert a description for the element";
            } else{
                // This is the <ul id="myList"> element that will contains the new elements
                
                if(weed_type==1    ) { divName="myListINDICA"; }
                if(weed_type==2    ) { divName="myListSATIVA"; }
                if(weed_type==3    ) { divName="myListHYBRID"; }
                if(weed_type==4    ) { divName="myListEDIBLE"; }
                if(weed_type==5    ) { divName="myListCONCENTRATE"; }
                if(weed_type==6    ) { divName="myListDRINK"; }
                if(weed_type==7    ) { divName="myListCLONE"; }
                if(weed_type==8    ) { divName="myListSEED"; }
                if(weed_type==9    ) { divName="myListTINCTURE"; }
                if(weed_type==10) { divName="myListGEAR"; }
                if(weed_type==11) { divName="myListTOPICALS"; }
                if(weed_type==12) { divName="myListPREROLL"; }
                if(weed_type==13) { divName="myListWAX"; }
                
                var container = document.getElementById(divName);
                // Create a new <li> element for to insert inside <ul id="myList">
                var new_Weed = document.createElement('li');
                
                new_Weed.innerHTML = element;
                
                //$.get("/scripts/addWeed.php");
                <?php //require("/scripts/addWeed.php");  ?>
                
                
                container.insertBefore(new_Weed, container.firstChild);
                // Show a message if the element has been added;
                document.getElementById('msg').style.display="block";
                document.getElementById('msg').innerHTML = "New Weed Strain added!";
                
                thanksWeedFade();

                // Clean input field
                document.getElementById('searchword').value="";
            }
            
            jQuery.post(
                    '/administrator/index.php?option=com_jreviews&format=raw&tmpl=component', 
                    {
                        'data[action]':    '_save',
                        'data[controller]':    'admin/fields',
                        'data[Field][name]':  weedName,
                        'data[Field][title]': weedName,
                        'data[Field][location]':    "content",
                        'data[Field][groupid]':    "5",
                        'data[Field][type]':    "selectmultiple",
                        'data[Field][description]': 'description',
                        'data[Field][listview]1': '1',
                        'data[Field][contentview]1': '1',
                        'data[Field][compareview]1': '1'
                    }
            );
            jQuery.post(
                    '/administrator/index.php?option=com_jreviews&format=raw&tmpl=component', 
                    {
                        'data[action]':    '_save',
                        'data[controller]':    'admin/fields',
                        'data[Field][name]':  weedName + "grams",
                        'data[Field][title]': "grams",
                        'data[Field][location]':    "content",
                        'data[Field][groupid]':    "5",
                        'data[Field][type]':    "integer",
                        'data[Field][description]': 'description',
                        'data[Field][listview]1': '0',
                        'data[Field][contentview]1': '1',
                        'data[Field][compareview]1': '1'
                    }
            );
            jQuery.post(
                    '/administrator/index.php?option=com_jreviews&format=raw&tmpl=component', 
                    {
                        'data[action]':    '_save',
                        'data[controller]':    'admin/fields',
                        'data[Field][name]':  weedName + "eight",
                        'data[Field][title]': "1/8",
                        'data[Field][location]':    "content",
                        'data[Field][groupid]':    "5",
                        'data[Field][type]':    "integer",
                        'data[Field][description]': 'description',
                        'data[Field][listview]1': '0',
                        'data[Field][contentview]1': '1',
                        'data[Field][compareview]1': '1'
                    }
            );
            jQuery.post(
                    '/administrator/index.php?option=com_jreviews&format=raw&tmpl=component', 
                    {
                        'data[action]':    '_save',
                        'data[controller]':    'admin/fields',
                        'data[Field][name]':  weedName + "fourth",
                        'data[Field][title]': "1/4",
                        'data[Field][location]':    "content",
                        'data[Field][groupid]':    "5",
                        'data[Field][type]':    "integer",
                        'data[Field][description]': 'description',
                        'data[Field][listview]1': '0',
                        'data[Field][contentview]1': '1',
                        'data[Field][compareview]1': '1'
                    }
            );
            jQuery.post(
                    '/administrator/index.php?option=com_jreviews&format=raw&tmpl=component', 
                    {
                        'data[action]':    '_save',
                        'data[controller]':    'admin/fields',
                        'data[Field][name]':  weedName + "half",
                        'data[Field][title]': "1/2",
                        'data[Field][location]':    "content",
                        'data[Field][groupid]':    "5",
                        'data[Field][type]':    "integer",
                        'data[Field][description]': 'description',
                        'data[Field][listview]1': '0',
                        'data[Field][contentview]1': '1',
                        'data[Field][compareview]1': '1'
                    }
            );
            jQuery.post(
                    '/administrator/index.php?option=com_jreviews&format=raw&tmpl=component', 
                    {
                        'data[action]':    '_save',
                        'data[controller]':    'admin/fields',
                        'data[Field][name]':  weedName + "oz",
                        'data[Field][title]': "oz",
                        'data[Field][location]':    "content",
                        'data[Field][groupid]':    "5",
                        'data[Field][type]':    "integer",
                        'data[Field][description]': 'description',
                        'data[Field][listview]1': '0',
                        'data[Field][contentview]1': '1',
                        'data[Field][compareview]1': '1'
                    }
            );
        }
        </script>
        <!--  XB -->
        <?php
        // XB
        $required_fields = array("listing-details");
        foreach ($groups AS $group_title => $group) {
            if (isset($showGroup[$group['Group']['group_id']])
                    || $group_name != '')
                foreach ($required_fields as $key => $value) {
                    if (preg_match("/" . $value . "/i", $group['Group']['name'])) {
                        $required_groups_found = 1;
                    }
                }
        }
        // XB
        $weed_groups =
                array("indica", "sativa", "hybrid", "edible", "concentrate",
                        "tincture", "preroll");
        foreach ($groups AS $group_title => $group) {
            if (isset($showGroup[$group['Group']['group_id']])
                    || $group_name != '')
                foreach ($weed_groups as $key => $value) {
                    if (preg_match("/" . $value . "/i", $group['Group']['name'])) {
                        $weed_groups_found = 1;
                    }
                }
        }
        // XB
        // Weed Menu //
        ?>
        <?php $doc = &JFactory::getDocument(); ?>
        <?php $doc->addScript("templates/my420life/warp/js/searchWeed.js"); ?>
        <?php
        if ($canEdit) {
            $output .=
                    '<div id="wmenu-new_strain" style="display: block;">'
                            . "\n";
            //$output.='<form name="myForm">'."\n";
            $output .=
                    '<div id="wmenu-message"></div>
                <label for="wmenu-name" id="lbl-name">Name</label>
                <div id="wmenu-complete_menu_close" style="display: none;">X</div>'
                            . "\n"
                            . '
                <div id="wmenu-complete_menu" class=" wmsearch admin" style="display: none;"></div>'
                            . "\n";
            #$output.='<input onkeydown="wm_keyDown(event);" onblur="return wm_hideComplete();" autocomplete="off" id="wmenu-name">'."\n";                
            // <!-- auto-complete -->
            $output .=
                    '<div id="weedmenu">
                <h1 class="edit_large">Add Item!</h1><br>
                <div id="search">';
            $output .=
                    '<form id="searchboxWeed" name="adminForm" role="search" method="post" action="/index.php?option=com_jreviews&view=category&Itemid=580">
                        <input class="searchword" name="searchword" id="searchword" type="text" placeholder="Weed Menu Search..." value="" autocomplete="off">
                        <ul class="results" style="display: none;"></ul>
                            <!-- <input class="searchgo" type="submit" value="Go!" name="task"> -->
                    </form>' . "\n";
            $output .=
                    "<script type=\"text/javascript\">
                    jQuery(function($) {
                        $('#searchboxWeed input[name=searchword]').searchWeed(
                        {
                            'url': '/scripts/weedmenu/index.php?type=json&ordering=&searchphrase=all', 
                            'param': 'searchword', 
                            'msgResultsHeader': 'Weed Menu Results', 
                            'msgMoreResults': 'More Weed Results', 
                            'msgNoResults': 'No Weed found'
                        }).placeholder();
                    });
                    </script>" . "\n";
            $output .= '</div>' . "\n"; //<!-- search -->
            $output .= '</div>' . "\n"; //<!-- weedmenu -->
            $output .= '<div class="separator"></div>';
            $output .= "\n";
            $output .= '<label for="title" id="title">title</label>';
            $output .=
                    '<input type="text" value="" max_size="200" id="title" name="data[Field][title]">'
                            . "\n";
            $output .= '<label for="name" id="name">name</label>';
            $output .=
                    '<input type="text" value="" max_size="200" id="title" name="data[Field][name]">'
                            . "\n";
            $output .=
                    '<!-- auto-complete -->
                <p>
                <label for="weed_type" id="kind-lbl">Kind</label>
                <select id="weed_type">
                    <option value="">&lt; choose &gt;</option>
                    <option value="1">Indica</option>
                    <option value="2">Sativa</option>
                    <option value="3">Hybrid</option>
                    <option value="4">Edible</option>
                    <option value="5">Concentrate</option>
                    <option value="6">Drink</option>
                    <option value="7">Clone</option>
                    <option value="8">Seed</option>
                    <option value="9">Tincture</option>
                    <option value="10">Gear</option>
                    <option value="11">Topicals</option>
                    <option value="12">Preroll</option>
                    <option value="13">Wax</option>
                </select>
                <select id="wmenu-strength">
                    <option value="1">1 Weak</option>
                    <option value="2">2 Mids</option>
                    <option value="3">3 Mellow</option>
                    <option value="5">4 Chronic</option>
                    <option value="5">5 Intense</option>
                    <option value="5">6 Orbital</option>
                    <option value="5">7 Mythical</option>
                </select>
                </p>
                <div id="wmenu-price_dialog">
                    <label for="price_g" id="lbl-price">Price</label>
                    <input class="price" autocomplete="off" id="price_g">
                    <label for="price_g" class="pricel" id="pricel">gram</label>
                    <div id="wmenu-dummy">
                        <label class="pricep"></label>
                    </div>
                    <div id="eighth">
                        <label class="pricep"></label>
                        <input class="price" autocomplete="off" id="price_8">
                        <label for="price_8" class="pricel" id="pricep">1/8</label>
                    </div>
                    <div id="quarter">
                        <label class="pricep"></label>
                        <input class="price" autocomplete="off" id="price_4">
                        <label for="price_4" class="pricel">1/4</label>
                    </div>
                    <div id="half">
                        <label class="pricep"></label>
                        <input class="price" autocomplete="off" id="price_2">
                        <label for="price_2" class="pricel">1/2</label>
                    </div>
                    <div id="ounce">
                        <label class="pricep"></label>
                        <input class="price" autocomplete="off" id="price_oz">
                        <label for="price_oz" class="pricel">oz</label>
                    </div>
                </div>
                <div id="wmenu-notes_dialog">
                  <label for="wmenu-notes" id="lbl-notes">Notes</label>
                  <textarea autocomplete="off" id="wmenu-notes" tag="What\'s it taste like?"></textarea>
                  <br>
            
                  <label for="action">&nbsp;</label>
                <script>
                var divName="myList' . $group['Group']['title']
                            . '";
                </script>
                <input type="button" id="wmenu-action" value="submit" name="submit" onClick="addElement(divName);">                
                
                <div id="msg"></div>
                
                </div>' . "\n";
            //$output.='</form>'."\n";
            $output .= '</div>' . "\n";
        }
        if ($canEdit) {
            $output .=
                    '<div id="jr_listingFormOuter" class="jr_pgContainer">'
                            . "\n";
            $output .=
                    '<iframe id="listing_submit" name="listing_submit" width="0" height="0" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" ></iframe>'
                            . "\n";
            $output .=
                    '<form target="listing_submit" id="jr_listingForm" name="jr_listingForm" action="/index.php?option=com_jreviews&format=raw&tmpl=component&lang=en&amp;format=raw&amp;tmpl=component&amp;Itemid=&amp;url=listings/_save" enctype="multipart/form-data" method="post">'
                            . "\n";
            $output .= '<div class="jr_formChooser">' . "\n"; //<!-- jr_formChooser -->
        }
        if ($this->cmsVersion == CMS_JOOMLA15) :
            $output .= '<span id="jr_Sections">' . "\n";
            $output .=
                    $Form
                            ->select('data[Listing][sectionid]',
                                    array_merge(
                                            array(
                                                    array('value' => null,
                                                            'text' => __t(
                                                                    "Select Section",
                                                                    true))),
                                            $sections),
                                    $listing['Listing']['section_id'],
                                    array('id' => 'section_id',
                                            'class' => 'jrSelect',
                                            'size' => '1',
                                            'onchange' => "jreviews.listing.submitSection(this);"));
            $output .= '</span>
                    &nbsp;&nbsp;' . "\n";
        else :
            __t("Category");
            $output .= '&nbsp;' . "\n";
        endif;
        $output .= '<span id="jr_Categories">' . "\n";
        $output .=
                '<select id="cat_id1" class="jrSelect" onchange="" size="1" name="data[Listing][catid][]">'
                        . "\n";
        $output .= '<option value="0">- Select Category -</option>' . "\n";
        $output .=
                '<option disabled="disabled" value="77">My 420 Life Guide</option>'
                        . "\n";
        $output .=
                '<option selected="selected" value="' . $cat_id
                        . '">- Dispensaries</option>' . "\n";
        $output .= '</select>' . "\n";
        $output .= '</span>' . "\n";
        $output .= '<span class="jr_loadingSmall jr_hidden"></span>' . "\n";
        $output .= '<br />' . "\n";
        if ($canEdit)
            $output .= '</div>' . "\n";
        //<!-- jr_formChooser -->
        if ($canEdit) {
            $output .= '<div class="jr_form jr_formContainer">' . "\n"; //2 //<!-- jr_formContainer -->
            $output .= '<div id="jr_newFields">' . "\n"; //3
        }
        // XB //
        if ($canEdit) {
            $output .= '<fieldset>' . "\n";
            $output .= '<div class="jr_fieldDiv">' . "\n";
            $output .=
                    '<label class="jrLabel">Summary<span class="required">*</span></label>'
                            . "\n";
            $output .=
                    '<textarea id="introtext" name="data[Listing][introtext]" cols="45" rows="5" class="jrTextArea wysiwyg_editor">'
                            . "\n";
            $output .= '</textarea>' . "\n";
            $output .= '</div>' . "\n";
            $output .= '<div class="jr_fieldDiv">' . "\n";
            $output .= '<label class="jrLabel">Description</label>' . "\n";
            $output .=
                    '<textarea id="fulltext" name="data[Listing][fulltext]" cols="45" rows="10" class="jrTextArea wysiwyg_editor"></textarea>'
                            . "\n";
            $output .= '</div>' . "\n";
            $output .= '</fieldset>' . "\n";
        }
        // XB //
        ?>
        <!-- tab key press -->
        <script type="text/javascript">
        var obj;
        var TAB = 9; // tab key
        function catchTAB(evt,elem) {
          obj = elem;
          var keyCode;
          if ("which" in evt) {// NN4 & FF &amp; Opera
            keyCode=evt.which;
          } 
          else if ("keyCode" in evt) {// Safari & IE4+
            keyCode=evt.keyCode;
          } 
          else if ("keyCode" in window.event) {// IE4+
            keyCode=window.event.keyCode;
          } 
          else if ("which" in window.event) {
            keyCode=evt.which;
          }
          else  {
            alert("the browser don't support");
          }
          if (keyCode == TAB) {
            // obj.value = obj.value + "\t";
            // alert("TAB was pressed");
            // setTimeout("obj.focus()",1);// the focus is set back to the text input
          }
        }
        </script>
        <?php
        jimport('joomla.environment.browser');
        $doc = &JFactory::getDocument();
        $browser = &JBrowser::getInstance();
        $browserType = $browser->getBrowser();
        $browserVersion = $browser->getMajor();
        if (isset($debug))
            echo "browser: " . $browserType . "<br>\n";
        $i = 0;
        $j = 0;
        foreach ($groups AS $group_title => $group) {
            if (isset($showGroup[$group['Group']['group_id']])
                    || $group_name != '') {
                // XB                
                if (isset($debug) && $debug >= 1)
                    $output .=
                            "Group type: [" . $group['Group']['name'] . "]"
                                    . "\n";
                foreach ($weed_groups as $key => $value) {
                    if (preg_match("/" . $value . "/i", $group['Group']['name'])) {
                        $weed_groups_found = 1;
                    }
                }
                if ($weed_groups_found == 1 && $canEdit) {
                    $output .=
                            '<fieldset class="jr_hidden" id="group_'
                                    . $group['Group']['name']
                                    . '" style="display: block;">' . "\n";
                    $i++;
                }
                elseif ($required_groups_found == 1 && $canEdit) {
                    $output .= '<fieldset>' . "\n";
                }
                // XB
                $output .=
                        '<div class="fieldGroup ' . $group['Group']['name']
                                . '">'; //4
                $group['Group']['show_title']
                        and $output .=
                                '<h3 class="fieldGroupTitle">'
                                        . $group['Group']['title'] . '</h3>';
                if ($weed_groups_found == 1) {
                    $output .=
                            '<div class="fieldRowWeed ' . 'fieldRowWeed' . $j
                                    . '">' . "\n"; // XB //5
                }
                $output .=
                        '<ul id="myList' . $group['Group']['title'] . '">'
                                . "\n";
                foreach ($group['Fields'] as $field) {
                    // XB //
                    $values = $this->display($field['name'], $entry);
                    if ($canEdit) {
                        if ($field['name'] == "jr_name") {
                            $output .= '<fieldset>' . "\n";
                            $output .=
                                    '<div id="jr_listingTitle" class="jr_fieldDiv">'
                                            . "\n";
                            $output .= '<label class="jrLabel">' . "\n";
                            $output .= 'Title' . "\n";
                            $output .=
                                    '<span class="required">*</span>' . "\n";
                            $output .= '</label>' . "\n";
                            $output .=
                                    '<input id="title" class="jrTitle" type="text" maxlength="255" value="'
                                            . $values[0]
                                            . '" name="data[Listing][title]">'
                                            . "\n";
                            $output .= '</div>' . "\n";
                            $output .= '</fieldset>' . "\n";
                        }
                        elseif ($field['name'] == "jr_tagline") {
                            $output .= '<fieldset>' . "\n";
                            $output .=
                                    '<div id="jr_listingTitleAlias" class="jr_fieldDiv">'
                                            . "\n";
                            $output .=
                                    '<label class="jrLabel">Title Alias</label>'
                                            . "\n";
                            $output .=
                                    '<input id="slug" class="jrTitle" type="text" maxlength="255" value="'
                                            . $values[0]
                                            . '" name="data[Listing][alias]">'
                                            . "\n";
                            $output .= '</div>' . "\n";
                            $output .= '</fieldset>' . "\n";
                        }
                    }
                    // XB //
                    if ($field['title'] == "grams") {
                        $output .=
                                '<!-- <div class="separator_micro"></div> -->'
                                        . "\n";
                        $output .= '<li>' . "\n";
                    }
                    if (($field['properties'][$page . 'view'] == 1)
                            && $this->Access
                                    ->in_groups(
                                            $field['properties']['access_view'])) {
                        // IE problems
                        /* if($canEdit) {
                            $output.= "\n".'<div class="fieldRow ' . $field['name'] . ' ' . $j .'" >'."\n";  //6
                        }
                        else {
                            $output.= "\n".'<div class="fieldRow ' . $field['name'] . ' ' . $j .' small">'."\n"; //6
                        }
                         */
                        $output .= "\n" . '<div>' . "\n";
                        $output .=
                                '<div class="fieldLabel'
                                        . ($field['properties']['show_title'] ? ''
                                                : 'Disabled') . '">'
                                        . ($field['properties']['show_title'] ? $field['title']
                                                : '') . '</div>' . "\n";
                        $values = $this->display($field['name'], $entry);
                        // XB //
                        if (count($values) == 1) {
                            //$output.= '<div class="fieldValue ' . ($field['properties']['show_title'] ? '' : 'labelDisabled') . '">' . $values[0] . '</div>';
                            #$output.= "\n".'<div class="fieldValue">'."\n";
                            if (preg_match("/msie/i", $browserType)) {
                                $output .= $values[0];
                            }
                            else {
                                if ($required_groups_found == 1) {
                                    if (is_numeric($values[0]) && $canEdit) {
                                        $output .=
                                                '<div class="jr_fieldDiv '
                                                        . $field['name'] . '">'
                                                        . "\n";
                                        $output .=
                                                '<label for="' . $field['name']
                                                        . '" class="jrLabel" >'
                                                        . $field['title']
                                                        . '<span class="jr_infoTip">&nbsp;</span><div class="jr_tooltipBox">'
                                                        . $field['title']
                                                        . '</div></label>'
                                                        . "\n";
                                        $output .=
                                                '<input type="text" value="'
                                                        . $values[0]
                                                        . '" class="jrInteger" data-click2add="0" id="'
                                                        . $field['name']
                                                        . '" name="data[Field][Listing]['
                                                        . $field['name']
                                                        . ']">' . "\n";
                                        $output .= '</div>' . "\n"; //<!-- jr_fieldDiv -->
                                    }
                                    elseif (!is_numeric($values[0]) && $canEdit) {
                                        $output .=
                                                '<div class="jr_fieldDiv '
                                                        . $field['name'] . '">'
                                                        . "\n";
                                        $output .=
                                                '<label for="' . $field['name']
                                                        . '" class="jrLabel" >'
                                                        . $field['title']
                                                        . '<span class="jr_infoTip">&nbsp;</span><div class="jr_tooltipBox">'
                                                        . $field['title']
                                                        . '</div></label>'
                                                        . "\n";
                                        $output .=
                                                '<input type="text" value="'
                                                        . $values[0]
                                                        . '" class="jrText" data-click2add="0" id="'
                                                        . $field['name']
                                                        . '" name="data[Field][Listing]['
                                                        . $field['name']
                                                        . ']">' . "\n";
                                        $output .= '</div>' . "\n"; //<!-- jr_fieldDiv -->
                                    }
                                    else {
                                        $output . $values[0];
                                    }
                                }
                                elseif ($weed_groups_found == 1) {
                                    if (is_numeric($values[0]) && $canEdit) {
                                        $output .=
                                                '<div class="jr_fieldDiv '
                                                        . $field['name'] . '">'
                                                        . "\n";
                                        $output .=
                                                '<label for="' . $field['name']
                                                        . '" class="jrLabel" >'
                                                        . $field['title']
                                                        . '<span class="jr_infoTip">&nbsp;</span><div class="jr_tooltipBox">'
                                                        . $field['title']
                                                        . '</div></label>'
                                                        . "\n";
                                        $output .=
                                                '<input type="text" onkeydown="catchTAB(event,this);" value="'
                                                        . $values[0]
                                                        . '" class="jrInteger" data-click2add="0" id="'
                                                        . $field['name']
                                                        . '" name="data[Field][Listing]['
                                                        . $field['name']
                                                        . ']">' . "\n";
                                        $output .= '</div>' . "\n"; //<!-- jr_fieldDiv -->
                                    }
                                    elseif ($canEdit) {
                                        $output .= $values[0];
                                    }
                                    else {
                                        $output .= $values[0];
                                    }
                                }
                                else {
                                    $output .= $values[0];
                                }
                            } // fix for IE
                            #$output.="\n".'</div>'."\n"; //5 //<!-- '.'fieldValue'.' -->
                        }
                        else {
                            //$output.= '<div class="fieldValue ' . ($field['properties']['show_title'] ? '' : 'labelDisabled') . '"><ul class="fieldValueList"><li>' . implode('</li><li>', $values) . '</li></ul></div>';
                            if (!preg_match("/msie/i", $browserType)) {
                                $output .=
                                        "\n" . '<div class="fieldValue '
                                                . ($field['properties']['show_title'] ? ''
                                                        : 'labelDisabled')
                                                . '">' . "\n"
                                                . '<ul class="fieldValueList">'
                                                . "\n" . '<li>'
                                                . implode(
                                                        '</li>' . "\n" . '<li>',
                                                        $values) . '</li>'
                                                . "\n" . '</ul>' . "\n"
                                                . '</div><!-- fieldValue -->'
                                                . "\n";
                            }
                            else {
                                #$output.= "\n".'<div>'."\n".'<ul>'."\n".'<li>' . implode('</li>'."\n".'<li>', $values) . '</li>'."\n".'</ul>'."\n".'</div><!-- fieldValue -->'."\n";
                            }
                        }
                        $output .= '</div>' . "\n"; //4 //<!-- fieldRow '.$field['name'].' '.$j.' -->
                    }
                    if ($field['title'] == "oz") {
                        $output .=
                                '<div class="separator_micro"></div>' . "\n";
                        $output .= '</li>' . "\n"; //<!-- list item for ajax -->
                    }
                }
                // $output.='</ul>'."\n"; //<!-- unnumbered list for ajax -->
                if ($weed_groups_found == 1) {
                    $output .= '</div>' . "\n"; // XB //2 //<!-- fieldRowWeed -->
                    $j++;
                }
                $output .= '</div>' . "\n"; //1 //<!-- fieldGroup '.$group['Group']['name'].' -->
                if ($weed_groups_found == 1 && $canEdit) {
                    $output .= '</fieldset>' . "\n"; //<!-- '.$group['Group']['name'].' -->
                }
                elseif ($required_groups_found == 1 && $canEdit) {
                    $output .= '</fieldset>' . "\n"; //<!-- '.$group['Group']['name'].' -->
                }
                unset($weed_groups_found);
                unset($required_groups_found);
            }
            if (preg_match("/msie/i", $browserType)) {
                //echo "<h3>j: ".$j."</h3><br>\n";
                //if($j>=1) break;
                //$output.= '</div>'."\n"; //3
            }
        }
        // XB
        if ($canEdit) {
            $output .=
                    '<div id="jr_listingFormValidation" class="jr_validation"></div>'
                            . "\n";
            $output .=
                    '<input id="jr_submitListing" class="jrButton" type="button" value="Submit" onclick="jreviews.listing.submit(this);">'
                            . "\n";
            $output .=
                    '<input id="jr_canceListing" class="jrButton" type="button" value="Cancel" onclick="history.back();">'
                            . "\n";
            $output .= '</div>' . "\n"; //0 //<!-- jr_newFields -->
            $output .=
                    '<input type="hidden" value="com_jreviews" name="option">'
                            . "\n";
            $output .=
                    '<input id="section" type="hidden" value="" name="data[section]">'
                            . "\n";
            $output .=
                    '<input id="parent_category" type="hidden" value="" name="data[parent_category]">'
                            . "\n";
            $output .=
                    '<input id="category" type="hidden" value="Dispensaries" name="data[category]">'
                            . "\n";
            $output .=
                    '<input id="listing_id" type="hidden" value="'
                            . $listing_id . '" name="data[Listing][id]">'
                            . "\n";
            $output .=
                    '<input type="hidden" value="raw" name="format">' . "\n";
            $output .=
                    '<input type="hidden" value="535" name="Itemid">' . "\n";
            $output .= cmsFramework::getTokenInput() . "\n";
            $output .=
                    $listing_id ? cmsFramework::formIntegrityToken(
                                    $listing['Listing'], $formTokenKeys)
                            : '' . "\n";
            $output .= '</div>' . "\n"; //2 //<!-- jr_form jr_formContainer -->
        }
        #$output.='<input id="jr_planFields" type="hidden" value="jr_email,jr_hoursofoperation,jr_phone,jr_web,jr_caviarhashhalf,jr_cottoncandybubblemelthalf,jr_gdpbubblemelthashhalf,jr_hashoilhalf,jr_honeycombwaxhalf,jr_jadekeifhalf,jr_lemonkeifhalf,jr_caviarhashfourth,jr_cottoncandybubblemeltfourth,jr_gdpbubblemelthashfourth,jr_hashoilfourth,jr_honeycombwaxfourth,jr_jadekeiffourth,jr_lemonkeiffourth,jr_caviarhasheight,jr_cottoncandybubblemelteight,jr_gdpbubblemelthasheight,jr_hashoileight,jr_honeycombwaxeight,jr_jadekeifeight,jr_lemonkeifeight,jr_caviarhashgrams,jr_co...kherereight,jr_lambsbreatheight,jr_mauiewowieeight,jr_missincredibleeight,jr_sweettootheight,jr_bluedreamgrams,jr_calihazegrams,jr_drwhograms,jr_hawaiinsnowgrams,jr_jackherergrams,jr_lambsbreathgrams,jr_mauiewowiegrams,jr_missincrediblegrams,jr_sweettoothgrams,jr_bluedreamoz,jr_calihazeoz,jr_drwhooz,jr_hawaiinsnowoz,jr_jackhereroz,jr_lambsbreathoz,jr_mauiewowieoz,jr_missincredibleoz,jr_sweettoothoz,jr_tincturehalf,jr_tincturefourth,jr_tinctureeight,jr_tincture,jr_tincturegrams,jr_tinctureoz,jr_gm_distance" name="data[Paid][fields]">'."\n";
        #$output.='<input id="jr_planImages" type="hidden" value="6" name="data[Paid][images]">'."\n";
        if ($canEdit) {
            $output .= '</form>' . "\n";
            $output .= '</div>' . "\n"; //1 //<!-- jr_listingFormOuter -->
            $output .= '</div>' . "\n"; //0 //<!-- jr_customFields -->
        }
        $output .= '</div>' . "\n"; //<!-- community-wrap -->
        return $output;
    }
    /**
     * Returns true if there's a date field. Used to check whether datepicker library is loaded
     *
     * @param array $fields
     * @return boolean
     */
    function findDateField($fields)
    {
        if (!empty($fields)) {
            foreach ($fields AS $group => $group_fields) {
                foreach ($group_fields['Fields'] AS $field) {
                    if ($field['type'] == 'date') {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    function label($name, &$entry)
    {
        if (empty($entry['Field']) || !isset($entry['Field']['pairs'][$name])) {
            return null;
        }
        return $entry['Field']['pairs'][$name]['title'];
    }
    function isMultipleOption($name, $element)
    {
        if (isset($element['Field']['pairs'][$name])
                && in_array($element['Field']['pairs'][$name]['type'],
                        $this->multipleOptionTypes)) {
            return true;
        }
        return false;
    }
    function onDisplay(&$field, $showImage = true, $value = false,
            $return = false)
    {
        if (empty($field)) {
            return null;
        }
        $values = array();
        $option = $value ? 'value' : 'text';
        foreach ($field[$option] AS $key => $text) {
            switch ($field['type']) {
                case 'banner':
                    $text = '{FIELDTEXT}';
                    $field['properties']['output_format'] =
                            Sanitize::getString($field, 'description');
                    $field['description'] == '';
                    break;
                case 'date':
                    $format =
                            Sanitize::getString($field['properties'],
                                    'date_format');
                    $text = $this->Time->nice($text, $format, 0);
                    break;
                case 'integer':
                    $text =
                            Sanitize::getInt($field['properties'],
                                    'curr_format') ? number_format($text, 0,
                                            __l('DECIMAL_SEPARATOR', true),
                                            __l('THOUSANDS_SEPARATOR', true))
                                    : $text;
                    break;
                case 'decimal':
                    $text =
                            Sanitize::getInt($field['properties'],
                                    'curr_format') ? number_format($text, 2,
                                            __l('DECIMAL_SEPARATOR', true),
                                            __l('THOUSANDS_SEPARATOR', true))
                                    : round($text, 2);
                    break;
                case 'email':
                    break;
                case 'website':
                    $text = S2ampReplace($text);
                    !strstr($text, '://') and $text = 'http://' . $text;
                    break;
                case 'code':
                    $text = stripslashes($text);
                    break;
                case 'textarea':
                case 'text':
                    if (!Sanitize::getBool($field['properties'], 'allow_html')) {
                        $text = nl2br($text);
                    }
                    break;
                case 'selectmultiple':
                case 'checkboxes':
                case 'select':
                case 'radiobuttons':
                    $imgSrc = '';
                    if ($showImage && isset($field['image'][$key])
                            && $field['image'][$key] != '') // Image assigned to this option
 {
                        if ($imgSrc =
                                $this
                                        ->locateThemeFile('theme_images',
                                                cmsFramework::locale() . '.'
                                                        . $field['image'][$key],
                                                '', true)) {
                            $imgSrc = pathToUrl($imgSrc, true);
                        }
                        elseif ($imgSrc =
                                $this
                                        ->locateThemeFile('theme_images',
                                                $field['image'][$key], '',
                                                true)) {
                            $imgSrc = pathToUrl($imgSrc, true);
                        }
                        if ($imgSrc != '') {
                            $text =
                                    '<img src="' . $imgSrc . '" title="'
                                            . $text . '" alt="' . $text
                                            . '" border="0" />';
                        }
                    }
                    break;
                default:
                    $text = stripslashes($text);
                    break;
            }
            $values[] = $text;
            $this->output[] = $text;
        }
        if ($return) {
            return $values;
        }
    }
    function click2Search($field, $criteriaid, $catid, $Itemid)
    {
        if (isset($field['properties']['click2search'])) {
            $Itemid = $Itemid ? $Itemid : '';
            if (isset($field['properties']['click2searchlink'])
                    && $field['properties']['click2searchlink'] != '') {
                $click2searchlink = $field['properties']['click2searchlink'];
            }
            else {
                $click2searchlink =
                        'index.php?option='
                                . S2Paths::get('jreviews', 'S2_CMSCOMP')
                                . '&amp;Itemid={ITEMID}&amp;url=tag/{FIELDNAME}/{FIELDTEXT}/criteria'
                                . _PARAM_CHAR . '{CRITERIAID}';
            }
            foreach ($this->output AS $key => $text) {
                if ($field['type'] == 'date') {
                    $field['value'][$key] =
                            str_ireplace(' 00:00:00', '', $field['value'][$key]);
                }
                $url = $click2searchlink;
                if ($Itemid > 0) {
                    $url = str_ireplace('{ITEMID}', $Itemid, $url);
                }
                else {
                    $url =
                            str_ireplace(
                                    array('_m{ITEMID}', '&Itemid={ITEMID}'),
                                    '', $url);
                }
                $url =
                        str_ireplace(
                                array('{FIELDNAME}', '{FIELDTEXT}',
                                        '{CRITERIAID}', '{CATID}'),
                                array(substr($field['name'], 3),
                                        urlencode($field['value'][$key]),
                                        urlencode($criteriaid),
                                        urlencode($catid)), $url);
                $url = s2ampReplace($url);
                $url = cmsFramework::route($url);
                $this->output[$key] = "<a href=\"$url\">$text</a>";
            }
        }
    }
    function outputReformat($name, &$fields, $element = array(),
            $return = false)
    {
        $field_names = array_keys($fields);
        // Listing vars
        $title =
                isset($element['Listing']) ? $element['Listing']['title'] : '';
        $category =
                isset($element['Listing']) && isset($element['Category']) ? Sanitize::getString(
                                $element['Category'], 'title') : '';
        $section =
                isset($element['Listing']) && isset($element['Section']) ? Sanitize::getString(
                                $element['Section'], 'title') : '';
        // Check if there's anything to do
        if ((isset($fields[$name]['properties']['output_format'])
                && trim($fields[$name]['properties']['output_format'])
                        != '{FIELDTEXT}') || $fields[$name]['type'] == 'banner') {
            $format = $fields[$name]['properties']['output_format'];
            // Remove any references to current field in the output format to avoid an infinite loop
            $format = str_ireplace('{' . $name . '}', '{fieldtext}', $format);
            $curr_value = '';
            // Find all custom field tags to replace in the output format
            $matches = array();
            $regex = '/(jr_[a-z]{1,}\|value)|(jr_[a-z]{1,})/i';
            preg_match_all($regex, $format, $matches);
            $matches = $matches[0];
            // Loop through each field and make output format {tag} replacements
            foreach ($this->output AS $key => $text) {
                $text = str_ireplace('{fieldtext}', $text, $format);
                $text =
                        str_ireplace('{fieldtitle}', $fields[$name]['title'],
                                $text);
                !empty($title)
                        and $text = str_ireplace('{title}', $title, $text);
                !empty($category)
                        and $text =
                                str_ireplace('{category}', $category, $text);
                !empty($section)
                        and $text =
                                str_ireplace('{section}', $section, $text);
                strstr(strtolower($text), '{optionvalue}')
                        and $text =
                                str_ireplace('{optionvalue}',
                                        $fields[$name]['value'][$key], $text);
                // Quick check to see if there are custom fields to replace
                if (empty($matches)) {
                    $this->output[$key] = $text;
                }
                foreach ($matches AS $curr_key) {
                    $backupOutput = $this->output;
                    $this->output = array();
                    $parts = explode('|', $curr_key);
                    $fname = $parts[0];
                    $value_only =
                            isset($parts[1])
                                    && strtolower($parts[1]) == 'value';
                    $curr_text =
                            $this
                                    ->field($fname, $element, !$value_only,
                                            !$value_only); //stripslashes($fields[strtolower($curr_key)]['text'][0]);
                    $this->output = $backupOutput;
                    $text =
                            str_ireplace('{' . $curr_key . '}', $curr_text,
                                    $text);
                }
                $this->output[$key] = $text;
            }
        }
    }
    /**
     * Dynamic form creation for custom fields with default layout
     *
     * @param unknown_type $formFields
     * @param unknown_type $fieldLocation
     * @param unknown_type $search
     * @param unknown_type $selectLabel
     * @return unknown
     */
    function makeFormFields(&$formFields, $fieldLocation, $search = null,
            $selectLabel = 'Select')
    {
        if (!is_array($formFields)) {
            return '';
        }
        $groupSet = array();
        $fieldLocation = Inflector::camelize($fieldLocation);
        foreach ($formFields AS $group => $fields) {
            $inputs = array();
            $group_name =
                    'group_' . str_replace(' ', '', $fields['group_name']);
            foreach ($fields['Fields'] AS $key => $value) {
                if ((!$search
                        && $this->Access
                                ->in_groups($value['properties']['access']))
                        || ($search
                                && $this->Access
                                        ->in_groups(
                                                $value['properties']['access_view']))) {
                    $autoComplete = false;
                    if ($value['type'] == 'banner')
                        continue;
                    if ($search && $this->Config->search_field_conversion
                            && Sanitize::getInt($value['properties'],
                                    'autocomplete') == 0
                            && isset($value['optionList'])
                            && !empty($value['optionList'])) {
                        switch ($value['type']) {
                            case 'radiobuttons':
                                $value['type'] = 'checkboxes';
                                break;
                            case 'select':
                                $value['type'] = 'selectmultiple';
                                break;
                        }
                    }
                    $inputs["data[Field][$fieldLocation][$key]"] =
                            array('id' => $value['name'],
                                    'type' => $this->types[$value['type']]);
                    // Check for AutoCompleteUI
                    if ((!$search
                            && Sanitize::getString($value['properties'],
                                    'autocomplete') == 1)
                            || ($search
                                    && Sanitize::getString(
                                            $value['properties'],
                                            'autocomplete.search') == 1)) {
                        $autoComplete = true;
                        $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                'jrAutoComplete';
                        $inputs["data[Field][$fieldLocation][$key]"]['data-field'] =
                                htmlentities(
                                        json_encode(
                                                array(
                                                        'name' => $value['name'],
                                                        'id' => $value['field_id'])),
                                        ENT_QUOTES, 'utf-8');
                    }
                    !$search
                            and $inputs["data[Field][$fieldLocation][$key]"]['data-click2add'] =
                                    Sanitize::getInt($value['properties'],
                                            'click2add');
                    //  Assign field classes and other field type specific changes
                    switch ($value['type']) {
                        case 'decimal':
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                    'jrDecimal';
                            break;
                        case 'integer':
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                    'jrInteger';
                            break;
                        case 'code':
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                    'jrCode';
                            break;
                        case 'website':
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                    'jrWebsite';
                            break;
                        case 'email':
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                    'jrEmail';
                            break;
                        case 'text':
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                    'jrText';
                            break;
                        case 'relatedlisting':
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                    'jrRelatedListing';
                            $inputs["data[Field][$fieldLocation][$key]"]['data-listingtype'] =
                                    Sanitize::getString($value['properties'],
                                            'listing_type');
                            break;
                        case 'textarea':
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                    'jrTextArea';
                            break;
                        case 'select':
                            if (isset(
                                    $inputs["data[Field][$fieldLocation][$key]"]['class'])) {
                                $inputs["data[Field][$fieldLocation][$key]"]['class'] .=
                                        ' jrSelect';
                            }
                            else {
                                $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                        'jrSelect';
                            }
                            break;
                        case 'selectmultiple':
                            if (isset(
                                    $inputs["data[Field][$fieldLocation][$key]"]['class'])) {
                                $inputs["data[Field][$fieldLocation][$key]"]['class'] .=
                                        ' jrSelectMultiple';
                            }
                            else {
                                $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                        'jrSelectMultiple';
                            }
                            break;
                        case 'date':
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                    'jrDate';
                            $yearRange =
                                    Sanitize::getString($value['properties'],
                                            'year_range');
                            $inputs["data[Field][$fieldLocation][$key]"]['data-yearrange'] =
                                    $yearRange != '' ? $yearRange : 'c-10:c+10';
                            break;
                    }
                    $inputs["data[Field][$fieldLocation][$key]"]['label']['text'] =
                            $value['title'];
                    $inputs["data[Field][$fieldLocation][$key]"]['label']['class'] =
                            "jrLabel";
                    # Add tooltip
                    if (!$search
                            && Sanitize::getString($value, 'description', null)) {
                        switch (Sanitize::getInt($value['properties'],
                                'description_position')) {
                            case 0:
                            case 1:
                                $inputs["data[Field][$fieldLocation][$key]"]['label']['text'] .=
                                        '<span class="jr_infoTip">&nbsp;</span><div class="jr_tooltipBox">'
                                                . $value['description']
                                                . '</div>';
                                break;
                            case 2:
                                $inputs["data[Field][$fieldLocation][$key]"]['between'] =
                                        '<div class="jrFieldDescription">'
                                                . $value['description']
                                                . '</div>';
                                break;
                            case 3:
                                $inputs["data[Field][$fieldLocation][$key]"]['after'] =
                                        '<div class="jrFieldDescription">'
                                                . $value['description']
                                                . '</div>';
                                break;
                        }
                    }
                    if (!$search && $value['required']) {
                        $inputs["data[Field][$fieldLocation][$key]"]['label']['text'] .=
                                '<span class="required">' . __t("*", true)
                                        . '</span>';
                    }
                    if (in_array($value['type'], $this->multipleTypes)) {
                        $inputs["data[Field][$fieldLocation][$key]"]['multiple'] =
                                'multiple';
                    }
                    if (isset($value['optionList'])
                            && $value['type'] == 'select') {
                        $value['optionList'] =
                                array('' => $selectLabel)
                                        + $value['optionList'];
                    }
                    if (isset($value['optionList'])) {
                        $inputs["data[Field][$fieldLocation][$key]"]['options'] =
                                $value['optionList'];
                    }
                    # Add click2add capability for select lists
                    if (!$autoComplete && $fieldLocation == 'Listing'
                            && !$search
                            && $this->types[$value['type']] == 'select'
                            && $value['properties']['click2add']) {
                        $inputs["data[Field][$fieldLocation][$key]"]['style'] =
                                'float:left;';
                        $click2AddLink =
                                $this->Form
                                        ->button(__t("Add", true),
                                                array('style' => 'float:left',
                                                        'class' => 'jrButton',
                                                        'onclick' => "jQuery('#click2Add_{$value['field_id']}').toggle('fast');"));
                        $click2AddInput =
                                $this->Form
                                        ->text(
                                                'jrFieldOption'
                                                        . $value['field_id'],
                                                array(
                                                        'id' => 'jrFieldOption'
                                                                . $value['field_id'],
                                                        'class' => 'jrFieldOptionInput',
                                                        'data-fid' => $value['field_id'],
                                                        'data-fname' => $value['name']));
                        $click2AddButton =
                                $this->Form
                                        ->button(__t("Submit", true),
                                                array(
                                                        'onclick' => "jreviews.field.addOption(this);",
                                                        'div' => false,
                                                        'id' => 'submitButton'
                                                                . $value['field_id'],
                                                        'class' => 'jrButton'));
                        $inputs["data[Field][$fieldLocation][$key]"]['after'] =
                                $click2AddLink
                                        . "<div id='click2Add_{$value['field_id']}' class='jr_newFieldOption'>"
                                        . $click2AddInput . ' '
                                        . $click2AddButton
                                        . "<span class=\"jr_loadingSmall jr_hidden\"></span>"
                                        . '</div>';
                    }
                    # Prefill values when editing                 
                    if (isset($value['selected'])) {
                        $inputs["data[Field][$fieldLocation][$key]"]['value'] =
                                $value['selected'];
                    }
                    # Add search operator fields for date, decimal and integer fields
                    if ($search
                            && in_array($value['type'], $this->operatorTypes)) {
                        $options =
                                array('equal' => '=', 'higher' => '&gt;=',
                                        'lower' => '&lt;=',
                                        'between' => __t("between", true));
                        $inputs["data[Field][$fieldLocation][$key]"]['multiple'] =
                                true; // convert field to array input for range searches                        
                        $attributes =
                                array('id' => $key . 'high', 'multiple' => true);
                        switch ($value['type']) {
                            case 'integer':
                                $attributes['class'] = 'jrInteger';
                                break;
                            case 'decimal':
                                $attributes['class'] = 'jrDecimal';
                                break;
                            case 'date':
                                $attributes['class'] = 'jrDate';
                                break;
                        }
                        // This is the high value input in a range search
                        $inputs["data[Field][$fieldLocation][$key]"]['after'] =
                                '<span id="' . $key
                                        . 'highDiv" style="display:none;">&nbsp;'
                                        . $this->Form
                                                ->text(
                                                        "data[Field][Listing][{$key}]",
                                                        $attributes)
                                        . '</span>';
                        $inputs["data[Field][$fieldLocation][$key]"]['between'] =
                                $this->Form
                                        ->select(
                                                "data[Field][Listing][{$key}_operator]",
                                                $options, null,
                                                array(
                                                        'class' => 'jrSearchOptions input',
                                                        'onchange' => "jreviews.search.showRange(this,'{$key}high');"));
                    }
                    # Input styling
                    $inputs["data[Field][$fieldLocation][$key]"]['div'] =
                            'jr_fieldDiv ' . $value['name'];
                    if (in_array($this->types[$value['type']],
                            $this->legendTypes)) {
                        // Input styling
                        $inputs["data[Field][$fieldLocation][$key]"]['option_class'] =
                                'jr_fieldOption';
                        $inputs["data[Field][$fieldLocation][$key]"]['after'] =
                                $this->Html->div('clr', ' '); // To break the float                    
                    }
                } // end access check
            } // end foreach
            if (!empty($inputs)) {
                $groupSet[$group_name] =
                        array('fieldset' => true, 'legend' => $group);
                foreach ($inputs AS $dataKey => $dataValue) {
                    $groupSet[$group_name][$dataKey] = $dataValue;
                }
            }
        }
        $output = '';
        foreach ($groupSet AS $group => $form) {
            $output .=
                    $this->Form
                            ->inputs($form,
                                    array('id' => $group,
                                            'class' => 'jr_hidden'));
        }
        return $output;
    }
    /**
     * Dynamic form creation for custom fields using custom layout - {field tags} in view file
     *
     * @param unknown_type $formFields
     * @param unknown_type $fieldLocation
     * @param unknown_type $search
     * @param unknown_type $selectLabel
     * @return array of form inputs for each field
     */
    function getFormFields(&$formFields, $fieldLocation = 'listing',
            $search = null, $selectLabel = 'Select')
    {
        if (!is_array($formFields)) {
            return '';
        }
        $groupSet = array();
        $fieldLocation = Inflector::camelize($fieldLocation);
        foreach ($formFields AS $group => $fields) {
            $inputs = array();
            foreach ($fields['Fields'] AS $key => $value) {
                $autoComplete = false;
                // Convert radio button to checkbox if multiple search is enabled in the config settings
                if ($search && $this->Config->search_field_conversion
                        && $value['type'] == 'radiobuttons') {
                    $value['type'] = 'checkboxes';
                }
                $inputs["data[Field][$fieldLocation][$key]"] =
                        array('id' => $value['name'],
                                'type' => $this->types[$value['type']]);
                // Check for AutoCompleteUI
                if ((!$search
                        && Sanitize::getString($value['properties'],
                                'autocomplete') == 1)
                        || ($search
                                && Sanitize::getString($value['properties'],
                                        'autocomplete.search') == 1)) {
                    $autoComplete = true;
                    $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                            'jrAutoComplete';
                    $inputs["data[Field][$fieldLocation][$key]"]['data-field'] =
                            htmlentities(
                                    json_encode(
                                            array('name' => $value['name'],
                                                    'id' => $value['field_id'])),
                                    ENT_QUOTES, 'utf-8');
                }
                $inputs["data[Field][$fieldLocation][$key]"]['div'] = array();
                # Add tooltip
                if (!$search
                        && Sanitize::getString($value, 'description', null)) {
                    switch (Sanitize::getInt($value['properties'],
                            'description_position')) {
                        case 0:
                        case 1:
                            $inputs["data[Field][$fieldLocation][$key]"]['label']['text'] .=
                                    '<span class="jr_infoTip">&nbsp;</span><div class="jr_tooltipBox">'
                                            . $value['description'] . '</div>';
                            break;
                        case 2:
                            $inputs["data[Field][$fieldLocation][$key]"]['between'] =
                                    '<div class="jrFieldDescription">'
                                            . $value['description'] . '</div>';
                            break;
                        case 3:
                            $inputs["data[Field][$fieldLocation][$key]"]['after'] =
                                    '<div class="jrFieldDescription">'
                                            . $value['description'] . '</div>';
                            break;
                    }
                }
                //  Assign field classes and other field type specific changes
                switch ($value['type']) {
                    case 'decimal':
                        $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                'jrDecimal';
                        break;
                    case 'integer':
                        $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                'jrInteger';
                        break;
                    case 'code':
                        $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                'jrCode';
                        break;
                    case 'website':
                        $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                'jrWebsite';
                        break;
                    case 'email':
                        $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                'jrEmail';
                        break;
                    case 'text':
                        $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                'jrText';
                        break;
                    case 'relatedlisting':
                        $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                'jrRelatedListing';
                        $inputs["data[Field][$fieldLocation][$key]"]['data-listingtype'] =
                                Sanitize::getString($value['properties'],
                                        'listing_type');
                        break;
                    case 'textarea':
                        $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                'jrTextArea';
                        break;
                    case 'select':
                        if (isset(
                                $inputs["data[Field][$fieldLocation][$key]"]['class'])) {
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] .=
                                    ' jrSelect';
                        }
                        else {
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                    'jrSelect';
                        }
                        break;
                    case 'selectmultiple':
                        if (isset(
                                $inputs["data[Field][$fieldLocation][$key]"]['class'])) {
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] .=
                                    ' jrSelectMultiple';
                        }
                        else {
                            $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                    'jrSelectMultiple';
                        }
                        break;
                    case 'date':
                        $inputs["data[Field][$fieldLocation][$key]"]['class'] =
                                'jrDate';
                        $inputs["data[Field][$fieldLocation][$key]"]['readonly'] =
                                'readonly';
                        $yearRange =
                                Sanitize::getString($value['properties'],
                                        'year_range');
                        $inputs["data[Field][$fieldLocation][$key]"]['data-yearrange'] =
                                $yearRange != '' ? $yearRange : 'c-10:c+10';
                        break;
                }
                if (in_array($value['type'], $this->multipleTypes)) {
                    $inputs["data[Field][$fieldLocation][$key]"]['multiple'] =
                            'multiple';
                    if (($size =
                            Sanitize::getInt($value['properties'], 'size'))) {
                        $inputs["data[Field][$fieldLocation][$key]"]['size'] =
                                $size;
                    }
                }
                if (isset($value['optionList']) && $value['type'] == 'select') {
                    $value['optionList'] =
                            array('' => $selectLabel) + $value['optionList'];
                }
                if (isset($value['optionList'])) {
                    $inputs["data[Field][$fieldLocation][$key]"]['options'] =
                            $value['optionList'];
                }
                # Add click2add capability for select lists
                if (!$autoComplete && !$search && $fieldLocation == 'Listing'
                        && $this->types[$value['type']] == 'select'
                        && $value['properties']['click2add']) {
                    $inputs["data[Field][$fieldLocation][$key]"]['style'] =
                            'float:left;';
                    $click2AddLink =
                            $this->Form
                                    ->button(__t("Add", true),
                                            array('style' => 'float:left',
                                                    'class' => 'jrButton',
                                                    'onclick' => "jQuery('#click2Add_{$value['field_id']}').toggle('fast');"));
                    $click2AddInput =
                            $this->Form
                                    ->text(
                                            'jrFieldOption'
                                                    . $value['field_id'],
                                            array(
                                                    'id' => 'jrFieldOption'
                                                            . $value['field_id'],
                                                    'class' => 'jrFieldOptionInput',
                                                    'data-fid' => $value['field_id'],
                                                    'data-fname' => $value['name']));
                    $click2AddButton =
                            $this->Form
                                    ->button(__t("Submit", true),
                                            array(
                                                    'onclick' => "submitOption({$value['field_id']},'{$value['name']}');",
                                                    'div' => false,
                                                    'id' => 'submitButton'
                                                            . $value['field_id'],
                                                    'class' => 'jrButton'));
                    $inputs["data[Field][$fieldLocation][$key]"]['after'] =
                            $click2AddLink
                                    . "<div id='click2Add_{$value['field_id']}' class='jr_newFieldOption'>"
                                    . $click2AddInput . ' ' . $click2AddButton
                                    . '</div>';
                }
                # Prefill values when editing
                if (isset($value['selected'])) {
                    if (in_array($value['type'], $this->operatorTypes)
                            && $value['selected'][0] == 'between') {
                        $inputs["data[Field][$fieldLocation][$key]"]['value'] =
                                $value['selected'][2];
                    }
                    else {
                        $inputs["data[Field][$fieldLocation][$key]"]['value'] =
                                $value['selected'];
                        $inputs["data[Field][$fieldLocation][$key]"]['data-selected'] =
                                implode('_', $value['selected']);
                    }
                }
                # Add search operator fields for date, decimal and integer fields
                if ($search && in_array($value['type'], $this->operatorTypes)) {
                    $options =
                            array('equal' => '=', 'higher' => '&gt;=',
                                    'lower' => '&lt;=',
                                    'between' => __t("between", true));
                    $inputs["data[Field][$fieldLocation][$key]"]['multiple'] =
                            true; // convert field to array input for range searches                        
                    $attributes =
                            array('id' => $key . 'high', 'multiple' => true);
                    switch ($value['type']) {
                        case 'integer':
                            $attributes['class'] = 'jrInteger';
                            break;
                        case 'decimal':
                            $attributes['class'] = 'jrDecimal';
                            break;
                        case 'date':
                            $attributes['class'] = 'jrDate';
                            break;
                    }
                    $showHighRange =
                            isset($value['selected'])
                                    && is_array($value['selected'])
                                    && $value['selected'][0] == 'between';
                    $showHighRange
                            and $attributes['value'] = $value['selected'][3];
                    $showHighRangeStyle =
                            $showHighRange ? '' : ' style="display:none;"';
                    $selectedOperator =
                            isset($value['selected'][0]) ? $value['selected'][0]
                                    : '';
                    // This is the high value input in a range search
                    $inputs["data[Field][$fieldLocation][$key]"]['after'] =
                            '<span id="' . $key . 'highDiv" '
                                    . $showHighRangeStyle . '>&nbsp;'
                                    . $this->Form
                                            ->text(
                                                    "data[Field][Listing][{$key}]",
                                                    $attributes) . '</span>';
                    $inputs["data[Field][$fieldLocation][$key]"]['between'] =
                            $this->Form
                                    ->select(
                                            "data[Field][Listing][{$key}_operator]",
                                            $options, $selectedOperator,
                                            array(
                                                    'class' => 'jrSearchOptions input',
                                                    'onchange' => "jreviews.search.showRange(this,'{$key}high');"));
                }
                if (in_array($this->types[$value['type']], $this->legendTypes)) {
                    // Input styling
                    $inputs["data[Field][$fieldLocation][$key]"]['option_class'] =
                            'jr_fieldOption';
                    $inputs["data[Field][$fieldLocation][$key]"]['after'] =
                            $this->Html->div('clr', ' '); // To break the float                    
                }
            }
            $groupSet[$group] = array('fieldset' => false, 'legend' => false);
            foreach ($inputs AS $dataKey => $dataValue) {
                $groupSet[$group][$dataKey] = $dataValue;
            }
        }
        $output = array();
        foreach ($groupSet AS $group => $form) {
            $output =
                    array_merge($output,
                            $this->Form->inputs($form, null, null, true));
        }
        return $output;
    }
}
//        return $this->Form->inputs
//            (
//                array(
//                    'fieldset'=>true,
//                    'legend'=>'Group XYZ',
//                    'data[Field][jr_text]'=>
//                    array(
//                        'label'=>array('for'=>'jr_text','text'=>'Text Field'),
//                        'id'=>'jr_text',
//                        'type'=>'text',
//                        'size'=>'10',
//                        'maxlength'=>'100',
//                        'class'=>'{required:true}'
//                    ),
//                    'data[Field][jr_select]'=>
//                    array(
//                        'label'=>array('for'=>'select','text'=>'Select Field'),
//                        'id'=>'select',
//                        'type'=>'select',
//                        'options'=>array('1'=>'1','2'=>'2'),
//                        'selected'=>2
//                    ),
//                    'data[Field][jr_selectmultiple]'=>
//                    array(
//                        'label'=>array('for'=>'selectmultiple','text'=>'Multiple Select Field'),
//                        'id'=>'selectmultiple',
//                        'type'=>'select',
//                        'multiple'=>'multiple',
//                        'size'=>'2',
//                        'options'=>array('1'=>'email','2'=>'asdfasdf'),
//                        'value'=>array(1,2)
//                    ),
//                    'data[Field][jr_checkbox]'=>
//                    array(
//                        'label'=>false,
//                        'legend'=>'Checkboxes',
//                        'type'=>'checkbox',
//                        'options'=>array('1'=>'Option 1','2'=>'Option 2'),
//                        'value'=>array(2),
//                        'class'=>'{required:true,minLength:2}'
//                    ),
//                    'data[Field][jr_radio]'=>
//                    array(
//                        'legend'=>'Radio Buttons',
//                        'type'=>'radio',
//                        'options'=>array('1'=>'Option 1','2'=>'Option 2'),
//                        'value'=>1,
//                        'class'=>'{required:true}'
//                    )        
//                    
//                )    
//            );        
if (isset($debug) && $debug >= 1)
    echo "<!-- custom_fields.php -->\n";
        ?>
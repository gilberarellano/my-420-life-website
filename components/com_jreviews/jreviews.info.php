<?php
defined( 'MVC_FRAMEWORK') or die( 'Direct Access to this location is not allowed.' );

$package_info['jreviews'] = array
(
    'version'=>'2.3.16.210',                   
    'min_s2_version_required'=>'1.4.12.70',
    'is_beta'=>0 
);
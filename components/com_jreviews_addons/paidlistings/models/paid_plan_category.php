<?php
/**
 * PaidListings Addon for JReviews
 * Copyright (C) 2010 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
**/
defined( 'MVC_FRAMEWORK') or die( 'Direct Access to this location is not allowed.' );

class PaidPlanCategoryModel extends MyModel  {
        
    var $name = 'PaidPlanCategory';
    
    var $useTable = '#__jreviews_paid_plans_categories AS `PaidPlanCategory`';
    
    var $primaryKey = 'PaidPlanCategory.plan_id';
    
    var $realKey = 'plan_id';
    
    var $fields = array('PaidPlanCategory.*');
    
    function isInPaidCategoryByListingId($listing_id) {
        $query = "
            SELECT 
                count(*) 
            FROM
                #__jreviews_paid_plans_categories AS Category
            LEFT JOIN
                #__jreviews_paid_plans AS Plan ON Category.plan_id = Plan.plan_id
            WHERE
                cat_id = (
                    SELECT catid FROM #__content WHERE id = " . $listing_id . "
                )
                AND
                plan_state = 1
            ";
                         
        $this->_db->setQuery($query);
        $plan_cats = $this->_db->loadResult();
        return !empty($plan_cats);
    }
    
    function isInPaidCategory($cat_id)
    {
        $query = "
            SELECT 
                count(*) 
            FROM
                #__jreviews_paid_plans_categories AS Category
            LEFT JOIN
                #__jreviews_paid_plans AS Plan ON Category.plan_id = Plan.plan_id
            WHERE
                cat_id = " . (int) $cat_id . "
                AND
                plan_state = 1
            ";
                         
        $this->_db->setQuery($query);
        $plan_cats = $this->_db->loadResult();
        return !empty($plan_cats);
    }
        
    function afterFind($results)
    {       
        if(isset($results['PaidPlanCategory']['plan_id']))
        {
            $results = array(
                'PaidPlanCategory'=>array('cat_id'=>$results['PaidPlanCategory']['cat_id']),
                'PaidPlanIdCats'=>array($results['PaidPlanCategory']['plan_id']=>array($results['PaidPlanCategory']['cat_id']))
                );
        } else {
            $cat_ids = array();
            foreach($results AS $key=>$result)
            {
                if(isset($result['PaidPlanCategory']['plan_id']))
                {
                    $cat_ids[] = $result['PaidPlanCategory']['cat_id'];
                    $planid_cats[$result['PaidPlanCategory']['plan_id']][] = $result['PaidPlanCategory']['cat_id'];
                }
            }
            $results = array(
                'PaidPlanCategory'=>array('cat_id'=>$cat_ids),
                'PaidPlanIdCats'=>$planid_cats
                );
        }                                                     

        return $results;
    }
}

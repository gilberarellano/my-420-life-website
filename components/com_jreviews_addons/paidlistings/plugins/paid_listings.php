<?php
/**
 * PaidListings Addon for JReviews
 * Copyright (C) 2010 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
**/
defined( 'MVC_FRAMEWORK') or die( 'Direct Access to this location is not allowed.' );

class PaidListingsComponent extends S2Component 
{
    var $plugin_order = 1;
    
    var $name = 'paid_listings';
    
    /**
    * Changed dynamically in startup method to restrict the plugin's callbacks to certain controller actions
    */
    var $published = false; 
    
    var $order = 0; 
    
//    var $validObserverModels = array('Listing');
    /**
    * Define where plugin should run
    */
    var $controllerActions = array(
        'com_content'=>'com_content_view',
        'module_listings'=>'index',
        'module_directories'=>'index', // for plans page
        'module_geomaps'=>'listings',
//        'module_reviews'=>'index', Processed directly in the Everywhere integration file for com_content
        'listings'=>array('_loadForm','_save','edit','create','_delete'), 
        'categories'=>array(
            'mylistings',
            'alphaindex',
            'section',
            'category',
            'favorites',
            'featured',
            'latest',    
            'mostreviews',    
            'toprated',
            'topratededitor',
            'popular',    
            'featuredrandom',    
            'random',
            'search'             
        ),
        'fields'=>'_loadFieldData',
        'paidlistings_orders'=>'_submit',
        'paidlistings_listings'=>'index',
        'admin/admin_paidlistings_orders'=>'_save',
        'admin_listings'=>array('index','edit','browse','moderation','_save','_saveModeration')
    );   
    
    function runPlugin(&$controller)
    {                           
        $this->c = &$controller;
        // Check if running in valid controller/actions
        if(!isset($this->controllerActions[$controller->name])){         
            return false;
        }

        App::import('Model',array('paid_plan','paid_plan_category','paid_order','paid_txn_log','paid_listing_field'),'jreviews');
        App::import('Helper','paid_routes','jreviews');
        
        $actions = !is_array($this->controllerActions[$controller->name]) ? array($this->controllerActions[$controller->name]) : $this->controllerActions[$controller->name];

        if(!in_array('all',$actions) && !in_array($controller->action,$actions)) {    
            return false;
        }
        return true;        
    }
        
    function startup(&$controller) 
    {              
        if(!$this->runPlugin($controller))
        {                             
            return false;
        } 
        elseif( 
            defined('MVC_FRAMEWORK_ADMIN')
            ||
            (isset($controller->Config) && Sanitize::getBool($controller->Config,'paid.stealth',true) && $controller->Access->isAdmin()) 
            ||
            isset($controller->Config) && !Sanitize::getBool($controller->Config,'paid.stealth',true)
        ) 
        {           
            Configure::write('PaidListings.enabled',true);
            $controller->helpers[] = 'paid';
            $controller->helpers[] = 'paid_routes';
            $this->published = true; // Enable the callbacks 
            # Trigger crons on list pages
            if($controller->name == 'categories' 
                || 
                $controller->name == 'com_content'
                ||
                ($controller->name == 'paidlistings' && $this->action == 'myaccount')
            )
            {       
                # Expiration notifications
                $expires1 = Sanitize::getInt($controller->Config,'paid.notify_expiration1');
                $expires2 = Sanitize::getInt($controller->Config,'paid.notify_expiration2');
                if($expires1 || $expires2)
                {                    
                    App::import('Component','paidlistings_notifications');
                    $PaidNotifications = new PaidlistingsNotificationsComponent();
                    $PaidNotifications->checkExpiringOrders($controller);
                }

                # Order expirations
                $this->processAllExpiredOrders();
                # Order renewals            
                $this->processOrderRenewals();
            }

            if(!defined('MVC_FRAMEWORK_ADMIN'))
            {          
                switch($controller->name)
                {                      
                    case 'listings':
                        $controller->action != 'delete' and $this->loadAssets();
                        if($controller->action == '_save')
                        {
                            if(isset($controller->data['Paid'])) 
                            {
                                $controller->Config->content_images_total_limit = 1; // Override for global/listing type setting for paid listings so image uploads always enforced
                                $controller->Config->content_images = Sanitize::getInt($controller->data['Paid'],'images');
                                $override_ban = Configure::read('JreviewsSystem.OverrideBan');
                                if(!is_array($override_ban)) 
                                {
                                    $override_ban = array('content_images');    
                                }
                                else {
                                    $override_ban[] = 'content_images';
                                }
                                Configure::write('JreviewsSystem.OverrideBan',$override_ban);
                            }
                        }
                    break;
                    case 'paidlistings_plans':
                    case 'com_content': 
                    case 'categories':
                    case 'module_geomaps':      
                        $this->applyBeforeFindListingChanges($controller->Listing);
                    break;
                    case 'module_listings':    
                        isset($controller->params['module']) and $extension = Sanitize::getString($controller->params['module'],'extension') or $extension = Sanitize::getString($controller->data,'extension','com_content');
                        $extension == 'com_content' and $this->applyBeforeFindListingChanges($controller->Listing);
                    break;
                    case 'module_directories':
                        strstr($controller->viewSuffix,'_plans') and $controller->Directory->conditions[] = "JreviewsCategory.id IN (SELECT cat_id FROM #__jreviews_paid_plans_categories)"; 
                    break;
                }
            } 
            else 
            {                                
                switch($controller->name)
                {
                    case 'admin_listings':
                        in_array($controller->action,array('index','moderation','browse','edit')) and $this->applyBeforeFindListingChanges($controller->Listing);
                    break;                      
                }                
            }
        }
    }
    
    function applyBeforeFindListingChanges(&$model)
    {          
        $db = cmsFramework::getDB();          

        $query = "
             DROP TEMPORARY TABLE IF EXISTS tmpPlanCategories;
        ";
        $db->setQuery($query);
        $db->query();
        $query = "
             CREATE TEMPORARY TABLE tmpPlanCategories (cat_id INT UNSIGNED NOT NULL PRIMARY KEY) ENGINE = MEMORY;
        ";
        $db->setQuery($query);
        $db->query();
        $query = "
            INSERT IGNORE INTO tmpPlanCategories
            SELECT PaidPlanCategory.cat_id
            FROM #__jreviews_paid_plans_categories AS PaidPlanCategory INNER JOIN #__jreviews_paid_plans AS PaidPlan ON PaidPlan.plan_id = PaidPlanCategory.plan_id WHERE PaidPlan.plan_state = 1;
        ";
        $db->setQuery($query);
        $db->query();
        
        !isset($model->fields['paid_plan_category']) and $model->fields['paid_plan_category'] = 'PaidPlanCategory.cat_id AS `PaidPlanCategory.cat_id`';
        !isset($model->joins['paid_plan']) and $model->joins['paid_plan'] = '
            LEFT JOIN tmpPlanCategories AS PaidPlanCategory ON Category.id = PaidPlanCategory.cat_id 
        '; 
    }
    
/************************************************************************
* CALLBACK METHODS 
************************************************************************/

/*    function plgAfterFind(&$model, $results)
    {
    }*/
 
    function plgAfterAfterFind(&$model, $results) 
    {                       
        if(empty($results))
        {
            return $results;
        }
        switch($this->c->name) 
        {   
            case (($this->c->name=='admin_listings' || $this->c->name=='listings') && defined('MVC_FRAMEWORK_ADMIN')):        
                if(in_array($this->c->action,array('index','moderation','browse','create')))
                {           
                    $PaidOrder = ClassRegistry::getClass('PaidOrderModel');
                    $results = $PaidOrder->completeOrderInfo($results, array() /* retrieves all orders for listing */);  
                }             
            break;
            case 'listings':
                $this->c->action == 'edit' and $this->processPaidData($results);            
            break;
            case 'paidlistings_listings':
            case 'module_listings':
            case 'module_geomaps':
            case 'com_content': 
                $this->processPaidData($results);
            break;
            case 'categories':
                switch($this->c->action)
                {
                    case 'alphaindex':
                    case 'section':
                    case 'category':
                    case 'favorites':
                    case 'featured':
                    case 'latest':    
                    case 'mostreviews':    
                    case 'toprated':
                    case 'topratededitor':
                    case 'popular':    
                    case 'featuredrandom':    
                    case 'random': 
                    case 'mylistings':
                    case 'search':
                        $this->processPaidData($results);
                    break; 
                    default:
                    break;
                }            
                break;
            break;  
        }

        return $results;
    }   
    
    function plgAfterDelete(&$model,$data)
    {                               
        $PaidListingField = ClassRegistry::getClass('PaidListingFieldModel');
        $data = array_shift($data);
        $listing_id = Sanitize::getInt($data['Listing'],'id');
        $PaidListingField->delete('listing_id',$listing_id);
    }

    function plgBeforeSave(&$model,$data)
    {     
        switch($this->c->name) 
        {   
            case 'admin_listings':
                if($this->c->action == '_save')
                {          
                    $PaidCategory = ClassRegistry::getClass('PaidPlanCategoryModel');
                    if($PaidCategory->isInPaidCategory($data['Listing']['catid']))
                    {
                        $data['paid_category'] = 1;  // read in fields model save method      
                    }
                }
            break;
            case 'listings':
                if($this->c->action == '_save')
                {          
                    $PaidCategory = ClassRegistry::getClass('PaidPlanCategoryModel');
                    if($PaidCategory->isInPaidCategory($data['Listing']['catid']))
                    {
                        $data['paid_category'] = 1;  // read in fields model save method      

                        // Make sure it's a new listing
                        if(!isset($data['Listing']['id']) or ($data['Listing']['id']==''))
                        {
                            if(isset($data['PaidOrder']) && $plan_id = Sanitize::getInt($data['PaidOrder'],'plan_id'))
                            {
                                $PaidPlan = ClassRegistry::getClass('PaidPlanModel');
                                $plan = $PaidPlan->findRow(array('conditions'=>'PaidPlan.plan_id = ' . $plan_id),array('afterFind'));
                                
                                // Apply plan moderation setting only if it's a free plan, otherwise we wait for payment
                                if($plan['PaidPlan']['payment_type'] == 2)
                                {
                                    $data['Listing']['state'] = !$plan['PaidPlan']['plan_array']['moderation'];
                                }
                                else
                                {
                                    $data['Listing']['state'] = 0;
                                }
                            }
                            else
                            {
                                $data['Listing']['state'] = 0;
                            }
                        }                            
                    }
                }
            break;
        }
        return $data;                
    }
    /**
    * Executed before rendering the theme file. 
    * All variables sent to theme are available in the $this->c->viewVars array and can be modified on the fly
    * 
    */
    function plgBeforeRender() 
    {                     
        if(($this->c->name == 'listings' && $this->c->action == 'edit')
            || ($this->c->name == 'listings' && defined('MVC_FRAMEWORK_ADMIN') && $this->c->action == 'create') )
        {          
            $listing = & $this->c->viewVars['listing'];
   
            if(isset($listing['Paid']))
            {    
                // Override global images settings
                $this->c->Config->content_images_edit = 1;

                $plans_json['plan0'] = array(  
                    'fields'=>$listing['Paid']['fields'],
                    'images'=>$listing['Paid']['images']
                );
                
                $plans_json = json_encode($plans_json);      
                
                echo $this->c->makeJS("var plans; jQuery(document).ready(function(){
                    plans = ".$plans_json.";                
                });"); /* Alejandro - removed JRPaid.Plans.submitFilter(0); because it's run in control.fields.js */
                // Override global images settings
                $this->c->Config->content_images = Sanitize::getInt($listing['Paid'],'images');
            }
        }           
          
        if($this->c->name == 'listings' && $this->c->action == 'create')
        {   
            if($plan_id = Sanitize::getInt($this->c->params,'plan_id'))
            {
                cmsFramework::addScript($this->c->makeJS("jQuery(document).ready(function(){
                    jQuery('#jr_listingForm').append('<input type=\"hidden\" name=\"data[submit_plan_id]\" value=\"{$plan_id}\" />');
                })"),true);
            }
        }
        
        if($this->c->name == 'listings' && $this->c->action == '_loadForm')
        {               
            $PlanModel = ClassRegistry::getClass('PaidPlanModel');
            $cat_id = Sanitize::getVar($this->c->data['Listing'],'catid');
            $level = (int) str_replace('cat_id','',Sanitize::getVar($this->c->data,'level'));
            if(is_array($cat_id)) 
            {
                $cat_id = array_filter($cat_id);
            
                switch($level) {
                    case 1:
                        $cat_id = (int) $cat_id[0];
                    break;
                    default:
                        $cat_id = (int) array_pop($cat_id);
                    break;
                }
            } 
            else {
                $cat_id = (int) $cat_id;
            }

            $plans = $PlanModel->getCatPlans($cat_id);
 
            $plans_json = $plans_json2 = array();
            if(empty($plans['PlanType0'])) {
                if(isset($plans['used_trials']) && $plans['used_trials'] > 0) {
                    $this->c->viewVars['paid_plans'] = array('used_trials'=>$plans['used_trials']); 
                } 
                return;  
            }
			
            // Check for default plan set via url
			$url_default_plan = Sanitize::getInt($this->c->data,'submit_plan_id');

			// Remove plans hidden in settings
            foreach($plans['PlanType0'] AS $payment_type=>$plan_rows)
            {
                foreach($plan_rows AS $plan_id=>$plan)
                {
                    if(!Sanitize::getInt($plan['plan_array'],'submit_form',1))
                    {
                        if((!$url_default_plan && $plan['plan_default']) || $plan_id == $url_default_plan) {
                            $plans['PlanDefault'] = $plan; 
                            $plans_json['plan'.$plan_id] = array(
                                'fields'=>$plan['plan_array']['fields'],
                                'images'=>$plan['plan_array']['images']
                            );
                                                    
                        }
                        unset($plans['PlanType0'][$payment_type][$plan_id]);
                        if(empty($plans['PlanType0'][$payment_type])) unset($plans['PlanType0'][$payment_type]);
                    } 
                    else
                    {
                        $plans_json['plan'.$plan_id] = array(
                            'fields'=>$plan['plan_array']['fields'],
                            'images'=>$plan['plan_array']['images']
                        );
                    }
                }        
            }

            if(!empty($plans_json))
            {                            
                $this->c->response[] = "var plans = " . json_encode($plans_json) . ';';    
//                $this->c->response[] = "jQuery('#jr_paidPlans').find('input:radio:checked').click();"; /* run in control.field.js
            }
             
            if(!empty($plans['PlanType0']) || !empty($plans['PlanDefault']))
            {
                $this->c->viewVars['paid_plans'] = $plans;    
            } 
        }
    }
    
    /**
    * Intercepts the response after all validation checks have completed
    * and custom fields and review have been saved.
    * Redirects the user to the orders page for payment options
    */
    function plgBeforeRenderListingSave(&$model)
    {             
        $PaidPlan = ClassRegistry::getClass('PaidPlanModel');
        $PaidOrder = ClassRegistry::getClass('PaidOrderModel');
        $PaidCategory = ClassRegistry::getClass('PaidPlanCategoryModel');
        
        $response = array();
        $cat_id = $model->data['Listing']['catid'];
        $plan_id = isset($model->data['PaidOrder']) ? Sanitize::getInt($model->data['PaidOrder'],'plan_id') : 0;
        $listing_id = $model->data['Listing']['id'];
        
        if($plan_id > 0 && $plan = $PaidPlan->findRow(array(
            'conditions'=>array(
                'PaidCategory.cat_id = ' . $cat_id,
//                'PaidPlan.plan_default = 1',
                'PaidPlan.plan_state = 1',
                'PaidPlan.plan_id = ' . $plan_id
            ),
            'joins'=>array(
                'LEFT JOIN #__jreviews_paid_plans_categories AS PaidCategory ON PaidPlan.plan_id = PaidCategory.plan_id'
            )
        ))){

            /**
             * Tracking code
             */  
            $price = $plan['PaidPlan']['plan_price'];
            
            if($track = Sanitize::getVar($this->c->Config,'paid.track_listing_submit',''))
            {
                $track = PaidListingsComponent::trackingReplacements($track,array(
                    'PaidOrder'=>array(
                        'order_amount'=>$plan['PaidPlan']['plan_price'],
                        'plan_info'=>array('plan_name'=>addslashes($plan['PaidPlan']['plan_name']))
                    ),                
                ));
                echo html_entity_decode($track,ENT_QUOTES,cmsFramework::getCharset());   
            } 
            
            if($price == 0)
            {
                $listing = $model->findRow(array('conditions'=>array('Listing.id = ' . $listing_id)),array());
                $order = $PaidOrder->makeOrder($plan,$listing);
                $PaidOrder->store($order);            
                $this->processFreeOrder($order);   
                return (string) $plan['PaidPlan']['plan_array']['moderation'];
            }

            $response[] = "parent.JRPaid.OrderForm.load('jr_listingFormOuter',{'referrer':'create','listing_title':'".addslashes($model->data['Listing']['title'])."','plan_type':{$plan['PaidPlan']['plan_type']},'listing_id':{$listing_id},'plan_id':{$plan_id},'page':2,'price':{$price}});";
            $response[] = "parent.jQuery('#jr_listingFormOuter').scrollTo({duration:400,offset:-100});";
            return $this->c->makeJS($response);                
            
            # Redirect to MyAccount page 
            /*
            $PaidRoutes = ClassRegistry::getClass('PaidRoutesHelper');
            $response[] = 'parent.location.href = \''.$PaidRoutes->myaccount().'\'';
            return $this->c->makeJS($response);
            */                
        } 
        elseif($PaidCategory->isInPaidCategory($cat_id))
        {
            $response[] = "parent.JRPaid.OrderForm.load('jr_listingFormOuter',{'listing_title':'".addslashes($model->data['Listing']['title'])."','listing_id':{$listing_id}});";
            $response[] = "parent.jQuery('#jr_listingFormOuter').scrollTo({duration:400,offset:-100});";
            return $this->c->makeJS($response);                
        }
        else // Continue with normal processing of listings, nothing to do with plans 
        {
            return '';
        }    
    }    
    
    /**
    * Gets rid of everything that shouldn't be shown on the page
    * like images and fields. Adds paid variables from the selected plan if any
    * 
    * @param array $results
    */
    
    function processPaidData(&$results)
    {                                     
        if(!isset($this->c->Config))
        {
            $this->c->Config = Configure::read('JreviewsSystem.Config');            
        } 

        // We need to retrieve payment plan info for each listing and remove fields not applicable to the payment plan
        $PaidOrder = ClassRegistry::getClass('PaidOrderModel');
        $PaidPlan = ClassRegistry::getClass('PaidPlanModel');
        // Geomaps integration
        $jr_lat = Sanitize::getString($this->c->Config,'geomaps.latitude');
           
        $results = $PaidOrder->completeOrderInfo($results);       
        !isset($this->c->_user) and $this->c->_user = & cmsFramework::getUser();
        $this->c->_user->id > 0 and $results = $PaidPlan->completePlanInfo($results);

        // Now remove un-paid stuff       
        foreach($results AS $listing_id=>$listing)
        {                                 
            if(
                isset($listing['PaidPlanCategory'])
                && 
                $listing['PaidPlanCategory']['cat_id'] > 0
                && 
                (
                !isset($listing['Listing']['extension']) || $listing['Listing']['extension']=='com_content'
                )
            ){          
                // Process Images
                if(is_array($listing['Listing']['images'])
                    && 
                    !empty($listing['Listing']['images'])
                )
                {                                                   
                    if(isset($listing['Paid'])) // The listing has a plan attached to it
                    {             
                        // Truncate image array to paid plan's images
                        $results[$listing_id]['Listing']['images'] = array_splice($listing['Listing']['images'],0,Sanitize::getVar($listing['Paid'],'images',array()));
                    } 
                    else 
                    {
                        // Remove images
                        $results[$listing_id]['Listing']['images'] = array();
                    }
                }        

                /**
                *  Geomaps integration
                */
                // Add check for GeoMaps coordinate fields and remove them if not in paid fields
                if(!isset($listing['Paid'])  || 
                    (
                    isset($listing['Paid']) && !in_array($jr_lat,$listing['Paid']['fields'])
                    )
                ) {
                    unset($results[$listing_id]['Geomaps']);
                } 
                      
                // Process Fields
                if(empty($listing['Field']['groups'])) continue;
                foreach($listing['Field']['groups'] AS $group_name=>$group_fields)           
                {        
                    if(isset($listing['Paid'])) // The listing has a plan attached to it
                    {     
                        // Remove fields not included with the plan
                        $fields_flip = array_flip($listing['Paid']['fields']);
                        $results[$listing_id]['Field']['groups'][$group_name]['Fields'] = array_intersect_key($listing['Field']['groups'][$group_name]['Fields'],$fields_flip);
                        $results[$listing_id]['Field']['pairs'] = array_intersect_key($listing['Field']['pairs'],$fields_flip);                        
                    } 
                    else 
                    {
                        // If listing in paid category without a plan, remove all fields
                        $results[$listing_id]['Field']['groups'][$group_name]['Fields'] = $results[$listing_id]['Field']['pairs'] = array();
                    }
                }                
            }
        }
    }
    
    /**
    * Called from the payment handler to finalize the processing of the order
    * Makes changes to the listing and adds notes to the txn record for the order which is saved in the handler itself.
    * 
    * @param array $order
    */
    function processSuccessfulOrder($order,$listing_state = null)
    {    
        $PaidTxnLog = ClassRegistry::getClass('PaidTxnLogModel');
        $PaidOrder = ClassRegistry::getClass('PaidOrderModel');
        if(!is_array($order['PaidOrder']['plan_info'])) // Make sure we are dealing with an array
        {
            $order['PaidOrder']['plan_info'] = json_decode($order['PaidOrder']['plan_info'],true);
        }
        
        # For renewals don't process the order. It will be done via cron on renewal date
        $renewal_date = Sanitize::getVar($order['PaidOrder'],'order_renewal',null);
        if(!is_null($renewal_date) && $renewal_date != _NULL_DATE)
        {
            $PaidTxnLog->addNote(__t("Order renewal set.",true));
            !defined('MVC_FRAMEWORK_ADMIN') and $PaidOrder->updateOrder($order,array('order_status'=>'Complete','order_active'=>0)); // Do not run when editing an order via the administration
            return true;
        } 
        
        # Process Order by Plan Type
        switch($order['PaidOrder']['plan_type'])
        {
            case 0: $process = $this->processNewListing($order,$listing_state); break; // New Listing Plan
            case 1: $process = $this->processUpgradeListing($order); break; // Upgrade Listing Plan
        }        

         if(!$process){ 
            $PaidTxnLog->addNote(__t("Order processing error.",true));
            return false;
         }

        # Transfer fields from paid_listing_fields to jreviews_content
        $PaidListingField = ClassRegistry::getClass('PaidListingFieldModel');
        $transferred = $PaidListingField->moveFieldsToListing($order['PaidOrder']['listing_id'],$order['PaidOrder']['plan_info']);

        if(!$transferred)
        {
            $PaidTxnLog->addNote("Error updating field values");
        }

        if(!defined('MVC_FRAMEWORK_ADMIN') || (defined('MVC_FRAMEWORK_ADMIN') && $this->c->name == 'admin_paidlistings_orders' && $this->c->action != '_save'))
        {
            $PaidTxnLog->addNote("Payment status: " . $order['PaidOrder']['order_status']);
            $PaidTxnLog->addNote("Order active: 1");
            $PaidOrder->updateOrder($order,array('order_renewal'=>_NULL_DATE,'order_active'=>1));
        }
        return true;
    }
    
    function processFreeOrder(&$order)
    {
        $PaidTxnLog = ClassRegistry::getClass('PaidTxnLogModel');
        $PaidOrder = ClassRegistry::getClass('PaidOrderModel');
        $this->c->action = '_process'; // To make the notifications component send the correct emails 
        $order['PaidOrder']['order_status'] = 'Complete';
        if($this->processSuccessfulOrder($order))
        {             
            $PaidTxnLog->save($order, '', 'FREE-'.$order['PaidOrder']['order_id'], true);                
            return true;
        }
        return false;                
    }
    
    function processNewListing($order,$listing_state)
    {
        $txn_duplicate = false;
        $PaidTxnLog = ClassRegistry::getClass('PaidTxnLogModel');
        $PaidOrder = ClassRegistry::getClass('PaidOrderModel');
        
        if(isset($order['txn_id']))
        {
            $txn_duplicate = $PaidTxnLog->findDuplicate($order['txn_id']);    
        }
        
        $listing_state = !is_null($listing_state) ? $listing_state : !$order['PaidOrder']['plan_info']['plan_array']['moderation'];
        
        $listing = array(
                'Listing'=>array(
                    'id'=>$order['PaidOrder']['listing_id'],
                    'state'=>$listing_state,
                    'publish_down'=>$order['PaidOrder']['order_expires'],
                )
            );
        
        $this->c->Listing->isNew = true; // Used by other plugins

        $this->c->_user->id = $order['PaidOrder']['user_id'];
              
        /**
        *  If run from the listings controller then we want to avoid running the callbacks a second time
        *  The listing state should have been updated in the plgBeforeSave method in this plugin before other plugins are run
        * 
        *  If payment handler post request don't run plugins because they are run when returned to the site
        */
        if($this->c->name == 'listings' || $txn_duplicate)
        {
            $store = $this->c->Listing->store($listing,false,array() /* Disable callbacks*/);
        }
        else 
        {
            $store = $this->c->Listing->store($listing);
        } 
        
        if($store)
        {
            $PaidTxnLog->addNote(sprintf(__t("Listing expiration: %s",true),$order['PaidOrder']['order_expires']));
            $PaidTxnLog->addNote(sprintf(__t("Listing state: %s",true),$listing_state));
            
            if($PaidOrder->changeFeaturedState($order['PaidOrder']['listing_id'],$order['PaidOrder']['plan_info']['plan_featured']))
            {
                $PaidTxnLog->addNote("Listing featured: " . $order['PaidOrder']['plan_info']['plan_featured']);
            }
            return true;
        }
        return false;        
    }
    
    function processUpgradeListing($order)
    {
        $PaidTxnLog = ClassRegistry::getClass('PaidTxnLogModel');
        $PaidOrder = ClassRegistry::getClass('PaidOrderModel');
        
        // Upgrades consist on making the listing featured, adding more images or new fields. Only the featured addition requires updates to the listing
        if($order['PaidOrder']['plan_info']['plan_featured'] 
            && 
            $PaidOrder->changeFeaturedState($order['PaidOrder']['listing_id'],$order['PaidOrder']['plan_info']['plan_featured'])
        )
        {
            $PaidTxnLog->addNote("Listing featured: " . $order['PaidOrder']['plan_info']['plan_featured']);
        } 
        return true;
    }
        
    /**
    * Called from the payment handler to process a failed payment attempt and unpublish the listing
    * @param array $order
    * @param int $listing_state
    */
    function processFailedOrder($order,$listing_state = 0)
    {
        $PaidTxnLog = ClassRegistry::getClass('PaidTxnLogModel');
        $listing = array(
            'Listing'=>array(
                'id'=>$order['PaidOrder']['listing_id'],
                'state'=>$listing_state
            )
        );
        if($this->c->Listing->store($listing))
        {
            $PaidTxnLog->addNote(sprintf(__t("Listing state: %s",true),$listing_state));
        }        
    }
        
    /**
    * Searches for orders expiring today and marks them as inactive
    * 
    */
    function processAllExpiredOrders()
    {                         
        $PaidOrder = ClassRegistry::getClass('PaidOrderModel');
        
        $last_run = Sanitize::getInt($this->c->Config,'paid.notify_last_run');   
        if(!$last_run 
            || time() - $last_run >= 24*60*60 
            || Sanitize::getBool($this->c->Config,'paid.expired_testing',false))
        {      
            $PaidOrder->updateAllExpiredOrders() and $this->c->Config->store(array('paid.notify_last_run'=>time()));
        }
    }
    
    /**
    * Finds renewal orders and processes them
    * 
    */
    function processOrderRenewals() 
    {
        $PaidOrder = ClassRegistry::getClass('PaidOrderModel');
        $PaidTxnLog = ClassRegistry::getClass('PaidTxnLogModel');
        
        $orders = $PaidOrder->findAll(array(
            'conditions'=>array(
                "PaidOrder.order_status = 'Complete'",
                "PaidOrder.order_renewal <= '".gmdate('Y-m-d',time())."'",
                'PaidOrder.order_renewal != "'._NULL_DATE.'"', 
                'PaidOrder.order_active = 0'
            )
        ));    
        foreach($orders AS $order)
        {
            $PaidTxnLog->addNote("Modified by: cron");
            $order['PaidOrder']['order_renewal'] = _NULL_DATE; // To allow the order to process
            $success = $this->processSuccessfulOrder($order,1/* set listing as published */);  
            $PaidTxnLog->save($order, '', 'Renewal processed', $success);
        }
    }
    
        
/************************************************************************
* AUXILIARY METHODS 
************************************************************************/    
    
    /**
    * Adds js and css assets to the $this->c->assets array to be processed later on by the assets helper
    * Need to be set here instead of theme files for pages that can be cached
    * 
    */
    function loadAssets() 
    {
        if($this->c->name == 'listings' && in_array($this->c->action,array('create','edit')))
        {              
            $this->c->assets['js'] = array('paidlistings');
            $this->c->action == 'create' and $this->c->assets['css'] = array('paidlistings');
        }        
    } 
    
    /**
    * Replacers order related tags in tracking code added by admins for payment steps
    * 
    * @param mixed $code
    * @param mixed $order
    * @return mixed
    */
    function trackingReplacements(&$code, $order)
    {
        !is_array($order['PaidOrder']['plan_info']) and $order['PaidOrder']['plan_info'] = json_decode($order['PaidOrder']['plan_info'],true);
        $code = str_replace(
            array(
                '{order_amount}',
                '{order_id}',
                '{plan_name}'
            ),
            array(
                Sanitize::getFloat($order['PaidOrder'],'order_amount'),
                Sanitize::getInt($order['PaidOrder'],'order_id'),
                addslashes(Sanitize::getString($order['PaidOrder']['plan_info'],'plan_name')),
            )
            ,$code
        );
        return $code;
    }
}

admin_paidlistings = 
{
    FormatCurrency: function(num,symbol) {
        num = num.toString().replace(/\$|\,/g,'');
        if (isNaN(num)) num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        {
            num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
        }
        return (((sign) ? '' : '-') + symbol + num + '.' + cents);        
    },
    TreeChecked: function(tree_id){
        var checked_ids = [];
        jQuery("#"+tree_id).find("li.jstree-checked").each(function () {
            if(this.id.match(/^([0-9]+|jr_[a-z]+)$/)!=null){
                checked_ids.push(this.id);
            }
        });
        return checked_ids;        
    }, 
    Coupons: {
        initCategoryTree: function() {
            /************************************
            *  Category tree
            ************************************/
            jQuery("#cat-tree").bind("loaded.jstree",function (e,data) { 
                // Select current categories using the curr_cat_ids hidden field
                jQuery.each(jQuery('#curr_cat_ids').val().split(","),function(i,id){  
                    data.inst.check_node(jQuery("#"+id));   
                });                        
            })
            .jstree({
                // Attach jstree plugin to cat-tree div
                "themes": {"theme": "default","icons":false},
                "plugins": ["themes","ui","json_data","checkbox"],  
                "json_data" : { 
                    "ajax" : {
                        "type": "get",
                        "url" : s2AjaxUri + '&url=admin_paidlistings_coupons/jsonCategoryTree'
                    }
                },
                "types": { "types" : 
                    {"default": {renameable: false, deletable: false, creatable: false, draggable: false} } 
                }
            });            
        },        
        del: function(coupon_id, confirm_text){
            options = {};
            jreviews_admin.dialog.remove('admin_paidlistings_coupons','_delete',confirm_text,{'data[entry_id]':coupon_id},options);
        },
        delAll : function(confirm_text) {
            var ids = [];
            jQuery('input:checkbox','#couponsTable').each(function(){
               if(this.checked && this.id.match(/^(cb[0-9]+)$/)!=null){
                   ids.push(this.value);
                }
            });
            var coupon_ids = ids.join(",");
            if(coupon_ids != '') {
                jreviews_admin.dialog.remove('admin_paidlistings_coupons','_delete',confirm_text,{'data[entry_id]':coupon_ids},{});
            }
        },        
        save: function() {
            jQuery('#cat_ids').val(admin_paidlistings.TreeChecked('cat-tree'));            
            jQuery.post(s2AjaxUri,jQuery('#couponForm').serialize(),function(res) {
                if(res.action == 'success') {
                    jQuery('#page').html(res.page).fadeIn('normal',function(){jreviews_admin.tools.flashRow(res.row_id);});                       
                } else {
                    var text = '';
                    jQuery(res.validation).each(function(i){
                        text = text + "<p>"+res.validation[i]+"</p>";
                    });
                    jQuery('#validation_msg').html(text).fadeIn();
                }
            },'json'); 
        }
    },  
    Plans: {
        save: function() {
            jQuery('#cat_ids').val(admin_paidlistings.TreeChecked('cat-tree'));
            jQuery('#field_names').val(admin_paidlistings.TreeChecked('field-tree'));
            jQuery.post(s2AjaxUri,jQuery('#planForm').serialize(),function(res) {
                if(res.action == 'success') {
                    jQuery('#page').html(res.page).fadeIn('normal',function(){jreviews_admin.tools.flashRow(res.row_id);});                       
                } else {
                    var text = '';
                    jQuery(res.validation).each(function(i){
                        text = text + "<p>"+res.validation[i]+"</p>";
                    });
                    jQuery('#validation_msg').html(text).fadeIn();
                }
            },'json'); 
        },        
        init: function(plan_id) {
            var durationClone = jQuery('#duration').clone();
            admin_paidlistings.Plans.initCategoryTree(plan_id);
            admin_paidlistings.Plans.initFieldGroupsTree(plan_id,null);
            
            jQuery('#planForm').change(function()
            {                  
                var plan_type = jQuery('#plan_type option:selected').val(); 
                var payment_type = jQuery('#payment_type option:selected').val();
                var durationSel = jQuery('#duration option:selected').val();
                durationSel == 'never' ? jQuery('#duration_number').val(0).attr("disabled","disabled") : jQuery('#duration_number').removeAttr("disabled");                
                payment_type == 2 
                    ? 
                    jQuery(function(){ // Free 
                        jQuery('#plan_price').val(0).attr('disabled','disabled');
                        jQuery('#duration').html(durationClone.html()).val(durationSel);
                        jQuery('#payment_type_msg1').show();
                        jQuery('#free_limit_row').show();
                    }) 
                    : 
                    jQuery(function(){
                        jQuery('#plan_price').removeAttr('disabled');
                        jQuery('#payment_type_msg1').hide();
                        jQuery('#free_limit_row').hide();
                    });
                switch(plan_type){
                    case '0': //New listing
                        jQuery("#moderation-row").fadeIn();
                        jQuery('#plan_exclusive_row').css('display','none');
                        jQuery('#plan_submit_form').show();
                        switch(payment_type){
                            case '0': // single payment
                                jQuery('#duration').html(durationClone.html()).val(durationSel);
                            break;
                            case '1': // subscription 
                                jQuery('#duration').html(durationClone.html()).val(durationSel);
                                jQuery('#duration').find("option[value='never']").remove().val('months');
                            break;                            
                        }
                    break;
                    case '1': //Upgrade listing
                        jQuery("#moderation-row").fadeOut();
                        jQuery('#plan_exclusive_row').css('display','inline');
                        jQuery('#plan_submit_form').hide();
                        durationSel!='never' ? jQuery('#upgrade_msg1').show() : jQuery('#upgrade_msg1').hide();
                        switch(payment_type){
                            case '0': // single payment
                                jQuery('#duration').html(durationClone.html()).val(durationSel);
                            break;
                            case '1': // subscription 
                                jQuery('#duration').html(durationClone.html()).val(durationSel).find("option[value='never']").remove().val('months');
                            break;                            
                        }
                    break;
                }
            }).change();
        },
        initCategoryTree: function() {
            /************************************
            *  Category tree
            ************************************/
            var $fieldTree = jQuery('#field-tree');
            var $catTree = jQuery("#cat-tree");
            $catTree.data('page_setup',true);
            
            $catTree.bind("loaded.jstree",function (e,data) { 
                // Select current categories using the curr_cat_ids hidden field
               jQuery.each(jQuery('#curr_plan_cat_ids').val().split(","),function(i,id){
                    if(id > 0) data.inst.check_node(jQuery("#"+id,'#cat-tree')); 
                });   
                $catTree.jstree('close_all');   
                $catTree.data('page_setup',false);
            })
            .bind("uncheck_node.jstree",function (e,data) {
                var sel_node_id = data.rslt.obj.attr('id');
                jQuery('#curr_plan_field_ids').val(admin_paidlistings.TreeChecked('field-tree'));
                jQuery.get(s2AjaxUri,{
                        "data[controller]":"admin/admin_paidlistings_plans",
                        "data[action]":"getGroupIdsByNode",
                        "data[node_id]": admin_paidlistings.TreeChecked('cat-tree') // Current selected cat nodes
                    },function(group_ids){ // Returns groups that should be shown for current selected cat nodes
                        $fieldTree.find('li:not(.jstree-leaf)').each(function(){
                            if(-1 == jQuery.inArray(jQuery(this).attr('id'),group_ids)){
                                // Remove groups that are not in the list of returned groups
                                $fieldTree.jstree("delete_node",'#'+jQuery(this).attr('id')); 
                            }    
                        });
                },'json');
            })
            .bind("check_node.jstree",function(e,data) {
                // Create field group nodes in Field Tree - those that don't already exist   
                var sel_node_id = [];
                $catTree.find("li.jstree-checked").each(function(){
                    if(this.id.match(/^([0-9]+)$/)!=null){
                        sel_node_id.push(this.id);
                    }
                }); 
                sel_node_id = sel_node_id.join(",");  
                if(false == $catTree.data('page_setup')) {
                    jQuery.get(s2AjaxUri,{
                            "data[controller]":"admin/admin_paidlistings_plans",
                            "data[action]":"jsonFieldGroups",
                            "cat_id":sel_node_id
                        },function(node){
                            jQuery.each(node,function(i,n){
                                if(!$fieldTree.find('#'+n.attr.id).length){
                                    $fieldTree.jstree("create_node",$fieldTree,"first",n,function(){
                                        jQuery.each(n.children,function(i,child){
                                            $fieldTree.jstree("create_node",jQuery('#'+n.attr.id),"last",child);
                                        });
                                    });
                                    // Group is automatically opened in the create_node event for the field-tree
                                }    
                            });
                    },'json');
                }
            })
            .jstree({
                "themes": {"theme": "default","icons":false},
                "plugins": ["themes","ui","json_data","checkbox"],  
                "json_data" : { 
                    "ajax" : {
                        "type": "get",
                        "url" : s2AjaxUri + '&url=admin_paidlistings_plans/jsonCategoryTree'
                    }
                },
                "types": { "types" : 
                    {"default": {renameable: false, deletable: false, creatable: false, draggable: false} } 
                }
            });            
        },
        initFieldGroupsTree: function(plan_id,cat_id) {
            // Field Groups tree
            jQuery("#field-tree")
            .bind("loaded.jstree",function(e,data){
               jQuery.each(jQuery('#curr_plan_group_ids').val().split(","),function(i,id){
                    data.inst.open_node(jQuery("#g"+id)); 
                });
                jQuery(this).jstree('close_all');   
            })
            .bind("open_node.jstree",function(e,data){
               jQuery.each(jQuery('#curr_plan_field_ids').val().split(","),function(i,id){
                    if(id!='') { data.inst.is_checked('#'+id) || data.inst.check_node("#"+id); }
                });
            })
            .bind("check_node.jstree",function(e,data){
                var sel_node_id = data.rslt.obj.attr('id');
                data.inst.open_node(jQuery("#"+sel_node_id));   
            })
            .jstree({
                "themes": {"theme": "default","icons":false},
                "plugins": ["themes","ui","json_data","checkbox","sort"],  
                "json_data" : { 
                    "ajax" : {
                        "type": "get",
//                        "data": function(n){return {"group_id":jQuery(n).attr('id') || 0, "action":"checked"};},
                        "url" : s2AjaxUri + '&url=admin_paidlistings_plans/jsonFieldGroups/plan_id:'+plan_id+'/cat_id:'+cat_id
                    }/*,
                    "progressive_render": true*/
                },
                "types": { "types" : 
                    {"default": {renameable: false, deletable: false, creatable: false, draggable: false} } 
                }
            });            
        }
    },
    Orders: {
        edit: function(order_id){
            var dialog_id = 'jr_formDialog';
            var settings = {
                'modal': true,
                'autoOpen': true,
                 'buttons': {
                    "Cancel":function(){jQuery(this).dialog('close'); },
                    "Save":function() {
                        jQuery('#field_names').val(admin_paidlistings.TreeChecked('field-tree'));
                        jreviews_admin.tools.saveUpdateRow('orderForm');
                    }
                },
                'width': '800px',
                'height': 'auto',
                'position': 'top',  
                'title':'Edit Order',
                'open': function() {
                    jQuery.post(s2AjaxUri,{
                        'data[controller]':'admin/admin_paidlistings_orders',
                        'data[action]':'edit',
                        'data[order_id]':order_id,
                        'tmpl':'component',
                        'format':'raw'
                        },
                        function(html){
                            jQuery('#'+dialog_id).html(html);
                        }
                    );
                }
            };        
            jQuery('.dialog').dialog('destroy').remove(); 
            jQuery("body").append('<div id="'+dialog_id+'" class="dialog"><span class="jr_loadingMedium" style="margin-left:47%;"></span></div>');
            jQuery('#'+dialog_id).dialog(settings);                
        },
        del: function(order_id, confirm_text){
            options = {};
            jreviews_admin.dialog.remove('admin_paidlistings_orders','_delete',confirm_text,{'data[entry_id]':order_id},options);
        },
        delAll : function(confirm_text) {
            var ids = [];
            jQuery('input:checkbox','#ordersTable').each(function(){
               if(this.checked && this.id.match(/^(cb[0-9]+)$/)!=null){
                   ids.push(this.value);
                }
            });
            var order_ids = ids.join(",");
            if(order_ids != '') {
                jreviews_admin.dialog.remove('admin_paidlistings_orders','_delete',confirm_text,{'data[entry_id]':order_ids},{});
            }
        },
        viewTxnLog: function(data){
            var dialog_id = 'jr_formDialog';
            var settings = {
                'modal': true,
                'autoOpen': true,
                'buttons': function() {},
                'width': '800px',
                'height': '600',
                'position': 'top',  
                'title':'Order transactions',
                'open': function() {
                    jQuery.post(s2AjaxUri,{
                        'data[controller]':'admin/admin_paidlistings_txn',
                        'data[action]':'getOrderTxn',
                        'data[order_id]':data.order_id,
                        'data[listing_id]':data.listing_id,
                        'tmpl':'component',
                        'format':'raw'
                        },
                        function(html){jQuery('#jr_formDialog').html(html)}
                    );
                }
            };        
            jQuery('.dialog').dialog('destroy').remove(); 
            jQuery("body").append('<div id="'+dialog_id+'" class="dialog"><span class="jr_loadingMedium" style="margin-left:47%;"></span></div>');
            jQuery('#'+dialog_id).dialog(settings);              
        },
        search: function(){
            jQuery('#msg').hide();
            jQuery('#search').val(1);
            jQuery.post(s2AjaxUri,
                jQuery('#adminForm').serialize(),
                function(html){
                    if(html != '')
                    {            
                        jQuery('#ordersTable').html(html);
                    } else {
                        jQuery('#msg').show();    
                    }
                },
                'html'
            );               
        }, 
        generateOrders: function(plan_id,task)                                                
        {   
            jQuery('body').data('plan_id',plan_id);
            jQuery('body').data('Orders.abort',0); // Used to stop the ajax request            
            if(task == 'process')
            {
                admin_paidlistings.Orders.generateOrders(plan_id, 'process');
                return;
            }
            jQuery('#jr_genOrderDialog').find('#listingCount').html('0');
            var dialog_id = 'jr_genOrderDialog';
            var settings = {
                'modal': true,
                'autoOpen': false,
                'buttons': {
                    "Cancel":function(){
                        jQuery(this).dialog('close');
                    },
                    "Go":function() {
                        admin_paidlistings.Orders.generateOrdersGo(jQuery('body').data('plan_id'));
                    }
                },
                'width': '600px',
                'height': 'auto',
                'title':'Generate Orders',
                'close': function() { jQuery('body').data('Orders.abort',1); },
                'open': function() {    
                    jQuery.post(s2AjaxUri,{
                        'data[controller]':'admin/admin_paidlistings_orders',
                        'data[action]':'generateOrders',
                        'data[plan_id]':jQuery('body').data('plan_id'),
                        'data[task]':'init',
                        'tmpl':'component',
                        'format':'raw'
                        },
                        function(count){
                            if(parseInt(count)==0) jQuery('.ui-dialog-buttonpane').slideUp();
                            jQuery('#jr_genOrderDialog').find('#listingCount').html(count);
                        }
                    );
                }
            };        
            jQuery('#'+dialog_id).dialog(settings).dialog('open');
            jQuery('.ui-dialog-buttonpane').css('display','block');
        },
        generateOrdersGo: function(plan_id)
        {
            jQuery('body').data('plan_id',plan_id);
            var xhr = jQuery.post(s2AjaxUri,{
                'data[controller]':'admin/admin_paidlistings_orders',
                'data[action]':'generateOrders',
                'data[plan_id]':plan_id,
                'data[task]':'process',
                'tmpl':'component',
                'format':'raw'
                },
                function(count){
                    jQuery('#jr_genOrderDialog').find('#listingCount').html(count);
                    if(jQuery('body').data('Orders.abort')==1) {return false;}
                    if(parseInt(count)==0) {
                        jQuery('.ui-dialog-buttonpane').slideUp(); 
                    } else {
                        admin_paidlistings.Orders.generateOrdersGo(jQuery('body').data('plan_id'));
                    }
                }
            );
        },
        updateOrders: function(plan_id,task)                                                
        {
            jQuery('body').data('plan_id',plan_id);
            jQuery('body').data('UpdateOrders.abort',0); // Used to stop the ajax request            
            if(task == 'process')
            {
                admin_paidlistings.Orders.updateOrders(plan_id, 'process');
                return;
            }
            var dialog_id = 'jr_updateOrdersDialog';
            jQuery('#'+dialog_id).find('#orderCount').html('0');
            var settings = {
                'modal': true,
                'autoOpen': false,
                'buttons': {
                    "Cancel":function(){
                        jQuery(this).dialog('close');
                    },
                    "Go":function() {
                        admin_paidlistings.Orders.updateOrdersGo(plan_id);
                    }
                },
                'width': '600px',
                'height': 'auto',
                'title':'Push Plan Updates to Existing Orders',
                'close': function() { jQuery('body').data('UpdateOrders.abort',1); },
                'open': function() {                     
                    jQuery.post(s2AjaxUri,{
                        'data[controller]':'admin/admin_paidlistings_orders',
                        'data[action]':'updateOrders',
                        'data[plan_id]':jQuery('body').data('plan_id'),
                        'data[task]':'init',
                        'tmpl':'component',
                        'format':'raw'
                        },
                        function(count){
                            if(parseInt(count)==0) jQuery('.ui-dialog-buttonpane').slideUp();
                            jQuery('#'+dialog_id).find('#orderCount').html(count);
                        }
                    );
                }
            };        
            jQuery('#'+dialog_id).dialog(settings).dialog('open');
            jQuery('.ui-dialog-buttonpane').css('display','block');
        },
        updateOrdersGo: function(plan_id)
        {
            jQuery('body').data('plan_id',plan_id);
            var dialog_id = 'jr_updateOrdersDialog';
            var xhr = jQuery.post(s2AjaxUri,{
                'data[controller]':'admin/admin_paidlistings_orders',
                'data[action]':'updateOrders',
                'data[plan_id]':jQuery('body').data('plan_id'),
                'data[task]':'process',
                'tmpl':'component',
                'format':'raw'
                },
                function(count){
                    jQuery('#'+dialog_id).find('#orderCount').html(count);
                    if(jQuery('body').data('UpdateOrders.abort')==1) {return false;}
                    if(parseInt(count)==0) {
                        jQuery('.ui-dialog-buttonpane').slideUp(); 
                    } else {
                        admin_paidlistings.Orders.updateOrdersGo(jQuery('body').data('plan_id'));
                    }
                }
            );
        }        
    },
    Transactions: {
        search: function(){
            jQuery('#msg').hide();
            jQuery('#search').val(1);
            jQuery.post(s2AjaxUri,
                jQuery('#adminForm').serialize(),
                function(html){
                    if(html != '')
                    {            
                        jQuery('#txnTable').html(html);
                    } else {
                        jQuery('#msg').show();    
                    }
                },
                'html'
            );               
        }        
    }
};
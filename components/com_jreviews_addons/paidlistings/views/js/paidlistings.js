JRPaid = 
{
    FormatCurrency: function(num,symbol) {
        if(undefined==symbol) symbol = '';
        num = num.toString().replace(/\$|\,/g,'');
        if (isNaN(num)) num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        {
            num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
        }
        return (((sign) ? '' : '-') + symbol + num + '.' + cents);        
    },    
    Plans: {
        /**
        * Shows/hides fields based on selected plan
        */
        submitFilter: function(plan_id)
        {     
            var planFields = plans['plan'+plan_id].fields;
            var planImages = plans['plan'+plan_id].images;
            var $form = jQuery('#jr_listingForm');
            var $planFields = $form.find('#jr_planFields');
            var $planImages = $form.find('#jr_planImages');
            var $tabs = jreviews.controlFieldListing.getTabs();
            if($planFields.length === 0) {
                $form.append('<input type="hidden" id="jr_planFields" name="data[Paid][fields]" value="'+planFields+'" />');
                $form.append('<input type="hidden" id="jr_planImages" name="data[Paid][images]" value="'+planImages+'" />');
            } else {
                $planFields.val(planFields);
                $planImages.val(planImages);
            }

            $form.find('fieldset').each(function() {
                var $fieldset = jQuery(this);
                if($fieldset.attr('class')=='reviewForm') return false;
                var $fieldDiv = $fieldset.children('.jr_fieldDiv');
                var fieldDivCount = $fieldDiv.length;
                if(fieldDivCount > 0) 
                {
                    $fieldDiv.each(function() {
                        var field_id = jQuery(this).find(':input[id^="jr_"]').attr('id');
                        if(undefined != field_id) {
                            var matches= field_id.match(/^(jr_[a-z]+)/);
                            var field = matches != null ? matches[0] : undefined; 
                            if(field!=undefined)
                            {
                                // load the field object from the controlField class
                                if(typeof jreviews.controlFieldListing.getFieldObj == 'function')
                                {
                                     var $field = jreviews.controlFieldListing.getFieldObj(field); 
                                     if($field != false) {
        //                                var $field = jQuery(this).find(':input[id^="jr_"]');  
                                        if(jQuery.inArray(field,planFields) == -1)
                                        {    
                                            $field.data('isActive',false).parents('div.jr_fieldDiv').css('display','none');
                                            fieldDivCount--
                                        } 
                                        else if($field.data('isControlled') === false)  
                                        {
                                            $field.parents('div.jr_fieldDiv').show('fast');
                                        }                                   
                                    }                                    
                                }
                            } 
                        }
                    });

                    if(fieldDivCount == 0) {
                        $fieldset.css('display','none');
                        if($tabs.length && $fieldset.attr('id')) $tabs.filter('li#tab_'+$fieldset.attr('id').replace('group_','')).hide('fast');
                    }
                    else if ($fieldset.data('isControlled')!==true || $fieldset.data('isActive')===true) {
                        /* Only unhide groups that are not controlled by other fields */
                        $fieldset.fadeIn();
                        if($tabs.length && $fieldset.attr('id')) $tabs.filter('li#tab_'+$fieldset.attr('id').replace('group_','')).fadeIn('slow');
                    }
                } 
            });  
            var imgDiv = jQuery('#image_upload_container');
            var currNumImages = parseInt(jQuery('#content_images').find('.jr_imageEdit').length);
            imgDiv.find('input').remove();
            planImages = planImages - currNumImages;            
            if(planImages > 0)
            {
                jQuery('#images').show();
                jQuery('#image_upload_container').show();
                imgDiv.parent('fieldset').show();
                for(i=1;i<=planImages;i++) { 
                    imgDiv.append('<input type="file" name="image[]" class="jr_imageUpload" size="20">'); 
                }                
            } else {               
                if(!currNumImages) jQuery('#images').hide();
                jQuery('#image_upload_container').hide();
                imgDiv.find('input').remove();
            }
        },
        load: function (params) {
           jQuery.get(s2AjaxUri+'&cat_id='+params.cat_id,{
                'data[controller]':'paidlistings_plans',
                'data[action]':'index',
                'tmpl':'component',
                'format':'raw'
                },
                function(html){
                    jQuery('#jr_plansPg').html(html);
                }
            );
        }
    },
    OrderForm: {
        init: function()
        {
            jQuery("input[type=radio][id^='jr_plan']","#jr_paymentForm").change(function(){  
                var planTitle = jQuery(this).attr("title"),
					planPrice = jQuery(this).attr("price"),
					currencySymbol = jQuery('#jrPaidCurrencySymbol').html();
				jQuery('#jr_chosenPlanTitle').html(planTitle);
				jQuery('#jrSelectedPlan').html(planTitle + ' ' + currencySymbol + planPrice);
                jQuery('body').data('Order.plan_id',jQuery(this).val());
                jQuery('body').data('Order.price',planPrice);
            });   
        },
        dialog: function(params)
        {
            var dialog_id = 'jr_formDialog';
            var settings = {
                'modal': true,
                'autoOpen': true,
                'buttons': function() {},
                'width': '600px',
                'height': '500',
                'title':params.title,
                'open': function() {
                    params.afterLoad = function() {jQuery('#jr_listingTitle').html(jQuery('.ui-dialog-title').html());};
                    JRPaid.OrderForm.load(dialog_id,params);
                }
            };        
            jQuery('.dialog').dialog('destroy').remove(); 
            jQuery("body").append('<div id="'+dialog_id+'" class="dialog"><span class="jr_loadingMedium" style="margin-left:47%;"></span></div>');
            jQuery('#'+dialog_id).dialog(settings);    
        },        
        load: function(element_id,params)
        {
           jQuery.get(s2AjaxUri,{
                'data[controller]':'paidlistings_orders',
                'data[action]':'_getOrderForm',
                'data[referrer]': params.referrer == undefined ? '' : params.referrer,
                'data[listing_title]': params.listing_title == undefined ? '' : params.listing_title,
                'data[plan_type]':params.plan_type == undefined ? 0 : params.plan_type,
                'data[Listing][listing_id]':params.listing_id,
                'data[plan_id]':params.plan_id == undefined ? 0 : params.plan_id,
                'data[order_id]': params.order_id == undefined ? 0 : params.order_id,
                'data[renewal]': params.renewal == undefined ? 0 : params.renewal,
                'tmpl':'component',
                'format':'raw'
                },
                function(html){
                    jQuery('#'+element_id).html(html);
                    JRPaid.OrderForm.init();
                    jQuery('#jr_paymentForm').find('input:radio:checked').trigger('change').click();   
                    if(undefined!=params.page) {
                        JRPaid.OrderForm.next(params.page);
                    }
                    if(undefined!=params.afterLoad) params.afterLoad();
                }
            );
        },
        next: function(step)
        {
            if(jQuery('body').data('Order.price') == 0) {
                jQuery('#jr_couponBox').hide();        
                jQuery('.jr_msgStep').fadeOut();
                jQuery('#jr_payStep'+parseInt(step-1)).hide('fast',function(){jQuery('#jr_payStep'+parseInt(step+1)).show('fast');});        
            } else if(!jQuery('#jr_payStep'+parseInt(step-1)+' :radio:checked').length){
                jQuery('#jr_payStep'+parseInt(step-1)+' .jr_msgStep').fadeIn();                   
            } else {
                jQuery('#jr_couponBox').show();        
                jQuery('.jr_msgStep').fadeOut();
                jQuery('#jr_payStep'+parseInt(step-1)).hide('fast',function(){jQuery('#jr_payStep'+step).show('fast');});        
            }
        },
        previous: function(step)
        {
            jQuery('.jr_msgStep').fadeOut();
            if(jQuery('body').data('Order.price') == 0) {
                jQuery('#jr_payStep'+parseInt(step+1)).hide('fast',function(){jQuery('#jr_payStep'+parseInt(step-1)).show('fast');});
            } else {
                jQuery('#jr_payStep'+parseInt(step+1)).hide('fast',function(){jQuery('#jr_payStep'+step).show('fast');});        
            }            
        },
        paymentType: function(type,price,tax_rate)
        {
            if(type == 0) {
                jQuery('#jr_singlePayment').css('display','block');
                jQuery('#jr_subsPayment').css('display','none');
            } else {
                jQuery('#jr_singlePayment').css('display','none');
                jQuery('#jr_subsPayment').css('display','block');
            }
            jQuery('#order_price').html(JRPaid.FormatCurrency((price).toFixed(2))); 
            jQuery('#order_subtotal').html(JRPaid.FormatCurrency((price).toFixed(2)));        
            jQuery('#order_tax').html(JRPaid.FormatCurrency((price*tax_rate).toFixed(2)));       
            jQuery('#order_total').html(JRPaid.FormatCurrency((price+price*tax_rate).toFixed(2)));        
            
            // enable / disable payment methods
            jQuery('#jr_singlePayment input:radio[data-points-balance]').each(function(idx, el) {
                var balance = parseFloat(jQuery(el).data('pointsBalance'));
                if(balance < price) {
                    jQuery(el).attr('disabled', 'disabled');
                    jQuery('#' + el.id + '_balance').removeClass('jr_hidden');
                } else {
                    jQuery(el).removeAttr('disabled');
                }
            });
			
			jQuery('#jrSelectedPlan').html();
        },
        submit: function(tos)
        {
            jQuery('.jr_payStep3Msg').hide();
            jQuery('.jr_msgStep').fadeOut();
            var tos_cb = jQuery('#jr_tos_cb');
            if(tos == 2 && tos_cb && !tos_cb.is(':checked')){
                jQuery('#jr_payStep3 .jr_msgStep').fadeIn();
            } 
            else 
            {
                jQuery('#jr_paymentForm').find('.jr_button').attr('disabled','disabled').removeClass('ui-state-default').addClass('ui-state-disabled');           
                jQuery.post(s2AjaxUri,
                    jQuery('#jr_paymentForm').serialize(),
                    function(s2out){
                        jQuery('#s2AjaxResponse').remove();    
                        jQuery("body").append('<div id="s2AjaxResponse" style="display:none;"></div>'); 
                        if(s2out.tracking!='') jQuery('#s2AjaxResponse').html(s2out.tracking);
                        jQuery('#s2AjaxResponse').append(s2out.response);
                    },
                    'json'
                );                 
            }
        },
        validateCoupon : function(coupon,plan_id,listing_id)
        {
            jQuery('#jr_couponMsgErr').fadeOut();           
            jQuery('#jr_couponCodeHidden').val('');
            jQuery.post(s2AjaxUri+'&url=paidlistings_orders/validateCoupon&tmpl=component&format=raw',
                {'data[PaidOrder][coupon_name]':coupon,'data[PaidOrder][plan_id]':plan_id,'data[PaidOrder][listing_id]':listing_id,'data[PaidOrder][order_amount]':jQuery('body').data('Order.price')},
                function(s2out){
                    if(s2out.invalid) {
                        jQuery('#jr_couponMsgErr').fadeIn();    
                    } else {
                        jQuery('#jr_couponCodeHidden').val(coupon);
                        for (var key in s2out ) {    
                            if(jQuery('#'+key)){ jQuery('#'+key).html(s2out[key]);}
                        }
                        jQuery('#jr_couponCode').val('');
                        jQuery('#jr_couponMsgSuccess').fadeIn(function(){setTimeout( function(){ 
                            jQuery('#jr_couponMsgSuccess').fadeOut("fast");}, 2000);
                        });                        
                    }
                },
                'json'
            );              
        },
        tosToggle : function(element)
        {
            tos = jQuery(element);
            if(tos.data('height')==undefined) tos.data('height',tos.height());
            var tos_h = tos.data('height');
            var tos_h2 = tos.height();
            tos.height(tos_h2 == tos_h ? (tos_h2*2) : tos_h);
        }
    },
    invoice: 
    {
        print : function(url)
        {
            window.open(
                url,
                'invoice',
                'location=no,status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=yes,resizable=yes,width=900,height=600,directories=no'
            ); 
        }
    }    
}

jQuery(document).ready(function() 
{
    if(jQuery(".jr_myAccount").length) jQuery(".jr_myAccount").find('.jr_tabs').tabs({ cache: true });
});
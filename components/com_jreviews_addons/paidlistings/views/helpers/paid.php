<?php
/**
 * PaidListings Addon for JReviews
 * Copyright (C) 2010 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
**/
defined( 'MVC_FRAMEWORK') or die( 'Direct Access to this location is not allowed.' );

class PaidHelper extends MyHelper    
{
    var $submit_plan_id = null;
    
    function getDurationPeriods($key = null)
    {
        $array = array(
            'days'=>__t("Day(s)",true),
            'weeks'=>__t("Week(s)",true),
            'months'=>__t("Month(s)",true),
            'years'=>__t("Year(s)",true),
            'never'=>__t("Never Expires",true),
        ); 
        return !is_null($key) ? $array[$key] : $array;
    }
    function getOrderStatus($status)
    {
        $array = array(
            'Incomplete'=>__t("Incomplete",true),
            'Pending'=>__t("Pending",true),
            'Processing'=>__t("Processing",true),
            'Complete'=>__t("Complete",true),
            'Cancelled'=>__t("Cancelled",true),
            'Fraud'=>__t("Fraud",true),
            'Failed'=>__t("Failed",true)
            );
        return $array[$status];
    }
    function getPaymentTypes($key = null)
    {
        $array = array(
            '0'=>__t("One Time Payment",true),
            '1'=>__t("Subscription",true),
            '2'=>__t("Free or Trial",true)
        );
        return !is_null($key) ? $array[$key] : $array;
    }
    function getPlanTypes($key = null)
    {
        $array = array(
            '0'=>__t("New Listing",true),
            '1'=>__t("Upgrade Listing",true)
        );
        return !is_null($key) ? $array[$key] : $array;
    } 
    function getVar($name,$listing)
    {     
        if(isset($listing['Paid']))
        {
            return Sanitize::getString($listing['Paid']['custom_vars'],$name);        
        }
        return false;
    }  
    function setSubmitPlanId($plan_id)
    {
        $this->submit_plan_id = $plan_id;
    }
    function planChecked($plan_id,$default)
    {
        if((!$this->submit_plan_id && $default))
        {
            return true;
        } 
        elseif($this->submit_plan_id && $this->submit_plan_id == $plan_id)
        {
            return true;
        }
        return false;
    } 

    function canOrder(&$listing)
    {
        $User = & cmsFramework::getUser();
        return $listing['Listing']['user_id'] == $User->id && isset($listing['PaidPlanCategory']) && !empty($listing['PaidPlanCategory']['cat_id']);   
    }
}
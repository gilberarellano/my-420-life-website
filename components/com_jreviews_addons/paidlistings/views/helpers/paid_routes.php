<?php
/**
 * PaidListings Addon for JReviews
 * Copyright (C) 2010 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
**/

defined( 'MVC_FRAMEWORK') or die( 'Direct Access to this location is not allowed.' );

class PaidRoutesHelper extends MyHelper
{    
    var $Html;
    
    var $routes = array(
        'myaccount'=>'index.php?option=com_jreviews&amp;Itemid=%s', 
        'order_complete'=>'index.php?option=com_jreviews&amp;url=paidlistings_orders/complete/%s',
        'category_plans'=>'index.php?option=com_jreviews&amp;url=paidlistings_plans/cat_id:%s',
        'listing_new_category'=>'index.php?option=com_jreviews&amp;Itemid=%s&amp;url=new-listing_s%s_c%s/plan_id:%s',
        'invoice'=>'index.php?option=com_jreviews&amp;Itemid=%s&amp;tmpl=component&amp;url=paidlistings_invoices/view/invoice:%s',        
        'unpaid_invoice'=>'index.php?option=com_jreviews&amp;Itemid=%s&amp;tmpl=component&amp;url=paidlistings_invoices/unpaid/order:%s'        
    );

    function __construct()
    {
        $this->Html = new HtmlHelper('jreviews');        
    }    
    
    function myaccount($attributes = array()) 
    {
        if(isset($attributes['title']))
        {
            $title = $attributes['title'];
            unset($attributes['title']);
        } else {
            $title = '';
            $attributes['return_url'] = true;
        }
        
        $params = array();
        if(isset($attributes['params']))
        {
            foreach($attributes['params'] AS $key=>$value)
            {
                $params[] = $key.':'.$value;
            }
            unset($attributes['params']);
        }
        $params = implode('/',$params);
        
        $menuModel = ClassRegistry::getClass('MenuModel');
        $menu_id = $menuModel->getMenuIdByAction(19);
        $url = sprintf($this->routes['myaccount'], $params . ($menu_id>0 ? $menu_id : '&amp;url=paidlistings/myaccount'));
        return $this->Html->sefLink($title,$url,$attributes);
    }   
    
    function categoryPlans($category,$attributes)
    {
        // Check if there's a menu to prevent duplicate urls
        $menuModel = ClassRegistry::getClass('MenuModel');
        $menu_id = '';//$menuModel->getMenuIdByAction(19);
        $url = sprintf($this->routes['category_plans'], $category['cat_id'], ($menu_id>0 ? '&amp;Itemid='.$menu_id : '&amp;url=paidlistings/myaccount'));
        return $this->Html->sefLink($category['title'],$url,$attributes);        
    }     
    
    function invoice($invoice_id)
    {
        $menuModel = ClassRegistry::getClass('MenuModel');
        $menu_id = $menuModel->getMenuIdByAction(19);
        $url = sprintf($this->routes['invoice'], $menu_id, $invoice_id);
        return cmsFramework::route($url);        
    }
    
    function unpaidInvoice($order_id)
    {
        $menuModel = ClassRegistry::getClass('MenuModel');
        $menu_id = $menuModel->getMenuIdByAction(19);
        $url = sprintf($this->routes['unpaid_invoice'], $menu_id, $order_id);
        return cmsFramework::route($url);        
    }

    function newListing($category,$plan_id) 
    {     
        $url = sprintf($this->routes['listing_new_category'],Sanitize::getInt($category['Category'],'menu_id') ? $category['Category']['menu_id'] : '',$category['Category']['section_id'],$category['Category']['cat_id'],$plan_id);
        return $this->Html->sefLink($title,$url,array('return_url'=>true));
    }    
    
    function orderComplete($paramsArray)
    {
        $params = array();
        // Check if there's a menu for this category to prevent duplicate urls
        if(isset($paramsArray['menu_id']))
        {
            $menu_id = (int) $paramsArray['menu_id'];
            unset($paramsArray['menu_id']);
        } else {
            $menuModel = ClassRegistry::getClass('MenuModel');
            $menu_id = $menuModel->getMenuIdByAction(19);
        }

        foreach($paramsArray AS $key=>$val)
        {
            $params[] = $key.':'.$val;                  
        }
        $url = sprintf($this->routes['order_complete'], implode('/',$params) . ($menu_id>0 ? '&amp;Itemid='.$menu_id : ''));
        return cmsFramework::route($url);
    }
    
    function getPaymentLink($listing,$attributes = array())
    {
//        prx($listing['Paid']);
//        prx($listing['PaidPlan']);
        if(isset($listing['Paid']['plans']['PlanType0']))
        {   
            $base_plan = current($listing['Paid']['plans']['PlanType0']);
            $base_plan_never_expires = $base_plan['order_never_expires'] ? true : false;
      
            // Get ids of available listing upgrade plans
            $available_upgrade_plan_ids = array();
            if(isset($listing['PaidPlan']['PlanType1']))
            {
                foreach($listing['PaidPlan']['PlanType1'] AS $payment_type=>$plans)
                {                                            
                    foreach($plans AS $plan_id=>$plan)
                    {                                                               
                        if($base_plan_never_expires || (!$base_plan_never_expires && $plan['plan_array']['duration_period']=='never')) // Base plan expires, so we remove expiring upgrades
                        {
                            $available_upgrade_plan_ids[] = $plan_id;                            
                        }
                    }
                }
            }            
            
            // Listing has an active upgrade plan - check if it is exclusive and abort if it is
            if(isset($listing['Paid']['plans']['PlanType1']))
            { 
                $upgrade = current($listing['Paid']['plans']['PlanType1']);
                if($upgrade['plan_upgrade_exclusive'])
                {  
                    return '';        
                }
            }
                           
            // Upgrade not exclusive - filter valid upgrades
            $applicable_upgrade_plan_ids = isset($listing['Paid']['plans']['PlanType1']) 
                ? 
                array_diff($available_upgrade_plan_ids,array_keys($listing['Paid']['plans']['PlanType1']))
                :
                $available_upgrade_plan_ids
                ;
                
            if(isset($listing['PaidPlan']['PlanType1']) && !empty($applicable_upgrade_plan_ids)) 
            {   // If there are upgrade plans different from the active ones, show upgrade link
                return $this->upgradeLink($listing, $attributes);
            }           
        }
        elseif(isset($listing['PaidPlan']['PlanType0']))
        {   // Listing without base plan and there are base plans available
            return $this->newLink($listing, $attributes);
        }
        
        return false;                
    }
    
    function upgradeLink($listing, $attributes)
    {
        $lazy_load_js = Sanitize::getBool($attributes,'lazy_load',false);
        $plan_id = Sanitize::getInt($attributes,'plan_id',0);
        $listing_id = $listing['Listing']['listing_id'];
        $listing_title = addslashes($listing['Listing']['title']);
        if($lazy_load_js)
        {
            return 
                "<span class=\"jrIcon jrIconCart\"></span> <a href=\"javascript:void(0);\" onclick=\"jr_paidLoadScript(function(){JRPaid.OrderForm.dialog({'plan_type':1,'listing_id':{$listing_id},'plan_id':{$plan_id},'title':'{$listing_title}'})});return false;\">"
                . __t("Order Upgrade",true)
                . "</a>";
        } 

        return 
            "<span class=\"jrIcon jrIconCart\"></span> <a href=\"javascript:void(0);\" onclick=\"JRPaid.OrderForm.dialog({'plan_type':1,'listing_id':{$listing_id},'plan_id':{$plan_id},'title':'{$listing_title}'});return false;\">"
            . __t("Order Upgrade",true)
            . "</a>";

/*        return 
            "<button class=\"jr_button ui-state-default ui-priority-primary ui-corner-all\" onclick=\"JRPaid.OrderForm.dialog({'plan_type':1,'listing_id':{$listing_id},'plan_id':0,'title':'{$listing_title}'});return false;\">"
            . __t("Order Upgrade",true)
            . "</button>";*/
    }
    
    function newLink($listing, $attributes)
    {                    
        $lazy_load_js = Sanitize::getBool($attributes,'lazy_load',false);
        $plan_id = Sanitize::getInt($attributes,'plan_id',0);
        $plan_type = Sanitize::getInt($attributes,'plan_type',0);
        $order_id = Sanitize::getInt($attributes,'order_id',0);
        $renewal = Sanitize::getInt($attributes,'renewal',0);
        $link_text = Sanitize::getString($attributes,'link_text');
        !$link_text and $link_text = __t("Order Plan",true); 
        $listing_id = $listing['Listing']['listing_id'];
        $listing_title = $listing['Listing']['title'];
        $listing_title = addslashes( str_replace('"', "&quot", $listing_title) );
        if($lazy_load_js)
        {                                                   
            return 
                "<span class=\"jrIcon jrIconCart\"></span> <a href=\"javascript:void(0);\" onclick=\"jr_paidLoadScript(function(){JRPaid.OrderForm.dialog({'renewal':{$renewal},'order_id':{$order_id},'plan_type':{$plan_type},'listing_id':{$listing_id},'plan_id':{$plan_id},'title':'{$listing_title}'})});return false;\">"
                . $link_text
                ."</a>";
        } 

        return 
            "<span class=\"jrIcon jrIconCart\"></span> <a href=\"javascript:void(0);\" onclick=\"JRPaid.OrderForm.dialog({'renewal':{$renewal},'order_id':{$order_id},'plan_type':{$plan_type},'listing_id':{$listing_id},'plan_id':{$plan_id},'title':'{$listing_title}'});return false;\">"
            . $link_text
            . "</a>";

/*        return 
            "<button class=\"jr_button ui-state-default ui-priority-primary ui-corner-all\" onclick=\"JRPaid.OrderForm.dialog({'plan_type':0,'listing_id':{$listing_id},'plan_id':0,'title':'{$listing_title}'});return false;\">"
            . __t("Order Plan",true)
            . "</button>";*/
    }
    
}
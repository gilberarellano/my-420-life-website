<?php
/**
 * PaidListings Addon for JReviews
 * Copyright (C) 2010 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
**/
//error_reporting(E_ALL);ini_set('display_errors','On');
defined( 'MVC_FRAMEWORK') or die( 'Direct Access to this location is not allowed.' );

class AdminPaidlistingsHandlersController extends MyController 
{
    var $uses = array('paid_handler');
    
    var $helpers = array('html','form');
                     
    var $components = array('config');        
  
    var $autoRender = false;

    var $autoLayout = false;
           
    /**
    * Controller specific vars
    *           
    */
           
    function beforeFilter()
    {
        parent::beforeFilter();
    }

    function index()
    {          
        // Get list of handler plugins
        $Folder = new Folder();
        $Folder->path = PATH_ROOT . DS . 'components' . DS . 'com_jreviews_addons' . DS . 'paidlistings' . DS . 'controllers' . DS . 'components' . DS;
        $handler_files = next($Folder->read(true,true,true));

        foreach($handler_files AS $handler)
        {
            $parts = pathinfo($handler);
            if(!strstr($parts['basename'],'index.html') && strstr($parts['basename'],'handler_'))
            {
                $handler_plugins[$parts['basename']] = $parts['basename'];                
            }
        }
        $handlers = $this->PaidHandler->findAll(array('order'=>array('PaidHandler.ordering')));

        foreach($handlers AS $handler)
        {
            unset($handler_plugins[$handler['PaidHandler']['plugin_file'].'.php']);
        }
        
        $this->set(array(
            'handlers'=>$handlers,
            'handler_plugins'=>$handler_plugins
        ));
        
        return $this->render('paidlistings_handlers','index');
    }    
    
    /**
    * Creates a new handler instances using a handler plugin from the add-on's controllers/components folder
    * 
    */
    function _create()
    {
        $this->data['PaidHandler']['plugin_file'] = $this->data['PaidHandler']['theme_file'] =  str_replace('.php','',Sanitize::getString($this->data['PaidHandler'],'plugin_file'));
        $this->PaidHandler->store($this->data);
    }
    
    function _edit()
    {
        $handler_id = Sanitize::getInt($this->params,'id');
        if($handler_id)
        {
            $handler = $this->PaidHandler->findRow(array('conditions'=>'PaidHandler.handler_id = ' . $handler_id));
        }
        $this->set(array(
            'handler'=>$handler,           
            'handler_theme'=>rtrim($handler['PaidHandler']['theme_file']),    
        ));
        return $this->render('paidlistings_handlers','handler_theme');
    }  
    
    function _changeOrder() 
    {
        $row_id = Sanitize::getInt($this->params,'entry_id');
        $inc = Sanitize::getVar($this->params,'direction');
        
        // Move row
        $handler = $this->PaidHandler->findRow(array('conditions'=>array('handler_id = ' . $row_id)));

        $this->PaidHandler->Result = $handler;

        $this->PaidHandler->move( $inc );
    
        return $this->index();
    }
    
    function _saveOrder() 
    {
        $response = array();

        $cid = Sanitize::getVar($this->data,'cid',array()); 
        $total = count( $cid );
        $order = Sanitize::getVar($this->data,'order',array());
            
        for( $i=0; $i < $total; $i++ ) 
        {
            $this->data['PaidHandler']['handler_id'] = $cid[$i];
            $this->data['PaidHandler']['ordering'] = $order[$i];
            if (!$error = $this->PaidHandler->store($this->data)) {
                $response[] = "s2Alert('$error');";
                return $this->ajaxResponse($response);
            }
        }
    
        $this->Group->PaidHandler();
    }        
    
    function _delete()
    {        
        $id = (int) $this->data['entry_id'];
        $this->response[] = "jreviews_admin.dialog.close();";
        $this->response[] = "jreviews_admin.tools.removeRow('handler{$id}');";
        $this->PaidHandler->delete('handler_id',$id);
        return $this->ajaxResponse($this->response);    
    }
    
    function _save()
    {
        if(isset($this->data['__raw']['PaidHandler']['settings']['offline']))
        {
            $this->data['PaidHandler']['settings']['offline'] = $this->data['__raw']['PaidHandler']['settings']['offline'];
        }
        $this->data['PaidHandler']['settings'] = json_encode($this->data['PaidHandler']['settings']);
        $this->PaidHandler->store($this->data);
        $action = 'success';
        $page = $this->index();
        $row_id = "handler".$this->data['PaidHandler']['handler_id']; 
        return $this->ajaxResponse(compact('action','page','row_id'),false);
    }
}

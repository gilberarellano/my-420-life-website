<?php
/**
 * PaidListings Addon for JReviews
 * Copyright (C) 2010 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
**/

defined( 'MVC_FRAMEWORK') or die( 'Direct Access to this location is not allowed.' );

class AdminPaidlistingsInstallController extends MyController {
    
    var $components = array('config');
    var $autoLayout = false;
    var $autoRender = false;

    function install()
    {   
        $db = cmsFramework::getDB();
                
        $old_version = Sanitize::getString($this->Config,'paid.version');
        ob_start();
        include(dirname(dirname(__FILE__)) . DS . 'paidlistings.info.php');
        ob_end_clean();
        $new_version = $_addon['version'];
        $version_parts = explode('.',$new_version);
        $new_build = array_pop($version_parts);
        $tables = $db->getTableList();
        $dbprefix = cmsFramework::getConfig('dbprefix');
        $old_build = 0;
        if(is_array($tables) && in_array($dbprefix . 'jreviews_paid_plans',array_values($tables))) 
        {
            if($old_version!='') {
                $version_parts = explode('.',$old_version);
                $old_build = array_pop($version_parts);
            }
            
            if(isset($this->params) && Sanitize::getBool($this->params,'sql')) 
            {
                $old_build = 0;
            }           
             
//            prx($old_build . '<br/>' . $new_build) ;
            
            if($new_build > $old_build) 
            {
                $result = true;
                $i = $old_build+1;
                for($i = $old_build+1; $i<=$new_build; $i++) 
                {
                    // Run sql updates
                    $sql_file = dirname(dirname(__FILE__)) .DS . 'upgrades' . DS . 'upgrade_build'.$i.'.sql';
                    file_exists($sql_file) and $result = $result && $this->__parseMysqlDump($sql_file,$dbprefix);
                    
                    // Run php updates
                    $php_file = dirname(dirname(__FILE__)) .DS . 'upgrades' . DS . 'upgrade_build'.$i.'.php';
                    file_exists($php_file) and include($php_file);
                }
                
                if($result)
                { 
                    $this->Config->store((object) array('paid.version'=>$new_version));              
                    return '<div style="color:green;">The add-on was successfully installed/updated.</div>';
                } 
                
                return '<div style="color:red;">There was a problem updating the database.</div>';
            }
            
        } else {

            # It's a clean install so we use the complete sql file
            $sql_file = dirname(dirname(__FILE__)) . DS . 'upgrades' . DS . 'paidlistings.sql';

            if($this->__parseMysqlDump($sql_file,$dbprefix))
            {     
                $this->Config->store((object) array('paid.version'=>$new_version));              
                return '<div style="color:green;">The add-on was successfully installed/updated.</div>';
            } else {
                return '<div style="color:red;">There was a problem creating updating the database.</div>';
            }        
        }
    }
}
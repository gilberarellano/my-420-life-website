<?php
/**
 * PaidListings Addon for JReviews
 * Copyright (C) 2010 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
**/
//error_reporting(E_ALL);ini_set('display_errors','On');
defined( 'MVC_FRAMEWORK') or die( 'Direct Access to this location is not allowed.' );

class AdminPaidlistingsEmailsController extends MyController 
{
    var $uses = array('paid_email');
    
    var $helpers = array('html','form');
                     
    var $components = array('config');        
  
    var $autoRender = false;

    var $autoLayout = false;
           
           
    function beforeFilter()
    {  
        parent::beforeFilter();
    }

    function index()
    {        
        $admin_emails = $this->PaidEmail->findAll(array('conditions'=>'PaidEmail.trigger LIKE "admin_%"'));
        $user_emails = $this->PaidEmail->findAll(array('conditions'=>'PaidEmail.trigger LIKE "user_%"'));
        $this->set(array(
            'admins'=>$admin_emails
            ,'users'=>$user_emails
        ));
        return $this->render('paidlistings_emails','index');
    }
    
    function _edit()
    {
        $email_id = Sanitize::getInt($this->data['PaidEmail'],'email_id');
        $email = $this->PaidEmail->findRow(array(
            'conditions'=>array('PaidEmail.email_id = ' . $email_id)
        ));
        $this->set('email',$email);
        
        return $this->render('paidlistings_emails','edit');
    } 
    
    function _save()
    {
        if(isset($this->data['emails']))
        {
            foreach($this->data['emails'] AS $key=>$data)
            {
                $this->PaidEmail->store($data);
            }            
            $action = 'success';
            $response = array();
            return $this->ajaxResponse($response);
        } 
        else
        {
            $action = 'success';
            $page = $this->index();
            $row_id = "email".$this->data['PaidEmail']['email_id']; 
            $response = array();
            $response[] = "jQuery('#jr_formDialog').dialog('close').dialog('destroy');";
            $response[] = "jQuery('.jr_dialog').remove();";
            $this->PaidEmail->store($this->data['__raw']);
            return $this->ajaxResponse(compact('action','page','row_id','response'),false);        
        }
    }      
}

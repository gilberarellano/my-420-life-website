<?php
/**
 * PaidListings Addon for JReviews
 * Copyright (C) 2010 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
**/
//error_reporting(E_ALL);ini_set('display_errors','On');
defined( 'MVC_FRAMEWORK') or die( 'Direct Access to this location is not allowed.' );

class AdminPaidlistingsTxnController extends MyController 
{
    var $uses = array('menu','paid_order','paid_txn_log','paid_handler','paid_plan');
    
    var $helpers = array('html','form','time','paginator');
                     
    var $components = array('config');        
  
    var $autoRender = false;

    var $autoLayout = false;
           
/*    function beforeFilter()
    {
        $this->Access->init($this->Config);    
        parent::beforeFilter();
    }*/

    function index()
    {     
        if(Sanitize::getBool($this->data,'search'))
        {     
            $this->action = "search";
            return $this->search();
        }
                
       // For search/filtering
        $plans = $this->PaidPlan->getPlanList();
        $handler_list = $this->PaidHandler->getList();
       
        $this->getAllTxn();
        
        $this->set(array(
            'plans'=>$plans,
            'handler_list'=>$handler_list    
        ));
        
        return $this->render('paidlistings_txn','index');       
    }
    
    function getAllTxn($conditions = array())
    {
        $total = 0;
        $joins = array();
 
        $joins = array(
            "LEFT JOIN #__jreviews_paid_orders AS PaidOrder ON PaidOrder.order_id = PaidTxnLog.order_id",
            'INNER JOIN #__content AS Listing ON Listing.id = PaidOrder.listing_id',        
            "LEFT JOIN #__jreviews_paid_handlers AS PaidHandler ON PaidTxnLog.handler_id = PaidHandler.handler_id"
        );
        
        $conditions[] = 'PaidTxnLog.txn_success = 1';
        $conditions[] = 'PaidOrder.handler_id > 0';

        $txns = $this->PaidTxnLog->findAll(array(
            'fields'=>array(
                'Listing.title AS `Listing.title`',
                'PaidOrder.user_id AS `PaidOrder.user_id`',
                'PaidOrder.plan_info AS `PaidOrder.plan_info`',
                'PaidOrder.listing_info AS `PaidOrder.listing_info`',
                'PaidOrder.order_amount AS `PaidOrder.order_amount`', 
                'PaidOrder.coupon_name AS `PaidOrder.coupon_name`',
                'PaidHandler.name AS `PaidHandler.name`' 
            ),
            'conditions'=>$conditions,
            'joins'=>$joins,
            'limit'=>$this->limit,
            'offset'=>$this->offset,
            'order'=>array('PaidTxnLog.log_id DESC')
        ));

        !empty($txns) and $total = $this->PaidTxnLog->findCount(array('conditions'=>$conditions,'joins'=>$joins));
       
        $this->set(array(
            'total'=>$total,
            'txns'=>$txns
        ));
        
        return $total;
    }
    
    function getOrderTxn()
    {     
        $conditions = $joins = array();
        $order_id = Sanitize::getInt($this->data,'order_id');
        $listing_id = Sanitize::getInt($this->data,'listing_id');
        $order_id and $conditions[] = 'PaidTxnLog.order_id = ' . $order_id;
        $listing_id and $conditions[] = 'PaidOrder.listing_id = ' . $listing_id;
        $listing_id and $joins = array("LEFT JOIN #__jreviews_paid_orders AS PaidOrder ON PaidOrder.order_id = PaidTxnLog.order_id");
        $txns = $this->PaidTxnLog->findAll(array(
            'conditions'=>$conditions,
            'joins'=>$joins,
            'order'=>array('PaidTxnLog.log_id DESC')
        ));
        
        $this->set('txns',$txns);
        return $this->render('paidlistings_txn','order_txn');
    }
    
    function _delete()
    {
        $response = array();
        $txn_log_id = Sanitize::getInt($this->data,'entry_id');
        $this->PaidTxnLog->delete('log_id',$txn_log_id);
        $response[] = "jreviews_admin.tools.removeRow('txn_log{$txn_log_id}');";
        return $this->ajaxResponse($response);                                
    }   
    
    function search()
    {            
        $conditions = array();
        if($listing_title = Sanitize::getString($this->data['Listing'],'title'))
        {
            $query = "
                SELECT 
                    Listing.id 
                FROM 
                    #__content AS Listing
                INNER JOIN
                    #__jreviews_paid_orders AS PaidOrder ON Listing.id = PaidOrder.listing_id 
                WHERE
                    Listing.title LIKE " . $this->quoteLike($listing_title) . "
            ";
            $this->_db->setQuery($query);

            if($listing_ids = $this->_db->loadResultArray())
            {    
                $this->data['PaidOrder']['listing_id'] = $listing_ids;
            }
            if(empty($listing_ids)) return '';
        }
         
       if(isset($this->data['PaidOrder']['order_id'])) $this->data['PaidOrder']['order_id'] = (int) $this->data['PaidOrder']['order_id'];
        foreach($this->data['PaidOrder'] AS $key=>$value)
        {
            if($value=='') continue;
            if(is_array($value))
            {
                $value = array_filter($value);
                if(empty($value)) continue;
                
                if(count($value)==2 && !is_numeric($value[0]))
                {
                    $conditions[] = "PaidOrder.{$key} BETWEEN {$this->Quote($value[0])} AND {$this->Quote($value[1])}";
                } elseif(count($value) > 1) {
                    $conditions[] = "PaidOrder.{$key} IN ( ". implode(",",$value) .")";
                } else {
                    $conditions[] = "PaidOrder.{$key} = {$this->Quote($value[0])}";
                }
            } 
            else 
            {
                $conditions[] = "PaidOrder.{$key} = {$this->Quote($value)}";    
            }   
        }

        $total = $this->getAllTxn($conditions);
                    
        if($total > 0)
        {       
            return $this->render('paidlistings_txn','txn_table');
        } else {
            return '';
        }
    }        
}

ALTER TABLE `#__jreviews_paid_plans` ADD INDEX `plan_state` (  `plan_state` );
ALTER TABLE `#__jreviews_paid_txn_logs` ADD INDEX `txn_success` ( `txn_success` ); 
ALTER TABLE `#__jreviews_paid_txn_logs` ADD INDEX `txn_date` ( `txn_date` ); 
ALTER TABLE `#__jreviews_paid_orders` CHANGE `order_amount` `order_amount` DECIMAL( 8, 2 ) NOT NULL;
ALTER TABLE `#__jreviews_paid_orders` ADD `order_discount` DECIMAL( 8, 2 ) NOT NULL AFTER  `order_amount`;
ALTER TABLE `#__jreviews_paid_orders` ADD `order_tax` DECIMAL( 8, 2 ) NOT NULL AFTER  `order_discount`;
ALTER TABLE `#__jreviews_paid_orders` ADD INDEX `order_created` (  `order_created` );
ALTER TABLE `#__jreviews_paid_orders` ADD INDEX `order_renewal` (  `order_renewal` );
ALTER TABLE `#__jreviews_paid_orders` ADD INDEX `order_expires` (  `order_expires` );
ALTER TABLE `#__jreviews_paid_orders` ADD INDEX `order_never_expires` (  `order_never_expires` );
ALTER TABLE `#__jreviews_paid_orders` ADD INDEX `order_active` (  `order_active` );
ALTER TABLE `#__jreviews_paid_orders` ADD INDEX `order_notify1` (  `order_notify1` );
ALTER TABLE `#__jreviews_paid_orders` ADD INDEX `order_notify2` (  `order_notify2` );
ALTER TABLE `#__jreviews_paid_orders` CHANGE  `order_renewal`  `order_renewal` DATE NOT NULL;
ALTER TABLE `#__jreviews_paid_orders` CHANGE  `order_expires`  `order_expires` DATE NOT NULL;



CREATE TABLE IF NOT EXISTS `#__jreviews_paid_coupons` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_name` varchar(50) NOT NULL,
  `coupon_discount` int(3) NOT NULL,
  `coupon_starts` datetime NOT NULL,
  `coupon_ends` datetime NOT NULL,
  `coupon_users` text NOT NULL,
  `coupon_plans` text NOT NULL,
  `coupon_categories` text NOT NULL,
  `coupon_count` int(11) NOT NULL,
  `coupon_count_type` enum('user','global') NOT NULL,
  `coupon_renewals_only` tinyint(1) NOT NULL,
  `coupon_state` tinyint(1) NOT NULL,
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `coupon_name` (`coupon_name`)
) ENGINE=MyISAM;

ALTER TABLE `#__jreviews_paid_orders` ADD `coupon_name` VARCHAR( 50 ) NOT NULL AFTER `order_amount`; 
ALTER TABLE  `#__jreviews_paid_orders` ADD  `plan_updated` DATETIME NOT NULL AFTER  `plan_info`;
ALTER TABLE  `#__jreviews_paid_orders` ADD INDEX `plan_updated` (  `plan_updated` );
ALTER TABLE  `#__jreviews_paid_plans` ADD  `plan_updated` DATETIME NOT NULL;
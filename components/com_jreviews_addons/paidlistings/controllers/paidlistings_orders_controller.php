<?php
/**
 * PaidListings Addon for JReviews
 * Copyright (C) 2010 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
**/
defined( 'MVC_FRAMEWORK') or die( 'Direct Access to this location is not allowed.' );
   
App::import('Model','paid_txn_log','jreviews');
   
class PaidlistingsOrdersController extends MyController                                     
{
    var $uses = array('menu','paid_plan','paid_order','paid_plan_category','paid_handler','paid_order','paid_email','paid_coupon','article');
    var $components = array('config','access','everywhere','paidlistings_notifications');
    var $helpers = array('html','form','assets','paginator','time','paid','routes','paid_routes');
    var $autoRender = false;
    var $autoLayout = false;
    var $couponProcess = false;

    function beforeFilter() 
    {           
        $this->PaidOrder = ClassRegistry::getClass('PaidOrderModel');
        # Call beforeFilter of MyController parent class
        parent::beforeFilter();
    }

    // Need to return object by reference for PHP4
/*    function &getObserverModel() {
        return $this->Listing;
    }    */
        
    // Need to return object by reference for PHP4
    function &getPluginModel() {
        return $this->Listing;  // Listing returned because plugins will work on this model, not PaidOrders
    }    

    // Need to return object by reference for PHP4
    function &getNotifyModel() {
        $PaidOrder = ClassRegistry::getClass('PaidOrderModel');
        return $PaidOrder;
    }
      
    function afterFilter()
    {
        if(in_array($this->action,array('process','_process','complete')))
        {                         
            $this->assets = array_merge(
                $this->assets,
                array('css'=>array('jq.ui.core'))
            );    
            parent::afterFilter();
        }
    }
          
    function index()
    {  
        if(!$this->_user->id) {
            echo $this->render('elements','login');
            return;
        }         

        $conditions = array(
                'PaidOrder.user_id = ' . (int) $this->_user->id,
                'PaidOrder.order_status != "Incomplete"'
            );
            
        $fields = array(
                'DATEDIFF(DATE(PaidOrder.order_expires),CURDATE())' . ' AS `PaidOrder.daysToExpiration`',
                'PaidOrder.listing_id AS `Listing.listing_id`',
                'Listing.title AS `Listing.title`',
                'Listing.catid AS `Listing.cat_id`',
                'Listing.alias AS `Listing.slug`',
                'Category.alias AS `Category.slug`'                
            );

        $this->cmsVersion == CMS_JOOMLA15 and $fields[] = 'Listing.sectionid AS `Listing.section_id`';                  
        
        $orders = $this->PaidOrder->findAll(array(
            'fields'=>$fields,            
            'joins'=>array(
                'LEFT JOIN #__content AS Listing ON Listing.id = PaidOrder.listing_id',
                'LEFT JOIN #__categories AS Category ON Category.id = Listing.catid'
            ),
            'conditions'=>$conditions,
            'order'=>array('PaidOrder.order_created DESC'),
            'limit'=>$this->limit,
            'offset'=>$this->offset            
        ));

        # Add Menu ID info for each row (Itemid)
        $Menu = ClassRegistry::getClass('MenuModel');
        $Routes = ClassRegistry::getClass('RoutesHelper');
        foreach($orders AS $key=>$order)
        {                                                                               
            if($this->cmsVersion == CMS_JOOMLA15)
            {
                $orders[$key]['Listing']['menu_id'] = $Menu->getCategory($order['Listing']['cat_id'],$order['Listing']['section_id'],null,$order['Listing']['listing_id']);
            }
            else
            {
                $menu_id_array = array(
                    'cat_id'=>$order['Listing']['cat_id'],
                    'dir_id'=>null,
                    'listing'=>$order['Listing']['listing_id']
                );
                $orders[$key]['Listing']['menu_id'] = $Menu->getCategory($menu_id_array);
            }
            $orders[$key]['Listing']['url'] = $Routes->content('',$orders[$key],array('return_url'=>true));   
        }

        $total = $this->PaidOrder->findCount(array('conditions'=>$conditions));
        
        $this->set(array(
            'orders'=>$orders,
            'total'=>$total
        ));
        echo $this->render('paidlistings', 'paidlistings_orders');
    }
                
    /**
    * Displays available plans for individual listings allowing users to place and order
    * 
    */
    function _getOrderForm()    
    {   
        if(!$this->_user->id) {
            cmsFramework::noAccess();
            return;                    
        }
        
        $listing_id = Sanitize::getInt($this->data['Listing'],'listing_id');
        $listing_title = Sanitize::getString($this->data,'listing_title');
        $plan_type = Sanitize::getVar($this->data,'plan_type',null);
        $renewal = Sanitize::getInt($this->data,'renewal');
        $plan_id = Sanitize::getInt($this->data,'plan_id');
        $order_id = Sanitize::getInt($this->data,'order_id');

        // Check if listing has pending orders to avoid creating a duplicate
        $order_conditions = array(
            "PaidOrder.plan_type = " . $plan_type,
            "PaidOrder.listing_id = " . $listing_id,
            "DATE(PaidOrder.order_expires) > CURDATE()",
            "PaidOrder.order_status = 'Incomplete'"
        );
        $renewal and $order_id and $order_conditions = array("PaidOrder.order_id = " . $order_id);
        $order = $this->PaidOrder->findRow(array('conditions'=>$order_conditions));
          
        // If upgrade plans, then check if listing has a base plan that never expires. Otherwise don't show subscription plans
        $plans = $this->PaidPlan->getValidPlans($listing_id,$plan_type,$renewal);

        // If loaded fater the listing form is submitted, remove the hidden plans
        if(Sanitize::getString($this->data,'referrer') == 'create')
        {
            foreach($plans AS $key=>$plan)
            {
                if(isset($plan['plan_array']) && $plan['plan_array']['submit_form']) unset($plans[$key]);
            }    
        }
        
        if($renewal){ // Remove subscription and free plans from renewals page. Subscriptions are processed automatically
            unset($plans['PaidPlan']['PlanType0']['PaymentType1'],$plans['PaidPlan']['PlanType0']['PaymentType2'],$plans['PaidPlan']['PlanType1']['PaymentType1'],$plans['PaidPlan']['PlanType1']['PaymentType2']);
            // Use line below instead to allow renewals using free plans...
//            unset($plans['PaidPlan']['PlanType0']['PaymentType1'],$plans['PaidPlan']['PlanType1']['PaymentType1']);
        }
        
        $handlers_single = $this->PaidHandler->findAll(array(
            'conditions'=>array(
                'PaidHandler.state > 0'
            )
        ));
        $handlers_subs = $this->PaidHandler->findAll(array(
            'conditions'=>array(
                'PaidHandler.state > 0',
                'PaidHandler.subscriptions = 1'
            )
        ));
        
        # Terms of Service
        if(Sanitize::getInt($this->Config,'paid.tos') 
            &&
            $tos_id = Sanitize::getInt($this->Config,'paid.tos_articleid')
        ) {
            if($this->cmsVersion!=CMS_JOOMLA15) {
                unset($this->Article->fields['section'],$this->Article->joins['section']);
            }
            
            $tos_article = $this->Article->findRow(array(
                'conditions'=>array
                (
                    'Article.id = ' . $tos_id
                )
            ));
            $this->set('tos_article',$tos_article);      
        }
        
		# Enable / Disable handlers (points)
		foreach($handlers_single as &$handler) {
			$handler_class = Inflector::camelize($handler['PaidHandler']['plugin_file']).'Component';
			App::import('Component',$handler['PaidHandler']['plugin_file']);
			$Handler = new $handler_class();
			$Handler->startup($this);
			if(method_exists($handler_class, 'getPointBalance')) {
				$balance = $Handler->getPointBalance($handler, $this->_user->id);
				$handler['balance'] = $balance;
			}
		}
		
        $this->set(array(
            'plan_type'=>$plan_type,
            'renewal'=>$renewal,
            'order_id'=>$order_id,
            'listing_id'=>$listing_id,
            'listing_title'=>$listing_title,
            'order'=>$order,
            'plans'=>$plans,
            'sel_plan_id'=>$plan_id,
            'handlers_single'=>$handlers_single,
            'handlers_subs'=>$handlers_subs
        ));
        
        return $this->render('paidlistings','paidlistings_order_form');         
    }
                    
    function _submit()
    {   
        if(!$this->_user->id) {
            cmsFramework::noAccess();
            return;                    
        }
        
        $this->couponProcess = true; // Forces validation to return the discounted price instead of JSON      
        $listing_id = Sanitize::getInt($this->data['PaidOrder'],'listing_id');
        $plan_id = Sanitize::getInt($this->data['PaidOrder'],'plan_id');
        $handler_id = Sanitize::getInt($this->data['PaidOrder'],'handler_id',0);
        $order_id = Sanitize::getInt($this->data['PaidOrder'],'order_id',0);
        $coupon = Sanitize::getString($this->data['PaidOrder'],'coupon_name');
        $track = '';
        if(!$plan = $this->PaidPlan->findRow(array('conditions'=>array('PaidPlan.plan_id = ' . $plan_id))))
        {
            return; // Do something on error
        }
        
        $this->data['PaidOrder']['order_amount'] = $plan['PaidPlan']['plan_price']; // Required for coupon validation
                        
        # Free plan limit validation
        if($plan['PaidPlan']['payment_type'] == 2 && Sanitize::getInt($plan['PaidPlan']['plan_array'],'trial_limit') > 0)
        {
            $trial_limit = $plan['PaidPlan']['plan_array']['trial_limit'];
            // Find the number of existing Complete orders with the same plan
            $used_trials = $this->PaidOrder->findCount(array(
                'conditions'=>array(
                    "PaidOrder.user_id = " . $this->_user->id,
                    "PaidOrder.plan_id = " . $plan_id,
                    "PaidOrder.order_status = 'Complete'"
                )
            ));
           if($used_trials >= $trial_limit)
           {
                $response = array("jQuery('#jr_paymentForm').find('.jr_button').removeAttr('disabled').removeClass('ui-state-disabled').addClass('ui-state-default');jQuery('#jr_payStep3Msg3').show();"); 
                return $this->ajaxResponse($response);                    
           }
        }
        
        # Find active orders for the same listing and plan type
        $curr_order_conditions = array(
            "PaidOrder.listing_id = " . $listing_id, 
            "(DATE(PaidOrder.order_expires) > CURDATE() OR PaidOrder.order_never_expires = 1)",
            "PaidOrder.plan_type = " . $plan['PaidPlan']['plan_type']
        );
        $order_id and $curr_order_conditions[] = "PaidOrder.order_id = " . $order_id;
        $curr_order = $this->PaidOrder->findRow(array('conditions'=>$curr_order_conditions));
        $handler = $this->PaidHandler->findRow(array(
            'conditions'=>array('PaidHandler.handler_id = ' . $handler_id)
        ));
        $listing = $this->Listing->findRow(array(
            'conditions'=>array('Listing.id = ' . $listing_id)
        ),array('afterFind'));
         
        # Check if plan is valid for this listing
        $renewal = in_array($curr_order['PaidOrder']['payment_type'],array(0,2)) /* Free or Single payment plans */
                && $curr_order['PaidOrder']['order_status'] == "Complete" 
                && $curr_order['PaidOrder']['order_active'];
        
        if(!$this->PaidPlan->validatePlan($listing,$plan,$renewal))
        {          
            $response = array("jQuery('#jr_paymentForm').find('.jr_button').removeAttr('disabled').removeClass('ui-state-disabled').addClass('ui-state-default');jQuery('#jr_payStep3Msg2').show();"); 
            return $this->ajaxResponse($response);                    
        }

        if(($handler || $plan['PaidPlan']['payment_type'] == 2) && $plan && $listing)
        { 
            $discounted_price = $this->validateCoupon(true);

            // Create new order array
            $order = $this->PaidOrder->makeOrder($plan,$listing,array('handler_id'=>$handler_id,'discounted_price'=>$discounted_price,'coupon'=>$coupon,'tax_rate'=>Sanitize::getVar($this->Config,'paid.tax',0)/100));

            // For incomplete orders
            $curr_order and !empty($curr_order['PaidOrder']['order_id']) and $curr_order['PaidOrder']['order_status'] == 'Incomplete' and $order['PaidOrder']['order_id'] = $curr_order['PaidOrder']['order_id']; 

            // For renewals 
            if($order_id && $curr_order && $renewal)
            {                          
                $order['PaidOrder']['order_renewal'] = $curr_order['PaidOrder']['order_expires'];
                $order['PaidOrder']['order_expires'] = $this->PaidOrder->getExpirationDate($plan['PaidPlan']['plan_array']['duration_period'],$plan['PaidPlan']['plan_array']['duration_number'], $curr_order['PaidOrder']['order_expires']);
            }            

            if($this->PaidOrder->store($order) && $order_id)
            {
                // Add the order_id_renewal to the order that was renewed so the expiring soon message is no longer displayed
                $curr_order['PaidOrder']['order_id_renewal'] = $order['PaidOrder']['order_id'];
                $this->PaidOrder->store($curr_order);                
            }  
         
         /**
           * Tracking code
           */   
           if($track = Sanitize::getVar($this->Config,'paid.track_order_submit','')) 
           {
                $track = PaidListingsComponent::trackingReplacements($track, $order);
                $track = html_entity_decode($track,ENT_QUOTES,cmsFramework::getCharset());
           } 
                          
            # Process free or zero amount (discounted) plans
            if($discounted_price === 0 || $plan['PaidPlan']['payment_type'] == 2)
            {          
                if($this->PaidListings->processFreeOrder($order))
                {                                      
                    $PaidRoutes = ClassRegistry::getClass('PaidRoutesHelper');
                    $url = str_replace('&amp;','&',$PaidRoutes->myaccount());
                    $response[] = "window.location.href = '{$url}';";
                    return $this->ajaxResponse($response,true,array('tracking'=>Sanitize::stripWhitespace($track)));        
                } else {
                    // problem processing the free order
                }
            }
 
            $handler_class = Inflector::camelize($handler['PaidHandler']['plugin_file']).'Component';
            App::import('Component',$handler['PaidHandler']['plugin_file']);
            $Handler = new $handler_class();
            $Handler->startup($this);

           $response = $Handler->submit($handler, $plan, $listing, $order); 
           return $this->ajaxResponse($response,true,array('tracking'=>$track));
        }
        
        $response = array("jQuery('#jr_payStep3Msg').show();"); 
        return $this->ajaxResponse($response);          
    }    
    
    function complete()
    {     
        // If it's a post request and the handler id is set
        if(!empty($this->params['form'])) 
        {
            $this->action = '_process';
            return $this->_process();
        }
        
        // For offline payments and other handlers that return via GET instead of POST
        if($order_id = Sanitize::getInt($this->params,'order_id'))
        {     
            $PaidOrder = ClassRegistry::getClass('PaidOrderModel');
            $order = $PaidOrder->findRow(array('conditions'=>array("PaidOrder.order_id = " . $order_id)));
            $handler = $this->PaidHandler->findRow(array('conditions'=>array('PaidHandler.handler_id = ' . $order['PaidOrder']['handler_id'])));
            
            /**
            * Special case for Paypal subscriptions which don't post back the transaction variables
            */
            Sanitize::getString($this->params,'auth') and $order['PaidOrder']['order_status'] != 'Complete' and $order['PaidOrder']['order_status'] = "Processing";            
            
            /**
            * Special case for 2Checkout "Header Redirect" which returns via GET without sending IPN */
            if(Sanitize::getString($this->params,'sid')  && Sanitize::getString($this->params,'key'))
            {
                $this->action = '_process';
                $this->params['form'] = $this->params;
                return $this->_process();                
            }
            
            $this->set(array('order'=>$order,'handler'=>$handler));
            return $this->render('paidlistings','paidlistings_order_complete');
        }            
    }
    
    function _process()
    {         
        $PaidTxnLog = ClassRegistry::getClass('PaidTxnLogModel');
        $handler_id = Sanitize::getInt($this->params,'handler_id');
        if(isset($this->params['form']) && $handler_id > 0) 
        {
            if($handler = $this->PaidHandler->findRow(array(
                'conditions'=>array('PaidHandler.handler_id = ' . $handler_id)
            ))) 
            {
                $handler_class = Inflector::camelize($handler['PaidHandler']['plugin_file']).'Component';
                App::import('Component',$handler['PaidHandler']['plugin_file']);
                $PaidTxnLog->addNote("Modified by: Payment Handler");
                $Handler = new $handler_class();    
                $Handler->startup($this);
                $result = $Handler->process($handler);
                unset($handler);        
                return $result;
            }        
        }  
    } 
    
    function validateCoupon()
    {
        $coupon_code = Sanitize::getString($this->data['PaidOrder'],'coupon_name');
        $plan_id = Sanitize::getInt($this->data['PaidOrder'],'plan_id');
        $listing_id = Sanitize::getInt($this->data['PaidOrder'],'listing_id');
        $price = Sanitize::getInt($this->data['PaidOrder'],'order_amount');
        $error_code = 200;
        # Do some input validation
        if($coupon_code=='') 
        {
            if($this->couponProcess) 
            {  // Returned on order submit
                return null;            
            }            
            return $this->invalidCoupon($error_code);              
        }
       
       $error_code++; //1
              
        # Get the coupon
        if(!$coupon = $this->PaidCoupon->findRow(array('conditions'=>array('PaidCoupon.coupon_name = ' . $this->PaidCoupon->Quote($coupon_code)))))
        {    
            return $this->invalidCoupon($error_code);              
        }
       
       $error_code++; //2

        # Check coupon dates
        /*----------------------*/
        if(($coupon['PaidCoupon']['coupon_starts']!='' && $coupon['PaidCoupon']['coupon_starts']!=NULL_DATE) 
            || ($coupon['PaidCoupon']['coupon_ends']!='' && $coupon['PaidCoupon']['coupon_ends']!=NULL_DATE)
        ){
            $now = strtotime(_CURRENT_SERVER_TIME);
            if($coupon['PaidCoupon']['coupon_starts']!='' && $coupon['PaidCoupon']['coupon_ends']!='' 
                && (
                    $now < strtotime($coupon['PaidCoupon']['coupon_starts']) 
                    || 
                    $now > strtotime($coupon['PaidCoupon']['coupon_ends'])
                ) 
            )
            {
                return $this->invalidCoupon($error_code);              
            }
            if($coupon['PaidCoupon']['coupon_starts']=='' && $coupon['PaidCoupon']['coupon_ends']!='' 
                && $now > strtotime($coupon['PaidCoupon']['coupon_ends']) 
            )
            {
                return $this->invalidCoupon($error_code);              
            }           
        }
           
       $error_code++; //3
           
        # Check renewals - based on previous orders for the same listing
        if($coupon['PaidCoupon']['coupon_renewals_only'])
        {
            if(!$listingHasOrder = $this->PaidOrder->findCount(array('conditions'=>array(
                'PaidOrder.listing_id = ' . $listing_id
            )))) {
                return $this->invalidCoupon($error_code);              
            }    
        }
        
       $error_code++; //4
        
        # Check user
        if(!empty($coupon['PaidCoupon']['coupon_users']) && !in_array($this->_user->id,$coupon['PaidCoupon']['coupon_users']))
        {
            return $this->invalidCoupon($error_code);              
        } 
        
       $error_code++; //5
        
        # Check usage count
        if($coupon['PaidCoupon']['coupon_count'] > 0 &&  $coupon['PaidCoupon']['coupon_count_type'] == 'global')
        {
            $count = $this->PaidOrder->findCount(array('conditions'=>array(
                'PaidOrder.coupon_name = ' . $this->PaidOrder->Quote($coupon_code),
                'PaidOrder.order_status = "Complete"'                            
            )));                   
            if($count >= $coupon['PaidCoupon']['coupon_count']) 
            {
                return $this->invalidCoupon($error_code);              
            }
        }
        
       $error_code++; //6

        # Check plan
        if(!empty($coupon['PaidCoupon']['coupon_plans']) && !in_array($plan_id,$coupon['PaidCoupon']['coupon_plans']))
        {
            return $this->invalidCoupon($error_code);              
        }
        
       $error_code++; //7

        # Check categories
        if(!empty($coupon['PaidCoupon']['coupon_categories']))
        {
            // Get listing cat id
            $query = "
                SELECT 
                    catid
                FROM 
                    #__content
                WHERE 
                    id = " . $listing_id
            ;
            $this->_db->setQuery($query);
            if($cat_id = $this->_db->loadResult())
            {
                if(!in_array($cat_id,$coupon['PaidCoupon']['coupon_categories']))
                {
                    return $this->invalidCoupon($error_code++);              
                }
            }
        }
        
        $discount = $coupon['PaidCoupon']['coupon_discount']/100;
        if($this->couponProcess) 
        {  // Returned on order submit
            return $price-$price*$discount;            
        }                             
                                 
        $tax_rate = Sanitize::getFloat($this->Config,'paid.tax')/100;
                                 
        // Returned on coupon validation
        return json_encode(array(
            'order_discount'=>'('.number_format($price*$discount,2).')',
            'order_subtotal'=>number_format($price-$price*$discount,2),
            'order_tax'=>number_format($tax_rate*($price-$price*$discount),2),
            'order_total'=>number_format(($price-$price*$discount)+$tax_rate*($price-$price*$discount),2)
        ));
    }   
    
    function invalidCoupon($error_code)
    {
        if($this->couponProcess) return false;
        return json_encode(array('invalid'=>true,'code'=>$error_code));
    }       
}

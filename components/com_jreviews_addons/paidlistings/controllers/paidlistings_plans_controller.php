<?php
/**
 * PaidListings Addon for JReviews
 * Copyright (C) 2010 ClickFWD LLC
 * This is not free software, do not distribute it.
 * For licencing information visit http://www.reviewsforjoomla.com
 * or contact sales@reviewsforjoomla.com
**/
                       
defined( 'MVC_FRAMEWORK') or die( 'Direct Access to this location is not allowed.' );
//error_reporting(E_ALL);ini_set('display_errors','On'); 
   
class PaidlistingsPlansController extends MyController
{
    var $uses = array('paid_plan','category','menu');
    var $components = array('config','access');
    var $helpers = array('assets','html','paid','paid_routes');
    var $autoRender = false;
    var $autoLayout = false;
    var $assets = array('js'=>array('jquery','paidlistings'),'css'=>array('jq.ui.core','paidlistings')) ;
    
    function beforeFilter() 
    {
        # Make configuration available in models
//        $this->Listing->Config = &$this->Config;    
                
        # Call beforeFilter of MyController parent class
        parent::beforeFilter(); 
    }
        
    function index()
    {       
        $plans = array();
        $category = array();
        $cat_id = Sanitize::getInt($this->params,'cat_id',Sanitize::getInt($this->data,'catid'));
        $this->viewSuffix = Sanitize::getString($this->data,'tmpl_suffix',$this->viewSuffix);
        $page_title = Sanitize::getString($this->data,'title');

        if($cat_id)
        {
            $plans = $this->PaidPlan->getCatPlans($cat_id);            
            $category = $this->Category->findRow(array('conditions'=>array('Category.id = ' . $cat_id)));
        } 

        if($page_title)
        {
            cmsFramework::meta('title', $page_title);            
        } 
        elseif(!empty($category))
        {
            cmsFramework::meta('title', sprintf(__t("Plans for %s",true),$category['Category']['title']));            
        }

        $this->set(array('cat_id'=>$cat_id,'plans'=>$plans,'category'=>$category));  
        echo $this->render('paidlistings', 'paidlistings_plans');
    }
}
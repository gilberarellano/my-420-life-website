<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2012 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/
 
# Setting up Classified Ads options:

# Admin password  
$adm_passw="Wabas2004";

# Admin e-mail
$adm_email="admin@my420life.com";

# Use verification code for ads submitting, sending privacy mail, 
# membership subscribing (yes, no)
$usevrfcode="no";

# Places visits counter on ads details pages  for most visited ads (yes, no)
$plcntdtl="yes";

# Use IP logs to count only one visit (yes, no)
$uselogsvmv="yes";

# time (days) for latest ads link on the top page
$tmltads=20; 
# Protection for ads duplication (yes, no).
$pradsdupl='no';
 
# Number of ads displayed on the index page
$adsonpage="20";

# Max ads title size on ads index pages (chars)
$maxttlsz=25;

# Expiration period for ads (days)
$exp_period="100";

# Set up privacy mail (yes, no) 
$privacy_mail="yes";

# Privacy mail template 
$pmailtp="
For you ad: --title-- (--ad_url--)
the following message was sent:

--message--

Reply e-mail: --r_email--

My420Life.com

"; 

# If privacy mail is set up , send copy of privacy messages 
# to admin (yes, no)
$sendcopytoadm="yes";
# If privacy mail is set up , redirect privacy messages 
# to admin (yes, no)
$redirtoadm="yes";

# Max number of photos allowed
$photos_count=10;

# Set max size for all photos on the second ad page (bytes)
$phptomaxsize=5000000;

# Width of preview photos on the second ad page (pixels)
$prphscnd="100";
# Max width of photos on the second ad page (pixels)
$maximgswith="550";

# Set up resizing of large photos before saving in the database (yes, no)
$resphdb="yes";
# Max width or height (pixels) of photos for resizing
$maxpixph="960";

# Set up javascript checking of  ads submitting form (yes, no)
$jsfrmch="yes";

# Place on detailed ad page special link to search 
# all ads posted by the user ( search with the same contact e-mail )
# if user place more then 1 ad (yes, no)
$schallusrads='yes';
 
# set up html header/footer for user's interface
/* $ht_header=" <p class='pst1'>
  &nbsp; <font style='color: #555599; font-weight: bold; '>Almond Classifieds Component for Joomla! (version 5.4, GPL License)</font>
<p class='pst1'>
<font style='font-size: 12px;'> 
<a href='http://www.ads-programming.com/jm/index.php?option=com_aclsfstd'>
Click here to try demo of Almond Classifieds (Standard Edition, GPL)</a> 
with the  powerful search, ads moderating, <br>
optional membership, the ability to choose location, and other features. 
<a href='http://www.ads-programming.com/acj'>More info</a>
</font>
<p align='justify'> 
";
*/

$ht_header=" <p class='pst1'>
  &nbsp; My420Life Classifieds
<p class='pst1'>
<a href='/index.php'>
My420Life</a> 
with the  powerful search, ads moderating, <br>
optional membership, the ability to choose location, and other features. 
<a href='/index.php'>My420Life</a>
<p align='justify'> 
";

$ht_footer="
<hr size=1>
<table width='100%'  border='0' cellspacing='0' cellpadding='0' class='tb1'>
<tr><td><font class='smallfnt'>
<center>&nbsp;  <br>
 Copyright  &copy; 2012 <a href='/index.php'>My420Life</a>.
</font></center></td></tr></table>
<p class='pst1'>  &nbsp;   <p class='pst1'>  
";



############################################################
# Congratulations!  You've finished defining the variables.#
############################################################

?>
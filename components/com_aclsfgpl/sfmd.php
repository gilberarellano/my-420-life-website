<?php 
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

global $admconf_url, $db_name, $host_name, $db_user, $db_password, $table_ads, 
$urlclscrpt, $opt_header, $opt_footer, $adm_passw, $adm_email, $id,
 $photos_path, $usevrfcode, $plcntdtl,$vstminnmbr,
$plcntpml,$voterate,$maxvalrate,$uselogsvmv, $exprlogs, $table_logs, $use_spmg,
$exp_period, $adsonpage, $sndadmnotif, $moderating, $hltadsf,$exp_perdhlt,$dsplhltpht,
 $pradsdupl,$tmltads, $privacy_mail, $pmailtp, $sendcopytoadm, $redirtoadm, $sndexpmsg1,
$photos_count,$phptomaxsize, $incl_mtmdfile, $mtmdfile_maxs, $flattchext, $plsflsrvrs, $infldflsrv,
$prphscnd, $maximgswith, $resphdb, $maxpixph, $ppactv, $pladddp,$mbac_sndml, $mbac_addad,$mbac_second,
$jsfrmch, $schallusrads, $ch_nmusr, $usrads_max, $usrads_chcktime, $titleclpg, $ht_header, $ht_footer,
$moderating_ct, $moderating_vl, $mbac_second_vl, $mbac_second_ct, $dstnem,
$mbac_addad_vl, $mbac_addad_ct, $mbac_sndml_vl, $mbac_sndml_ct, $admcookpassw, $admpassw1,
$adsontoppage, $colr_hltads, $capt_hltads, $tpadsindd, $bc2adsfrm, $width_2tpf, $adbnrcll, $fntclr_3, 
$fntclr_2, $tbclr_5, $fntclr_1, $tbclr_4, $tbclr_3, $tbclr_2, $tbclr_1, $prphscrwdth, $incl_prevphoto,
$ad_ind_width, $top_page_width, $maximgswith, $prphscnd, $ad_second_width, $detl_leftcol, $ind_leftcol, 
$btmtophtml, $top_rightcol, $top_leftcol, $prwdly, $photos_url, $displaflusr,$waflnscr, 
$dtl_rightcol, $ind_rightcol, $ht_leftcol, $cnt_htl_det, $cnt_htl_ind, $ordhltrate,
$ads_fields, $fields_sets, $categories, $dlmtr, $hghltcat, $_FILES, $_GET, $_COOKIE, $_POST, $fields_comm, 
$reply_catg, $allcatfields, $tbclhlads, $mtg_keywrds, $mtg_descr, $mtfldkwrs, $photo_path, $incl_prevpht1, $prphlrwdth,
$fields_layer, $use_adslayer, $slctcntr, $fld_dim, $fld_dimd, $slctcntr, $ctgroups_sets,
$HTTP_GET_VARS,$HTTP_POST_VARS, $templ, $msg, $msg2, $locations,  $slctcntr, $choosecntr, $schopt,
 $subcnmb, $indx_url, $javastl,$plcntpml, $topcmmnt,$pltopcontln,$plcntdtl,$pltopvisitln,
$pltopratedln,$cat_fields, $ct, $select_text, $check_subcateg, $months_short, $months_nm, $ed_add,
$weekdays,$mds, $ed_id, $ed_passw, $userfile, $userfile_name,  $jpath_url, $vis, $REMOTE_ADDR,
$style_css_url,$jm_cmpath, $indx_url, $indxjf_get, $indadm_url, $list_id, $jloginfo, $md,
$HTTP_COOKIE_VARS, $cookie_time, $use_gl_map, $glkey, $clmnmap, $nmcolmap, $mpzoomp, $mpzoomdt, $rmmbrloc,
$mapdtpg, $nomapsmb,$drdir_url, $gmapurl,$maxrepldt, $ads_rt, $ads_count, $page, $mosConfig_dbprefix, 
$sturlvar, $mosConfig_live_site, $urlclscrpt1,   $_REQUEST, $pm_subject, $mail_message, $pm_email, 
$pm_message, $edit_delete, $urlclscrpt2, $use_ajax, $preloadpht, $jstoplads, $jstphlads, $maxttlsz,
$kwsrchfrm, $keywrdtable, $kwminln,$kwminadsind, 
$kwminsgl, $kwmaxsgst, $spamlnkfl, $spamcntnt, $use_fltxtind, $maxlntlsturl, $kwfldsrch, $keywords_search_info, $kw_no_ads,
$kwlst_fields1, $kwlst_fields2, $kwlst_prdfields, $kwfrcharc, $kwcmmdlmt, $kwaddrdlmt, $kwsplcdlmt, 
$kwfldsrch, $kwrealamount, $kwind_one_q  ;

$phpvrsn=explode('.',phpversion()); if($phpvrsn[0]*10+$phpvrsn[1] >= 53 ){ error_reporting(E_ALL  & ~E_NOTICE & ~E_DEPRECATED);}
else {error_reporting(E_ALL & ~E_NOTICE);}

if($_COOKIE!=""){
foreach ($_COOKIE as $c_key => $c_value)
{$_REQUEST[$c_key]=$c_value;}
}

$ct=$_REQUEST['ct'];
$md=$_REQUEST['md'];
$page=$_REQUEST['page'];
$ed_id=$_REQUEST['ed_id'];
$ed_passw=$_REQUEST['ed_passw'];
$edit_delete=$_REQUEST['edit_delete'];
$ps_email=$_REQUEST['ps_email'];
$mds=$_REQUEST['mds'];
$onlywithphoto=$_REQUEST['onlywithphoto'];
$id=$_REQUEST['id'];
$idnum=$_REQUEST['idnum'];
$mblogin=$_REQUEST['mblogin'];
$admpassw1=$_REQUEST['admpassw1'];
$vis=$_REQUEST['vis'];
$list_id=$_REQUEST['list_id'];
$ads_rt=$_REQUEST['ads_rt'];
$ammlk=$_REQUEST['ammlk'];
$admcookpassw=$_REQUEST['admcookpassw'];
$cook_login=$_REQUEST['cook_login'];
$cook_passw=$_REQUEST['cook_passw'];
$f_login=$_REQUEST['f_login'];
$f_passw=$_REQUEST['f_passw'];
$viunvis=$_REQUEST['viunvis'];
$mbprf_login=$_REQUEST['mbprf_login'];
$mbprf_passw=$_REQUEST['mbprf_passw'];
$emltp=$_REQUEST['emltp'];
$emllogin=$_REQUEST['emllogin'];
$ps1=$_REQUEST['ps1'];
$ps2=$_REQUEST['ps2'];
$pm_message=$_REQUEST['pm_message'];
$pm_email=$_REQUEST['pm_email'];
$pm_subject=$_REQUEST['pm_subject'];
$userfile=$HTTP_POST_FILES['userfile']['tmp_name'];
$userfile_name=$HTTP_POST_FILES['userfile']['name'];
if($_POST!=""){$HTTP_POST_VARS=$_POST;}
if($_COOKIE!=""){$HTTP_COOKIE_VARS=$_COOKIE;}
if($_GET!=""){$HTTP_GET_VARS=$_GET;}
if($_FILES!=""){$HTTP_POST_FILES=$_FILES;
$userfile=$_FILES['userfile']['tmp_name']; $userfile_name=$_FILES['userfile']['name']; }
$REMOTE_ADDR=getenv("REMOTE_ADDR"); 
if($id!=""){settype ($id, "integer");} settype ($ed_id, "integer"); settype ($idnum, "integer"); 

?>
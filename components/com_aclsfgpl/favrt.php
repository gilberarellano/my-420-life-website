<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

$cookie_time=time()+3600000;
if ($HTTP_GET_VARS['mfvrt']=="add"){add_favorite();}
if ($HTTP_GET_VARS['mfvrt']=="rmv"){remove_favorite();}
if ($HTTP_GET_VARS['mfvrt']=="rmall"){remove_all();}
if ($HTTP_GET_VARS['mfvrt']=='1'){$adsonpage="100";}


function print_fav_ads_ind()
{
global $HTTP_COOKIE_VARS, $HTTP_GET_VARS, $msg;
if ($HTTP_COOKIE_VARS['ckfvr']!='')
{
$var="
  &nbsp; &nbsp;  <a href='index.php?md=browse&mfvrt=1' target='_blank'>
 ".$msg['your_favorite_ads']." 
</a>
  &nbsp; 
";

if ($HTTP_GET_VARS['mfvrt']=='1'){
$var="
  &nbsp; &nbsp;  ".$msg['your_favorite_ads']." 
  &nbsp;  
";
;}
}
return $var;
}

function print_add_fvrt()
{
global $HTTP_COOKIE_VARS, $HTTP_GET_VARS, $msg;
$mss_cookies=split(',',$HTTP_COOKIE_VARS['ckfvr']);

$ck_var=""; 
foreach ($mss_cookies as $cook_value)
{ 
if ($cook_value==$HTTP_GET_VARS['id']){$ck_var="1";}
}
if ($HTTP_GET_VARS['mfvrt']=='add'){$ck_var="1";}
if (($HTTP_GET_VARS['mfvrt']=='rmv') or ($HTTP_GET_VARS['mfvrt']=='rmall')){$ck_var="0";}


if ($ck_var=="1"){$var1="&nbsp;Favorite ad";}
else {$var1=
" &nbsp; <a href='index.php?ct=".$HTTP_GET_VARS['ct']."&md=".$HTTP_GET_VARS['md']."&id=".$HTTP_GET_VARS['id']."&mfvrt=add'>
 ".$msg['add_to_favorites']."</a>
";} 
$var="
<FONT class='smallfnt'> 
$var1  
 </font>
";
return $var;
}


function print_fvrt_dtl()
{
global $HTTP_COOKIE_VARS, $HTTP_GET_VARS, $msg, $indx_url;
$mss_cookies=split(',',$HTTP_COOKIE_VARS['ckfvr']);

$ck_var=""; 
$ck_var2=""; 
foreach ($mss_cookies as $cook_value)
{ 
if ($cook_value==$HTTP_GET_VARS['id']){$ck_var="1";}
if ($cook_value!=""){$ck_var2="1";}
}

if ($HTTP_GET_VARS['mfvrt']=='add'){$ck_var="1";}
if (($HTTP_GET_VARS['mfvrt']=='rmv') or ($HTTP_GET_VARS['mfvrt']=='rmall')){$ck_var="0";}

if ($ck_var=="1"){
$var1="
 
<a href='{$indx_url}ct=".$HTTP_GET_VARS['ct']."&md=".$HTTP_GET_VARS['md']."&id=".$HTTP_GET_VARS['id']."&mfvrt=rmv'>
".$msg['remove_this_ad']." </a> &nbsp;&nbsp;

<a href='{$indx_url}ct=".$HTTP_GET_VARS['ct']."&md=".$HTTP_GET_VARS['md']."&id=".$HTTP_GET_VARS['id']."&mfvrt=rmall'>
".$msg['remove_all_favorites']."</a>&nbsp;
";
}
else {$var1=
"<a href='{$indx_url}ct=".$HTTP_GET_VARS['ct']."&md=".$HTTP_GET_VARS['md']."&id=".$HTTP_GET_VARS['id']."&mfvrt=add'>
".$msg['add_ad_to_favorite']."</a>

";} 

$var2="";
if (($ck_var2=="1") and ($HTTP_GET_VARS['mfvrt']!="rmall")){
$var2="
<a href='{$indx_url}md=browse&mfvrt=1'>
".$msg['your_favorite_ads']."
</a>
 &nbsp;
";
}
$var="
 
$var2 $var1    
</b> 
";
return $var;
}



function add_favorite()
{
global $HTTP_COOKIE_VARS, $HTTP_GET_VARS,$cookie_time;

$mss_cookies=split(',',$HTTP_COOKIE_VARS['ckfvr']);

$ck_var=""; 
foreach ($mss_cookies as $cook_value)
{ 
if ($cook_value==$HTTP_GET_VARS['id']){$ck_var="1";}
}

if ($ck_var!="1"){ 
$vfvr_cookie=$HTTP_COOKIE_VARS['ckfvr'].$HTTP_GET_VARS['id'].",";
setcookie ("ckfvr", $vfvr_cookie, $cookie_time);
}
}

function remove_favorite()
{
global $HTTP_COOKIE_VARS, $HTTP_GET_VARS;
$mss_cookies=split(',',$HTTP_COOKIE_VARS['ckfvr']);

$vfvr_cookie="";

foreach ($mss_cookies as $cook_value)
{ 
if (($cook_value!='') and ($cook_value!=$HTTP_GET_VARS['id']))
{$vfvr_cookie=$vfvr_cookie.$cook_value.",";}
 }
setcookie ("ckfvr", $vfvr_cookie, $cookie_time);
 
}

function remove_all()
{
setcookie ("ckfvr");
}

function view_fv_ads()
{
global $HTTP_COOKIE_VARS, $HTTP_GET_VARS;

$mss_cookies=split(',',$HTTP_COOKIE_VARS['ckfvr']);
$where_string1="";
foreach ($mss_cookies as $cook_value)
{ 
if ($cook_value!='')
{$where_string1=$where_string1."idnum=$cook_value or ";}
} 
 
$where_string1=$where_string1."fdkspkdsanbf";
$db_dcf="or fdkspkdsanbf";
$where_string1=ereg_replace($db_dcf,"",$where_string1);

if ($HTTP_COOKIE_VARS['ckfvr']==''){$where_string1="idnum=0 ";}
return $where_string1;
}

?>
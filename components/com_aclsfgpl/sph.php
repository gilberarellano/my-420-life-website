<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

include("config.php");
$id=$_REQUEST['id'];
$wd=$_REQUEST['wd'];
$np=$_REQUEST['np'];
 
$photofile=$photos_path."p".$id."n".$np.".jpg";
if ($np=='0'){$photofile=$photos_path."p".$id."prw".".jpg";}

if ($ph_size = @getimagesize($photofile) )
{
$width = $ph_size[0]; 
$height = $ph_size[1]; 

$new_width=$wd;
$new_height=$height/$width*$new_width;
}

if(  $im1 = @imagecreatefromjpeg($photofile) )
{  
$im2 = imagecreatetruecolor($new_width,$new_height); 
#$im2 = imagecreate($new_width,$new_height); 
@imagecopyresized($im2, $im1, 0, 0, 0, 0, $new_width,$new_height,$width,$height); 
header('Content-type: image/jpeg'); 
imagejpeg($im2); 
imagedestroy($im1); 
imagedestroy($im2);
} 

else {

if(  $im1 = @ImageCreateFromGIF ($photofile) )
{  
$im2 = imagecreatetruecolor($new_width,$new_height); 
#$im2 = imagecreate($new_width,$new_height); 
@imagecopyresized($im2, $im1, 0, 0, 0, 0, $new_width,$new_height,$width,$height); 
header('Content-type: image/gif'); 
imagegif($im2); 
imagedestroy($im1); 
imagedestroy($im2);
} 

}
?>
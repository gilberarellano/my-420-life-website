<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

if (!defined('_JEXEC')){defined( '_VALID_MOS' ) or die( 'access denied' );}


global $mosConfig_db;
if($mosConfig_db==""){
$jmvar5 = new JConfig;
$jhost_name5=$jmvar5->host;
$jdb_user5=$jmvar5->user;
$jdb_password5=$jmvar5->password;
mysql_connect("$jhost_name5","$jdb_user5","$jdb_password5"); 
$jdb_name5=$jmvar5->db;
mysql_select_db("$jdb_name5");
}



if (defined('JPATH_ROOT')){$jmlconfpath=JPATH_ROOT;}else{$jmlconfpath=$mosConfig_absolute_path;}
$config_path=$jmlconfpath."/components/com_aclsfgpl/config.php";
include($config_path);
 
require("{$compnt_path}funcs1.php");
require("{$compnt_path}favrt.php"); 
require("{$compnt_path}adshtm.php");
require("{$compnt_path}fsturl.php");


$javastl="
<script language='JavaScript'>
<!--
 function displ(nmdiv){if (document.getElementById){
if (document.getElementById(nmdiv).style.display=='none') document.getElementById(nmdiv).style.display ='block';
else document.getElementById(nmdiv).style.display ='none';} else if (document.all)
{ if(document.all[nmdiv].style.display=='none') document.all[nmdiv].style.display ='block';
else document.all[nmdiv].style.display ='none';}}
-->
</script>
";  
 

$msgs_fl="msg1_".$intlang.".php"; require("{$compnt_path}$msgs_fl");
if (($md!="details") and ($md!="") and ($md!="browse"))
{$msgs_fl="msg2_".$intlang.".php"; require("{$compnt_path}$msgs_fl");} 

$select_text=$msg['Please_choose_one_sl'];

$sqlaflcl="";
if ($displaflusr=="yes"){$sqlaflcl="afprtid1='".$_REQUEST['afflprid']."' and "; 
if ($_REQUEST['afflprid']==""){$sqlaflcl="afprtid1='11111' and ";}
}


if ($md=="chcity"){include("{$compnt_path}ch_city.php"); ch_city(); return; }

if ($md=="rmbloc"){include("{$compnt_path}rmbloc.php"); }
if (($ct=="") and ($HTTP_GET_VARS['city']=="") and ($HTTP_COOKIE_VARS['kcity']!='') and ($HTTP_COOKIE_VARS['city']==''))
{$HTTP_GET_VARS['city']=$HTTP_COOKIE_VARS['kcity'];}
if ($_REQUEST['allc']=='1'){setcookie ("kcity"); setcookie ("$schopt"); $HTTP_GET_VARS[$schopt]=''; $_REQUEST[$schopt]='';}
if (($HTTP_GET_VARS[$schopt]!="") and ($ct=="") and ($mds==""))
{$_REQUEST[$schopt]=$HTTP_GET_VARS[$schopt]; setcookie ("$schopt", $HTTP_GET_VARS[$schopt]);} 
if (($HTTP_GET_VARS[$schopt]=="")  and (($mds=="") or (adsordtp!=""))){$HTTP_GET_VARS[$schopt]=$_REQUEST[$schopt];}
$rmmbrloc="";
if (($HTTP_COOKIE_VARS['kcity']!=$_REQUEST[$schopt]) and ($_REQUEST[$schopt]!=""))
{$rmmbrloc=" &nbsp; &nbsp;  <a href='' onclick=\"n=window.open('{$indx_url}md=rmbloc&lc=".$HTTP_GET_VARS[$schopt]."',
 'n', 'toolbar=no, status=no, width=400, height=200');\">".$msg['rememb_loc']."</a>";}

start();

if ($md=="chct") {include("{$compnt_path}ch_catg.php");ch_categ();}

if (($md=="details") and ($mbac_second=="m"))
{
$msgs_fl="msg2_".$intlang.".php"; require("{$compnt_path}$msgs_fl");
include("{$compnt_path}mb_check.php"); if(!check_mb_login()){return;}}

if ((($md=="add_form") or ($md=="submitad")) and ($mbac_addad=="m"))
{include("{$compnt_path}mb_check.php"); if(!check_mb_login()){return;}}
if ((($md=="privacy_mail") or ($md=="send_mail")) and ($mbac_sndml=="m"))
{include("{$compnt_path}mb_check.php"); if(!check_mb_login()){return;}} if ($md=="adscnt"){adscnt1();}
if ($md=="forgmbpassw"){include("{$compnt_path}mb_check.php"); forgot_mb_passw();}
if ($md=="sendmbps"){include ("{$compnt_path}mb_conf.php"); include("{$compnt_path}mb_check.php"); send_mb_passw($ps_email);}
if ($md=="mblogout"){include("{$compnt_path}mb_check.php"); mb_log_out();}
 
if (($md=="") and ($ct=="") ){include("{$compnt_path}top.php"); print_categories(); return;} 
if (($md=="browse") or ($md=="")){browse_ads();} 

if ($flattchext!=""){include_once("{$compnt_path}flattch.php");}

if ($md=="details"){include("{$compnt_path}details.php"); ad_details();} 
if ($md=="add_form") {include("{$compnt_path}forms.php"); print_add_form();} 
if ($md=="editlogin"){ include("{$compnt_path}forms.php"); edit_login(" ");}
if ($md=="editform"){ include("{$compnt_path}forms.php");   edit_form(); return;}
if ($md=="forgotpassw"){include("{$compnt_path}forms.php"); forgotpassw($ps_email);}
if ($md=="sendpassw"){include("{$compnt_path}funcs2.php");sendpassw($ps_email);}
if ($md=="privacy_mail"){include("{$compnt_path}forms.php"); privacy_form($idnum);}
if ($md=="send_mail"){include("{$compnt_path}funcs2.php"); send_mail($idnum);}
if ($md=="submitad") {include("{$compnt_path}submit.php"); submit_ad();}
if ($md=="edit") {include("{$compnt_path}submit.php"); edit_ad();}
if ($md=="adsvtrate"){include("{$compnt_path}funcs2.php"); adsvtrate(); return;} 

 
function getlink_topads()
{
global $plcntdtl, $plcntpml, $ct, $voterate, $HTTP_GET_VARS, $topcmmnt, $msg,$tbclr_2,$tbclr_4,
$pltopratedln, $pltopcontln, $pltopvisitln, $reply_catg, $indx_url;

if (($plcntpml=="yes") and ($pltopcontln=='yes')){$most_cnt="
<a href='{$indx_url}ct=$ct&md=browse&mds=search&adsordtp=pml' >".$msg['top_contacted_ads']."</a>&nbsp;&nbsp; ";}

if ((($topcmmnt=="yes") and ($reply_catg[$ct]!="")) or ($ct=="")) {$most_cmmnt="
<a href='{$indx_url}ct=$ct&md=browse&mds=search&adsordtp=cmmnt' >".$msg['top_commented_ads']."</a>&nbsp;&nbsp; ";}

if (($plcntdtl=="yes") and ($pltopvisitln=='yes')){$most_vst="
<a href='{$indx_url}ct=$ct&md=browse&mds=search&adsordtp=vis' >".$msg['top_visited_ads']."</a>&nbsp;&nbsp; ";}

if (($plcntdtl=="yes") and ($pltopratedln=='yes')){$most_vrt="  
<a href='{$indx_url}ct=$ct&md=browse&mds=search&adsordtp=vote' >".$msg['top_rated_ads']."</a>&nbsp;&nbsp; ";}

$res_link="
$most_vrt  
  
$most_cmmnt   
 
$most_cnt   
 
$most_vst    
";
return $res_link;
}

function category_name()
{
global $categories, $ct, $ratedads, $HTTP_GET_VARS, $schresinfo, $fntclr_1, $msg, $indx_url;
if ((($ct!="") and ($ratedads!="")) or  ($HTTP_GET_VARS['mds']=='search'))
{
$cat1_name="
<a href='{$indx_url}ct=$ct'><b>".$categories[$ct][0]."</b></a>
";
 
if ($HTTP_GET_VARS['adsordtp']=='pml'){$txtinf1=$msg['top_contacted_ads'].":";}
if ($HTTP_GET_VARS['adsordtp']=='vis'){$txtinf1=$msg['top_visited_ads'].":";}
if ($HTTP_GET_VARS['adsordtp']=='vote'){$txtinf1=$msg['top_rated_ads'].":";}
if ($HTTP_GET_VARS['adsordtp']=='cmmnt'){$txtinf1=$msg['top_commented_ads'].":";}
$schresinfo="
$txtinf1
";
}
else
{
$cat1_name=$categories[$ct][0];
}
return $cat1_name;
}

function nb_best_ads()
{
global $cat_fields, $table_ads, $ct, $page, $adsonpage, $ratedads, $idemail,$plhltads,$msg, $indx_url; 

if ($plhltads=="2"){

if ($ratedads=="1")
{$res_mess="
<font class='stfnt'><b>
".$msg['ads_rated_by_admin']."
</b>
</font>
";}
else { 
if ($ct!=""){$qy_ct="catname='$ct' and "; } else {$qy_ct="";}
$sql_query="select count(idnum) from $table_ads where 
 $qy_ct adrate > 0";
$sql_res=mysql_query("$sql_query");
$row=mysql_fetch_row($sql_res);
$count=$row[0];
if ($count > 0)
{
$res_mess="
<font class='smallfnt'> 
<a href='{$indx_url}ct=$ct&md=browse&ratedads=1'>".$msg['Best_Ads']."</a> 
[$count]</font>
";
}
}
}
if ($idemail!=""){$res_mess="";}
return $res_mess;
}

function check_row($row)
{
global $cat_fields, $select_text,$real_format;
 
if($row['homeurl']=='http://'){$row['homeurl']="--";}
foreach ($cat_fields as $key => $value) 
{
if (($row[$key]=="") or ($row[$key]==$select_text) or ($row[$key]=='http://')
or ($row[$key]=='NULL') or ($row[$key]=='null')) 
{$row[$key]="--";}
else {
if ($cat_fields[$key][6]=='real') {$row[$key]=sprintf($real_format, $row[$key]);}
}

if ($cat_fields[$key][4] == 'date'){
$row[$key]=get_date_f($row[$key]);
}
 
}

$row['moredetails']="&nbsp;".preg_replace("/\n/", "<br>",$row['moredetails']);

return $row;
}

function check_favorites()
{
global $HTTP_COOKIE_VARS;

$val=0;

if ($HTTP_COOKIE_VARS['ckfvr']!='')
{
$val=1;
}

return $val;
}

function msg_favorites()
{
global $msg, $HTTP_COOKIE_VARS, $HTTP_GET_VARS, $hltadsf;

$val=0;

if ($HTTP_COOKIE_VARS['ckfvr']=='' and $HTTP_GET_VARS['mfvrt']!='')
{
$msgfv="<p><center><font class='stfntb'><b> ".$msg['no_favorites']."</b></font></center> ";
}
else {$msgfv="";}

if($HTTP_GET_VARS['mfvrt']!=''){$hltadsf="";}
return $msgfv;
}

function print_subcat($key)
{
global $categories, $fntclr_1, $msg, $ads_fields, $subcnmb, $getcityval,$popctgr, $indx_url;

if (($categories[$key][2] != 'h') and ($categories[$key][2] != ''))
{
$sctgv='sct'.$popctgr.$key;
$vr1=split(',',$categories[$key][2]);
foreach ($vr1 as $key1){

$ads_fields[$key1][7]=ereg_replace("\n", '', $ads_fields[$key1][7]);
$ads_fields[$key1][7]=ereg_replace("\r", '', $ads_fields[$key1][7]);
$vr2=split('<option>',$ads_fields[$key1][7]);

$htmlv=$htmlv." <font class='fsct1'>".$ads_fields[$key1][0].":</font>
 &nbsp; <font class='fsct2'> 
";

$sctnbmass=subctadsnumb($key, $key1);

$b11='';
foreach ($vr2 as $key2){
if($key2!=''){
if($sctnbmass[$key2]==''){$sctnbmass[$key2]='(0)';}
$trgtval="sct".$key;

global $_REQUEST;
if($_REQUEST[$key1]==$key2){$stfld1="style='font-weight: bold;'";}else{$stfld1="";}

$htmlv=$htmlv."$b11 &nbsp;<nobr><a href='{$indx_url}".$getcityval."ct=$key&md=browse&mds=search&$key1=".checkfld2($key2)."' $stfld1 >$key2</a>".$sctnbmass[$key2]."</nobr>
"; $b11=',';

}
 
}

$htmlv=$htmlv."$b11 &nbsp;<nobr><a href='{$indx_url}".$getcityval."ct=$key&md=browse&'>".$msg['all_ads1']."</a>".subctadsnumb($key, "")."</nobr></font><br>";
}
$htmlv=$htmlv."<p class='pst1'>";
}

return $htmlv;
}

function subctadsnumb($ctg, $fld)
{

global $table_ads, $schopt, $_REQUEST;
if ($_REQUEST[$schopt]!=''){$wrvl="and $schopt='".$_REQUEST[$schopt]."'";};
$sql_query="select $fld, count(idnum) from $table_ads where visible=1 and catname='$ctg' $wrvl group by $fld ";
if($fld==""){$sql_query="select count(idnum) from $table_ads where visible=1 and catname='$ctg' $wrvl";}
$sql_res=mysql_query("$sql_query"); 
while ($row = mysql_fetch_row($sql_res))
{$fldn=$row[0];  
$fmass[$fldn]="(".$row[1].")";
if($fld==""){$fmass="(".$row[0].")";}
}
return $fmass;
}

function checkfld2($value)
{
$value=ereg_replace(' ', '+', $value);
$value=ereg_replace('@', '%40', $value);
$value=ereg_replace('!', '%21', $value);
return $value;
}


function get_tm_tags($title,$row)
{
global $mtg_keywrds, $mtg_descr,$html_header,$mtfldkwrs,$ct;

if($row=="")
{
if ($mtg_descr[$ct]!="") {$descr=$mtg_descr[$ct];} else {$descr=$mtg_descr['all'];}
if ($mtg_keywrds[$ct]!="") {$keywrds=$mtg_keywrds[$ct];} else {$keywrds=$mtg_keywrds['all'];}
}
else {
$row['title']=ereg_replace('\"', '\'',$row['title']);
$row['brief']=ereg_replace('\"', '\'',$row['brief']);

$rwmss = preg_split ("/[\W]+/", $row['title']);
foreach ($rwmss as $value){if (strlen($value) > 3) {$vlkw=$vlkw.$value.", ";}}

foreach ($mtfldkwrs as $value){if ($row[$value]!="" and $row[$value]!="--")
{ $row[$value]=ereg_replace('\"', ' ',$row[$value]); $vlkw=$vlkw.$row[$value].", ";}}

if ($mtg_keywrds[$ct]!="") {$keywrds=$mtg_keywrds[$ct];} else {$keywrds=$mtg_keywrds['all'];}
$keywrds=$vlkw." almclsfds, ".$keywrds ;
$descr=$row['brief'];

}

$mtgvl="
<title>$title</title>
<meta name=\"keywords\" content=\"$keywrds\">
<meta name=\"description\" content=\"$descr\">
";


return $mtgvl;

}  

function get_tm_tagnt($title,$row)
{
global $mtg_keywrds, $mtg_descr,$html_header,$mtfldkwrs,$ct;

$mtgvl2=get_tm_tags($title,$row);

$mtgvl2="<!--tmt $mtgvl2 tmt-->";

return $mtgvl2;
}
?>
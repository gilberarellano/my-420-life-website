<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function get_ads_captions()
{

global $cat_fields, $ads_count, $ct, $photos_count, $incl_prevphoto, $prwph_layer,
$tbclr_1, $tbclr_2, $tbclr_3, $tbclr_4, $fntclr_1, $fntclr_2,$tbclr_5,$colspn,
$lnkttlfl, $msg, $prphscr, $use_gl_map,  $clmnmap, $nmcolmap, $incl_prevpht1;

$bgcol21="$tbclr_5";
$bgcol22="$fntclr_2";
$colspn=0;
$captions="<tr>";

if ($incl_prevpht1=='yes')  
{$colspn=$colspn+1;
$captions=$captions." 
<td align='center' class='adscpt'>
".$msg['preview_photo']."</td>
";
}

$captions=$captions." 
<td align='center' class='adscpt' width='40%' style='padding: 3px;'>
".$cat_fields['title'][0]."   
 </td>
";

foreach ($cat_fields as $key => $value)
{
$ik++;
if ( ($cat_fields[$key][1]=='1') or ($cat_fields[$key][1] == '12'))
{$colspn=$colspn+1;
$captions=$captions." 
<td align='center' class='adscpt' >
".$cat_fields[$key][0]." 
 </td>
";
}
}
if ($photos_count > 0)
{$colspn=$colspn+1;
$captions=$captions."
<td align='center' class='adscpt'>
 ".$msg['photoind']."
</td>
";
}


if ($use_gl_map=='yes' and  $clmnmap=='yes'){
$colspn=$colspn+1;
$captions=$captions."
<td align='center' class='adscpt' width='5%'> 
$nmcolmap 
</td>
";
}

$colspn=$colspn+1;
$captions=$captions."
<td align='center'class='adscpt' width='5%'  >
 ".$msg['posted']." 
</td>
";


if ($ct == '')
{$colspn=$colspn+1;
$captions=$captions." 
<td align='center' class='adscpt'width='15%'>
".$msg['category']."
</td>
" ;
}

global $hltnmbv;

$captions=$captions."</tr>";
if($ads_count == 0 and $hltnmbv!="1") $captions="";
return $captions;
}

function print_ads_ind ($row)
{
global $cat_fields, $photos_count, $ct, $categories, $incl_prevphoto, $urlphtn1,
$previewphoto_url, $previewphoto_path, $prphotolimits, $pr_lim_height, $pr_lim_width,
$prwph_layer, $colr_hltads, $capt_hltads, $prphscr,$prphscrwdth,$prwdly, $photo_path,
$lnkttlfl, $msg, $idadcol, $colspn, 
$tbclhlads, $coltblayer, $use_gl_map,  $clmnmap, $hltinf,$hltlstk, $incl_prevpht1, 
$indx_url, $jpath_url;

$stl1_ads='adslst1';
$stl2_ads='adslst2';

$capt1_hltads="";

 

if ($idadcol=="0"){$adtbstl="class='$stl1_ads'"; $idadcol="1";} else {$adtbstl="class='$stl2_ads'"; $idadcol="0";}
$row=check_row($row); 
  
$html_ad="<tr>";
$time1=$row['time'];
$ad_date=get_short_date($time1);
$idnum=$row['idnum'];

$ctval=$ct;
 


if ($incl_prevpht1=='yes')  
{ 
if ($prphotolimits=='yes'){
if ($pr_lim_height==""){
$phlimitinfo="width='$pr_lim_width'";
}
else{
$phlimitinfo="width='$pr_lim_width' height='$pr_lim_height'";
}
}
get_jpg_path($idnum);
if(file_exists($photo_path[1])){
     $html_ad=$html_ad."
      <td bgcolor='#ffffff'  align='center' width='5%'  >
<a href='".sturl_ads($ctval, $idnum)."' title='#$idnum'> 
<div style='margin:3px;'>
<img src='".$urlphtn1."{$jpath_url}sph.php?id=$idnum&wd=$prphscrwdth&np=1' $phlimitinfo border=0 > 
</div>
</a> 
</td>
     ";}
else {
$html_ad=$html_ad."
<td $adtbstl  align='center' width='5%'  >
-- 
</td>
";
}
} 

$row_title=$row['title'];
global $maxttlsz;
if (strlen($row_title)>$maxttlsz) 
{$row_title=substr($row_title,0,$maxttlsz); 
$row_title= preg_replace ("/\s\S+$/", "...", $row_title); }

$titlfldind="$capt1_hltads &nbsp;
<a href='".sturl_ads($ctval, $idnum)."' 
  title='#$idnum'   class='ad_title2' >".$row_title."</a>  
 
";

if ($row['adcommkey']==1){
$ttlbrf="
&nbsp;&nbsp;<a href='{$indx_url}md=details&id=".$row['replyid']."'  >
<font class='smallfnt'>".$msg['View_initia_ad']."(#".$row['replyid'].")</font></a>
";
}

if ($row['comment']!=""){$ttlbrf=$ttlbrf."<br> &nbsp;&nbsp;".$row['comment'];}

$html_ad=$html_ad."
<td $adtbstl style='text-align: left;' width='40%'  height='40'>
$titlfldind </td>";

foreach ($cat_fields as $key => $value) 
{
if ( ($cat_fields[$key][1]=='1') or ($cat_fields[$key][1] == '12'))
{$html_ad=$html_ad."
<td $adtbstl  >
<nobr>$row[$key]</nobr> </td>";}
}

if ($photos_count > 0)
{
if ($row['adphotos']=="yes"){$adphtovl=$msg['pictvl'];}
else {$adphtovl="-";}
$html_ad=$html_ad."
<td $adtbstl align='center' width='2%'>
<font class='smallfnt'> 
 $adphtovl 
</font>
</td>
";
}

if ($use_gl_map=='yes' and  $clmnmap=='yes'){
$html_ad=$html_ad."
<td $adtbstl  align='center' width='5%'> 
<font class='smallfnt'> 
".adsjsp($row['addrmap'],$idnum)."
</font> 
</td>
";
}

$html_ad=$html_ad."
<td $adtbstl class='td7'   width='10%'>
<font class='smallfnt'> 
 $ad_date 
</font>
</td>
";


if ($ct =='')
{
 
$key_ctn=$row['catname'];
$ctval=$key_ctn;
$html_ad=$html_ad."
<td $adtbstl align='center' width='10%'>
<font class='smallfnt'>
 <a href='{$indx_url}md=browse&ct=$ctval'>
".$categories[$key_ctn][0]."
 </a></font>
&nbsp;
</td>
";
}

$html_ad=$html_ad."</tr>";

return $html_ad;
}


?>
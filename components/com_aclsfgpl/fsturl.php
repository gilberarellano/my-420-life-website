<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

# to write root directory for categories first index page instead  "1.html" (yes, no)
$wrtrtdr="yes"; 

 
$sturlctname="no";

if ($waflnscr=="yes" or $mbac_second=='m'){$sturlvar="no";}

function sturl_top()
{
global $sturlvar, $urlclscrpt1, $indx_url;
if ($sturlvar=="yes"){$sturl="{$urlclscrpt1}cl/";}
else {$sturl="$indx_url";}
return $sturl;
}


function sturl_ct_top($ct,$getcityval, $page)
{
global $_REQUEST, $sturlvar, $urlclscrpt1, $categories, $sturlctname, $indx_url, $wrtrtdr;
$cityvar=$_REQUEST['city'];
if ($sturlvar=="yes"){

$pgnvar="{$page}.html"; if ($wrtrtdr=="yes" and $page=="1"){$pgnvar="";}

$sturlctvr=""; if ($sturlctname=="yes"){$sturlctvr="/".sturl_replc($categories[$ct][0]);}
if ($cityvar!=''){$sturl="{$urlclscrpt1}cl/cities/$cityvar/ct/{$ct}{$sturlctvr}/{$pgnvar}";}
else{
$sturl="{$urlclscrpt1}cl/ct/{$ct}{$sturlctvr}/{$pgnvar}";
}
}
else {$sturl="{$indx_url}ct=$ct&{$getcityval}";}
return $sturl;
}

function sturl_ads($ct, $id)
{
global $_REQUEST, $sturlvar, $urlclscrpt1, $categories, $sturlctname, $indx_url;
if ($sturlvar=="yes"){ 
$sturl="{$urlclscrpt1}cl/ads/{$id}.html";
}
else {$sturl="{$indx_url}ct=$ct&md=details&id=$id";}
return $sturl;
}

function sturl_ct_ind($ct)
{
global $_REQUEST, $sturlvar, $urlclscrpt1, $categories, $sturlctname, $indx_url, $wrtrtdr ;
if ($sturlvar=="yes"){
$sturlctvr=""; if ($sturlctname=="yes"){$sturlctvr="/".sturl_replc($categories[$ct][0]);}
$pgnvar="1.html"; if ($wrtrtdr=="yes"){$pgnvar="";}
$sturl="{$urlclscrpt1}cl/ct/{$ct}{$sturlctvr}/{$pgnvar}";}else {
$sturl="{$indx_url}md=browse&ct=$ct";
}
return $sturl;
}



function sturl_ct_pgs($ct, $i, $search_str)
{
global $_REQUEST, $sturlvar, $urlclscrpt1, $categories, $sturlctname, $indx_url, $wrtrtdr;


if ($sturlvar=="yes" and $_REQUEST['mds']==""){

$pgnvar="{$i}.html"; if ($wrtrtdr=="yes" and $i=="1"){$pgnvar="";}

$sturlctvr=""; if ($sturlctname=="yes"){$sturlctvr="/".sturl_replc($categories[$ct][0]);}
$cityvar=$_REQUEST['city'];
if ($cityvar!=""){$sturl="{$urlclscrpt1}cl/cities/$cityvar/ct/{$ct}{$sturlctvr}/{$pgnvar}";}
else {$sturl="{$urlclscrpt1}cl/ct/{$ct}{$sturlctvr}/{$pgnvar}";}

}

else {$sturl="{$indx_url}ct=$ct&md=browse&page=$i&$search_str";}
return $sturl;
}

function sturl_replc($vr)
{
$res=ereg_replace(' ', '_', $vr);
$res=ereg_replace('/', '_', $res);
$res=ereg_replace('!', '', $res);
$res=ereg_replace('\.', '', $res);
$res=ereg_replace('&', '', $res);
return $res;
}
?>
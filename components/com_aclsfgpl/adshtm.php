<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

function browse_ads()
{
global $ads_count,  $ct, $mds, $schopt,$urlclscrpt,
$categories, $ad_ind_width, $text_userinfo, $cat_fields, $ratedads, $ind_leftcol, $templ,
$tpadsindd,$bc2adsfrm,$HTTP_GET_VARS,
$adbnrcll, $width_2tpf, $schresinfo, $msg, $idadcol, $_REQUEST, $javastl, 
$pltopratedln, $pltopcontln, $pltopvisitln,
$fld_dim, $fld_dimd, $check_rate, $ad_date, $ad_fields, $idnum, $ad_comment, $ad_title, 
$check_photos, $categ_name, $cat_fields, $fields_layer, $photo_path, $lr_fields, $use_gl_map,
 $hltadsf, $indx_url ; 

if ($HTTP_GET_VARS['mfvrt']=="1") {$schresinfo=$msg['your_favorite_ads'];}

if($categories[$ct][2]!=""){$check_subcateg=1;}else{$check_subcateg="";}

$idadcol='0';
if ($mds=="search") 
{$count_info=" $ads_count ".$msg['ads_match_your_query'];}
else {$count_info="$ads_count ".$msg['entries'];}  
$citygetval="";
if(($mds!='search') and ($HTTP_GET_VARS[$schopt]!="")){$citygetval="?".$schopt."=".$HTTP_GET_VARS[$schopt];}
# get_ads_captions();


include($templ['index']);
return;
}


function subcat_options()
{
global $ads_fields,  $HTTP_GET_VARS, $fntclr_3, $fntclr_1;

 
$vhtml="&nbsp; &nbsp; ";

$k1='';
foreach ($ads_fields as $key => $value )
{
if ($HTTP_GET_VARS[$key] != "") { 
$vhtml=$vhtml." &nbsp;<nobr><font class=stfntb><b>
".$ads_fields[$key][0].":  ".$HTTP_GET_VARS[$key]."</b></font></nobr> ";
}
}

global $rmmbrloc, $msg, $indx_url;

if ($HTTP_GET_VARS['city']!=""){
if ($HTTP_GET_VARS['ct']!=""){$url_val="ct=".$HTTP_GET_VARS['ct']."&";}
if ($HTTP_GET_VARS['gmct']!=""){$url_val="md=browse&mds=search&gmct=".$HTTP_GET_VARS['gmct']."&";}
$vhtml=$vhtml." &nbsp;&nbsp;<font class='smallfnt'><nobr>(<a href='$indx_url".$url_val."allc=1'>".$msg['all_cities']."</a> $rmmbrloc )</nobr></font>";
}


return $vhtml;
}


function print_ad($row)
{
global $cat_fields, $photos_count, $ct, $categories, $incl_prevphoto, $urlclscrpt,
$previewphoto_url, $previewphoto_path, $prphotolimits, $pr_lim_height, $pr_lim_width,
$prwph_layer, $colr_hltads, $capt_hltads, $prphscr,$prphscrwdth,$prwdly, $photo_path,
$tbclr_1, $tbclr_2, $tbclr_3, $tbclr_4, $fntclr_1, $lnkttlfl, $msg,
$idadcol, $colspn, $tbclhlads, $coltblayer,
$check_rate, $ad_date, $ad_fields, $idnum, $ad_comment, $ad_title,  $fld_dim, $fld_dimd,
$check_photos, $categ_name, $lr_fields, $fields_layer, $vrwcomm, $indx_url;

$vrwcomm=$row['adcommkey'];

$row=check_row($row);


if($row['adrate']>0){$check_rate=1;} else {$check_rate=0;}
if($row['adphotos']=='yes'){$check_photos=1;} else {$check_photos=0;}
 

$time1=$row['time'];
$ad_date=get_short_date($time1);
$idnum=$row['idnum'];

$ctval=$ct;
 
$key_ctn=$row['catname'];
$categ_name="
<a href='{$indx_url}md=browse&ct=$key_ctn'>
".$categories[$key_ctn][0]."</a>
";


$ad_title="&nbsp;
<a href='{$indx_url}ct=$ctval&md=details&id=$idnum' 
  title='#$idnum' ".print_layerlink($idnum)." class='ad_title'>
".$row['title']."</a> 
";

$ad_fields=""; $lr_fields="";
foreach ($cat_fields as $key => $value) 
{
if (($fields_layer[$key] == 'yes') and ($row[$key]!="") and ($row[$key]!="--") )
{$lr_fields[$key]=$row[$key];}

if ( ($cat_fields[$key][1]=='1') or ($cat_fields[$key][1] == '12'))
{
if (($row[$key]!="--") and ($row[$key]!="")) {$ad_fields[$key]=$row[$key]; }
}

}

if ($row['adcommkey']==1){
$ad_comment="
 <font class='fnt6'><nobr><a href='{$indx_url}md=details&id=".$row['replyid']."'   >
 ".$msg['View_initia_ad']."(#".$row['replyid'].")</a></nobr></font>
";
}

if ($row['comment']!="")
{$ad_comment=$ad_comment."<br> &nbsp;&nbsp;<font class='fnt7'>".$row['comment']."</font>";}

}

function print_layerjavas()
{
global $use_adslayer, $md, $vrwcomm, $use_ajax;

if ($use_ajax=="yes"){
$html_layerjs=js_layerv();
}
else {
if (($use_adslayer=="yes") and (($md=="") or ($md=="browse") or ($md=="details")) and ($vrwcomm!=1))
{
$html_layerjs= " <script language='JavaScript'>
<!--
function showlayer(nlayer)
{
 
if (document.layers)
document.layers[nlayer].visibility='show';
else 
document.getElementById(nlayer).style.visibility='visible';
} 

function hidelayer(nlayer)
{
if (document.layers)
document.layers[nlayer].visibility='hide';
else 
document.getElementById(nlayer).style.visibility='hidden';
} 
-->
</script>
";
}
}

return $html_layerjs;
}

function print_layerlink($ads_id)
{
global $use_adslayer, $vrwcomm,  $hltlstk;

if ($use_adslayer=="yes" and $vrwcomm!=1)
{
$adsidq1="dd".$ads_id.$hltlstk;
$html_layerlink="
OnMouseOver=\"showlayer('$adsidq1');\"
OnMouseOut=\"hidelayer('$adsidq1');\"
"; 
}
return $html_layerlink;
}

?>
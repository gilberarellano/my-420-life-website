<?php
/**
*  This file is part of Almond Classifieds Component for Joomla! (site:http://www.ads-programming.com/acj)
*  Copyright (C) 2008-2009 Ads-Programming.Com. All rights reserved.
*  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL  
*/

$msg=array(

# top categories list
'categories' => 'Categories:',
'subcategories' => 'more...',
'choosecity' =>'Choose',
'choosecity2' =>'Choose City',
'all_cities' => "All Cities",
'ads_t' => 'ads',
'updated_t' => 'updated',
'choose_category' => 'Choose Category',
'all_categories' => 'All Categories',
'top_ads' => 'Top Ads:',
'events' => 'Events',
'rememb_loc' => 'Remember City',
'next_start_loc' => 'Your next classifieds sessions  will start <br> with  city',
'js_choose_corr_cat' =>" Sorry, you have selected a group title or empty item. Please choose the correct category!",
'pictvl' => 'pic', 
'all_ads1' => "All",
'Please_choose_one_sl' => 'Please choose one',

'no_favorites' => 'No ads have been chosen as favorites at present time',

'top_loading' => "<font class='stfnt' style='color: #777777'>Loading...</font>",
'no_favorites' => 'No ads have been chosen as favorites at present time',
'tp_highlighted_ads' => 'Highlighted Ads:',
'tp_latest_ads' => 'Latest Ads:',


# ads index 
'top' => 'Top',
'ads_match_your_query' => 'ads match your query',
'entries' => 'entries',
'post_new_ad' => 'Post New Ad',
'edit_ad' => 'Edit Ad',
'top_contacted_ads' => 'Top contacted ads',
'top_commented_ads' => 'Top commented ads',
'top_rated_ads' => 'Top rated ads',
'top_visited_ads' => 'Top visited ads',
'ads_rated_by_admin' => 'Ads rated by admin:',
'Best_Ads' => 'Best Ads',
'No_prev_photo' => 'No preview photo',
'view_all_ads' => '(View All Ads)',


# Search form
'Search' => 'Search',
'Keyword' => 'Keyword',
'only_for_last' => 'Only  for last ',
'days' => 'days',
'only_with_photos' => 'With photos',
# Submit button value 
'searchsubm' => 'Search',
'search_fields_optional' => 'All search fields are optional',

# ads layout
'category' => 'Category',
'adsid' => 'ID#',
'preview_photo' => ' Preview ',
'photoind' => 'Photo',
'posted' => 'Posted',
'no_preview_photo' => 'No preview photo',
'View_initia_ad' => 'View initial ad',
'photos2' => 'photos',
'Details' => 'Details',

# Comments
'ads_posted_by_user' => 'Ads posted by the same user',
'ads_posted_by_member' => 'Ads posted by member ',
'replies_for' => 'Replies for', 
'ad_rp' => 'ad',

# Ads pages listing
'listingads' => 'Listing',
'page_of' => 'Page',
'of_pg' => 'of',
'previous_pg' => 'Previous',
'next_pg' => 'Next',

# Ad details page
'photo_gallery' => 'Photo Gallery',
'photo_d' => 'Photo',
'multimedia_file' => 'Attachment file',
'dtlp_link' => 'Link',
'dtlp_video' => 'Video',
'Click_ph_to_enlarge' => 'Click the photos to enlarge',
'click_ph_to_hide' => 'click the photo to hide',

'privacy_mail' => 'Privacy Mail',
'members_information' => 'Ad owner\'s information',
'browse_ads_by_user' => 'Browse all ads posted by this user',
'ads_d' => 'Ads',
'visits_d' => 'visits',
'no_one_email_sent' => 'no one e-mail was sent to ad owner',
'was_sent_to_owner' => 'was sent to ad owner',
'email_snt' => 'e-mails',
'email_snt_one' => 'e-mail',
'browse_ads_by_member' => 'Browse all ads posted by member',
'Home_Page_d' => 'Home Page',
'contact_email' => 'Contact e-mail',
'Highlight_this_ad' => 'Highlight this ad (for ad owner)',
'ad_posted_member' => 'Ad posted by member',
'date_posted_d' => 'Date posted',
'expire_date_d' => 'Expire date',
'press_to_bookmark' => 'Press Ctrl+D to bookmark this page',
'close_window_d' => 'Close Window',
'not_rated_yet'  => 'Not rated yet. Be first to rate this ad !', 
'current_rate' => 'Current rate',
'votes_v' => 'votes',
'rate_this_ad' => 'Rate this ad',
'no_ad_with_id' => 'No Ad with ID#',

'k_no_ads_try_separately' => 'No ads found for this query. <p> Try to search keywords separately through all ads database :',
'k_no_ads_try_all_db' => 'No ads found for this query. <p>  Try to search keywords through all ads database:',
'k_no_ads_try_other_kw' => 'No ads found for this query. Try other keywords',
'k_search_keywords_all_db' => '&nbsp; &nbsp; &nbsp; Also try to search keywords through all ads database :',

'inaccessible_ad' => 'This ad is inaccessible at present time <br> and will apear 
                      in the index after approving by admin.',

# Comments on detailed page
'Comments_d' => 'Comments',
'post_your_comment' => 'Post your comment',
'posted_c' => 'posted',
'More_c' => 'More...',
'view_all_comments' => 'View all comments',
'your_favorite_ads' => 'Your favorite ads',
'add_to_favorites' => 'Add to favorites',
'remove_this_ad' => 'Remove this ad from favorites',
'remove_all_favorites' => 'Remove all favorites',
'add_ad_to_favorite' => 'Add this ad to favorite list',

'Incorect_operation_v' => 'Incorect operation !',

# login info
'Welcome_m' => 'Welcome',
'Your_ads_m' => 'Your ads',
'Your_Profile_m' => 'Your Profile',
'Log_out_m' => 'Log out',

'first_sel_country' => 'first select the country',
'opt_choose_one' => '<option>Please choose one',
'Choose_Country2' => 'Choose Country'
);

# Short names for months
$months_short =array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
# full names for month
$months_nm=array('January', 'February', 'March', 'April', 'May', 'June',  
'July', 'August', 'September', 'October', 'November','December');

$weekdays=array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
?>
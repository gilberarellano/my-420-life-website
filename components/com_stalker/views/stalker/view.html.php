<?php
/**
 * $Id: view.html.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Default View for Stalker Component
 * 
 * @package    	Stalker
 * @subpackage 	Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link 		http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 * 
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class StalkerViewStalker extends JView
{
    function display($tpl = null)
    {
		JHTML::stylesheet('stalker.css', 'components/com_stalker/assets/css/', '');

        $app =& JFactory::getApplication();
        $params =& $app->getParams();
		
		if($params->get('show_page_heading')) {
			$page_title = $params->get('page_heading', JText::_('COM_STALKER_STALK_ME'));
		} else {
			$page_title = '';
		}

		$iconsize		=  $params->get('iconsize', 32);
		$style			=  $params->get('style', 0);
		$position		=  $params->get('position', 'left');
		$group			=  $params->get('stalkergroup', "");
		$imageset 		=  $params->get('imageset', 'default');

	    $model 			= $this->getModel('stalker');
        $stalker 		= $model->getData($group);

		$this->assignRef('pageTitle',	$page_title);
		$this->assignRef('stalker',		$stalker);
		$this->assignRef('style',		$style);
		$this->assignRef('align',		$position);
		$this->assignRef('iconsize',	$iconsize);
		$this->assignRef('imageset',	$imageset);

        parent::display($tpl);
    }
}

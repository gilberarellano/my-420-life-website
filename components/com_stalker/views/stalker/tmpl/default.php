<?php
/**
 * $Id: default.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Default View for Stalker Component Front-end
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */

// No direct access
defined('_JEXEC') or die('Restricted access'); 
?>

<?php if (!empty ($this->pageTitle)): ?>
<h1 class="title"><?php echo $this->pageTitle; ?></h1>
<?php endif; ?>

<div id="comstalker">
  <div id="comstalker<?php echo $this->align ?>">
    <ul>
<?php
	$img_path = "media/stalker/icons/" . $this->imageset . "/";

    for ($i=0, $n=count($this->stalker); $i < $n; $i++)
   	{
      	$row 			=& $this->stalker[$i];
		$url	 		= str_replace("#id#", $row->username, $row->socneturl);
		$iid 			= str_replace(" ", "", $row->socnet);
		$linktitle		= (is_null($row->linktitle) || strlen(trim($row->linktitle)) == 0) ? $row->socnet . ": " . $row->username : $row->linktitle;
		$imagealt		= (is_null($row->imagealt) || strlen(trim($row->imagealt)) == 0) ? $row->socnet . ": " . $row->username : $row->imagealt;

		if (strlen(trim($row->target)) == 0) {
			$target = "_blank";
		} else {
			$target = $row->target;
		}

		switch($this->style) {
			case 2:						
				echo '<li class="' . $this->align . 'withtext"><a href="' . $url . '" rel="nofollow" title="' . $linktitle . '" target="' . $target . '" >' . $row->socnet . '</a></li>';
				break;
			case 1:
				if ($this->align == "right") {
						echo '<li class="' . $this->align . 'withtext"><a href="' . $url . '" rel="nofollow" title="' . $linktitle . '" target="' . $target . '">' . $row->socnet . '</a> <a href="' . $url . '" rel="nofollow" title="' . $linktitle . '" target="' . $target . '">' . JHTML::image($img_path . $row->image, $imagealt, 'width="' . $this->iconsize . '" height="' . $this->iconsize . '" id="' . $iid . '" style="width:' . $iconsize . 'px; height:' . $iconsize . 'px;" ') . '</a></li>';
					} else {
						echo '<li class="' . $this->align . 'withtext"><a href="' . $url . '" rel="nofollow" title="' . $linktitle . '" target="' . $target . '">' . JHTML::image($img_path . $row->image, $imagealt, 'width="' . $this->iconsize . '" height="' . $this->iconsize . '" id="' . $iid . '" style="width:' . $iconsize . 'px; height:' . $iconsize . 'px;" ') . '</a> <a href="' . $url . '" rel="nofollow" title="' . $linktitle . '" target="' . $target . '">' . $row->socnet . '</a></li>';
					}
				break;
			default:
				echo '<li class="' . $this->align . 'notext"><a href="' . $url . '" rel="nofollow" title="' . $linktitle . '" target="' . $target . '">' . JHTML::image($img_path . $row->image, $imagealt, 'width="' . $this->iconsize . '" height="' . $this->iconsize . '" id="' . $iid . '" style="width:' . $iconsize . 'px; height:' . $iconsize . 'px;" ') . '</a></li>';
				break;
		}
	}

?>
	</ul>
  </div>
  <div class="clear"></div>
</div>

<?php
/**
 * $Id: controller.php 199 2011-07-07 23:08:46Z meloman $
 * 
 * Controller for Stalker Component
 * 
 * @package     Stalker
 * @subpackage  Components
 * @author      Alain Rivest <info@aldra.ca>
 * @link        http://aldra.ca/
 * @license     GNU/GPL
 * 
 * Before v1.3.0 :
 *   Nick Texidor <nick@texidor.com>
 *   http://www.nicktexidor.com
 *   Copyright (C) 2008-2011 Nick Texidor. All rights reserved.
 */
 
// No direct access 
defined('_JEXEC') or die('Restricted access');
 
jimport('joomla.application.component.controller');

class StalkerController extends JController
{
    /**
     * Method to display the view
     *
     * @access    public
     */
    function display()
    {
        parent::display();
    }
}

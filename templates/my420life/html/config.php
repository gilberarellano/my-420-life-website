<?php
/**
* @package   my420life Template
* @file      config.php
* @version   1.0.2 May 2011
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright (C) 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// get config
require_once(dirname(dirname(__FILE__)).'/config.php');

// get warp
$warp =& Warp::getInstance();
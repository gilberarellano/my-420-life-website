<?php
/**
* @package   Warp Theme Framework
* @file      _item.php
* @version   6.0.2
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<?php if ($params->get('item_title')) : ?>
<h3>
	<?php if ($params->get('link_titles') && $item->linkOn != '') : ?>
		<a href="<?php echo $item->linkOn;?>"><?php echo $item->title;?></a>
	<?php else : ?>
		<?php echo $item->title; ?>
	<?php endif; ?>
</h3>
<?php endif; ?>

<?php if (!$params->get('intro_only')) echo $item->afterDisplayTitle; ?>

<?php echo $item->beforeDisplayContent; ?>

<?php echo $item->text; ?>

<?php if (isset($item->linkOn) && $item->readmore && $params->get('readmore')) : ?>
  <a class="readmore" href="<?php echo $item->linkOn; ?>"><?php echo $item->linkText ?></a>
<?php endif; ?>

<?php
/**
* @package   Warp Theme Framework
* @file      vert.php
* @version   6.0.2
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<?php if (count($list) > 0) : ?>
	<ul class="newsflash line">
		<?php for ($i = 0, $n = count($list); $i < $n; $i ++) : ?>
		<li class="item <?php if ($i == $n - 1) echo 'last'; ?>">
			<?php modNewsFlashHelper::renderItem($list[$i], $params, $access); ?>
		</li>
		<?php endfor; ?>
	</ul>
<?php endif; ?>

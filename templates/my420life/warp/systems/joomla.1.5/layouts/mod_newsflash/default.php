<?php
/**
* @package   Warp Theme Framework
* @file      default.php
* @version   6.0.2
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

srand((double) microtime() * 1000000);

$flashnum	= rand(0, $items -1);
$item		= $list[$flashnum];

modNewsFlashHelper::renderItem($item, $params, $access);

?>
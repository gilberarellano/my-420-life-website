<?php
/**
* @package   Warp Theme Framework
* @file      default.php
* @version   6.0.2
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$cparams =& JComponentHelper::getParams('com_media');

?>

<div id="system">

	<?php if ($this->params->get('show_page_title', 1)) : ?>
	<h1 class="title"><?php echo $this->escape($this->params->get('page_title')); ?></h1>
	<?php endif; ?>

	<?php if ($this->category->description || $this->category->image) :?>
	<div class="description">
		<?php if ($this->category->image) : ?>
			<img src="<?php echo $this->baseurl . '/' . $cparams->get('image_path') . '/'. $this->category->image;?>" alt="<?php echo $this->category->image; ?>" class="size-auto align-<?php echo $this->category->image_position;?>" />
		<?php endif; ?>
		<?php if ($this->category->description) echo $this->category->description; ?>
	</div>
	<?php endif; ?>

	<?php
		$this->items =& $this->getItems();
		echo $this->loadTemplate('items');
	?>
	
	<?php if ($this->access->canEdit || $this->access->canEditOwn) : ?>	
	<p class="edit"><?php echo JHTML::_('icon.create', $this->category  , $this->params, $this->access); ?>	 <?php echo JText::_('Create new article.'); ?></p>
	<?php endif; ?>

</div>
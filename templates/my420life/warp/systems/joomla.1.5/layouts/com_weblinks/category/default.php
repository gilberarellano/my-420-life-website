<?php
/**
* @package   Warp Theme Framework
* @file      default.php
* @version   6.0.2
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<div id="system">

	<?php if ($this->params->get('show_page_title', 1)) : ?>
	<h1 class="title"><?php echo $this->escape($this->params->get('page_title')); ?></h1>
	<?php endif; ?>

	<?php if (@$this->category->image || @$this->category->description) : ?>
	<div class="description">
		<?php if (isset($this->category->image)) echo $this->category->image; ?>
		<?php echo $this->category->description; ?>
	</div>
	<?php endif; ?>

	<?php echo $this->loadTemplate('items'); ?>

	<?php if ($this->params->get('show_other_cats', 1)): ?>
	<ul>
		<?php foreach ($this->categories as $category) : ?>
		<li>
			<a href="<?php echo $category->link; ?>"><?php echo $this->escape($category->title);?></a>
			<small>(<?php echo $category->numlinks;?>)</small>
		</li>
		<?php endforeach; ?>
	</ul>
	<?php endif; ?>

</div>
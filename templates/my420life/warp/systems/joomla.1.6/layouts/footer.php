<?php
/**
* @package   Warp Theme Framework
* @file      footer.php
* @version   6.0.2
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// output tracking code
echo $this['config']->get('tracking_code');
<?php
/**
* @package   Warp Theme Framework
* @file      default.php
* @version   6.0.2
* @author    My420Admin admin@my420life.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// add javascript
$warp = Warp::getInstance();
$warp['system']->document->addScript($warp['path']->url('js:search.js'));

?>

<form id="searchbox" action="<?php echo JRoute::_('index.php'); ?>" method="post" role="search">
	<input type="text" value="" name="searchword" class="searchword" placeholder="<?php echo JText::_('TPL_WARP_SEARCH'); ?>" />
	<!-- XB -->
	<!-- <button type="reset" value="Reset"></button> -->
	<input type="hidden" name="task" value="search" />
	<input type="submit" name="task"   value="Go!" class="searchgo">
	<input type="hidden" name="option" value="com_search" />
</form>

<script type="text/javascript">
jQuery(function($) {
	$('#searchbox input[name=searchword]').search({'url': '<?php echo JRoute::_("index.php?option=com_search&tmpl=raw&type=json&ordering=&searchphrase=all");?>', 'param': 'searchword', 'msgResultsHeader': '<?php echo JText::_("TPL_WARP_SEARCH_RESULTS"); ?>', 'msgMoreResults': '<?php echo JText::_("TPL_WARP_SEARCH_MORE"); ?>', 'msgNoResults': '<?php echo JText::_("TPL_WARP_SEARCH_NO_RESULTS"); ?>'}).placeholder();
});
</script>
<?php
/**
* @package   Warp Theme Framework
* @file      default.php
* @version   6.0.2
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="breadcrumbs"><?php

	if (!$params->get('showLast', 1)) array_pop($list);

	$count = count($list);

	for ($i = 0; $i < $count; $i ++) {
	
		// clean subtitle from breadcrumb
		if ($pos = strpos($list[$i]->name, '||')) {
			$name = trim(substr($list[$i]->name, 0, $pos));
		} else {
			$name = $list[$i]->name;
		}
		
		// mark-up last item as strong
		if ($i < $count-1) {
			if(!empty($list[$i]->link)) {
				echo '<a href="'.$list[$i]->link.'">'.$name.'</a>';
			} else {
				echo '<span>'.$name.'</span>';
			}
		} else {
			echo '<strong>'.$name.'</strong>';
		}

	}

?></div>
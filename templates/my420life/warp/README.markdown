# Warp Theme Framework #

- Version: 6.0.2
- Date: May 2011
- Author: My420Life GmbH
- Website: <http://www.yootheme.com/warp>

## Changelog

	6.0.2
	^ Added CSS class namespace for fluid images
	^ Changed attribute selectors to remove margin in system items
	# Fixed apply shortcodes in ajax search (WP)
	# Fixed home menu selection with index.php (WP)
	# Fixed ajax search with sef turned on (J15+J16)
	
	6.0.1
	^ Added login to offline page (J15+J16)
	^ Updated module layouts
	# Fixed DOM helper prev/next issue
	# Fixed system specific RTL CSS
	# Fixed IE asset caching
	^ Changed date in com_content override (J16)
	# Fixed login description in com_users override (J16)
	# Fixed even/odd style in com_content override (J16)
	# Fixed login style in mod_login override (J16)
	# Fixed login module (J16)
	# Fixed translation of TPL_WARP_POSTED_IN (J16)
	+ Added override for mod_custom (J16)

	6.0.0
	+ Initial Release

	* -> Security Fix
	# -> Bug Fix
	$ -> Language fix or change
	+ -> Addition
	^ -> Change
	- -> Removed
	! -> Note
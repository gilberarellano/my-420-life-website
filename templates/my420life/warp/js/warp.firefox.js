/* Copyright 2011 My420Life LLC, My420Life Proprietary Use License */
(function (g) {
    g.fn.matchHeight = function (e) {
        var a = 0;
        this.each(function () {
            a = Math.max(a, g(this).outerHeight());
        });
        return this.each(function () {
            var c = g(this),
                b = e ? c.find(e + ":first") : g(this);
            c = b.height() + (a - c.outerHeight());
            b.css("min-height", c + "px");
        });
    };
    g.fn.matchWidth = function (e) {
        return this.each(function () {
            var a = g(this),
                c = a.children(e),
                b = 0;
            c.width(function (d, f) {
                if (d < c.length - 1) {
                    b += f;
                    return f;
                }
                return a.width() - b;
            });
        });
    };
    g.fn.smoothScroller = function (e) {
        e = g.extend({
            duration: 1E3,
            transition: "easeOutExpo"
        }, e);
        return this.each(function () {
            g(this).bind("click", function () {
                var a = this.hash,
                    c = g(this.hash).offset().top,
                    b = window.location.href.replace(window.location.hash, ""),
                    d = g.browser.opera ? "html:not(:animated)" : "html:not(:animated),body:not(:animated)";
                if (b + a == this) {
                    g(d).animate({
                        scrollTop: c
                    }, e.duration, e.transition, function () {
                        window.location.hash = a.replace("#", "");
                    });
                    return false;
                }
            });
        });
    };
    g.fn.backgroundFx = function (e) {
        e = g.extend({
            duration: 9E3,
            transition: "swing",
            colors: ["#FFFFFF", "#999999"]
        }, e);
        return this.each(function () {
            var a = g(this),
                c = 0,
                b = e.colors;
            window.setInterval(function () {
                a.stop().animate({
                    "background-color": b[c]
                }, e.duration, e.transition);
                c = c + 1 >= b.length ? 0 : c + 1;
            }, e.duration * 2);
        });
    };
})(jQuery);
(function (g) {
    g.easing.jswing = g.easing.swing;    
})(jQuery);
(function (g) {
    function e(f) {
        var h = {}, i = /^jQuery\d+$/;
        g.each(f.attributes, function (k, j) {
            if (j.specified && !i.test(j.name)) h[j.name] = j.value;
        });
        return h;
    }

    var b = "placeholder" in document.createElement("input"),
    d = "placeholder" in document.createElement("textarea");
    
    g.fn.placeholder = function () {} //XB
    
})(jQuery);
<?php
/**
* @package   Warp Theme Framework
* @file      menu.php
* @version   6.0.2
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

/*
	Class: WarpMenu
		Menu base class
*/
class WarpMenu{

	/*
		Function: process
			Abstract function. New implementation in child classes.

		Returns:
			Xml Object
	*/	
	public function process($xmlobj, $level=0) {
		return $xmlobj;
	}

}
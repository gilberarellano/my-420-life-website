<?php
/**
* @package   Warp Theme Framework
* @file      stack.php
* @version   6.0.2
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

$i=0; // XB

foreach ($modules as $module) {

	/* XB */	
	if(isset($_GET["option"]) && $_GET["option"]=="com_jreviews") { 
		printf('<!-- XB -->');
		if($i%2==0) { 
			printf('<div class="grid-box-left grid-v">%s</div>', $module);
		}
		else {
			printf('<div class="grid-box-right grid-v">%s</div>', $module);
		}
		printf('<!-- XB //-->');
	}
	else {
	/* XB */
	
		printf('<div class="grid-box width100 grid-v">%s</div>', $module);
		
	} // XB
	$i++;
}
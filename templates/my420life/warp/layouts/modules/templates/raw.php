<?php
/**
* @package   Warp Theme Framework
* @file      raw.php
* @version   6.0.2
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// raw output
echo $content;
<?php
/**
* @package   my420life Template
* @file      changelog.php
* @version   1.0.2 May 2011
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright (C) 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// no direct access
die('Restricted access');

?>

Changelog
------------

1.0.2
# Fixed sidebar-b custom width

1.0.1
^ Updated offline page
# Fixed 3 column layout
# Removed import of nonexistent file in style.css
# Fixed shadow in error page
^ Updated Joomla favicon

1.0.0
+ Initial Release



* -> Security Fix
# -> Bug Fix
$ -> Language fix or change
+ -> Addition
^ -> Change
- -> Removed
! -> Note
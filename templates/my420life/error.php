<?php
/**
* @package   my420life Template
* @file      error.php
* @version   1.0.2 May 2011
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright (C) 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// include config	
include_once(dirname(__FILE__).'/config.php');

// get warp
$warp = Warp::getInstance();

// set messages
$title   = $this->title;
$error   = $this->error->get('code');
$message = $this->error->get('message');

// set 404 messages
if ($error == '404') {
	$title   = JText::_('TPL_WARP_404_PAGE_TITLE');
	$message = JText::sprintf('TPL_WARP_404_PAGE_MESSAGE', $warp['system']->url, $warp['config']->get('site_name'));
}

// render error layout
echo $warp['template']->render('error', compact('title', 'error', 'message'));
<?php
/**
* @package   my420life Template
* @file      raw.php
* @version   1.0.2 May 2011
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright (C) 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

?>
<jdoc:include type="component" />
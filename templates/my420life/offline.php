<?php
/**
* @package   my420life Template
* @file      offline.php
* @version   1.0.2 May 2011
* @author    Jorge Vazquez mrjvazquez@gmail.com
* @copyright Copyright (C) 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// include config	
include_once(dirname(__FILE__).'/config.php');

// get warp
$warp = Warp::getInstance();

// render offline layout
echo $warp['template']->render('offline', array('title' => JText::_('TPL_WARP_OFFLINE_PAGE_TITLE'), 'error' => 'Offline', 'message' => JText::_('TPL_WARP_OFFLINE_PAGE_MESSAGE')));
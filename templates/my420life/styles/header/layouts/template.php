<?php
/**
* @package   my420life Template
* @file      template.php
* @version   1.0.2 May 2011
* @author    My420Admin admin@my20life.com
* @copyright Copyright (C) 2011 My420Life LLC
* @license   My420Life Proprietary Use License
*/

// get template configuration
include($this['path']->path('layouts:template.config.php'));
	
?>
<!DOCTYPE HTML>
<html lang="<?php echo $this['config']->get('language'); ?>" dir="<?php echo $this['config']->get('direction'); ?>">

<head>
<!-- templates/my420life/styles/header/layouts/template.php -->
<?php /* XB */
include("skins.php"); 
?>
<!-- XB -->
<?php 
jimport ( 'joomla.utilities.arrayhelper' );
$my 	= & JFactory::getUser ();
$userid	=  JRequest::getVar('userid', $my->id);
$user	= CFactory::getUser($userid);
$profile->largeAvatar = $user->getAvatar(); 
?>
<?php echo $this['template']->render('head'); ?>
<?php $doc =& JFactory::getDocument(); ?>
<!-- cometchat -->
<?php //$doc->addStyleSheet( 'cometchat/cometchatcss.php' ); ?>
<?php //$doc->addScript("cometchat/cometchatjs.php"); ?>
<!-- rightbar -->
<?php #$doc->addStyleSheet( '/scripts/scrollerplus/scrollerplus.css'); ?>
<?php #$doc->addScript('/scripts/scrollerplus/scrollerplus.js'); ?>

<?php $language =& JFactory::getLanguage(); ?>
<?php $language->load('custom' , dirname(__FILE__), $language->getTag(), true); ?>
<?php //$doc->addStyleSheet('templates/my420life/warp/css/ie.css'); ?>
</head>

<body id="page" class="page <?php echo $this['config']->get('body_classes'); ?>">

	<?php if ($this['modules']->count('absolute')) : ?>
	<div id="absolute">
		<?php echo $this['modules']->render('absolute'); ?>
	</div>
	<?php endif; ?>
	
	
	<!-- rightbar -->
	<?php if ($this['modules']->count('cometchat_userstab_popup_rightbar')) : ?>		
		<div id="cometchat_userstab_popup_rightbar" class="cometchat_tabpopup">
			<?php echo $this['modules']->render('cometchat_userstab_popup_rightbar'); ?>
		</div>	
	<?php endif; ?>		
	<!-- XB -->
	
	<!--[if IE]>
	<center>
	<![endif]-->
	<?php if(is_numeric($userid) && $userid>0) { ?>
	<div class="wrapper grid-block">
	<?php } else { ?>
	<div class="wrapper grid-block guest">
	<? } ?>
	
	
		<header id="header">

			<?php if(is_numeric($userid) && $userid>0) { ?>
				<div id="toolbar" class="grid-block blue">
			<?php } else { ?>
				<div id="toolbar" class="grid-block">
			<? } ?>

				<?php if ($this['modules']->count('toolbar-l') || $this['config']->get('date')) : ?>
				<div class="float-left">
				
					<?php if ($this['config']->get('date')) : ?>
					<time datetime="<?php echo $this['config']->get('datetime'); ?>"><?php echo $this['config']->get('actual_date'); ?></time>
					<?php endif; ?>
				
					<?php echo $this['modules']->render('toolbar-l'); ?>
					
				</div>
				<?php endif; ?>
					
				<?php if ($this['modules']->count('toolbar-r')) : ?>
				<div class="float-right"><?php echo $this['modules']->render('toolbar-r'); ?></div>
				<?php endif; ?>
				
				<!-- XB -->
				<div class="fbSidebarGripper">
					<div>
						<span></span>
					</div>
				</div>	

				<!-- XB -->
				<div class="float-right">
					<a class="logout level2" href="/component/user/?task=logout&Itemid=872">
						<!-- <span>Logout</span> -->
					</a>
				</div>
				
			</div>
			
			<div id="headerbar" class="grid-block">
			
				<?php if ($this['modules']->count('logo')) : ?>	
				<a id="logo" href="<?php echo $this['system']->url; ?>"><?php echo $this['modules']->render('logo'); ?></a>
				<?php endif; ?>
	
		                <?php if ($this['modules']->count('banner')) : ?>
               			<div id="banner"><?php echo $this['modules']->render('banner'); ?></div>
                		<?php endif;  ?>

				<?php /*
				<?php if($this['modules']->count('headerbar')) : ?>
				<div class="left"><?php echo $this['modules']->render('headerbar'); ?></div>
				<?php endif; ?>
				*/
				?>

			</div>

			<div id="menubar" class="grid-block">
				
				<?php  if ($this['modules']->count('menu')) : ?>
				<nav id="menu"><?php echo $this['modules']->render('menu'); ?></nav>
				<?php endif; ?>
								
				<?php
				// XB
				if(isset($debug) && $debug>=1) { 
					echo "<h2>GET VARIABLES: ";
					print_r($_GET);				
					echo "</h2><br>\n";
				}
				?>
								
				<? 
				//if($_GET["option"]=="com_jreviews" && $_GET["view"]!="newlisting") { 
				if(!empty($_GET["view"])) $view=$_GET["view"];  else $view="";
				if(!empty($_GET["url"]))  $url=$_GET["url"];	else $url="";	

				if(isset($debug) && $debug>=1) { echo "<h2>URL: ".$url."</h2><br>\n"; }
				
				if(isset($_GET["option"]) && $_GET["option"]=="com_jreviews" && $view!="newlisting" && !preg_match("/"."search"."/i",$url)) {
				?>
				<!-- XB -->
				<?php if ($this['modules']->count('search')) : ?>
				<div id="search"><?php echo $this['modules']->render('search'); ?></div>				
				<?php endif; ?>
				<? } ?>
				<!-- XB -->			
				
			</div>
		
		</header>

		<?php if ($this['modules']->count('breadcrumbs')) : ?>
                <section id="breadcrumbs"><div class="grid-block"><?php echo $this['modules']->render('breadcrumbs', array('layout'=>$this['config']->get('breadcrumbs'))); ?></div></section>
                <?php endif; ?>
		
		
		<!-- XB -->
		<?php if ($this['modules']->count('top-a')) : ?>
		<div class="separator"></div>
		<section id="top-a">			
			<div class="grid-block">				
				<?php //echo $this['modules']->render('top-a', array('layout'=>$this['config']->get('top-a'))); ?>
				<?php echo $this['modules']->render('top-a'); ?>
			</div>			
		</section>
		<div id="gradient_top_a" class="gradient_top_a">
		</div>
		
		<?php endif; ?>
		
		<?php if ($this['modules']->count('top-b')) : ?>
		<section id="top-b"><div class="grid-block"><?php echo $this['modules']->render('top-b', array('layout'=>$this['config']->get('top-b'))); ?></div></section>
		<?php endif; ?>

		<?php if ($this['modules']->count('innertop + innerbottom + sidebar-a + sidebar-b') || $this['config']->get('system_output')) : ?>
		<?php if(is_numeric($userid) && $userid>0) { ?>
		<div id="main" class="grid-block">
		<?php } else { ?>
		<div id="main" class="grid-block guest">
		<?php } ?>
		
		<div id="maininner" class="grid-box">

				<?php if ($this['modules']->count('innertop')) : ?>
				<section id="innertop"><div class="grid-block"><?php echo $this['modules']->render('innertop', array('layout'=>$this['config']->get('innertop'))); ?></div></section>
				<?php endif; ?>

				<?php if ($this['config']->get('system_output')) : ?>
				
					<!-- leftbar_facebook_menu -->
					<?php if ($this['modules']->count('leftbar_facebook_menu')) : ?>
						<div id="leftbar_facebook_menu">
							<?php echo $this['modules']->render('leftbar_facebook_menu'); ?>
						</div>
					<?php endif; ?>
					
					<section id="content" class="grid-block">				
					<?php 
						$content=$this['template']->render('content');
						echo $content; 
					?>
					
					<?php if ($this['modules']->count('sidebar-b')) : ?>
					<aside id="sidebar-b" class="grid-box <?php echo $view; ?>"><?php echo $this['modules']->render('sidebar-b', array('layout'=>'stack')); ?><div class="sidebar-bg"></div></aside>
					<?php endif; ?>				
					</section>
				
				<?php endif; ?>

				<?php if ($this['modules']->count('innerbottom')) : ?>
				<section id="innerbottom"><div class="grid-block"><?php echo $this['modules']->render('innerbottom', array('layout'=>$this['config']->get('innerbottom'))); ?></div></section>
				<?php endif; ?>				

			</div>
			<!-- maininner end -->
			
			<?php if ($this['modules']->count('sidebar-menu')) : ?>
                        <nav id="sidebar-menu" class="grid-box"><?php echo $this['modules']->render('sidebar-menu', array('layout'=>'stack')); ?><div class="sidebar-bg"></div></nav>
                        <?php endif; ?>


			<?php if ($this['modules']->count('sidebar-a')) : ?>
			<aside id="sidebar-a" class="grid-box"><?php echo $this['modules']->render('sidebar-a', array('layout'=>'stack')); ?><div class="sidebar-bg"></div></aside>
			<?php endif; ?>
			
			</div>
		<?php endif; ?>
		<!-- main end -->

		<?php if ($this['modules']->count('bottom-a')) : ?>
		<section id="bottom-a"><div class="grid-block"><?php echo $this['modules']->render('bottom-a', array('layout'=>$this['config']->get('bottom-a'))); ?></div></section>
		<?php endif; ?>
		
		<?php if ($this['modules']->count('bottom-b')) : ?>
		<section id="bottom-b"><div class="grid-block"><?php echo $this['modules']->render('bottom-b', array('layout'=>$this['config']->get('bottom-b'))); ?></div></section>
		<?php endif; ?>		

		<?php  // XB
		// footer 
		?>		
		
		<div class="clearfooter"></div>
		
	</div>
	<!--[if IE]>
	</center>
	<![endif]-->
	
	
	<!-- XB -->
	<div id="footer">
	<?php if ($this['modules']->count('footer + debug') || $this['config']->get('warp_branding')) : ?>
		<footer id="footer" class="grid-block">

			<?php if ($this['config']->get('totop_scroller')) : ?>
			<a id="totop-scroller" href="#page"></a>
			<?php endif; ?>
			
			<?php
				echo $this['modules']->render('footer');
				$this->output('warp_branding');
				echo $this['modules']->render('debug');
			?>

		</footer>
	<?php endif; ?>
	<!-- XB -->
	</div>
	
	<?php echo $this->render('footer'); ?>
	

<!-- templates/my420life/styles/header/layouts/template.php -->
</body>
</html>
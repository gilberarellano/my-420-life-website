<?php

/* These are functions specific to these options settings and this theme */

/*-----------------------------------------------------------------------------------*/
/* Filter Footer Text
/*-----------------------------------------------------------------------------------*/

function childtheme_footer($thm_footertext) {
	$shortname =  get_option('of_shortname');
	if ($footertext = get_option($shortname . '_footer_text'))
    	return $footertext;
}

add_filter('thematic_footertext', 'childtheme_footer');

/*-----------------------------------------------------------------------------------*/
/* Show analytics code in footer */
/*-----------------------------------------------------------------------------------*/

?>

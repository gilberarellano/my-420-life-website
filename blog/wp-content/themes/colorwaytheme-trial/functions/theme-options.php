<?php

add_action('init', 'of_options');

if (!function_exists('of_options')) {

    function of_options() {
        // VARIABLES
        $themename = get_theme_data(STYLESHEETPATH . '/style.css');
        $themename = $themename['Name'];
        $shortname = "of";

        // Populate OptionsFramework option in array for use in theme
        global $of_options;
        $of_options = get_option('of_options');

        //Stylesheet Reader
        $alt_stylesheets = array("black" =>
            "black", "brown" => "brown", "blue" => "blue", "green" => "green", "pink" => "pink", "purple" => "purple", "red" => "red", "yellow" => "yellow");

        // Test data
        $test_array = array("one" => "One", "two" => "Two", "three" => "Three", "four" => "Four", "five" => "Five");

        // Multicheck Array
        $multicheck_array = array("one" => "French Toast", "two" => "Pancake", "three" => "Omelette", "four" => "Crepe", "five" => "Waffle");

        // Multicheck Defaults
        $multicheck_defaults = array("one" => "1", "five" => "1");

        // Background Defaults

        $background_defaults = array('color' => '', 'image' => '', 'repeat' => 'repeat', 'position' => 'top center', 'attachment' => 'scroll');


        // Pull all the categories into an array
        $options_categories = array();
        $options_categories_obj = get_categories();
        foreach ($options_categories_obj as $category) {
            $options_categories[$category->cat_ID] = $category->cat_name;
        }

        // Pull all the pages into an array
        $options_pages = array();
        $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
        $options_pages[''] = 'Select a page:';
        foreach ($options_pages_obj as $page) {
            $options_pages[$page->ID] = $page->post_title;
        }

        // If using image radio buttons, define a directory path
        $imagepath = get_bloginfo('stylesheet_directory') . '/images/';

        $options = array();

        $options[] = array("name" => "General Settings",
            "type" => "heading");

        $options[] = array("name" => "Custom Logo",
            "desc" => "Choose your own logo. Optimal Size: 215px Wide by 55px Height",
            "id" => "colorway_logo",
            "type" => "upload");

        $options[] = array("name" => "Custom Favicon",
            "desc" => "Specify a 16px x 16px image that will represent your website's favicon.",
            "id" => "colorway_favicon",
            "type" => "upload");

        $options[] = array("name" => "Tracking Code",
            "desc" => "Paste your Google Analytics (or other) tracking code here.",
            "id" => "colorway_analytics",
            "std" => "",
            "type" => "textarea");
        $options[] = array("name" => "Body Background Image",
            "desc" => "Select image to change your website background",
            "id" => "inkthemes_bodybg",
            "std" => "",
            "type" => "upload");
//****=============================================================================****//
//****-----------This code is used for creating slider settings--------------------****//							
//****=============================================================================****//						
        $options[] = array("name" => "Slider Settings",
            "type" => "heading");

        $options[] = array("name" => "Slide1 Image",
            "desc" => "Choose Image for your Slider. Optimal Size: 896px x 350px",
            "id" => "colorway_slideimage1",
            "type" => "upload");
        $options[] = array("name" => "Slide1 Heading",
            "desc" => "Enter the Heading for Slide1",
            "id" => "colorway_slideheading1",
            "std" => "",
            "type" => "text");

        $options[] = array("name" => "Slide1 Heading Link",
            "desc" => "Enter the Link URL in Heading for Slide1",
            "id" => "colorway_slidelink1",
            "std" => "",
            "type" => "text");
        $options[] = array("name" => "Slide1 Description",
            "desc" => "Description for Slide1",
            "id" => "colorway_slidedescription1",
            "std" => "",
            "type" => "textarea");
        $options[] = array("name" => "Slide2 Image (Only Available In Premium Version)",
            "desc" => "Choose Image for your Slider. Optimal Size: 896px x 350px",
            "id" => "colorway_slideimage21",
            "class" => "trialhint",
            "type" => "upload");

        $options[] = array("name" => "Slide2 Heading (Only Available In Premium Version)",
            "desc" => "Enter the Heading for Slide2",
            "id" => "colorway_slideheading21",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

        $options[] = array("name" => "Slide2 Heading Link (Only Available In Premium Version)",
            "desc" => "Enter the Link URL in Heading for Slide2",
            "id" => "colorway_slidelink21",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");
        $options[] = array("name" => "Slide2 Description (Only Available In Premium Version)",
            "desc" => "Description for Slide2",
            "id" => "colorway_slidedescription21",
            "std" => "",
            "class" => "trialhint",
            "type" => "textarea");

        $options[] = array("name" => "Slide3 Image (Only Available In Premium Version)",
            "desc" => "Choose Image for your Slider. Optimal Size: 896px x 350px",
            "id" => "colorway_slideimage31",
            "class" => "trialhint",
            "type" => "upload");

        $options[] = array("name" => "Slide3 Heading (Only Available In Premium Version)",
            "desc" => "Enter the Heading for Slide3",
            "id" => "colorway_slideheading31",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

        $options[] = array("name" => "Slide3 Heading Link (Only Available In Premium Version)",
            "desc" => "Enter the Link URL in Heading for Slide3",
            "id" => "colorway_slidelink32",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");
        $options[] = array("name" => "Slide3 Description (Only Available In Premium Version)",
            "desc" => "Description for Slide3",
            "id" => "colorway_slidedescription333",
            "std" => "",
            "class" => "trialhint",
            "type" => "textarea");
        $options[] = array("name" => "Slide4 Image (Only Available In Premium Version)",
            "desc" => "Choose Image for your Slider. Optimal Size: 896px x 350px",
            "id" => "colorway_slideimage465",
            "class" => "trialhint",
            "type" => "upload");

        $options[] = array("name" => "Slide4 Heading (Only Available In Premium Version)",
            "desc" => "Enter the Heading for Slide4",
            "id" => "colorway_slideheading454",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

        $options[] = array("name" => "Slide4 Heading Link (Only Available In Premium Version)",
            "desc" => "Enter the Link URL in Heading for Slide4",
            "id" => "colorway_slidelink465",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");
        $options[] = array("name" => "Slide4 Description (Only Available In Premium Version)",
            "desc" => "Description for Slide4",
            "id" => "colorway_slidedescription4554",
            "std" => "",
            "class" => "trialhint",
            "type" => "textarea");
//****=============================================================================****//
//****-----------This code is used for creating home page feature content----------****//							
//****=============================================================================****//	
        $options[] = array("name" => "Home Page Settings",
            "type" => "heading");

        $options[] = array("name" => "Home Page Intro (Only Available In Premium Version)",
            "desc" => "Enter your heading text for home page",
            "id" => "inkthemes_mainheading65",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");
        //***Code for first column***//
        $options[] = array("name" => "First Feature Image (Only Available In Premium Version)",
            "desc" => "Choose image for your feature column first. Optimal size 198px x 115px",
            "id" => "inkthemes_fimg1522",
            "std" => "",
            "class" => "trialhint",
            "type" => "upload");

        $options[] = array("name" => "First Feature Heading (Only Available In Premium Version)",
            "desc" => "Enter your heading line for first column",
            "id" => "inkthemes_headline1332",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

        $options[] = array("name" => "First Feature Link (Only Available In Premium Version)",
            "desc" => "Enter your link for feature column first",
            "id" => "inkthemes_link12255",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

        $options[] = array("name" => "First Feature Content (Only Available In Premium Version)",
            "desc" => "Enter your feature content for column first",
            "id" => "inkthemes_feature1655",
            "std" => "",
            "class" => "trialhint",
            "type" => "textarea");

        //***Code for second column***//	
        $options[] = array("name" => "Second Feature Image (Only Available In Premium Version)",
            "desc" => "Choose image for your feature column second. Optimal size 198px x 115px",
            "id" => "inkthemes_fimg2554",
            "std" => "",
            "class" => "trialhint",
            "type" => "upload");

        $options[] = array("name" => "Second Feature Heading (Only Available In Premium Version)",
            "desc" => "Enter your heading line for second column",
            "id" => "inkthemes_headline25544",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

        $options[] = array("name" => "Second Feature Link (Only Available In Premium Version)",
            "desc" => "Enter your link for feature column second",
            "id" => "inkthemes_link2665",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");


        $options[] = array("name" => "Second Feature Content (Only Available In Premium Version)",
            "desc" => "Enter your feature content for column second",
            "id" => "inkthemes_feature25544",
            "std" => "",
            "class" => "trialhint",
            "type" => "textarea");


        //***Code for third column***//	
        $options[] = array("name" => "Third Feature Image (Only Available In Premium Version)",
            "desc" => "Choose image for your feature column thrid. Optimal size 198px x 115px",
            "id" => "inkthemes_fimg3554",
            "std" => "",
            "class" => "trialhint",
            "type" => "upload");

        $options[] = array("name" => "Third Feature Heading (Only Available In Premium Version)",
            "desc" => "Enter your heading line for third column",
            "id" => "inkthemes_headline35544",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

        $options[] = array("name" => "Third Feature Link (Only Available In Premium Version)",
            "desc" => "Enter your link for feature column third",
            "id" => "inkthemes_link3665",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

        $options[] = array("name" => "Third Feature Content (Only Available In Premium Version)",
            "desc" => "Enter your feature content for third column",
            "id" => "inkthemes_feature3665",
            "std" => "",
            "class" => "trialhint",
            "type" => "textarea");

        //***Code for fourth column***//	
        $options[] = array("name" => "Fourth Feature Image (Only Available In Premium Version)",
            "desc" => "Choose image for your feature column fourth. Optimal size 198px x 115px",
            "id" => "inkthemes_fimg4554",
            "std" => "",
            "class" => "trialhint",
            "type" => "upload");

        $options[] = array("name" => "Fourth Feature Heading (Only Available In Premium Version)",
            "desc" => "Enter your heading line for fourth column",
            "id" => "inkthemes_headline4554",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

        $options[] = array("name" => "Fourth Feature link (Only Available In Premium Version)",
            "desc" => "Enter your link for feature column fourth",
            "id" => "inkthemes_link4554",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

        $options[] = array("name" => "Fourth Feature Content (Only Available In Premium Version)",
            "desc" => "Enter your feature content for fourth column",
            "id" => "inkthemes_feature4554",
            "std" => "",
            "class" => "trialhint",
            "type" => "textarea");

        $options[] = array("name" => "Home Page Testimonial (Only Available In Premium Version)",
            "desc" => "Enter your text for homepage testimonial in short paragraph.",
            "id" => "inkthemes_testimonial4455",
            "std" => "",
            "class" => "trialhint",
            "type" => "textarea");

//****=============================================================================****//
//****-----------This code is used for creating color styleshteet options----------****//							
//****=============================================================================****//				
        $options[] = array("name" => "Styling Options",
            "type" => "heading");
        $options[] = array("name" => "Theme Stylesheet (Only Available In Premium Version)",
            "desc" => "Select your themes alternative color scheme.",
            "id" => "colorway_altstylesheet5544",
            "std" => "green",
            "type" => "select",
            "class" => "trialhint",
            "options" => $alt_stylesheets);

        $options[] = array("name" => "Custom CSS (Only Available In Premium Version)",
            "desc" => "Quickly add some CSS to your theme by adding it to this block.",
            "id" => "colorway_customcss8844",
            "std" => "",
            "class" => "trialhint",
            "type" => "textarea");

        $options[] = array("name" => "Footer Settings",
            "type" => "heading");
        $options[] = array("name" => "Facebook URL (Only Available In Premium Version)",
            "desc" => "Enter your Facebook URL if you have one",
            "id" => "colorway_facebook55444",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");
        $options[] = array("name" => "Twitter URL (Only Available In Premium Version)",
            "desc" => "Enter your Twitter URL if you have one",
            "id" => "colorway_twitter5554",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

        $options[] = array("name" => "RSS Feed URL (Only Available In Premium Version)",
            "desc" => "Enter your RSS Feed URL if you have one",
            "id" => "colorway_rss6655",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");

//****=============================================================================****//
//****-----------This code is used for creating color SEO description--------------****//							
//****=============================================================================****//						
        $options[] = array("name" => "SEO Options",
            "type" => "heading");
        $options[] = array("name" => "Meta Keywords (comma separated) (Only Available In Premium Version)",
            "desc" => "Meta keywords provide search engines with additional information about topics that appear on your site. This only applies to your home page. Keyword Limit Maximum 8",
            "id" => "colorway_keyword55444",
            "std" => "",
            "class" => "trialhint",
            "type" => "textarea");
        $options[] = array("name" => "Meta Description (Only Available In Premium Version)",
            "desc" => "You should use meta descriptions to provide search engines with additional information about topics that appear on your site. This only applies to your home page.Optimal Length for Search Engines, Roughly 155 Characters",
            "id" => "colorway_description5544",
            "std" => "",
            "class" => "trialhint",
            "type" => "textarea");
        $options[] = array("name" => "Meta Author Name (Only Available In Premium Version)",
            "desc" => "You should write the full name of the author here. This only applies to your home page.",
            "id" => "colorway_author5544",
            "std" => "",
            "class" => "trialhint",
            "type" => "text");
        update_option('of_template', $options);
        update_option('of_themename', $themename);
        update_option('of_shortname', $shortname);
    }

}
?>

<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('FS_METHOD', 'direct');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '72a5dc3d145c6f6414ad466c43659040');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lOf,eCnM40c0G<nsxIiNisgD|yJ|l)V%ajxsx=5vruFbSI/u76x7N?~Xn{nI%=o[');
define('SECURE_AUTH_KEY',  '/[&hG9x&F*#@ZH,G++oF|-yuIHr3ICAlY;Af<{tW^+ij> xB@>f{^E|i2Kk9 DOS');
define('LOGGED_IN_KEY',    '*!&hT/3?Cvu,Ugdi|ZKm|P_]P[}+$hT5,!` B3{ S+@-+n;GK<>,ML|/iF_EH3^4');
define('NONCE_KEY',        '``yf^mRev^m?pJ<q+`>3}+x`X;FPII@I#{soE.[aOO(j+4-W^[n+oz>5T,t#Eh@@');
define('AUTH_SALT',        'F)vZas!>-7_p42_Nq:NRTW OpGZD1FEL-b#<OS.X WL<d |g!>>|oYxJ=JJ0M$pj');
define('SECURE_AUTH_SALT', 'B+L?rlviB]>MJzo=Vk{{Ic+xqgS]{+^!SM+MT*~qEQnQd#{].Y5_2lm,$qYi_E+]');
define('LOGGED_IN_SALT',   ';y;-!D3L^U-?|eg.Pc-SoAkglZ$kt^$1-Y#};1G|v-)1-2qylX#@DZ1ch:gHV#|b');
define('NONCE_SALT',       '0%qH8B#^o=Y}Aka}5rB+$A<PT08Yh+NS<^Y7ML49H_kcX-9zY1^ZwQ2-}7`)TRXF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
